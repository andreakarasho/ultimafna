/***************************************************************************
 *   DebugSettings.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using UltimaXNA.Core.Configuration;
#endregion

namespace UltimaXNA.Configuration
{
    public sealed class DebugSettings : ASettingsSection
    {
        bool _IsConsoleEnabled;
        bool _ShowFps;
        bool _LogPackets;

        public DebugSettings()
        {
            LogPackets = false;
            IsConsoleEnabled = true;
            ShowFps = true;
        }

        /// <summary>
        /// If true, all received packets will be logged to Tracer.Debug, and any active Tracer listeners (console, debug.txt file logger)
        /// </summary>
        public bool LogPackets
        {
            get { return _LogPackets; }
            set { SetProperty(ref _LogPackets, value); }
        }

        /// <summary>
        /// If true, FPS should display either in the window caption or in the game window. (not currently enabled).
        /// </summary>
        public bool ShowFps
        {
            get { return _ShowFps; }
            set { SetProperty(ref _ShowFps, value); }
        }

        /// <summary>
        /// If true, a console window which will display debug and error messages should appear at runtime. This may not work in Release configurations.
        /// </summary>
        public bool IsConsoleEnabled
        {
            get { return _IsConsoleEnabled; }
            set { SetProperty(ref _IsConsoleEnabled, value); }
        }
    }
}