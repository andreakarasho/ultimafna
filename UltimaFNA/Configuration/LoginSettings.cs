/***************************************************************************
 *   ServerSettings.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using UltimaXNA.Core.Configuration;
#endregion

namespace UltimaXNA.Configuration
{
    public sealed class LoginSettings : ASettingsSection
    {
        string _ServerAddress;
        int _ServerPort;
        string _UserName;
        bool _AutoSelectLastCharacter;
        string _LastCharacterName;

        public LoginSettings()
        {
            ServerAddress = "127.0.0.1";
            ServerPort = 2593;
            LastCharacterName = string.Empty;
            AutoSelectLastCharacter = false;
        }

        public string UserName
        {
            get { return _UserName; }
            set { SetProperty(ref _UserName, value); }
        }

        public int ServerPort
        {
            get { return _ServerPort; }
            set { SetProperty(ref _ServerPort, value); }
        }

        public string ServerAddress
        {
            get { return _ServerAddress; }
            set { SetProperty(ref _ServerAddress, value); }
        }

        public string LastCharacterName
        {
            get { return _LastCharacterName; }
            set { SetProperty(ref _LastCharacterName, value); }
        }

        public bool AutoSelectLastCharacter
        {
            get { return _AutoSelectLastCharacter; }
            set { SetProperty(ref _AutoSelectLastCharacter, value); }
        }
    }
}