/***************************************************************************
 *   GameSettings.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using UltimaXNA.Core.Configuration;
#endregion

namespace UltimaXNA.Configuration
{
    public sealed class EngineSettings : ASettingsSection
    {
        bool _IsVSyncEnabled;
        bool _IsFixedTimeStep;

        public EngineSettings()
        {
            IsFixedTimeStep = true;
            IsVSyncEnabled = false;
        }

        public bool IsFixedTimeStep
        {
            get { return _IsFixedTimeStep; }
            set { SetProperty(ref _IsFixedTimeStep, value); }
        }

        public bool IsVSyncEnabled
        {
            get { return _IsVSyncEnabled; }
            set { SetProperty(ref _IsVSyncEnabled, value); }
        }
    }
}