﻿/***************************************************************************
 *   WorldSettings.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using UltimaXNA.Core.Configuration;
using UltimaXNA.Core;
using UltimaXNA.Configuration.Properties;
using UltimaXNA.Ultima.Resources;
#endregion

namespace UltimaXNA.Configuration
{
    public class UserInterfaceSettings : ASettingsSection
    {
        ResolutionProperty _FullScreenResolution;
        ResolutionProperty _WindowResolution;
        ResolutionProperty _WorldGumpResolution;
        bool _PlayWindowPixelDoubling;
        bool _IsFullScreen;
        MouseProperty _Mouse;
        bool _AlwaysRun;
        bool _MenuBarDisabled;

        int _SpeechColor = 4 + Utility.RandomValue(0, 99) * 5;
        int _EmoteColor = 646;
        int _PartyMsgPrivateColor = 58;
        int _PartyMsgColor = 68;
        int _GuildMsgColor = 70;
        bool _IgnoreGuildMsg;
        int _AllianceMsgColor = 487;
        bool _IgnoreAllianceMsg;
        bool _CrimeQuery;

        public UserInterfaceSettings()
        {
            FullScreenResolution = new ResolutionProperty();
            WindowResolution = new ResolutionProperty();
            PlayWindowGumpResolution = new ResolutionProperty();
            _PlayWindowPixelDoubling = false;
            IsMaximized = false;
            Mouse = new MouseProperty();
            AlwaysRun = false;
            MenuBarDisabled = false;
            CrimeQuery = true;
        }

        public bool IsMaximized
        {
            get { return _IsFullScreen; }
            set { SetProperty(ref _IsFullScreen, value); }
        }

        public MouseProperty Mouse
        {
            get { return _Mouse; }
            set { SetProperty(ref _Mouse, value); }
        }

        public ResolutionProperty FullScreenResolution
        {
            get { return _FullScreenResolution; }
            set
            {
                if (!Resolutions.IsValidFullScreenResolution(value))
                    return;
                SetProperty(ref _FullScreenResolution, value);
            }
        }

        public ResolutionProperty WindowResolution
        {
            get { return _WindowResolution; }
            set { SetProperty(ref _WindowResolution, value); }
        }

        public ResolutionProperty PlayWindowGumpResolution
        {
            get { return _WorldGumpResolution; }
            set
            {
                if (!Resolutions.IsValidPlayWindowResolution(value))
                    SetProperty(ref _WorldGumpResolution, new ResolutionProperty());
                SetProperty(ref _WorldGumpResolution, value);
            }
        }

        public bool PlayWindowPixelDoubling
        {
            get { return _PlayWindowPixelDoubling; }
            set { SetProperty(ref _PlayWindowPixelDoubling, value); }
        }

        public bool AlwaysRun
        {
            get { return _AlwaysRun; }
            set { SetProperty(ref _AlwaysRun, value); }
        }

        public bool MenuBarDisabled
        {
            get { return _MenuBarDisabled; }
            set { SetProperty(ref _MenuBarDisabled, value); }
        }

        public int SpeechColor
        {
            get { return _SpeechColor; }
            set { SetProperty(ref _SpeechColor, Clamp(value, 0, HueData.HueCount - 1)); }
        }

        public int EmoteColor
        {
            get { return _EmoteColor; }
            set { SetProperty(ref _EmoteColor, Clamp(value, 0, HueData.HueCount - 1)); }
        }

        public int PartyPrivateMsgColor
        {
            get { return _PartyMsgPrivateColor; }
            set { SetProperty(ref _PartyMsgPrivateColor, Clamp(value, 0, HueData.HueCount - 1)); }
        }

        public int PartyMsgColor
        {
            get { return _PartyMsgColor; }
            set { SetProperty(ref _PartyMsgColor, Clamp(value, 0, HueData.HueCount - 1)); }
        }

        public int GuildMsgColor
        {
            get { return _GuildMsgColor; }
            set { SetProperty(ref _GuildMsgColor, Clamp(value, 0, HueData.HueCount - 1)); }
        }

        public bool IgnoreGuildMsg
        {
            get { return _IgnoreGuildMsg; }
            set { SetProperty(ref _IgnoreGuildMsg, value); }
        }

        public int AllianceMsgColor
        {
            get { return _AllianceMsgColor; }
            set { SetProperty(ref _AllianceMsgColor, Clamp(value, 0, HueData.HueCount - 1)); }
        }

        public bool IgnoreAllianceMsg
        {
            get { return _IgnoreAllianceMsg; }
            set { SetProperty(ref _IgnoreAllianceMsg, value); }
        }

        public bool CrimeQuery
        {
            get { return _CrimeQuery; }
            set { SetProperty(ref _CrimeQuery, value); }
        }
    }
}
