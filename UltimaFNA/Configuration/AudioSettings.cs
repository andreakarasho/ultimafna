﻿/***************************************************************************
 *   AudioSettings.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using UltimaXNA.Core.Configuration;
#endregion

namespace UltimaXNA.Configuration
{
    public class AudioSettings : ASettingsSection
    {
        int _MusicVolume;
        int _SoundVolume;
        bool _MusicOn;
        bool _SoundOn;
        bool _FootStepSoundOn;

        public AudioSettings()
        {
            MusicVolume = 100;
            SoundVolume = 100;

            MusicOn = true;
            SoundOn = true;
            FootStepSoundOn = true;
        }

        /// <summary>
        /// Volume of music. Value is percent of max volume, clamped to 0 - 100.
        /// </summary>
        public int MusicVolume
        {
            get { return _MusicVolume; }
            set{ SetProperty(ref _MusicVolume, Clamp(value, 0, 100)); }
        }

        /// <summary>
        /// Volume of sound effects. Value is percent of max volume, clamped to 0 - 100.
        /// </summary>
        public int SoundVolume
        {
            get { return _SoundVolume; }
            set { SetProperty(ref _SoundVolume, Clamp(value, 0, 100)); }
        }

        /// <summary>
        /// False = requests to play music are ignored.
        /// </summary>
        public bool MusicOn
        {
            get { return _MusicOn; }
            set { SetProperty(ref _MusicOn, value); }
        }

        /// <summary>
        /// False = requests to play sound effects are ignored.
        /// </summary>
        public bool SoundOn
        {
            get { return _SoundOn; }
            set { SetProperty(ref _SoundOn, value); }
        }

        /// <summary>
        /// False = no foot step sound effects ever play.
        /// </summary>
        public bool FootStepSoundOn
        {
            get { return _FootStepSoundOn; }
            set { SetProperty(ref _FootStepSoundOn, value); }
        }
    }
}
