/***************************************************************************
 *   UltimaOnlineSettings.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using UltimaXNA.Core.Configuration;
using UltimaXNA.Ultima.Data;
#endregion

namespace UltimaXNA.Configuration
{
    public sealed class UltimaOnlineSettings : ASettingsSection
    {
        bool _AllowCornerMovement;
        string _DataDirectory;
        byte[] _ClientVersion;

        public UltimaOnlineSettings()
        {
            PatchVersion = ClientVersion.DefaultVersion;
        }

        /// <summary>
        /// The patch version which is sent to the server. RunUO (and possibly other server software) rely on the
        /// client's reported patch version to enable/disable certain packets and features.
        /// </summary>
        public byte[] PatchVersion
        {
            get {
                if (_ClientVersion == null || _ClientVersion.Length != 4)
                    return ClientVersion.DefaultVersion;
                return _ClientVersion;
            }
            set
            {
                if (value == null || value.Length != 4)
                    return;
                // Note from ZaneDubya: I will not support your client if you change or remove this line:
               // if (!ClientVersion.EqualTo(value, ClientVersion.DefaultVersion)) return;
                SetProperty(ref _ClientVersion, value);
            }
        }
        
        /// <summary>
        /// The directory where the Ultima Online resource files and executable are located.
        /// </summary>
        public string DataDirectory
        {
            get { return _DataDirectory; }
            set { SetProperty(ref _DataDirectory, value); }
        }

        /// <summary>
        /// When true, allows corner-cutting movement (ala the God client and RunUO administrator-mode movement).
        /// </summary>
        public bool AllowCornerMovement
        {
            get { return _AllowCornerMovement; }
            set { SetProperty(ref _AllowCornerMovement, value); }
        }
    }
}