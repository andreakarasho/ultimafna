/***************************************************************************
 *   MouseSettings.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using UltimaXNA.Core.ComponentModel;
using UltimaXNA.Core.Input;
#endregion

namespace UltimaXNA.Configuration.Properties
{
    public class MouseProperty : NotifyPropertyChangedBase
    {
        MouseButton _InteractionButton = MouseButton.Left;
        MouseButton _MovementButton = MouseButton.Right;
        bool _IsEnabled = true;
        float _ClickAndPickUpMS = 800f; // this is close to what the legacy client uses.
        float _DoubleClickMS = 400f;

        public MouseProperty()
        {

        }

        public bool IsEnabled
        {
            get { return _IsEnabled; }
            set { SetProperty(ref _IsEnabled, value); }
        }

        public MouseButton MovementButton
        {
            get { return _MovementButton; }
            set { SetProperty(ref _MovementButton, value); }
        }

        public MouseButton InteractionButton
        {
            get { return _InteractionButton; }
            set { SetProperty(ref _InteractionButton, value); }
        }

        public float ClickAndPickupMS
        {
            get { return _ClickAndPickUpMS; }
            set { SetProperty(ref _ClickAndPickUpMS, Clamp(value, 0, 2000)); }
        }

        public float DoubleClickMS
        {
            get { return _DoubleClickMS; }
            set { SetProperty(ref _DoubleClickMS, Clamp(value, 0, 2000)); }
        }

        
    }
}