/***************************************************************************
 *   Settings.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

#region usings
using System;
using UltimaXNA.Configuration;
using UltimaXNA.Core.Configuration;
#endregion

namespace UltimaXNA
{
    public class Settings
    {
        // === Instance ===============================================================================================
        readonly DebugSettings _Debug;
        readonly EngineSettings _Engine;
        readonly GumpSettings _Gumps;
        readonly UserInterfaceSettings _UI;
        readonly LoginSettings _Login;
        readonly UltimaOnlineSettings _UltimaOnline;
        readonly AudioSettings _Audio;

        Settings()
        {
            _Debug = CreateOrOpenSection<DebugSettings>();
            _Login = CreateOrOpenSection<LoginSettings>();
            _UltimaOnline = CreateOrOpenSection<UltimaOnlineSettings>();
            _Engine = CreateOrOpenSection<EngineSettings>();
            _UI = CreateOrOpenSection<UserInterfaceSettings>();
            _Gumps = CreateOrOpenSection<GumpSettings>();
            _Audio = CreateOrOpenSection<AudioSettings>();
        }

        // === Static Settings properties =============================================================================
        public static DebugSettings Debug => s_Instance._Debug;
        public static LoginSettings Login => s_Instance._Login;
        public static UltimaOnlineSettings UltimaOnline => s_Instance._UltimaOnline;
        public static EngineSettings Engine => s_Instance._Engine;
        public static GumpSettings Gumps => s_Instance._Gumps;
        public static UserInterfaceSettings UserInterface => s_Instance._UI;
        public static AudioSettings Audio => s_Instance._Audio;

        static readonly Settings s_Instance;
        static readonly SettingsFile s_File;

        static Settings()
        {
            s_File = new SettingsFile("settings.cfg");
            s_Instance = new Settings();
            s_File.Load();
        }

        public static void Save()
        {
            s_File.Save();
        }

        public static T CreateOrOpenSection<T>()
            where T : ASettingsSection, new()
        {
            string sectionName = typeof(T).Name;
            T section = s_File.CreateOrOpenSection<T>(sectionName);
            // Resubscribe incase this is called for a section 2 times.
            section.Invalidated -= OnSectionInvalidated;
            section.Invalidated += OnSectionInvalidated;
            section.PropertyChanged -= OnSectionPropertyChanged;
            section.PropertyChanged += OnSectionPropertyChanged;
            return section;
        }

        static void OnSectionPropertyChanged(object sender, EventArgs e)
        {
            s_File.InvalidateDirty();
        }

        static void OnSectionInvalidated(object sender, EventArgs e)
        {
            s_File.InvalidateDirty();
        }
    }
}