﻿/***************************************************************************
 *   StreamOutputEventListener.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System;
using System.IO;
using System.Text;
#endregion

namespace UltimaXNA.Core.Diagnostics.Listeners
{
    public class StreamOuputEventListener : AEventListener
    {
        private const string Format = "{0} {1:yyyy-MM-dd HH\\:mm\\:ss\\:ffff} {2}";

        private readonly object _syncRoot = new object();
        private readonly bool _closeStreamOnDispose;

        private Stream _stream;
        private readonly StreamWriter _writer;

        public StreamOuputEventListener(Stream stream, bool closeStreamOnDispose)
        {
            _stream = stream;
            _writer = new StreamWriter(_stream, Encoding.Unicode);
            _closeStreamOnDispose = closeStreamOnDispose;
        }

        public void Dispose()
        {
            if (!_closeStreamOnDispose || _stream == null)
            {
                return;
            }

            _stream.Dispose();
            _stream = null;
        }

        public override void OnEventWritten(EventLevels level, string message)
        {
            string output = string.Format(Format, level, DateTime.Now, message);

            lock (_syncRoot)
            {
                _writer.WriteLine(output);
                _writer.Flush();
            }
        }
    }
}