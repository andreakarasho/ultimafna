﻿/***************************************************************************
 *   Profiler.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using System;
using System.Collections.Generic;
using UltimaXNA.Core.Diagnostics.Tracing;

namespace UltimaXNA.Core.Diagnostics
{
    public static class Profiler
    {
        public const int ProfileTimeCount = 60;

        static List<ContextAndTick> _Context;
        static HighPerformanceTimer _Timer;
        static List<Tuple<string[], double>> _ThisFrameData;
        static List<ProfileData> _AllFrameData;
        static ProfileData _TotalTimeData;
        static long _BeginFrameTicks;
        public static double LastFrameTimeMS { get; private set; }
        public static double TrackedTime => _TotalTimeData.TimeInContext;

        static Profiler()
        {
            _Context = new List<ContextAndTick>();
            _ThisFrameData = new List<Tuple<string[], double>>();
            _AllFrameData = new List<ProfileData>();
            _TotalTimeData = new ProfileData(null, 0d);
            _Timer = new HighPerformanceTimer();
            _Timer.Start();
        }

        public static void BeginFrame()
        {
            if (_ThisFrameData.Count > 0)
            {
                for (int i = 0; i < _ThisFrameData.Count; i++)
                {
                    bool added = false;
                    for (int j = 0; j < _AllFrameData.Count; j++)
                    {
                        if (_AllFrameData[j].MatchesContext(_ThisFrameData[i].Item1))
                        {
                            _AllFrameData[j].AddNewHitLength(_ThisFrameData[i].Item2);
                            added = true;
                            break;
                        }
                    }
                    if (!added)
                    {
                        _AllFrameData.Add(new ProfileData(_ThisFrameData[i].Item1, _ThisFrameData[i].Item2));
                    }
                }
                _ThisFrameData.Clear();
            }

            _BeginFrameTicks = _Timer.ElapsedTicks;
        }

        public static void EndFrame()
        {
            LastFrameTimeMS = HighPerformanceTimer.SecondsFromTicks(_Timer.ElapsedTicks - _BeginFrameTicks) * 1000d;
            _TotalTimeData.AddNewHitLength(LastFrameTimeMS);
        }

        public static void EnterContext(string context_name)
        {
            _Context.Add(new ContextAndTick(context_name, _Timer.ElapsedTicks));
        }

        public static void ExitContext(string context_name)
        {
            if (_Context[_Context.Count - 1].Name != context_name)
                Tracer.Error("Profiler.ExitProfiledContext: context_name does not match current context.");
            string[] context = new string[_Context.Count];
            for (int i = 0; i < _Context.Count; i++)
                context[i] = _Context[i].Name;

            double ms = HighPerformanceTimer.SecondsFromTicks(_Timer.ElapsedTicks - _Context[_Context.Count - 1].Tick) * 1000d;
            _ThisFrameData.Add(new Tuple<string[], double>(context, ms));
            _Context.RemoveAt(_Context.Count - 1);
        }

        public static bool InContext(string context_name)
        {
            if (_Context.Count == 0)
                return false;
            return (_Context[_Context.Count - 1].Name == context_name);
        }

        public static ProfileData GetContext(string context_name)
        {
            for (int i = 0; i < _AllFrameData.Count; i++)
                if (_AllFrameData[i].Context[_AllFrameData[i].Context.Length - 1] == context_name)
                    return _AllFrameData[i];
            return ProfileData.Empty;
        }

        public class ProfileData
        {
            public string[] Context;
            double[] _LastTimes = new double[ProfileTimeCount];
            uint _LastIndex;

            public double LastTime => _LastTimes[_LastIndex % ProfileTimeCount];

            public double TimeInContext
            {
                get
                {
                    double time = 0;
                    for (int i = 0; i < ProfileTimeCount; i++)
                    {
                        time += _LastTimes[i];
                    }
                    return time;
                }
            }

            public double AverageTime => TimeInContext / ProfileTimeCount;

            public ProfileData(string[] context, double time)
            {
                Context = context;
                _LastIndex = 0;
                AddNewHitLength(time);
            }

            public bool MatchesContext(string[] context)
            {
                if (Context.Length != context.Length)
                    return false;
                for (int i = 0; i < Context.Length; i++)
                    if (Context[i] != context[i])
                        return false;
                return true;
            }

            public void AddNewHitLength(double time)
            {
                _LastTimes[_LastIndex % ProfileTimeCount] = time;
                _LastIndex++;
            }

            public override string ToString()
            {
                string name = string.Empty;
                for (int i = 0; i < Context.Length; i++)
                {
                    if (name != string.Empty)
                        name += ":";
                    name += Context[i];
                }
                return $"{name} - {TimeInContext:0.0}ms";
            }

            public static ProfileData Empty = new ProfileData(null, 0d);
        }

        private struct ContextAndTick
        {
            public readonly string Name;
            public readonly long Tick;

            public ContextAndTick(string name, long tick)
            {
                Name = name;
                Tick = tick;
            }

            public override string ToString()
            {
                return string.Format("{0} [{1}]", Name, Tick);
            }
        }
    }
}
