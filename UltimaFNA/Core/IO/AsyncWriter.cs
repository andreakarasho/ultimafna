using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Threading;

namespace UltimaXNA.Core.IO
{
    public sealed class AsyncWriter : GenericWriter
    {
        private static int _ThreadCount;

        private readonly int BufferSize;

        private readonly bool PrefixStrings;

        private readonly FileStream _File;

        private readonly Queue _WriteQueue;
        private BinaryWriter _Bin;
        private bool _Closed;
        private long _CurPos;
        private long _LastPos;
        private MemoryStream _Mem;
        private Thread _WorkerThread;

        public AsyncWriter(string filename, bool prefix)
            : this(filename, 1048576, prefix) //1 mb buffer
        {
        }

        public AsyncWriter(string filename, int buffSize, bool prefix)
        {
            PrefixStrings = prefix;
            _Closed = false;
            _WriteQueue = Queue.Synchronized(new Queue());
            BufferSize = buffSize;

            _File = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None);
            _Mem = new MemoryStream(BufferSize + 1024);
            _Bin = new BinaryWriter(_Mem, Utility.UTF8WithEncoding);
        }

        public static int ThreadCount
        {
            get { return _ThreadCount; }
        }

        public MemoryStream MemStream
        {
            get { return _Mem; }
            set
            {
                if(_Mem.Length > 0)
                {
                    Enqueue(_Mem);
                }

                _Mem = value;
                _Bin = new BinaryWriter(_Mem, Utility.UTF8WithEncoding);
                _LastPos = 0;
                _CurPos = _Mem.Length;
                _Mem.Seek(0, SeekOrigin.End);
            }
        }

        public override long Position
        {
            get { return _CurPos; }
        }

        private void Enqueue(MemoryStream mem)
        {
            _WriteQueue.Enqueue(mem);

            if(_WorkerThread == null || !_WorkerThread.IsAlive)
            {
                _WorkerThread = new Thread(new WorkerThread(this).Worker);
                _WorkerThread.Priority = ThreadPriority.BelowNormal;
                _WorkerThread.Start();
            }
        }

        private void OnWrite()
        {
            long curlen = _Mem.Length;
            _CurPos += curlen - _LastPos;
            _LastPos = curlen;
            if(curlen >= BufferSize)
            {
                Enqueue(_Mem);
                _Mem = new MemoryStream(BufferSize + 1024);
                _Bin = new BinaryWriter(_Mem, Utility.UTF8WithEncoding);
                _LastPos = 0;
            }
        }

        public override void Close()
        {
            Enqueue(_Mem);
            _Closed = true;
        }

        public override void Write(IPAddress value)
        {
            _Bin.Write(Utility.GetLongAddressValue(value));
            OnWrite();
        }

        public override void Write(string value)
        {
            if(PrefixStrings)
            {
                if(value == null)
                {
                    _Bin.Write((byte)0);
                }
                else
                {
                    _Bin.Write((byte)1);
                    _Bin.Write(value);
                }
            }
            else
            {
                _Bin.Write(value);
            }
            OnWrite();
        }

        public override void WriteDeltaTime(DateTime value)
        {
            long ticks = value.Ticks;
            long now = DateTime.Now.Ticks;

            TimeSpan d;

            try
            {
                d = new TimeSpan(ticks - now);
            }
            catch
            {
                if(ticks < now)
                {
                    d = TimeSpan.MaxValue;
                }
                else
                {
                    d = TimeSpan.MaxValue;
                }
            }

            Write(d);
        }

        public override void Write(DateTime value)
        {
            _Bin.Write(value.Ticks);
            OnWrite();
        }

        public override void Write(TimeSpan value)
        {
            _Bin.Write(value.Ticks);
            OnWrite();
        }

        public override void Write(decimal value)
        {
            _Bin.Write(value);
            OnWrite();
        }

        public override void Write(long value)
        {
            _Bin.Write(value);
            OnWrite();
        }

        public override void Write(ulong value)
        {
            _Bin.Write(value);
            OnWrite();
        }

        public override void WriteEncodedInt(int value)
        {
            uint v = (uint)value;

            while(v >= 0x80)
            {
                _Bin.Write((byte)(v | 0x80));
                v >>= 7;
            }

            _Bin.Write((byte)v);
            OnWrite();
        }

        public override void Write(int value)
        {
            _Bin.Write(value);
            OnWrite();
        }

        public override void Write(uint value)
        {
            _Bin.Write(value);
            OnWrite();
        }

        public override void Write(short value)
        {
            _Bin.Write(value);
            OnWrite();
        }

        public override void Write(ushort value)
        {
            _Bin.Write(value);
            OnWrite();
        }

        public override void Write(double value)
        {
            _Bin.Write(value);
            OnWrite();
        }

        public override void Write(float value)
        {
            _Bin.Write(value);
            OnWrite();
        }

        public override void Write(char value)
        {
            _Bin.Write(value);
            OnWrite();
        }

        public override void Write(byte value)
        {
            _Bin.Write(value);
            OnWrite();
        }

        public override void Write(byte[] value)
        {
            for(int i = 0; i < value.Length; i++)
            {
                _Bin.Write(value[i]);
            }
            OnWrite();
        }

        public override void Write(sbyte value)
        {
            _Bin.Write(value);
            OnWrite();
        }

        public override void Write(bool value)
        {
            _Bin.Write(value);
            OnWrite();
        }

        private class WorkerThread
        {
            private readonly AsyncWriter _Parent;

            public WorkerThread(AsyncWriter parent)
            {
                _Parent = parent;
            }

            public void Worker()
            {
                _ThreadCount++;
                while(_Parent._WriteQueue.Count > 0)
                {
                    MemoryStream mem = (MemoryStream)_Parent._WriteQueue.Dequeue();

                    if(mem != null && mem.Length > 0)
                    {
                        mem.WriteTo(_Parent._File);
                    }
                }

                if(_Parent._Closed)
                {
                    _Parent._File.Close();
                }

                _ThreadCount--;

                if(_ThreadCount <= 0)
                {
                    // Program.NotifyDiskWriteComplete();
                }
            }
        }
    }
}