﻿using System.Collections.Generic;

namespace UltimaXNA.Core.IO
{
    public class BufferPool
    {
        private static List<BufferPool> _Pools = new List<BufferPool>();

        public static List<BufferPool> Pools { get { return _Pools; } set { _Pools = value; } }

        private string _Name;

        private int _InitialCapacity;
        private int _BufferSize;

        private int _Misses;
        private Queue<byte[]> _FreeBuffers;

        public void GetInfo(out string name, out int freeCount, out int initialCapacity, out int currentCapacity, out int bufferSize, out int misses)
        {
            lock (this)
            {
                name = _Name;
                freeCount = _FreeBuffers.Count;
                initialCapacity = _InitialCapacity;
                currentCapacity = _InitialCapacity * (1 + _Misses);
                bufferSize = _BufferSize;
                misses = _Misses;
            }
        }

        public BufferPool(string name, int initialCapacity, int bufferSize)
        {
            _Name = name;

            _InitialCapacity = initialCapacity;
            _BufferSize = bufferSize;

            _FreeBuffers = new Queue<byte[]>(initialCapacity);

            for (int i = 0; i < initialCapacity; ++i)
                _FreeBuffers.Enqueue(new byte[bufferSize]);

            lock (_Pools)
                _Pools.Add(this);
        }
        
        public byte[] AcquireBuffer()
        {
            lock (this)
            {
                if (_FreeBuffers.Count > 0)
                    return _FreeBuffers.Dequeue();

                ++_Misses;

                for (int i = 0; i < _InitialCapacity; ++i)
                    _FreeBuffers.Enqueue(new byte[_BufferSize]);

                return _FreeBuffers.Dequeue();
            }
        }

        public void ReleaseBuffer(byte[] buffer)
        {
            if (buffer == null)
                return;

            lock (this)
                _FreeBuffers.Enqueue(buffer);
        }

        public void Free()
        {
            lock (_Pools)
                _Pools.Remove(this);
        }
    }
}
