using System;
using System.IO;
using System.Net;
using System.Text;

namespace UltimaXNA.Core.IO
{
    public class BinaryFileWriter : GenericWriter
    {
        const int BufferSize = 64 * 1024;
        const int LargeByteBufferSize = 256;

        readonly bool _PrefixStrings;
        readonly byte[] _Buffer;
        readonly Encoding _Encoding;
        readonly Stream _File;
        readonly char[] _SingleCharBuffer = new char[1];
        byte[] _CharacterBuffer;
        int _Index;
        int _MaxBufferChars;
        long _Position;

        public BinaryFileWriter(Stream strm, bool prefixStr)
        {
            _PrefixStrings = prefixStr;
            _Encoding = Utility.UTF8;
            _Buffer = new byte[BufferSize];
            _File = strm;
        }

        public BinaryFileWriter(string filename, bool prefixStr)
        {
            _PrefixStrings = prefixStr;
            _Buffer = new byte[BufferSize];
            _File = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None);
            _Encoding = Utility.UTF8WithEncoding;
        }

        public override long Position => _Position + _Index;

        public Stream UnderlyingStream
        {
            get
            {
                if(_Index > 0)
                {
                    Flush();
                }
                return _File;
            }
        }

        public void Flush()
        {
            if(_Index > 0)
            {
                _Position += _Index;

                _File.Write(_Buffer, 0, _Index);
                _Index = 0;
            }
        }

        public override void Close()
        {
            if(_Index > 0)
            {
                Flush();
            }
            _File.Close();
        }

        public override void WriteEncodedInt(int value)
        {
            uint v = (uint)value;
            while(v >= 0x80)
            {
                if((_Index + 1) > _Buffer.Length)
                {
                    Flush();
                }
                _Buffer[_Index++] = (byte)(v | 0x80);
                v >>= 7;
            }
            if((_Index + 1) > _Buffer.Length)
            {
                Flush();
            }
            _Buffer[_Index++] = (byte)v;
        }

        internal void InternalWriteString(string value)
        {
            if(value == null)
            {
                value = string.Empty;
            }
            int length = _Encoding.GetByteCount(value);
            WriteEncodedInt(length);
            if(_CharacterBuffer == null)
            {
                _CharacterBuffer = new byte[LargeByteBufferSize];
                _MaxBufferChars = LargeByteBufferSize / _Encoding.GetMaxByteCount(1);
            }

            if(length > LargeByteBufferSize)
            {
                int current = 0;
                int charsLeft = value.Length;

                while(charsLeft > 0)
                {
                    int charCount = (charsLeft > _MaxBufferChars) ? _MaxBufferChars : charsLeft;
                    int byteLength = _Encoding.GetBytes(value, current, charCount, _CharacterBuffer, 0);
                    if((_Index + byteLength) > _Buffer.Length)
                    {
                        Flush();
                    }
                    Buffer.BlockCopy(_CharacterBuffer, 0, _Buffer, _Index, byteLength);
                    _Index += byteLength;
                    current += charCount;
                    charsLeft -= charCount;
                }
            }
            else
            {
                int byteLength = _Encoding.GetBytes(value, 0, value.Length, _CharacterBuffer, 0);
                if((_Index + byteLength) > _Buffer.Length)
                {
                    Flush();
                }
                Buffer.BlockCopy(_CharacterBuffer, 0, _Buffer, _Index, byteLength);
                _Index += byteLength;
            }
        }

        public override void Write(string value)
        {
            if(_PrefixStrings)
            {
                if(value == null)
                {
                    if((_Index + 1) > _Buffer.Length)
                    {
                        Flush();
                    }

                    _Buffer[_Index++] = 0;
                }
                else
                {
                    if((_Index + 1) > _Buffer.Length)
                    {
                        Flush();
                    }

                    _Buffer[_Index++] = 1;

                    InternalWriteString(value);
                }
            }
            else
            {
                InternalWriteString(value);
            }
        }

        public override void Write(DateTime value)
        {
            Write(value.Ticks);
        }

        public override void WriteDeltaTime(DateTime value)
        {
            long ticks = value.Ticks;
            long now = DateTime.Now.Ticks;
            TimeSpan d;
            try
            {
                d = new TimeSpan(ticks - now);
            }
            catch
            {
                if(ticks < now)
                {
                    d = TimeSpan.MaxValue;
                }
                else
                {
                    d = TimeSpan.MaxValue;
                }
            }
            Write(d);
        }

        public override void Write(IPAddress value)
        {
            Write(Utility.GetLongAddressValue(value));
        }

        public override void Write(TimeSpan value)
        {
            Write(value.Ticks);
        }

        public override void Write(decimal value)
        {
            int[] bits = Decimal.GetBits(value);
            for(int i = 0; i < bits.Length; ++i)
            {
                Write(bits[i]);
            }
        }

        public override void Write(long value)
        {
            if((_Index + 8) > _Buffer.Length)
            {
                Flush();
            }
            _Buffer[_Index] = (byte)value;
            _Buffer[_Index + 1] = (byte)(value >> 8);
            _Buffer[_Index + 2] = (byte)(value >> 16);
            _Buffer[_Index + 3] = (byte)(value >> 24);
            _Buffer[_Index + 4] = (byte)(value >> 32);
            _Buffer[_Index + 5] = (byte)(value >> 40);
            _Buffer[_Index + 6] = (byte)(value >> 48);
            _Buffer[_Index + 7] = (byte)(value >> 56);
            _Index += 8;
        }

        public override void Write(ulong value)
        {
            if((_Index + 8) > _Buffer.Length)
            {
                Flush();
            }
            _Buffer[_Index] = (byte)value;
            _Buffer[_Index + 1] = (byte)(value >> 8);
            _Buffer[_Index + 2] = (byte)(value >> 16);
            _Buffer[_Index + 3] = (byte)(value >> 24);
            _Buffer[_Index + 4] = (byte)(value >> 32);
            _Buffer[_Index + 5] = (byte)(value >> 40);
            _Buffer[_Index + 6] = (byte)(value >> 48);
            _Buffer[_Index + 7] = (byte)(value >> 56);
            _Index += 8;
        }

        public override void Write(int value)
        {
            if((_Index + 4) > _Buffer.Length)
            {
                Flush();
            }
            _Buffer[_Index] = (byte)value;
            _Buffer[_Index + 1] = (byte)(value >> 8);
            _Buffer[_Index + 2] = (byte)(value >> 16);
            _Buffer[_Index + 3] = (byte)(value >> 24);
            _Index += 4;
        }

        public override void Write(uint value)
        {
            if((_Index + 4) > _Buffer.Length)
            {
                Flush();
            }
            _Buffer[_Index] = (byte)value;
            _Buffer[_Index + 1] = (byte)(value >> 8);
            _Buffer[_Index + 2] = (byte)(value >> 16);
            _Buffer[_Index + 3] = (byte)(value >> 24);
            _Index += 4;
        }

        public override void Write(short value)
        {
            if((_Index + 2) > _Buffer.Length)
            {
                Flush();
            }
            _Buffer[_Index] = (byte)value;
            _Buffer[_Index + 1] = (byte)(value >> 8);
            _Index += 2;
        }

        public override void Write(ushort value)
        {
            if((_Index + 2) > _Buffer.Length)
            {
                Flush();
            }
            _Buffer[_Index] = (byte)value;
            _Buffer[_Index + 1] = (byte)(value >> 8);
            _Index += 2;
        }

        public override unsafe void Write(double value)
        {
            if((_Index + 8) > _Buffer.Length)
            {
                Flush();
            }
            fixed(byte* pBuffer = _Buffer)
            {
                *((double*)(pBuffer + _Index)) = value;
            }
            _Index += 8;
        }

        public override unsafe void Write(float value)
        {
            if((_Index + 4) > _Buffer.Length)
            {
                Flush();
            }
            fixed(byte* pBuffer = _Buffer)
            {
                *((float*)(pBuffer + _Index)) = value;
            }
            _Index += 4;
        }

        public override void Write(char value)
        {
            if((_Index + 8) > _Buffer.Length)
            {
                Flush();
            }
            _SingleCharBuffer[0] = value;
            int byteCount = _Encoding.GetBytes(_SingleCharBuffer, 0, 1, _Buffer, _Index);
            _Index += byteCount;
        }

        public override void Write(byte value)
        {
            if((_Index + 1) > _Buffer.Length)
            {
                Flush();
            }
            _Buffer[_Index++] = value;
        }

        public override void Write(byte[] value)
        {
            for(int i = 0; i < value.Length; i++)
            {
                Write(value[i]);
            }
        }

        public override void Write(sbyte value)
        {
            if((_Index + 1) > _Buffer.Length)
            {
                Flush();
            }
            _Buffer[_Index++] = (byte)value;
        }

        public override void Write(bool value)
        {
            if((_Index + 1) > _Buffer.Length)
            {
                Flush();
            }
            _Buffer[_Index++] = (byte)(value ? 1 : 0);
        }
    }
}