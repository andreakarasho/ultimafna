﻿/***************************************************************************
 *   ASound.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;

namespace UltimaXNA.Core.Audio
{
    abstract class ASound : IDisposable
    {
        string _Name;
        public string Name
        {
            get { return _Name; }
            private set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    _Name = value.Replace(".mp3", "");
                }
                else
                {
                    _Name = string.Empty;
                }
            }
        }
        public DateTime LastPlayed = DateTime.MinValue;
        public static TimeSpan MinimumDelay = TimeSpan.FromSeconds(1d);

        abstract protected byte[] GetBuffer();
        abstract protected void OnBufferNeeded(object sender, EventArgs e);
        virtual protected void AfterStop() { }
        virtual protected void BeforePlay() { }

        static readonly List<Tuple<DynamicSoundEffectInstance, double>> _EffectInstances;
        static readonly List<Tuple<DynamicSoundEffectInstance, double>> _MusicInstances;
        protected DynamicSoundEffectInstance _ThisInstance;

        protected int Frequency = 22050;
        protected AudioChannels Channels = AudioChannels.Mono;

        static ASound()
        {
            _EffectInstances = new List<Tuple<DynamicSoundEffectInstance, double>>();
            _MusicInstances = new List<Tuple<DynamicSoundEffectInstance, double>>();
        }

        public ASound(string name)
        {
            Name = name;
            
        }

        public void Dispose()
        {
            if (_ThisInstance != null)
            {
                _ThisInstance.BufferNeeded -= OnBufferNeeded;
                if (!_ThisInstance.IsDisposed)
                {
                    _ThisInstance.Stop();
                    _ThisInstance.Dispose();
                }
                _ThisInstance = null;
            }
        }

        /// <summary>
        /// Plays the effect.
        /// </summary>
        /// <param name="asEffect">Set to false for music, true for sound effects.</param>
        public void Play(bool asEffect, AudioEffects effect = AudioEffects.None, float volume = 1.0f, bool spamCheck = false)
        {
            double now = (float)Service.Get<UltimaGame>().TotalMS;
            CullExpiredEffects(now);

            if (spamCheck && (LastPlayed + MinimumDelay > DateTime.Now))
                return;

            BeforePlay();
            _ThisInstance = GetNewInstance(asEffect);
            if (_ThisInstance == null)
            {
                Dispose();
                return;
            }

            switch (effect)
            {
                case AudioEffects.PitchVariation:
                    float pitch = Utility.RandomValue(-5, 5) * .025f;
                    _ThisInstance.Pitch = pitch;
                    break;
            }
            
            LastPlayed = DateTime.Now;

            byte[] buffer = GetBuffer();
            if (buffer != null && buffer.Length > 0)
            {
                _ThisInstance.BufferNeeded += OnBufferNeeded;
                _ThisInstance.SubmitBuffer(buffer);
                _ThisInstance.Volume = volume;
                _ThisInstance.Play();
                List<Tuple<DynamicSoundEffectInstance, double>> list = (asEffect) ? _EffectInstances : _MusicInstances;
                double ms = _ThisInstance.GetSampleDuration(buffer.Length).TotalMilliseconds;
                list.Add(new Tuple<DynamicSoundEffectInstance, double>(_ThisInstance, now + ms));
            }
        }

        public void Stop()
        {
            AfterStop();
        }

        private void CullExpiredEffects(double now)
        {
            // Check to see if any existing instances have stopped playing. If they have, remove the
            // reference to them so the garbage collector can collect them.
            for (int i = 0; i < _EffectInstances.Count; i++)
            {
                if (_EffectInstances[i].Item1.IsDisposed || _EffectInstances[i].Item1.State == SoundState.Stopped || _EffectInstances[i].Item2 <= now)
                {
                    _EffectInstances[i].Item1.Dispose();
                    _EffectInstances.RemoveAt(i);
                    i--;
                }
            }

            for (int i = 0; i < _MusicInstances.Count; i++)
            {
                if (_MusicInstances[i].Item1.IsDisposed || _MusicInstances[i].Item1.State == SoundState.Stopped)
                {
                    _MusicInstances[i].Item1.Dispose();
                    _MusicInstances.RemoveAt(i);
                    i--;
                }
            }
        }

        private DynamicSoundEffectInstance GetNewInstance(bool asEffect)
        {
            List<Tuple<DynamicSoundEffectInstance, double>> list = (asEffect) ? _EffectInstances : _MusicInstances;
            int maxInstances = (asEffect) ? 32 : 2;
            if (list.Count >= maxInstances)
                return null;
            else
                return new DynamicSoundEffectInstance(Frequency, Channels); // shouldn't we be recycling these?
        }
    }
}
