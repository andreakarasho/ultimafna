﻿/***************************************************************************
 *   InputEvent.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using UltimaXNA.Core.Windows;

namespace UltimaXNA.Core.Input
{
    public class InputEvent
    {
        readonly WinKeys _Modifiers;
        bool _Handled;

        public bool Alt => (_Modifiers & WinKeys.Alt) == WinKeys.Alt;

        public bool Control => (_Modifiers & WinKeys.Control) == WinKeys.Control;

        public bool Shift => (_Modifiers & WinKeys.Shift) == WinKeys.Shift;

        public bool Handled
        {
            get { return _Handled; }
            set { _Handled = value; }
        }

        public InputEvent(WinKeys modifiers)
        {
            _Modifiers = modifiers;
        }

        protected InputEvent(InputEvent parent)
        {
            _Modifiers = parent._Modifiers;
        }
    }
}
