﻿#region usings
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using UltimaXNA.Core.Windows;
using Microsoft.Xna.Framework.Input;
#endregion

namespace UltimaXNA.Core.Input
{
    public class InputManager
    {
        private double _LastMouseMove;
        private double _LastUpdateTime;
        private MouseState _MouseState;
        private KeyboardState _KeyboardState;
        private Keymap _Keymap;
        private int _Modifiers;
        private Keys _lastKey;
        private double _lastPressedKey;
        private InputEventMouse _lastMouseDown, _lastMouseClick;
        private float _lastMouseDownTime = 0, _theTime = -1, _lastMouseClickTime = 0;
        private List<InputEvent> _eventsThisFrame = new List<InputEvent>();
        private List<InputEvent> _eventsAccumulating = new List<InputEvent>();
        private List<InputEvent> _eventsAccumulatingAlternate = new List<InputEvent>();
        private bool _eventsaccumulating = false;
        private bool _isDragging;

        public InputManager(IntPtr handle)
        {
            _MouseState = Mouse.GetState();
            _KeyboardState = Keyboard.GetState();
            _Keymap = new Keymap();
            _Modifiers = 0;

            TextInputEXT.TextInput += delegate (char c) 
            {
              //  Console.WriteLine(Enum.Parse(typeof(WinKeys), c.ToString()));
                
            };
        }

        public bool IsCtrlDown => (_Modifiers & (int)WinKeys.Control) > 0;
        public bool IsShiftDown => (_Modifiers & (int)WinKeys.Shift) > 0;
        public int MouseStationaryTimeMS => (int)(_LastUpdateTime - _LastMouseMove);
        public Point MousePosition => new Point(_MouseState.X, _MouseState.Y);


        public bool IsKeyDown(WinKeys key) => _KeyboardState.IsKeyDown(TranslateToXNAKey(key));


        public List<InputEventKeyboard> GetKeyboardEvents()
        {
            var events = new List<InputEventKeyboard>();

            foreach (var e in _eventsThisFrame)
            {
                if (!e.Handled && e is InputEventKeyboard)
                    events.Add(e as InputEventKeyboard);
            }

            return events;
        }

        public List<InputEventMouse> GetMouseEvents()
        {
            var events = new List<InputEventMouse>();

            foreach (var e in _eventsThisFrame)
            {
                if (!e.Handled && e is InputEventMouse)
                    events.Add(e as InputEventMouse);
            }

            return events;
        }

        private static WinKeys TranslateToWinKey(Keys xnaKey)
        {
            switch (xnaKey)
            {
                case Keys.Back: return WinKeys.Back;
                case Keys.Tab: return WinKeys.Tab;
                case Keys.Enter: return WinKeys.Enter;
                case Keys.CapsLock: return WinKeys.CapsLock;
                case Keys.Escape: return WinKeys.Escape;
                case Keys.Space: return WinKeys.Space;
                case Keys.PageUp: return WinKeys.PageUp;
                case Keys.PageDown: return WinKeys.PageDown;
                case Keys.End: return WinKeys.End;
                case Keys.Home: return WinKeys.Home;
                case Keys.Left: return WinKeys.Left;
                case Keys.Up: return WinKeys.Up;
                case Keys.Right: return WinKeys.Right;
                case Keys.Down: return WinKeys.Down;
                case Keys.Select: return WinKeys.Select;
                case Keys.Print: return WinKeys.Print;
                case Keys.Execute: return WinKeys.Execute;
                case Keys.PrintScreen: return WinKeys.PrintScreen;

                case Keys.D0: return WinKeys.D0;
                case Keys.D1: return WinKeys.D1;
                case Keys.D2: return WinKeys.D2;
                case Keys.D3: return WinKeys.D3;
                case Keys.D4: return WinKeys.D4;
                case Keys.D5: return WinKeys.D5;
                case Keys.D6: return WinKeys.D6;
                case Keys.D7: return WinKeys.D7;
                case Keys.D8: return WinKeys.D8;
                case Keys.D9: return WinKeys.D9;

                case Keys.A: return WinKeys.A;
                case Keys.B: return WinKeys.B;
                case Keys.C: return WinKeys.C;
                case Keys.D: return WinKeys.D;
                case Keys.E: return WinKeys.E;
                case Keys.F: return WinKeys.F;
                case Keys.G: return WinKeys.G;
                case Keys.H: return WinKeys.H;
                case Keys.I: return WinKeys.I;
                case Keys.J: return WinKeys.J;
                case Keys.K: return WinKeys.K;
                case Keys.L: return WinKeys.L;
                case Keys.M: return WinKeys.M;
                case Keys.N: return WinKeys.N;
                case Keys.O: return WinKeys.O;
                case Keys.P: return WinKeys.P;
                case Keys.Q: return WinKeys.Q;
                case Keys.R: return WinKeys.R;
                case Keys.S: return WinKeys.S;
                case Keys.T: return WinKeys.T;
                case Keys.U: return WinKeys.U;
                case Keys.V: return WinKeys.V;
                case Keys.W: return WinKeys.W;
                case Keys.X: return WinKeys.X;
                case Keys.Y: return WinKeys.Y;
                case Keys.Z: return WinKeys.Z;

                case Keys.LaunchApplication1: return WinKeys.None;
                default:
                    return WinKeys.None;
            }
        }

        private char TranslateToPrintableChar(Keys xnaKey, KeyboardState state)
        {
            var shifted = state.IsKeyDown(Keys.LeftShift) ||
                          state.IsKeyDown(Keys.RightShift);

            return _Keymap.GetChar(xnaKey, shifted);
        }

        private static Keys TranslateToXNAKey(WinKeys winKey)
        {
            switch (winKey)
            {
                case WinKeys.Up: return Keys.Up;
                case WinKeys.Right: return Keys.Right;
                case WinKeys.Left: return Keys.Left;
                case WinKeys.Down: return Keys.Down;
                default: return Keys.None;
            }
        }

        public void Update(double totalTime, double frameTime)
        {
            _theTime = (float)totalTime;

            var mouseState = Mouse.GetState();
            var keyState = Keyboard.GetState();


            if (mouseState.X != _MouseState.X || mouseState.Y != _MouseState.Y)
            {
                _LastMouseMove = totalTime;
                HandleMouseMove(new InputEventMouse(MouseEvent.Move,(mouseState.LeftButton == ButtonState.Pressed ? WinMouseButtons.Left  :  WinMouseButtons.None), 0, _MouseState.X, _MouseState.Y, 0, 0));
            }

            if (mouseState.LeftButton != _MouseState.LeftButton)
            {
                if (mouseState.LeftButton == ButtonState.Pressed)
                {
                    HandleMouseDown(new InputEventMouse(MouseEvent.Down, WinMouseButtons.Left, 0, _MouseState.X, _MouseState.Y, 0, 0));
                }
                else
                {
                    HandleMouseUp(new InputEventMouse(MouseEvent.Up, WinMouseButtons.Left, 0, _MouseState.X, _MouseState.Y, 0, 0));
                }
            }

            if (mouseState.RightButton != _MouseState.RightButton)
            {
                if (mouseState.RightButton == ButtonState.Pressed)
                    HandleMouseDown(new InputEventMouse(MouseEvent.Down, WinMouseButtons.Right, 0, _MouseState.X, _MouseState.Y, 0, 0));
                else
                    HandleMouseUp(new InputEventMouse(MouseEvent.Up, WinMouseButtons.Right, 0, _MouseState.X, _MouseState.Y, 0, 0));
            }

            if (mouseState.MiddleButton != _MouseState.RightButton)
            {
                var buttonState = mouseState.MiddleButton == ButtonState.Pressed ? MouseEvent.Down : MouseEvent.Up;
                AddEvent(new InputEventMouse(buttonState, WinMouseButtons.Middle, 0, _MouseState.X, _MouseState.Y, 0, 0));
            }

            if (mouseState.ScrollWheelValue != _MouseState.ScrollWheelValue)
            {
                AddEvent(new InputEventMouse(MouseEvent.WheelScroll, WinMouseButtons.Middle, mouseState.ScrollWheelValue - _MouseState.ScrollWheelValue, _MouseState.X, _MouseState.Y, _MouseState.ScrollWheelValue, 0));
            }

            var oldKeys = new List<Keys>(_KeyboardState.GetPressedKeys());
            var newKeys = new List<Keys>(keyState.GetPressedKeys());
            _Modifiers = 0;

            if (keyState.IsKeyDown(Keys.LeftShift) || keyState.IsKeyDown(Keys.RightShift))
                _Modifiers |= (int)WinKeys.Shift;

            if (keyState.IsKeyDown(Keys.LeftAlt) || keyState.IsKeyDown(Keys.RightAlt))
                _Modifiers |= (int)WinKeys.Alt;

            if (keyState.IsKeyDown(Keys.LeftControl) || keyState.IsKeyDown(Keys.RightControl))
                _Modifiers |= (int)WinKeys.Control;
            
            foreach (var k in oldKeys)
            {
                if (!keyState.IsKeyDown(k) && _KeyboardState.IsKeyDown(k))
                {
                    if (_lastKey == k)
                        _lastKey = Keys.None;
                    AddEvent(new InputEventKeyboard(KeyboardEvent.Up, TranslateToWinKey(k), _Modifiers));
                }
            }

            foreach (var k in newKeys)
            {
                if (keyState.IsKeyDown(k) && _KeyboardState.IsKeyDown(k))
                {
                    if (_lastKey == k)
                    {
                        _lastPressedKey = totalTime + 500;
                        _lastKey = Keys.None;
                    }
                }
                else if (keyState.IsKeyDown(k) && !_KeyboardState.IsKeyDown(k))
                {
                    _lastKey = k;
                    _lastPressedKey = 0;
                }

                if (totalTime > _lastPressedKey)
                {
                    if (_lastKey == k)
                        AddEvent(new InputEventKeyboard(KeyboardEvent.Down, TranslateToWinKey(k), _Modifiers));
                    InputEventKeyboard press = new InputEventKeyboard(KeyboardEvent.Press, TranslateToWinKey(k), _Modifiers);
                    press.OverrideKeyChar(TranslateToPrintableChar(k, keyState));
                    AddEvent(press);
                    _lastPressedKey = totalTime + 25;
                }
            }

            _MouseState = mouseState;
            _KeyboardState = keyState;
            _LastUpdateTime = totalTime;

            CopyEvents();
        }

        private void HandleMouseDown(InputEventMouse e)
        {
            _lastMouseDown = e;
            _lastMouseDownTime = _theTime;
            AddEvent(_lastMouseDown);
        }

        private void HandleMouseUp(InputEventMouse e)
        {
            if (_isDragging)
            {

                AddEvent(new InputEventMouse(MouseEvent.DragEnd, e));
                _isDragging = false;
            }
            else
            {
                if (_lastMouseDown != null)
                {
                    if (!DistanceBetweenPoints(_lastMouseDown.Position, e.Position, 2))
                    {
                        AddEvent(new InputEventMouse(MouseEvent.Click, e));

                        if ((_theTime - _lastMouseClickTime <= Settings.UserInterface.Mouse.DoubleClickMS) && !DistanceBetweenPoints(_lastMouseClick.Position, e.Position, 2))
                        {
                            _lastMouseClickTime = 0f;
                            AddEvent(new InputEventMouse(MouseEvent.DoubleClick, e));
                        }
                        else
                        {
                            _lastMouseClickTime = _theTime;
                            _lastMouseClick = e;
                        }
                    }
                }
            }
            AddEvent(new InputEventMouse(MouseEvent.Up, e));
            _lastMouseDown = null;
        }

        private void HandleMouseMove(InputEventMouse e)
        {
            AddEvent(new InputEventMouse(MouseEvent.Move, e));
            if (!_isDragging && _lastMouseDown != null)
            {
                if (DistanceBetweenPoints(_lastMouseDown.Position, e.Position, 2))
                {
                    AddEvent(new InputEventMouse(MouseEvent.DragBegin, e));
                    _isDragging = true;
                }
            }
        }

        bool DistanceBetweenPoints(Point initial, Point final, int distance)
        {
            return Math.Abs(final.X - initial.X) + Math.Abs(final.Y - initial.Y) > distance;          
        }

        public bool HandleKeyboardEvent(KeyboardEvent type, WinKeys key, bool shift, bool alt, bool ctrl)
        {
            List<InputEventKeyboard> events = GetKeyboardEvents();
            foreach (InputEventKeyboard e in events)
            {
                if (e.Handled)
                    continue;
                if (e.EventType == type &&
                   e.KeyCode == key &&
                   e.Shift == shift &&
                   e.Alt == alt &&
                   e.Control == ctrl)
                {
                    e.Handled = true;
                    return true;
                }
            }

            return false;
        }

        public bool HandleMouseEvent(MouseEvent type, MouseButton mb)
        {
            List<InputEventMouse> events = GetMouseEvents();
            foreach (InputEventMouse e in events)
            {
                if (e.Handled)
                    continue;
                if (e.EventType == type && e.Button == mb)
                {
                    e.Handled = true;
                    return true;
                }
            }

            return false;
        }

        private void AddEvent(InputEvent e)
        {
            List<InputEvent> list = _eventsaccumulating ? _eventsAccumulatingAlternate : _eventsAccumulating;
            list.Add(e);
        }

        private void CopyEvents()
        {
            _eventsaccumulating = true;

            _eventsThisFrame.Clear();
            foreach (InputEvent e in _eventsAccumulating)
                _eventsThisFrame.Add(e);
            _eventsAccumulating.Clear();

            _eventsaccumulating = false;

            foreach (InputEvent e in _eventsAccumulatingAlternate)
                _eventsThisFrame.Add(e);
            _eventsAccumulatingAlternate.Clear();
        }
    }
}