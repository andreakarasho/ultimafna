﻿/***************************************************************************
 *   InputManager.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

#region usings
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using UltimaXNA.Core.Diagnostics.Tracing;
using UltimaXNA.Core.Extensions;
using UltimaXNA.Core.Windows;
#endregion

namespace UltimaXNA.Core.Input
{
  /*  public class InputService : InputManager
    {
        WndProc _WndProc;
        List<InputEvent> _Events = new List<InputEvent>();
        List<InputEvent> _EventsNext = new List<InputEvent>();
        MouseState _MouseState;
        MouseState _MouseStateLast;

        // Mouse dragging support
        const int MouseDragBeginDistance = 2;
        const int MouseClickMaxDelta = 2;
        InputEventMouse _LastMouseClick;
        float _LastMouseClickTime;
        InputEventMouse _LastMouseDown;
        float _LastMouseDownTime;
        bool _MouseIsDragging;
        float _TheTime = -1f;

        public InputService(IntPtr handle)
        {
            _WndProc = new WndProc(handle);
            _WndProc.MouseWheel += AddEvent;
            _WndProc.MouseMove += OnMouseMove;
            _WndProc.MouseUp += OnMouseUp;
            _WndProc.MouseDown += OnMouseDown;
            _WndProc.KeyDown += OnKeyDown;
            _WndProc.KeyUp += OnKeyUp;
            _WndProc.KeyChar += OnKeyChar;
        }

        public void Dispose()
        {
            _WndProc.MouseWheel -= AddEvent;
            _WndProc.MouseMove -= OnMouseMove;
            _WndProc.MouseUp -= OnMouseUp;
            _WndProc.MouseDown -= OnMouseDown;
            _WndProc.KeyDown -= OnKeyDown;
            _WndProc.KeyUp -= OnKeyUp;
            _WndProc.KeyChar -= OnKeyChar;
            _WndProc.Dispose();
            _WndProc = null;
        }

        public void Update(double totalTime, double frameTime)
        {
            _TheTime = (float)totalTime;
            _MouseStateLast = _MouseState;
            _MouseState = _WndProc.MouseState.CreateWithDPI(DpiManager.GetSystemDpiScalar());
            lock (_EventsNext)
            {
                _Events.Clear();
                foreach (InputEvent e in _EventsNext)
                {
                    _Events.Add(e);
                }
                _EventsNext.Clear();
            }
        }

        public bool IsCtrlDown => NativeMethods.GetKeyState((int)WinKeys.ControlKey) < 0;

        public bool IsShiftDown => NativeMethods.GetKeyState((int)WinKeys.ShiftKey) < 0;

        public bool IsKeyDown(WinKeys key) => NativeMethods.GetKeyState((int)key) < 0;

        public Point MousePosition => new Point(_MouseState.X, _MouseState.Y);

        public bool IsMouseButtonDown(MouseButton btn)
        {
            switch (btn)
            {
                case MouseButton.Left:
                    return _MouseState.LeftButton == ButtonState.Pressed;
                case MouseButton.Middle:
                    return _MouseState.MiddleButton == ButtonState.Pressed;
                case MouseButton.Right:
                    return _MouseState.RightButton == ButtonState.Pressed;
                case MouseButton.XButton1:
                    return _MouseState.XButton1 == ButtonState.Pressed;
                case MouseButton.XButton2:
                    return _MouseState.XButton2 == ButtonState.Pressed;
                default:
                    return false;
            }
        }

        InputEventKeyboard LastKeyPressEvent
        {
            get
            {
                for (int i = _EventsNext.Count; i >= 0; i--)
                {
                    if ((_EventsNext[i - 1] as InputEventKeyboard)?.EventType == KeyboardEvent.Press)
                    {
                        return _EventsNext[i - 1] as InputEventKeyboard;
                    }
                }
                return null;
            }
        }

        public List<InputEventKeyboard> GetKeyboardEvents()
        {
            List<InputEventKeyboard> list = new List<InputEventKeyboard>();
            foreach (InputEvent e in _Events)
            {
                if (!e.Handled && e is InputEventKeyboard)
                {
                    list.Add(e as InputEventKeyboard);
                }
            }
            return list;
        }

        public List<InputEventMouse> GetMouseEvents()
        {
            List<InputEventMouse> list = new List<InputEventMouse>();
            foreach (InputEvent e in _Events)
            {
                if (!e.Handled && e is InputEventMouse)
                {
                    list.Add(e as InputEventMouse);
                }
            }
            return list;
        }

        public bool HandleKeyboardEvent(KeyboardEvent type, WinKeys key, bool shift, bool alt, bool ctrl)
        {
            foreach (InputEvent e in _Events)
            {
                if (!e.Handled && e is InputEventKeyboard)
                {
                    InputEventKeyboard ek = e as InputEventKeyboard;
                    if (ek.EventType == type && ek.KeyCode == key && 
                        ek.Shift == shift && ek.Alt == alt && ek.Control == ctrl)
                    {
                        e.Handled = true;
                        return true;
                    }
                }
            }
            return false;
        }

        public bool HandleMouseEvent(MouseEvent type, MouseButton mb)
        {
            foreach (InputEvent e in _Events)
            {
                if (!e.Handled && e is InputEventMouse)
                {
                    InputEventMouse em = (InputEventMouse)e;
                    if (em.EventType == type && em.Button == mb)
                    {
                        e.Handled = true;
                        return true;
                    }
                }
            }
            return false;
        }

        void OnMouseDown(InputEventMouse e)
        {
            _LastMouseDown = e;
            _LastMouseDownTime = _TheTime;
            AddEvent(_LastMouseDown);
        }

        void OnMouseUp(InputEventMouse e)
        {
            if (_MouseIsDragging)
            {
                AddEvent(new InputEventMouse(MouseEvent.DragEnd, e));
                _MouseIsDragging = false;
            }
            else
            {
                if (_LastMouseDown != null)
                {
                    if (!DistanceBetweenPoints(_LastMouseDown.Position, e.Position, MouseClickMaxDelta))
                    {
                        AddEvent(new InputEventMouse(MouseEvent.Click, e));

                        if ((_TheTime - _LastMouseClickTime <= Settings.UserInterface.Mouse.DoubleClickMS) &&
                           !DistanceBetweenPoints(_LastMouseClick.Position, e.Position, MouseClickMaxDelta))
                        {
                            _LastMouseClickTime = 0f;
                            AddEvent(new InputEventMouse(MouseEvent.DoubleClick, e));
                        }
                        else
                        {
                            _LastMouseClickTime = _TheTime;
                            _LastMouseClick = e;
                        }
                    }
                }
            }
            AddEvent(new InputEventMouse(MouseEvent.Up, e));
            _LastMouseDown = null;
        }

        void OnMouseMove(InputEventMouse e)
        {
            AddEvent(new InputEventMouse(MouseEvent.Move, e));
            if (!_MouseIsDragging && _LastMouseDown != null)
            {
                if (DistanceBetweenPoints(_LastMouseDown.Position, e.Position, MouseDragBeginDistance))
                {
                    AddEvent(new InputEventMouse(MouseEvent.DragBegin, e));
                    _MouseIsDragging = true;
                }
            }
        }

        void OnKeyDown(InputEventKeyboard e)
        {
            if (e.DataPreviousState == 0)
            {
                AddEvent(new InputEventKeyboard(KeyboardEvent.Down, e));
            }
            for (int i = 0; i < e.DataRepeatCount; i++)
            {
                AddEvent(new InputEventKeyboard(KeyboardEvent.Press, e));
            }
        }

        void OnKeyUp(InputEventKeyboard e)
        {
            AddEvent(new InputEventKeyboard(KeyboardEvent.Up, e));
        }

        void OnKeyChar(InputEventKeyboard e)
        {
            // Control key sends a strange w_char message ...
            if (e.Control && !e.Alt)
            {
                return;
            }
            InputEventKeyboard ek = LastKeyPressEvent;
            if (ek == null)
            {
                Tracer.Warn("No corresponding KeyPress event for a W_CHAR message.");
            }
            else
            {
                ek.OverrideKeyChar(e.KeyCode);
            }
        }

        void AddEvent(InputEvent e)
        {
            _EventsNext.Add(e);
        }

        bool DistanceBetweenPoints(Point initial, Point final, int distance)
        {
            if (Math.Abs(final.X - initial.X) + Math.Abs(final.Y - initial.Y) > distance)
            {
                return true;
            }
            return false;
        }
    }*/
}