﻿using System;

namespace UltimaXNA.Core.Network
{

    class PacketChunk
    {
        readonly byte[] _Buffer;
        int _Length;

        public PacketChunk(byte[] buffer)
        {
            _Buffer = buffer;
        }

        public byte[] Buffer => _Buffer;

        public int Length
        {
            get { return _Length; }
        }

        public void Write(byte[] source, int offset, int length)
        {
            System.Buffer.BlockCopy(source, offset, _Buffer, _Length, length);

            _Length += length;
        }

        public void Prepend(byte[] dest, int length)
        {
            // Offset the intial buffer by the amount we need to prepend
            if (length > 0)
            {
                System.Buffer.BlockCopy(dest, 0, dest, _Length, length);
            }

            // Prepend the buffer to the destination buffer
            System.Buffer.BlockCopy(_Buffer, 0, dest, 0, _Length);
        }

        public void Clear()
        {
            _Length = 0;
        }
    }
}
