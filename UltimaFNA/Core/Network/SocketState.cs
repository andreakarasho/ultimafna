﻿/***************************************************************************
 *   SocketState.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System.Net.Sockets;
#endregion

namespace UltimaXNA.Core.Network
{
    public class SocketState
    {
        private readonly Socket _Socket;
        private byte[] _Buffer;
        private int _DataLength;

        public Socket Socket
        {
            get { return _Socket; }
        }

        public byte[] Buffer
        {
            get { return _Buffer; }
            set { _Buffer = value; }
        }

        public int DataLength
        {
            get { return _DataLength; }
            set { _DataLength = value; }
        }

        public SocketState(Socket socket, byte[] buffer)
        {
            _Socket = socket;
            _Buffer = buffer;
            _DataLength = 0;
        }
    }
}
