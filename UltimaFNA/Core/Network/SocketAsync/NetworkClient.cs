﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UltimaFNA.Ultima.Network;
using UltimaXNA.Core.Diagnostics;
using UltimaXNA.Core.Diagnostics.Tracing;
using UltimaXNA.Core.Network;
using UltimaXNA.Core.Network.Packets;

namespace UltimaFNA.Core.Network.SocketAsync
{
    public sealed class NetworkClient
    {
        private const int MESSAGE_BUFFER = 0x10000;

        readonly List<PacketHandler>[] _TypedHandlers;
        readonly List<PacketHandler>[][] _ExtendedTypedHandlers;

        private Queue<QueuedPacket> _received, _workingRecv;

        private bool _sending;
        private readonly object _send = new object();
        private readonly object _queueObj = new object();
        private SendQueue _sendQueue;
        private Socket _socket;
        private CircularBuffer _circularBuffer;
        private byte[] _recvBuffer;
        private readonly UltimaFNA.Core.Network.SocketAsync.BufferPool _messagePool = new UltimaFNA.Core.Network.SocketAsync.BufferPool(8, MESSAGE_BUFFER);

        private SocketAsyncEventArgs _recvEventArgs;
        private SocketAsyncEventArgs _sendEventArgs;
        private Huffman _huffmanDecompression;

        private PacketChunk _incompleteDecomp;

        public bool IsRunning { get; private set; }
        public bool IsConnected => _socket != null && _socket.Connected;
        public bool Compression { get; internal set; }

        public NetworkClient()
        {
            _huffmanDecompression = new Huffman();

            _TypedHandlers = new List<PacketHandler>[0x100];
            _ExtendedTypedHandlers = new List<PacketHandler>[0x100][];

            for (int i = 0; i < _TypedHandlers.Length; i++)
            {
                _TypedHandlers[i] = new List<PacketHandler>();
            }
        }

        public bool Connect(string ip, int port)
        {
            _huffmanDecompression = new Huffman();

            _received = new Queue<QueuedPacket>();
            _workingRecv = new Queue<QueuedPacket>();

            IPAddress address = Resolve(ip);
            IPEndPoint endPoint = new IPEndPoint(address, port);

            _circularBuffer = new CircularBuffer();
            _recvBuffer = _messagePool.GetFreeSegment();
            _sendQueue = new SendQueue();

            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            _socket.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.Debug, 1);

            _sendEventArgs = new SocketAsyncEventArgs();
            _sendEventArgs.Completed += _sendEventArgs_Completed;

            _recvEventArgs = new SocketAsyncEventArgs();
            _recvEventArgs.SetBuffer(_recvBuffer, 0, _recvBuffer.Length);
            _recvEventArgs.Completed += _recvEventArgs_Completed;


            _incompleteDecomp = new PacketChunk(_messagePool.GetFreeSegment());

            IsRunning = true;
            Compression = false;

            try
            {
                _socket.Connect(endPoint);
            }
            catch (Exception e)
            {
                Tracer.Error(e);
            }

            if (_socket.Connected)
            {
                Tracer.Debug("Connected.");
                RecvStart();
                return true;
            }

            return false;
        }

        public void Slice()
        {
            if (IsConnected && IsRunning)
            {
                lock (_queueObj)
                {
                    var q = _workingRecv;
                    _workingRecv = _received;
                    _received = q;
                }

                while (_received.Count > 0)
                {
                    QueuedPacket packet = _received.Dequeue();
                    InvokeHandler(packet.PacketHandler, packet.PacketBuffer, packet.RealLength);
                }

                Flush();
            }
        }

        public void Disconnect()
        {
            Tracer.Debug("Disconnect.");
            if (!IsRunning) return;

            Flush();

            IsRunning = false;
            Compression = false;

            try
            {
                _socket.Shutdown(SocketShutdown.Both);
            }
            catch { }
            try
            {
                _socket.Close();

            }
            catch { }

            if (_recvBuffer != null)
            {
                lock (_messagePool)
                    _messagePool.AddFreeSegment(_recvBuffer);
            }

            if (_incompleteDecomp != null)
            {
                lock (_messagePool)
                    _messagePool.AddFreeSegment(_incompleteDecomp.Buffer);
            }

            _socket = null;
            _huffmanDecompression = null;
            _recvBuffer = null;
            _incompleteDecomp = null;
            //_connectEventArgs = null;
            _recvEventArgs = null;
            _sendEventArgs = null;

            _circularBuffer.Clear();

            lock (_sendQueue)
            {
                if (!_sendQueue.IsEmpty)
                    _sendQueue.Clear();
            }

            // SocketDisconnected.Raise();
        }

        private void RecvStart()
        {
            try
            {
                bool flag = false;
                do
                {
                    flag = !_socket.ReceiveAsync(_recvEventArgs);
                    if (flag)
                        RecvProcess(_recvEventArgs);
                }
                while (flag);
            }
            catch (Exception ex)
            {
                //Utility.Log.Write(ex);
                Disconnect();
            }
        }

        private void RecvProcess(SocketAsyncEventArgs e)
        {
            int bytesTransferred = e.BytesTransferred;
            if (e.SocketError != SocketError.Success /*|| bytesTransferred <= 0*/)
            {
                Disconnect();
                return;
            }
            if (bytesTransferred > 0)
            {
                byte[] recvBuffer = _recvBuffer;

                if (Compression)
                    DecompressBuffer(ref recvBuffer, ref bytesTransferred);

                lock (_circularBuffer)
                    _circularBuffer.Enqueue(recvBuffer, 0, bytesTransferred);

                RecvHandler();
            }
        }

        private void _recvEventArgs_Completed(object sender, SocketAsyncEventArgs e)
        {
            RecvProcess(e);
            if (IsRunning)
                RecvStart();
        }

        private void SendStart()
        {
            try
            {
                bool flag;
                do
                {
                    flag = !_socket.SendAsync(_sendEventArgs);
                    if (flag)
                        SendProcess(_sendEventArgs);
                }
                while (flag);
            }
            catch (Exception ex)
            {
                // Utility.Log.Write(ex);
                Disconnect();
            }
        }

        private void SendProcess(SocketAsyncEventArgs e)
        {
            int bytesTransferred = e.BytesTransferred;
            if (e.SocketError != SocketError.Success || bytesTransferred <= 0)
            {
                Disconnect();
                return;
            }
        }

        private void _sendEventArgs_Completed(object sender, SocketAsyncEventArgs e)
        {
            SendProcess(e);

            SendQueue.Gram gram;

            lock (_sendQueue)
            {
                gram = _sendQueue.Dequeue();
                if (gram == null && _sendQueue.IsFlushReady)
                    gram = _sendQueue.CheckFlushReady();
            }
            if (gram != null)
            {
                _sendEventArgs.SetBuffer(gram.Buffer, 0, gram.Length);
                SendStart();
            }
            else
            {
                lock (_send)
                    _sending = false;
            }
        }

        public bool Flush()
        {
            if (_socket == null)
                return false;

            lock (_send)
            {
                if (_sending)
                    return false;

                SendQueue.Gram gram;

                lock (_sendQueue)
                {
                    if (!_sendQueue.IsFlushReady)
                        return false;
                    gram = _sendQueue.CheckFlushReady();
                }

                if (gram != null)
                {
                    _sending = true;
                    _sendEventArgs.SetBuffer(gram.Buffer, 0, gram.Length);
                    SendStart();
                }
            }
            return false;
        }

        public void Send(SendPacket packet)
        {
            Send(packet.Compile(), packet.Length);
        }

        public void Send(byte[] data, int length)
        {
            if (data == null || data.Length <= 0 || length <= 0) return;

            try
            {
                SendQueue.Gram gram;

                lock (_send)
                {
                    lock (_sendQueue)
                        gram = _sendQueue.Enqueue(data, length);
                    if (gram != null && !_sending)
                    {
                        _sending = true;
                        _sendEventArgs.SetBuffer(gram.Buffer, 0, gram.Length);
                        SendStart();
                    }
                }
            }
            catch (CapacityExceededException)
            {
                Disconnect();
            }
        }

        private void DecompressBuffer(ref byte[] buffer, ref int length)
        {
            /*byte[] decbuf = _messagePool.GetFreeSegment();
            int outsize = 65536;
            int len = length;

            _huffmanDecompression.DecompressAll(buffer, len, decbuf, ref outsize);

            buffer = decbuf;
            length = outsize;
            _messagePool.AddFreeSegment(decbuf);*/

            byte[] source = _messagePool.GetFreeSegment();
            int incompleteLength = _incompleteDecomp.Length;
            int sourceLength = incompleteLength + length;

            if (incompleteLength > 0)
            {
                _incompleteDecomp.Prepend(source, 0);
                _incompleteDecomp.Clear();
            }

            Buffer.BlockCopy(buffer, 0, source, incompleteLength, length);

            int processedOffset = 0;
            int sourceOffset = 0;
            int offset = 0;

            while (_huffmanDecompression.DecompressChunk(ref source, ref sourceOffset, sourceLength, ref buffer, offset, out int outSize))
            {
                processedOffset = sourceOffset;
                offset += outSize;
            }

            length = offset;

            // We've run out of data to parse, or the packet was incomplete. If the packet was incomplete,
            // we should save what's left for the next socket receive event.
            if (processedOffset >= sourceLength)
            {
                _messagePool.AddFreeSegment(source);
                return;
            }

            _incompleteDecomp.Write(source, processedOffset, sourceLength - processedOffset);
        }

        public void RecvHandler()
        {
            if (!IsRunning || _circularBuffer == null || _circularBuffer.Length <= 0) return;

            lock (_circularBuffer)
            {
                int length = _circularBuffer.Length;

                while (length > 0 && IsRunning)
                {
                    byte id = _circularBuffer.GetID();

                    PacketHandler handler = GetHandlers(id);

                    int packetlength = handler.Length;
                    if (packetlength <= 0)
                    {
                        if (length >= 3)
                            packetlength = _circularBuffer.GetLength();
                        else
                        {
                            Tracer.Warn("Packet {0} size < 3", id);
                            break;
                        }
                    }

                    /*if (PacketHandlers.ToClient.Handlers[id].Count <= 0)
                    {
                        byte[] toremove = new byte[packetlength];
                        length = _circularBuffer.Dequeue(toremove, 0, toremove.Length);
                        break;
                    }
                    */

                    if (length < packetlength)
                    {
                        Tracer.Warn("WRONG PACKET SIZE. ID 0x{0:X2} - RecvLength {1} - ReadLength {2}", id, length, packetlength);
                        //byte[] toremove = new byte[length];
                        //length = _circularBuffer.Dequeue(toremove, 0, length);
                        break;
                    }

                    byte[] packetbuffer = MESSAGE_BUFFER >= packetlength ? _messagePool.GetFreeSegment() : new byte[packetlength];
                    packetlength = _circularBuffer.Dequeue(packetbuffer, 0, packetlength);

                    // Packet packet = new Packet(packetbuffer, packetlength);

                    lock (_queueObj)
                        _workingRecv.Enqueue(new QueuedPacket(handler, packetbuffer, packetlength));

                    length = _circularBuffer.Length;

                    if (MESSAGE_BUFFER >= packetlength) _messagePool.AddFreeSegment(packetbuffer);
                }
            }
        }

        private void InvokeHandler(PacketHandler packetHandler, byte[] buffer, int length)
        {
            if (packetHandler == null)
            {
                return;
            }
            PacketReader reader = PacketReader.CreateInstance(buffer, length, packetHandler.Length != -1);
            packetHandler.Invoke(reader);
        }

        private IPAddress Resolve(string addr)
        {
            IPAddress result = IPAddress.None;
            if (string.IsNullOrEmpty(addr))
                return result;

            if (!IPAddress.TryParse(addr, out result))
            {
                try
                {
                    IPHostEntry hostEntry = Dns.GetHostEntry(addr);
                    if (hostEntry.AddressList.Length != 0)
                        result = hostEntry.AddressList[hostEntry.AddressList.Length - 1];
                }
                catch
                {
                }
            }
            return result;
        }

        public PacketHandler GetHandlers(byte id)
        {
            return _TypedHandlers[id][0];
        }

        public void Register<T>(object client, int id, int length, Action<T> onReceive) where T : IRecvPacket
        {
            Type type = typeof(T);
            ConstructorInfo[] ctors = type.GetConstructors();
            bool valid = false;
            for (int i = 0; i < ctors.Length && !valid; i++)
            {
                ParameterInfo[] parameters = ctors[i].GetParameters();
                valid = (parameters.Length == 1 && parameters[0].ParameterType == typeof(PacketReader));
            }
            if (!valid)
            {
                throw new NetworkException($"Unable to register packet type {type} without a public constructor with a {typeof(PacketReader)} parameter");
            }
            if (id > byte.MaxValue)
            {
                throw new NetworkException($"Unable to register packet id 0x{id:X4} because it is greater than 0xff");
            }

            int probalLen = PacketsTable.GetPacketLength(id);
            if (probalLen != length)
            {
                Tracer.Warn($"Adjusted packet size: {id.ToString("X")} - Old: {length} - New: {probalLen}");
                length = probalLen;
            }

            PacketHandler handler = new PacketHandler<T>(id, length, type, client, onReceive);
            if (_TypedHandlers[id].Any())
            {
                int requiredLength = _TypedHandlers[id][0].Length;
                Guard.Requires(requiredLength == length,
                    "Invalid packet length.  All packet handlers for 0x{0:X2} must specify a length of {1}.", id,
                    requiredLength);
            }
            _TypedHandlers[id].Add(handler);
        }

        public void Unregister(object client)
        {
            for (int id = 0; id < byte.MaxValue; id++)
            {
                if (_TypedHandlers[id] != null)
                {
                    for (int i = 0; i < _TypedHandlers[id].Count; i++)
                    {
                        PacketHandler handler = _TypedHandlers[id][i] as PacketHandler;
                        if (handler.Client == client)
                        {
                            _TypedHandlers[id].RemoveAt(i);
                            i--;
                        }
                    }
                }
            }
        }

        public void Unregister(object client, int id)
        {
            for (int i = 0; i < _TypedHandlers[id].Count; i++)
            {
                PacketHandler handler = _TypedHandlers[id][i] as PacketHandler;
                if (handler.Client == client)
                {
                    _TypedHandlers[id].RemoveAt(i);
                    break;
                }
            }
        }

        public void RegisterExtended<T>(object client, int extendedId, int subId, int length, Action<T> onReceive) where T : IRecvPacket
        {
            Type type = typeof(T);
            ConstructorInfo[] ctors = type.GetConstructors();

            bool valid = false;

            for (int i = 0; i < ctors.Length && !valid; i++)
            {
                ParameterInfo[] parameters = ctors[i].GetParameters();
                valid = (parameters.Length == 1 && parameters[0].ParameterType == typeof(PacketReader));
            }

            if (!valid)
            {
                throw new NetworkException(string.Format("Unable to register packet type {0} without a public constructor with a {1} parameter", type, typeof(PacketReader)));
            }

            if (extendedId > byte.MaxValue)
            {
                throw new NetworkException(string.Format("Unable to register packet extendedId {0:X2} because it is greater than byte.MaxValue", extendedId));
            }

            if (subId > byte.MaxValue)
            {
                throw new NetworkException(string.Format("Unable to register packet subId {0:X2} because it is greater than byte.MaxValue", subId));
            }

            if (_ExtendedTypedHandlers[extendedId] == null)
            {
                _ExtendedTypedHandlers[extendedId] = new List<PacketHandler>[0x100];

                for (int i = 0; i < _ExtendedTypedHandlers[extendedId].Length; i++)
                {
                    _ExtendedTypedHandlers[extendedId][i] = new List<PacketHandler>();
                }
            }

            Tracer.Debug($"Registering Extended Command: id: 0x{extendedId:X2} subCommand: 0x{subId:X2} Length: {length}");

            PacketHandler handler = new PacketHandler<T>(subId, length, type, client, onReceive);
            _ExtendedTypedHandlers[extendedId][subId].Add(handler);
        }


    }
}
