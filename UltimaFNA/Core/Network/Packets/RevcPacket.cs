﻿/***************************************************************************
 *   RecvPacket.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
#endregion

namespace UltimaXNA.Core.Network.Packets
{
    public abstract class RecvPacket : IRecvPacket
    {
        readonly int _Id;
        readonly string _Name;

        public int Id
        {
            get { return _Id; }
        }

        public string Name
        {
            get { return _Name; }
        }

        public RecvPacket(int id, string name)
        {
            _Id = id;
            _Name = name;
        }
    }
}
