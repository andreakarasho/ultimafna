﻿/***************************************************************************
 *   SendPacket.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System.IO;
using UltimaFNA.Ultima.Network;
using UltimaXNA.Core.Diagnostics.Tracing;
#endregion

namespace UltimaXNA.Core.Network.Packets
{
    /// <summary>
    /// A formatted unit of data used in point to point communications.  
    /// </summary>
    public abstract class SendPacket : ISendPacket
    {
        private const int BufferSize = 4096;

        /// <summary>
        /// Used to create the a buffered datablock to be sent
        /// </summary>
        protected PacketWriter Stream;
        private int _Id;
        int _Length;
        readonly string _Name;

        /// <summary>
        /// Gets the name of the packet
        /// </summary>
        public string Name
        {
            get { return _Name; }
        }

        /// <summary>
        /// Gets the size in bytes of the packet
        /// </summary>
        public int Length
        {
            get { return _Length; }
        }

        /// <summary>
        /// Gets the Id, or Command that identifies the packet.
        /// </summary>
        public int Id
        {
            get { return _Id; }
        }

        /// <summary>
        /// Creates an instance of a packet
        /// </summary>
        /// <param name="id">the Id, or Command that identifies the packet</param>
        /// <param name="name">The name of the packet</param>
        public SendPacket(int id, string name)
        {
            _Id = id;
            _Name = name;
            Stream = PacketWriter.CreateInstance(_Length);
            Stream.Write((byte)id);
            Stream.Write((short)0);
        }

        /// <summary>
        /// Creates an instance of a packet
        /// </summary>
        /// <param name="id">the Id, or Command that identifies the packet</param>
        /// <param name="name">The name of the packet</param>
        /// <param name="length">The size in bytes of the packet</param>
        public SendPacket(int id, string name, int length)
        {
            _Id = id;
            _Name = name;

            if (length > 0)
            {
                int probalLen = PacketsTable.GetPacketLength(id);
                if (probalLen != length && probalLen > 0)
                {
                    Tracer.Warn($"Adjusted packet size: {id.ToString("X")} - Old: {length} - New: {probalLen}");
                    length = probalLen;
                }
            }
            _Length = length;

            Stream = PacketWriter.CreateInstance(length);
            Stream.Write((byte)id);
        }

        /// <summary>
        /// Resets the Packet Writer and ensures the packet's 2nd and 3rd bytes are used to store the length
        /// </summary>
        /// <param name="length"></param>
        public void EnsureCapacity(int length)
        {
            Stream = PacketWriter.CreateInstance(length);
            Stream.Write((byte)_Id);
            Stream.Write((short)length);
        }

        public void Resize(byte id, int length)
        {
            _Id = id;
            _Length = length;
            EnsureCapacity(length);
        }

        /// <summary>
        /// Compiles the packet into a System.Byte[] and Disposes the underlying Stream
        /// </summary>
        /// <returns></returns>
        public byte[] Compile()
        {
            Stream.Flush();

            if (Length == 0)
            {
                _Length = (int)Stream.Length;
                Stream.Seek((long)1, SeekOrigin.Begin);
                Stream.Write((ushort)_Length);
            }

            return Stream.Compile();
        }

        public override string ToString()
        {
            return string.Format("Id: {0:X2} Name: {1} Length: {2}", _Id, _Name, _Length);
        }
    }
}
