﻿/***************************************************************************
 *   SendRecvPacket.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System.IO;
#endregion

namespace UltimaXNA.Core.Network.Packets
{
    public abstract class SendRecvPacket: ISendPacket, IRecvPacket
    {
        readonly int _Id;
        readonly int _Length;
        readonly string _Name;

        public int Id
        {
            get { return _Id; }
        }

        public string Name
        {
            get { return _Name; }
        }

        public int Length
        {
            get { return _Length; }
        }

        private const int BufferSize = 4096;

        protected PacketWriter Stream;

        public SendRecvPacket(int id, string name)
        {
            _Id = id;
            _Name = name;
            Stream = PacketWriter.CreateInstance(_Length);
            Stream.Write(id);
            Stream.Write((short)0);
        }

        public SendRecvPacket(int id, string name, int length)
        {
            _Id = id;
            _Name = name;
            _Length = length;

            Stream = PacketWriter.CreateInstance(length);
            Stream.Write((byte)id);
        }

        public void EnsureCapacity(int length)
        {
            Stream = PacketWriter.CreateInstance(length);
            Stream.Write((byte)_Id);
            Stream.Write((short)length);
        }

        public byte[] Compile()
        {
            Stream.Flush();

            if (Length == 0)
            {
                long length = Stream.Length;
                Stream.Seek((long)1, SeekOrigin.Begin);
                Stream.Write((ushort)length);
                Stream.Flush();
            }

            return Stream.Compile();
        }

        public override string ToString()
        {
            return string.Format("Id: {0:X2} Name: {1} Length: {2}", _Id, _Name, _Length);
        }
    }
}
