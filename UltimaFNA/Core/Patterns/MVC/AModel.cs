﻿using System;

namespace UltimaXNA.Core.Patterns.MVC
{
    /// <summary>
    /// Abstract Model. Maintains the state, core data, and update logic of a model.
    /// </summary>
    public abstract class AModel
    {
        bool _IsInitialized;
        AView _View;
        AController _Controller;

        public AView GetView()
        {
            if (_View == null)
            {
                _View = CreateView();
            }
            return _View;
        }
        
        public AController GetController()
        {
            if (_Controller == null)
            {
                _Controller = CreateController();
            }
            return _Controller;
        }

        public void Initialize()
        {
            if (_IsInitialized)
            {
                return;
            }
            _IsInitialized = true;
            OnInitialize();
        }

        public void Dispose()
        {
            OnDispose();
        }

        public abstract void Update(double totalTime, double frameTime);

        protected abstract AView CreateView();
        protected abstract void OnInitialize();
        protected abstract void OnDispose();

        protected virtual AController CreateController()
        {
            throw new NotImplementedException();
        }
    }
}
