﻿/***************************************************************************
 *   ModelManager.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

namespace UltimaXNA.Core.Patterns.MVC
{
    class ModelManager
    {
        AModel _Model;
        AModel _QueuedModel;

        public AModel Next
        {
            get { return _QueuedModel; }
            set
            {
                if (_QueuedModel != null)
                {
                    _QueuedModel.Dispose();
                    _QueuedModel = null;
                }
                _QueuedModel = value;
                if (_QueuedModel != null)
                {
                    _QueuedModel.Initialize();
                }
            }
        }

        public AModel Current
        {
            get { return _Model; }
            set
            {
                if (_Model != null)
                {
                    _Model.Dispose();
                    _Model = null;
                }
                _Model = value;
                if (_Model != null)
                {
                    _Model.Initialize();
                }
            }
        }

        public void ActivateNext()
        {
            if (_QueuedModel != null)
            {
                Current = Next;
                _QueuedModel = null;
            }
        }
    }
}
