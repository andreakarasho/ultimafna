﻿/***************************************************************************
 *   MessageHook.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace UltimaXNA.Core.Windows
{
    public abstract class MessageHook : IDisposable
    {
        public abstract int HookType { get; }

        IntPtr _hWnd;
        WndProcHandler _Hook;
        IntPtr _prevWndProc;
        IntPtr _hIMC;

        public IntPtr HWnd
        {
            get { return _hWnd; }
        }

        protected MessageHook(IntPtr hWnd)
        {
            _hWnd = hWnd;
            _Hook = WndProcHook;
            _prevWndProc = (IntPtr)NativeMethods.SetWindowLong(
                hWnd,
                NativeConstants.GWL_WNDPROC, (int)Marshal.GetFunctionPointerForDelegate(_Hook));
            _hIMC = NativeMethods.ImmGetContext(_hWnd);
            Application.AddMessageFilter(new InputMessageFilter(_Hook));
        }

        ~MessageHook()
        {
            Dispose(false);
        }

        protected virtual IntPtr WndProcHook(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam)
        {
            switch (msg)
            {
                case NativeConstants.W_GETDLGCODE:
                    return (IntPtr)(NativeConstants.DLGC_WANTALLKEYS);
               case NativeConstants.W_IME_SETCONTEXT:
                    if ((int)wParam == 1)
                        NativeMethods.ImmAssociateContext(hWnd, _hIMC);
                    break;
                case NativeConstants.W_INPUTLANGCHANGE:
                    int rrr = (int)NativeMethods.CallWindowProc(_prevWndProc, hWnd, msg, wParam, lParam);
                    NativeMethods.ImmAssociateContext(hWnd, _hIMC);
                    
                    return (IntPtr)1;
            }

            return NativeMethods.CallWindowProc(_prevWndProc, hWnd, msg, wParam, lParam);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {

        }
    }

    // This is the class that brings back the alt messages
    // http://www.gamedev.net/community/forums/topic.asp?topic_id=554322
    class InputMessageFilter : IMessageFilter
    {
        readonly WndProcHandler _Hook;

        public InputMessageFilter(WndProcHandler hook)
        {
            _Hook = hook;
        }
        [DllImport("user32.dll", EntryPoint = "TranslateMessage")]
        protected extern static bool _TranslateMessage(ref System.Windows.Forms.Message m);

        bool IMessageFilter.PreFilterMessage(ref System.Windows.Forms.Message m)
        {
            switch (m.Msg)
            {
                case NativeConstants.W_SYSKEYDOWN:
                case NativeConstants.W_SYSKEYUP:
                    {
                        bool b = _TranslateMessage(ref m);
                        _Hook(m.HWnd, (uint)m.Msg, m.WParam, m.LParam);
                        return true;
                    }

                case NativeConstants.W_SYSCHAR:
                    {
                        _Hook(m.HWnd, (uint)m.Msg, m.WParam, m.LParam);
                        return true;
                    }
                case NativeConstants.W_KEYDOWN:
                case NativeConstants.W_KEYUP:
                    {
                        bool b = _TranslateMessage(ref m);
                        _Hook(m.HWnd, (uint)m.Msg, m.WParam, m.LParam);
                        return true;
                    }
                case NativeConstants.W_CHAR:
                    {
                        _Hook(m.HWnd, (uint)m.Msg, m.WParam, m.LParam);
                        return true;
                    }
                case NativeConstants.W_DEADCHAR:
                    {
                        _Hook(m.HWnd, (uint)m.Msg, m.WParam, m.LParam);
                        return true;
                    }
            }
            return false;
        }
    }
}
