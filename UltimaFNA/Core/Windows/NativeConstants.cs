﻿/***************************************************************************
 *   NativeConstants.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

namespace UltimaXNA.Core.Windows
{
    static class NativeConstants
    {
        public const int W_NULL = 0x00;
        public const int W_CREATE = 0x01;
        public const int W_DESTROY = 0x02;
        public const int W_MOVE = 0x03;
        public const int W_SIZE = 0x05;
        public const int W_ACTIVATE = 0x06;
        public const int W_SETFOCUS = 0x07;
        public const int W_KILLFOCUS = 0x08;
        public const int W_ENABLE = 0x0A;
        public const int W_SETREDRAW = 0x0B;
        public const int W_SETTEXT = 0x0C;
        public const int W_GETTEXT = 0x0D;
        public const int W_GETTEXTLENGTH = 0x0E;
        public const int W_PAINT = 0x0F;
        public const int W_CLOSE = 0x10;
        public const int W_QUERYENDSESSION = 0x11;
        public const int W_QUIT = 0x12;
        public const int W_QUERYOPEN = 0x13;
        public const int W_ERASEBKGND = 0x14;
        public const int W_SYSCOLORCHANGE = 0x15;
        public const int W_ENDSESSION = 0x16;
        public const int W_SYSTEMERROR = 0x17;
        public const int W_SHOWWINDOW = 0x18;
        public const int W_CTLCOLOR = 0x19;
        public const int W_WININICHANGE = 0x1A;
        public const int W_SETTINGCHANGE = 0x1A;
        public const int W_DEVMODECHANGE = 0x1B;
        public const int W_ACTIVATEAPP = 0x1C;
        public const int W_FONTCHANGE = 0x1D;
        public const int W_TIMECHANGE = 0x1E;
        public const int W_CANCELMODE = 0x1F;
        public const int W_SETCURSOR = 0x20;
        public const int W_MOUSEACTIVATE = 0x21;
        public const int W_CHILDACTIVATE = 0x22;
        public const int W_QUEUESYNC = 0x23;
        public const int W_GETMINMAXINFO = 0x24;
        public const int W_PAINTICON = 0x26;
        public const int W_ICONERASEBKGND = 0x27;
        public const int W_NEXTDLGCTL = 0x28;
        public const int W_SPOOLERSTATUS = 0x2A;
        public const int W_DRAWITEM = 0x2B;
        public const int W_MEASUREITEM = 0x2C;
        public const int W_DELETEITEM = 0x2D;
        public const int W_VKEYTOITEM = 0x2E;
        public const int W_CHARTOITEM = 0x2F;

        public const int W_SETFONT = 0x30;
        public const int W_GETFONT = 0x31;
        public const int W_SETHOTKEY = 0x32;
        public const int W_GETHOTKEY = 0x33;
        public const int W_QUERYDRAGICON = 0x37;
        public const int W_COMPAREITEM = 0x39;
        public const int W_COMPACTING = 0x41;
        public const int W_WINDOWPOSCHANGING = 0x46;
        public const int W_WINDOWPOSCHANGED = 0x47;
        public const int W_POWER = 0x48;
        public const int W_COPYDATA = 0x4A;
        public const int W_CANCELJOURNAL = 0x4B;
        public const int W_NOTIFY = 0x4E;
        public const int W_INPUTLANGCHANGEREQUEST = 0x50;
        public const int W_INPUTLANGCHANGE = 0x51;
        public const int W_TCARD = 0x52;
        public const int W_HELP = 0x53;
        public const int W_USERCHANGED = 0x54;
        public const int W_NOTIFYFORMAT = 0x55;
        public const int W_CONTEXTMENU = 0x7B;
        public const int W_STYLECHANGING = 0x7C;
        public const int W_STYLECHANGED = 0x7D;
        public const int W_DISPLAYCHANGE = 0x7E;
        public const int W_GETICON = 0x7F;
        public const int W_SETICON = 0x80;

        public const int W_NCCREATE = 0x81;
        public const int W_NCDESTROY = 0x82;
        public const int W_NCCALCSIZE = 0x83;
        public const int W_NCHITTEST = 0x84;
        public const int W_NCPAINT = 0x85;
        public const int W_NCACTIVATE = 0x86;
        public const int W_GETDLGCODE = 0x87;
        public const int W_NCMOUSEMOVE = 0xA0;
        public const int W_NCLBUTTONDOWN = 0xA1;
        public const int W_NCLBUTTONUP = 0xA2;
        public const int W_NCLBUTTONDBLCLK = 0xA3;
        public const int W_NCRBUTTONDOWN = 0xA4;
        public const int W_NCRBUTTONUP = 0xA5;
        public const int W_NCRBUTTONDBLCLK = 0xA6;
        public const int W_NCMBUTTONDOWN = 0xA7;
        public const int W_NCMBUTTONUP = 0xA8;
        public const int W_NCMBUTTONDBLCLK = 0xA9;

        public const int W_KEYFIRST = 0x100;
        public const int W_KEYDOWN = 0x100;
        public const int W_KEYUP = 0x101;
        public const int W_CHAR = 0x102;
        public const int W_DEADCHAR = 0x103;
        public const int W_SYSKEYDOWN = 0x104;
        public const int W_SYSKEYUP = 0x105;
        public const int W_SYSCHAR = 0x106;
        public const int W_SYSDEADCHAR = 0x107;
        public const int W_KEYLAST = 0x108;
        public const int W_UNICHAR = 0x109;

        public const int W_IME_STARTCOMPOSITION = 0x10D;
        public const int W_IME_ENDCOMPOSITION = 0x10E;
        public const int W_IME_COMPOSITION = 0x10F;
        public const int W_IME_KEYLAST = 0x10F;

        public const int W_INITDIALOG = 0x110;
        public const int W_COMMAND = 0x111;
        public const int W_SYSCOMMAND = 0x112;
        public const int W_TIMER = 0x113;
        public const int W_HSCROLL = 0x114;
        public const int W_VSCROLL = 0x115;
        public const int W_INITMENU = 0x116;
        public const int W_INITMENUPOPUP = 0x117;
        public const int W_MENUSELECT = 0x11F;
        public const int W_MENUCHAR = 0x120;
        public const int W_ENTERIDLE = 0x121;

        public const int W_CTLCOLORMSGBOX = 0x132;
        public const int W_CTLCOLOREDIT = 0x133;
        public const int W_CTLCOLORLISTBOX = 0x134;
        public const int W_CTLCOLORBTN = 0x135;
        public const int W_CTLCOLORDLG = 0x136;
        public const int W_CTLCOLORSCROLLBAR = 0x137;
        public const int W_CTLCOLORSTATIC = 0x138;

        public const int W_MOUSEFIRST = 0x200;
        public const int W_MOUSEMOVE = 0x200;
        public const int W_LBUTTONDOWN = 0x201;
        public const int W_LBUTTONUP = 0x202;
        public const int W_LBUTTONDBLCLK = 0x203;
        public const int W_RBUTTONDOWN = 0x204;
        public const int W_RBUTTONUP = 0x205;
        public const int W_RBUTTONDBLCLK = 0x206;
        public const int W_MBUTTONDOWN = 0x207;
        public const int W_MBUTTONUP = 0x208;
        public const int W_MBUTTONDBLCLK = 0x209;
        public const int W_MOUSEWHEEL = 0x20A;
        public const int W_XBUTTONDOWN = 0x20B;
        public const int W_XBUTTONUP = 0x20C;
        public const int W_XBUTTONDBLCLK = 0x20D;
        public const int W_MOUSEHWHEEL = 0x20E;

        public const int W_PARENTNOTIFY = 0x210;
        public const int W_ENTERMENULOOP = 0x211;
        public const int W_EXITMENULOOP = 0x212;
        public const int W_NEXTMENU = 0x213;
        public const int W_SIZING = 0x214;
        public const int W_CAPTURECHANGED = 0x215;
        public const int W_MOVING = 0x216;
        public const int W_POWERBROADCAST = 0x218;
        public const int W_DEVICECHANGE = 0x219;

        public const int W_MDICREATE = 0x220;
        public const int W_MDIDESTROY = 0x221;
        public const int W_MDIACTIVATE = 0x222;
        public const int W_MDIRESTORE = 0x223;
        public const int W_MDINEXT = 0x224;
        public const int W_MDIMAXIMIZE = 0x225;
        public const int W_MDITILE = 0x226;
        public const int W_MDICASCADE = 0x227;
        public const int W_MDIICONARRANGE = 0x228;
        public const int W_MDIGETACTIVE = 0x229;
        public const int W_MDISETMENU = 0x230;
        public const int W_ENTERSIZEMOVE = 0x231;
        public const int W_EXITSIZEMOVE = 0x232;
        public const int W_DROPFILES = 0x233;
        public const int W_MDIREFRESHMENU = 0x234;

        public const int W_IME_SETCONTEXT = 0x281;
        public const int W_IME_NOTIFY = 0x282;
        public const int W_IME_CONTROL = 0x283;
        public const int W_IME_COMPOSITIONFULL = 0x284;
        public const int W_IME_SELECT = 0x285;
        public const int W_IME_CHAR = 0x286;
        public const int W_IME_KEYDOWN = 0x290;
        public const int W_IME_KEYUP = 0x291;

        public const int W_MOUSEHOVER = 0x2A1;
        public const int W_NCMOUSELEAVE = 0x2A2;
        public const int W_MOUSELEAVE = 0x2A3;

        public const int W_CUT = 0x300;
        public const int W_COPY = 0x301;
        public const int W_PASTE = 0x302;
        public const int W_CLEAR = 0x303;
        public const int W_UNDO = 0x304;

        public const int W_RENDERFORMAT = 0x305;
        public const int W_RENDERALLFORMATS = 0x306;
        public const int W_DESTROYCLIPBOARD = 0x307;
        public const int W_DRAWCLIPBOARD = 0x308;
        public const int W_PAINTCLIPBOARD = 0x309;
        public const int W_VSCROLLCLIPBOARD = 0x30A;
        public const int W_SIZECLIPBOARD = 0x30B;
        public const int W_ASKCBFORMATNAME = 0x30C;
        public const int W_CHANGECBCHAIN = 0x30D;
        public const int W_HSCROLLCLIPBOARD = 0x30E;
        public const int W_QUERYNEWPALETTE = 0x30F;
        public const int W_PALETTEISCHANGING = 0x310;
        public const int W_PALETTECHANGED = 0x311;

        public const int W_HOTKEY = 0x312;
        public const int W_PRINT = 0x317;
        public const int W_PRINTCLIENT = 0x318;

        public const int W_HANDHELDFIRST = 0x358;
        public const int W_HANDHELDLAST = 0x35F;
        public const int W_PENWINFIRST = 0x380;
        public const int W_PENWINLAST = 0x38F;
        public const int W_COALESCE_FIRST = 0x390;
        public const int W_COALESCE_LAST = 0x39F;
        public const int W_DDE_FIRST = 0x3E0;
        public const int W_DDE_INITIATE = 0x3E0;
        public const int W_DDE_TERMINATE = 0x3E1;
        public const int W_DDE_ADVISE = 0x3E2;
        public const int W_DDE_UNADVISE = 0x3E3;
        public const int W_DDE_ACK = 0x3E4;
        public const int W_DDE_DATA = 0x3E5;
        public const int W_DDE_REQUEST = 0x3E6;
        public const int W_DDE_POKE = 0x3E7;
        public const int W_DDE_EXECUTE = 0x3E8;
        public const int W_DDE_LAST = 0x3E8;

        public const int W_USER = 0x400;
        public const int W_APP = 0x8000;

        public const int WH_JOURNALRECORD = 0;
        public const int WH_JOURNALPLAYBACK = 1;
        public const int WH_KEYBOARD = 2;
        public const int WH_GETMESSAGE = 3;
        public const int WH_CALLWNDPROC = 4;
        public const int WH_CBT = 5;
        public const int WH_SYSMSGFILTER = 6;
        public const int WH_MOUSE = 7;
        public const int WH_HARDWARE = 8;
        public const int WH_DEBUG = 9;
        public const int WH_SHELL = 10;
        public const int WH_FOREGROUNDIDLE = 11;
        public const int WH_CALLWNDPROCRET = 12;
        public const int WH_KEYBOARD_LL = 13;
        public const int WH_MOUSE_LL = 14;

        public const int GWL_WNDPROC = -4;
        public const int DLGC_WANTALLKEYS = 0x0004;
        public const int DLGC_WANTCHARS = 0x0080;
        public const int DLGC_WANTTAB = 0x0002;
        public const int DLGC_HASSETSEL = 0x0008;

        public const int LOCALE_IDEFAULTANSICODEPAGE = 0x1004;
        public const int LOCALE_RETURN_NUMBER = 0x20000000;
        public const int SORT_DEFAULT = 0x0;
    }
}
