﻿/***************************************************************************
 *   UserInterfaceService.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Input;
#endregion

namespace UltimaXNA.Core.UI
{
    public class UserInterfaceService
    {
        /// <summary>
        /// An array of all open root controls in the user interface.
        /// </summary>
        public readonly List<AControl> OpenControls;
        public int Width => _SpriteBatch.GraphicsDevice.Viewport.Width;
        public int Height => _SpriteBatch.GraphicsDevice.Viewport.Height;

        InputManager _Input;
        SpriteBatchUI _SpriteBatch;
        AControl[] _MouseDownControl = new AControl[5];
        private Game _game;

        // ============================================================================================================
        // Ctor, Dispose, Update, and Draw
        // ============================================================================================================

        public UserInterfaceService()
        {
            _Input = Service.Get<InputManager>();
            _SpriteBatch = Service.Get<SpriteBatchUI>();
            OpenControls = new List<AControl>();

            _game = Service.Get<UltimaGame>();
        }

        public void Dispose()
        {
            Reset();
        }

        /// <summary>
        /// Disposes of all controls.
        /// </summary>
        public void Reset()
        {
            foreach (AControl c in OpenControls)
            {
                c.Dispose();
            }
            OpenControls.Clear();
        }
        
        internal ICursor Cursor
        {
            get;
            set;
        }

        /// <summary>
        /// Returns the control directly under the Cursor.
        /// </summary>
        public AControl MouseOverControl
        {
            get;
            private set;
        }

        /// <summary>
        /// Returns True if the Cursor is over the UserInterface.
        /// </summary>
        public bool IsMouseOverUI => MouseOverControl != null;

        AControl _KeyboardFocusControl;
        public AControl KeyboardFocusControl
        {
            get
            {
                if (IsModalControlOpen)
                    return null;
                if (_KeyboardFocusControl == null)
                {
                    foreach (AControl c in OpenControls)
                    {
                        if (!c.IsDisposed && c.IsVisible && 
                            c.IsEnabled && c.HandlesKeyboardFocus)
                        {
                            _KeyboardFocusControl = c.FindControlThatAcceptsKeyboardFocus();
                            if (_KeyboardFocusControl != null)
                                break;
                        }
                    }
                }
                return _KeyboardFocusControl;
            }
            set
            {
                _KeyboardFocusControl = value;
            }
        }

        public bool IsModalControlOpen
        {
            get
            {
                foreach (AControl c in OpenControls)
                {
                    if (c.MetaData.IsModal)
                    {
                        return true;
                    }
                }
                return false;
            }
        }
        
        /// <summary>
        /// Adds or toggles the passed control to the list of active controls.
        /// If control succesfully added to active control list, returns control. If add unsuccessful, returns null.
        /// </summary>
        public AControl AddControl(AControl control, int x, int y)
        {
            if (control.IsDisposed)
            {
                return null;
            }
            control.Position = new Point(x, y);
            OpenControls.Insert(0, control);
            return control;
        }

        public void RemoveControl<T>(int? localID = null) where T : AControl
        {
            foreach (AControl c in OpenControls)
            {
                if (typeof(T).IsAssignableFrom(c.GetType()))
                {
                    if (!localID.HasValue || (c.GumpLocalID == localID))
                    {
                        if (!c.IsDisposed)
                        {
                            c.Dispose();
                        }
                    }
                }
            }
        }

        public AControl GetControl(int localID)
        {
            foreach (AControl c in OpenControls)
            {
                if (!c.IsDisposed && c.GumpLocalID == localID)
                    return c;
            }
            return null;
        }

        public AControl GetControlByTypeID(int typeID)
        {
            foreach (AControl c in OpenControls)
            {
                if (!c.IsDisposed && c.GumpServerTypeID == typeID)
                    return c;
            }
            return null;
        }

        public T GetControl<T>(int? localID = null) where T : AControl
        {
            foreach (AControl c in OpenControls)
            {
                if (!c.IsDisposed && c.GetType() == typeof(T) && 
                    (!localID.HasValue || c.GumpLocalID == localID))
                    return (T)c;
            }
            return null;
        }

        public void Update(double totalMS, double frameMS)
        {
            OrderControlsBasedOnUILayerMetaData();

            for (int i = 0; i < OpenControls.Count; i++)
            {
                AControl c = OpenControls[i];
                if (!c.IsInitialized && !c.IsDisposed)
                    c.Initialize();
                c.Update(totalMS, frameMS);
            }

            for (int i = 0; i < OpenControls.Count; i++)
            {
                if (OpenControls[i].IsDisposed)
                {
                    OpenControls.RemoveAt(i);
                    i--;
                }     
            }

            if (Cursor != null)
                Cursor.Update();

            if (_game.IsActive)
            {
                InternalHandleKeyboardInput();
                InternalHandleMouseInput();
            }
        }

        public void Draw(double frameMS)
        {
            OrderControlsBasedOnUILayerMetaData();
            _SpriteBatch.GraphicsDevice.Clear(Color.Transparent);
            _SpriteBatch.Reset();
            foreach (AControl c in OpenControls.Reverse<AControl>())
            {
                if (c.IsInitialized)
                {
                    c.Draw(_SpriteBatch, c.Position, frameMS);
                }
            }
            if (Cursor != null)
            {
                Cursor.Draw(_SpriteBatch, _Input.MousePosition);
            }
            _SpriteBatch.FlushSprites(false);
        }

        void InternalHandleKeyboardInput()
        {
            if (KeyboardFocusControl != null)
            {
                if (_KeyboardFocusControl.IsDisposed)
                {
                    _KeyboardFocusControl = null;
                }
                else
                {
                    List<InputEventKeyboard> k_events = _Input.GetKeyboardEvents();
                    foreach (InputEventKeyboard e in k_events)
                        _KeyboardFocusControl.KeyboardInput(e);
                }
            }
        }

        void OrderControlsBasedOnUILayerMetaData()
        {
            List<AControl> gumps = new List<AControl>();

            foreach (AControl c in OpenControls)
            {
                if (c.MetaData.Layer != UILayer.Default)
                    gumps.Add(c);
            }

            foreach (AControl c in gumps)
            {
                if (c.MetaData.Layer == UILayer.Under)
                {
                    for (int i = 0; i < OpenControls.Count; i++)
                    {
                        if (OpenControls[i] == c)
                        {
                            OpenControls.RemoveAt(i);
                            OpenControls.Insert(OpenControls.Count, c);
                        }
                    }
                }
                else if (c.MetaData.Layer == UILayer.Over)
                {
                    for (int i = 0; i < OpenControls.Count; i++)
                    {
                        if (OpenControls[i] == c)
                        {
                            OpenControls.RemoveAt(i);
                            OpenControls.Insert(0, c);
                        }
                    }
                }
            }
        }

        void InternalHandleMouseInput()
        {
            // clip the mouse position
            Point clippedPosition = _Input.MousePosition;
            ClipMouse(ref clippedPosition);

            // Get the topmost control that is under the mouse and handles mouse input.
            // If this control is different from the previously focused control,
            // send that previous control a MouseOut event.
            AControl focusedControl = InternalGetMouseOverControl(clippedPosition);
            if ((MouseOverControl != null) && (focusedControl != MouseOverControl))
            {
                MouseOverControl.MouseOut(clippedPosition);
                // Also let the parent control know we've been moused out (for gumps).
                if (MouseOverControl.RootParent != null)
                {
                    if (focusedControl == null || MouseOverControl.RootParent != focusedControl.RootParent)
                        MouseOverControl.RootParent.MouseOut(clippedPosition);
                }
            }

            if (focusedControl != null)
            {
                focusedControl.MouseOver(clippedPosition);
                if (_MouseDownControl[0] == focusedControl)
                {
                    AttemptDragControl(focusedControl, clippedPosition);
                }
                if (_IsDraggingControl)
                {
                    DoDragControl(clippedPosition);
                }
            }

            // Set the new MouseOverControl.
            MouseOverControl = focusedControl;

            // Send a MouseOver event to any control that was previously the target of a MouseDown event.
            for (int iButton = 0; iButton < 5; iButton++)
            {
                
                if ((_MouseDownControl[iButton] != null) && (_MouseDownControl[iButton] != focusedControl))
                {
                    _MouseDownControl[iButton].MouseOver(clippedPosition);
                }
            }

            // The cursor and world input objects occasionally must block input events from reaching the UI:
            // e.g. when the cursor is carrying an object.
            if ((IsModalControlOpen == false) && (ObjectsBlockingInput == true))
                return;

            List<InputEventMouse> events = _Input.GetMouseEvents();
            foreach (InputEventMouse e in events)
            {
                if (e.EventType == MouseEvent.WheelScroll)
                {
                    if (focusedControl != null)
                        focusedControl.MouseWheel(clippedPosition, e.WheelValue);
                }

                if (e.EventType == MouseEvent.Move)
                {
                    if (focusedControl != null)
                    {                    
                        focusedControl.MouseMove(e.Position);
                    }
                }
                // MouseDown event: the currently focused control gets a MouseDown event, and if
                // it handles Keyboard input, gets Keyboard focus as well.
                if (e.EventType == MouseEvent.Down)
                {
                    if (focusedControl != null)
                    {
                        MakeTopMostGump(focusedControl);
                        focusedControl.MouseDown(clippedPosition, e.Button);
                        if (focusedControl.HandlesKeyboardFocus)
                            _KeyboardFocusControl = focusedControl;
                        _MouseDownControl[(int)e.Button] = focusedControl;
                    }
                    else
                    {
                        // close modal controls if they can be closed with a mouse down outside their drawn area
                        if (IsModalControlOpen)
                        {
                            foreach (AControl c in OpenControls)
                                if (c.MetaData.IsModal && c.MetaData.ModalClickOutsideAreaClosesThisControl)
                                    c.Dispose();
                        }
                    }
                }

                // MouseUp and MouseClick events
                if (e.EventType == MouseEvent.Up)
                {
                    int btn = (int)e.Button;

                    // If there is a currently focused control:
                    // 1.   If the currently focused control is the same control that was MouseDowned on with this button,
                    //      then send that control a MouseClick event.
                    // 2.   Send the currently focused control a MouseUp event.
                    // 3.   If the currently focused control is NOT the same control that was MouseDowned on with this button,
                    //      send that MouseDowned control a MouseUp event (but it does not receive MouseClick).
                    // If there is NOT a currently focused control, then simply inform the control that was MouseDowned on
                    // with this button that the button has been released, by sending it a MouseUp event.

                    EndDragControl(e.Position);

                    if (focusedControl != null)
                    {
                        if (_MouseDownControl[btn] != null && focusedControl == _MouseDownControl[btn])
                        {
                            focusedControl.MouseClick(clippedPosition, e.Button);
                        }
                        focusedControl.MouseUp(clippedPosition, e.Button);
                        if (_MouseDownControl[btn] != null && focusedControl != _MouseDownControl[btn])
                        {
                            _MouseDownControl[btn].MouseUp(clippedPosition, e.Button);
                        }
                    }
                    else
                    {
                        if (_MouseDownControl[btn] != null)
                        {
                            _MouseDownControl[btn].MouseUp(clippedPosition, e.Button);
                        }
                    }

                    _MouseDownControl[btn] = null;
                }
            }
        }

        void MakeTopMostGump(AControl control)
        {
            AControl c = control;
            while (c.Parent != null)
                c = c.Parent;

            for (int i = 0; i < OpenControls.Count; i++)
            {
                if (OpenControls[i] == c)
                {
                    AControl cm = OpenControls[i];
                    OpenControls.RemoveAt(i);
                    OpenControls.Insert(0, cm);
                }
            }
        }

        AControl InternalGetMouseOverControl(Point atPosition)
        {
            if (_IsDraggingControl)
            {
                return _DraggingControl;
            }

            List<AControl> possibleControls;
            if (IsModalControlOpen)
            {
                possibleControls = new List<AControl>();
                foreach (AControl c in OpenControls)
                    if (c.MetaData.IsModal)
                        possibleControls.Add(c);
            }
            else
            {
                possibleControls = OpenControls;
            }

            AControl[] mouseOverControls = null;
            // Get the list of controls under the mouse cursor
            foreach (AControl c in possibleControls)
            {
                AControl[] controls = c.HitTest(atPosition, false);
                if (controls != null)
                {
                    mouseOverControls = controls;
                    break;
                }
            }

            if (mouseOverControls == null)
                return null;

            // Get the topmost control that is under the mouse and handles mouse input.
            // If this control is different from the previously focused control,
            // send that previous control a MouseOut event.
            if (mouseOverControls != null)
            {
                for (int i = 0; i < mouseOverControls.Length; i++)
                {
                    if (mouseOverControls[i].HandlesMouseInput)
                    {
                        return mouseOverControls[i];
                    }
                }
            }

            return null;
        }

        // ============================================================================================================
        // Input blocking objects
        // ============================================================================================================

        List<object> _InputBlockingObjects = new List<object>();

        /// <summary>
        /// Returns true if there are any active objects blocking input.
        /// </summary>
        bool ObjectsBlockingInput => _InputBlockingObjects.Count > 0;

        /// <summary>
        /// Add an input blocking object. Until RemoveInputBlocker is called with this same parameter,
        /// GUIState will not process any MouseDown, MouseUp, or MouseClick events, or any keyboard events.
        /// </summary>
        public void AddInputBlocker(object obj)
        {
            if (!_InputBlockingObjects.Contains(obj))
                _InputBlockingObjects.Add(obj);
        }

        /// <summary>
        /// Removes an input blocking object. Only when there are no input blocking objects will GUIState
        /// process MouseDown, MouseUp, MouseClick, and all keyboard events.
        /// </summary>
        public void RemoveInputBlocker(object obj)
        {
            if (_InputBlockingObjects.Contains(obj))
                _InputBlockingObjects.Remove(obj);
        }

        // ============================================================================================================
        // Control dragging
        // ============================================================================================================

        AControl _DraggingControl;
        bool _IsDraggingControl;
        int _DragOriginX;
        int _DragOriginY;

        public void AttemptDragControl(AControl control, Point mousePosition, bool attemptAlwaysSuccessful = false)
        {
            if (_IsDraggingControl)
            {
                return;
            }
            AControl dragTarget = control;
            if (!dragTarget.IsMoveable)
            {
                return;
            }
            while (dragTarget.Parent != null)
            {
                dragTarget = dragTarget.Parent;
            }
            if (dragTarget.IsMoveable)
            {
                if (attemptAlwaysSuccessful)
                {
                    _DraggingControl = dragTarget;
                    _DragOriginX = mousePosition.X;
                    _DragOriginY = mousePosition.Y;
                }
                if (_DraggingControl == dragTarget)
                {
                    int deltaX = mousePosition.X - _DragOriginX;
                    int deltaY = mousePosition.Y - _DragOriginY;
                    if (attemptAlwaysSuccessful || Math.Abs(deltaX) + Math.Abs(deltaY) > 4)
                    {
                        _IsDraggingControl = true;
                    }
                }
                else
                {
                    _DraggingControl = dragTarget;
                    _DragOriginX = mousePosition.X;
                    _DragOriginY = mousePosition.Y;
                }
            }

            if (_IsDraggingControl)
            {

                for (int i = 0; i < 5; i++)
                {
                    if (_MouseDownControl[i] != null && _MouseDownControl[i] != _DraggingControl)
                    {
                        _MouseDownControl[i].MouseUp(mousePosition, (MouseButton)i);
                        _MouseDownControl[i] = null;
                    }
                }
            }
        }

        void DoDragControl(Point mousePosition)
        {
            if (_DraggingControl == null)
            {
                return;
            }
            int deltaX = mousePosition.X - _DragOriginX;
            int deltaY = mousePosition.Y - _DragOriginY;
            _DraggingControl.Position = new Point(_DraggingControl.X + deltaX, _DraggingControl.Y + deltaY);
            _DragOriginX = mousePosition.X;
            _DragOriginY = mousePosition.Y;
        }

        void EndDragControl(Point mousePosition)
        {
            if (_IsDraggingControl)
            {
                DoDragControl(mousePosition);
            }
            _DraggingControl = null;
            _IsDraggingControl = false;
        }

        void ClipMouse(ref Point position)
        {
            if (position.X < -8)
                position.X = -8;
            if (position.Y < -8)
                position.Y = -8;
            if (position.X >= Width + 8)
                position.X = Width + 8;
            if (position.Y >= Height + 8)
                position.Y = Height + 8;
        }
    }
}
