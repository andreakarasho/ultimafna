﻿/***************************************************************************
 *   RenderedTextList.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.UI.HTML;
using UltimaXNA.Core.Diagnostics.Tracing;
#endregion

namespace UltimaXNA.Core.UI
{
    /// <summary>
    /// A one dimensional list of rendered text objects which can be scrolled (up and down) and
    /// only display within a designated window.
    /// </summary>
    class RenderedTextList : AControl
    {
        private readonly List<RenderedText> _Entries;
        private IScrollBar _ScrollBar;

        private bool _IsMouseDown;
        private int _MouseDownHREF = -1;
        private int _MouseDownText = -1;
        private int _MouseOverHREF = -1;
        private int _MouseOverText = -1;

        /// <summary>
        /// Creates a RenderedTextList.
        /// Note that the scrollBarControl must be created and added to the parent gump before passing it as a param.
        /// </summary>
        public RenderedTextList(AControl parent, int x, int y, int width, int height, IScrollBar scrollBarControl)
            : base(parent)
        {
            _ScrollBar = scrollBarControl;
            _ScrollBar.IsVisible = false;

            HandlesMouseInput = true;

            Position = new Point(x, y);
            Width = width;
            Height = height;
            _Entries = new List<RenderedText>();
        }

        public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
        {
            base.Draw(spriteBatch, position, frameMS);

            Point p = new Point(position.X, position.Y);
            int height = 0;
            int maxheight = _ScrollBar.Value + _ScrollBar.Height;

            for (int i = 0; i < _Entries.Count; i++)
            {
                if (height + _Entries[i].Height <= _ScrollBar.Value)
                {
                    // this entry is above the renderable area.
                    height += _Entries[i].Height;
                }
                else if (height + _Entries[i].Height <= maxheight)
                {
                    int y = height - _ScrollBar.Value;
                    if (y < 0)
                    {
                        // this entry starts above the renderable area, but exists partially within it.
                        _Entries[i].Draw(spriteBatch, new Rectangle(p.X, position.Y, _Entries[i].Width, _Entries[i].Height + y), 0, -y);
                        p.Y += _Entries[i].Height + y;
                    }
                    else
                    {
                        // this entry is completely within the renderable area.
                        _Entries[i].Draw(spriteBatch, p);
                        p.Y += _Entries[i].Height;
                    }
                    height += _Entries[i].Height;
                }
                else
                {
                    int y = maxheight - height;
                    _Entries[i].Draw(spriteBatch, new Rectangle(p.X, position.Y + _ScrollBar.Height - y, _Entries[i].Width, y), 0, 0);
                    // can't fit any more entries - so we break!
                    break;
                }
            }
        }

        public override void Update(double totalMS, double frameMS)
        {
            base.Update(totalMS, frameMS);

            _ScrollBar.Position = new Point(X + Width - 14, Y);
            _ScrollBar.Height = Height;
            CalculateScrollBarMaxValue();
            _ScrollBar.IsVisible = _ScrollBar.MaxValue > _ScrollBar.MinValue;
        }

        protected override bool IsPointWithinControl(int x, int y)
        {
            _MouseOverText = -1; // this value is changed every frame if we mouse over a region.
            _MouseOverHREF = -1; // this value is changed every frame if we mouse over a region.

            int height = 0;
            for (int i = 0; i < _Entries.Count; i++)
            {
                RenderedText rendered = _Entries[i];
                if (rendered.Regions.Count > 0)
                {
                    HtmlLink region = rendered.Regions.RegionfromPoint(new Point(x, y - height + _ScrollBar.Value));
                    if (region != null)
                    {
                        _MouseOverText = i;
                        _MouseOverHREF = region.Index;
                        return true;
                    }
                }
                height += rendered.Height;
            }
            return false;
        }

        protected override void OnMouseDown(int x, int y, MouseButton button)
        {
            _IsMouseDown = true;
            _MouseDownText = _MouseOverText;
            _MouseDownHREF = _MouseOverHREF;

            if (button == MouseButton.Left)
            {
                if (_Entries[_MouseDownText].Regions.Region(_MouseDownHREF).HREF != null)
                    OnHtmlInputEvent(_Entries[_MouseDownText].Regions.Region(_MouseDownHREF).HREF, MouseEvent.Down);
            }
        }

        protected override void OnMouseUp(int x, int y, MouseButton button)
        {
            if (button == MouseButton.Left)
            {
				if (_MouseDownText != -1)
				{
					//if (_Entries[_MouseDownText].Regions.Region(_MouseDownHREF).HREF != null)
						OnHtmlInputEvent(_Entries[_MouseDownText].Regions.Region(_MouseDownHREF).HREF, MouseEvent.Up);
				}
            }

            _IsMouseDown = false;
            _MouseDownText = -1;
            _MouseDownHREF = -1;
        }

        protected override void OnMouseClick(int x, int y, MouseButton button)
        {
            if (_MouseOverText != -1 && _MouseOverHREF != -1 && _MouseDownText == _MouseOverText && _MouseDownHREF == _MouseOverHREF)
            {
                if (button == MouseButton.Left)
                {
                    if (_Entries[_MouseOverText].Regions.Region(_MouseOverHREF).HREF != null)
                        OnHtmlInputEvent(_Entries[_MouseOverText].Regions.Region(_MouseOverHREF).HREF, MouseEvent.Click);
                }
            }
        }

        protected override void OnMouseOver(int x, int y)
        {
            if (_IsMouseDown && _MouseDownText != -1 && _MouseDownHREF != -1 && _MouseDownHREF != _MouseOverHREF)
            {
				if (_Entries[_MouseDownText].Regions.Region(_MouseDownHREF) != null)
					OnHtmlInputEvent(_Entries[_MouseDownText].Regions.Region(_MouseDownHREF).HREF, MouseEvent.DragBegin);
            }
        }

        private void CalculateScrollBarMaxValue()
        {
            bool maxValue = _ScrollBar.Value == _ScrollBar.MaxValue;

            int height = 0;
            for (int i = 0; i < _Entries.Count; i++)
            {
                height += _Entries[i].Height;
            }

            height -= _ScrollBar.Height;

            if (height > 0)
            {
                _ScrollBar.MaxValue = height;
                if (maxValue)
                    _ScrollBar.Value = _ScrollBar.MaxValue;
            }
            else
            {
                _ScrollBar.MaxValue = 0;
                _ScrollBar.Value = 0;
            }
        }

        public void AddEntry(string text)
        {
            bool maxScroll = (_ScrollBar.Value == _ScrollBar.MaxValue);

            while (_Entries.Count > 99)
            {
                _Entries.RemoveAt(0);
            }
            _Entries.Add(new RenderedText(text, Width - 18));
            _ScrollBar.MaxValue += _Entries[_Entries.Count - 1].Height;
            if (maxScroll)
            {
                _ScrollBar.Value = _ScrollBar.MaxValue;
            }
        }

        public void UpdateEntry(int index, string text)
        {
            if (index < 0 || index >= _Entries.Count)
            {
                Tracer.Error(string.Format("Bad index in RenderedTextList.UpdateEntry: {0}", index.ToString()));
                return;
            }

            _Entries[index].Text = text;
            CalculateScrollBarMaxValue();
        }
    }
}
