﻿/***************************************************************************
 *   Images.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace UltimaXNA.Core.UI.HTML
{
    class HtmlImageList
    {
        public static HtmlImageList Empty = new HtmlImageList();
        readonly List<HtmlImage> _Images = new List<HtmlImage>();

        public HtmlImage this[int index]
        {
            get
            {
                if (_Images.Count == 0)
                    return null;
                if (index >= _Images.Count)
                    index = _Images.Count - 1;
                if (index < 0)
                    index = 0;
                return _Images[index];
            }
        }

        public int Count
        {
            get { return _Images.Count; }
        }

        public void AddImage(Rectangle area, Texture2D image)
        {
            _Images.Add(new HtmlImage(area, image));
        }

        public void AddImage(Rectangle area, Texture2D image, Texture2D overimage, Texture2D downimage)
        {
            AddImage(area, image);
            _Images[_Images.Count - 1].TextureOver = overimage;
            _Images[_Images.Count - 1].TextureDown = downimage;
        }

        public void Clear()
        {
            foreach (HtmlImage image in _Images)
            {
                image.Dispose();
            }
            _Images.Clear();
        }
    }

    
}
