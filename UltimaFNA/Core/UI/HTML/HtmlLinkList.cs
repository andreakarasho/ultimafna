﻿/***************************************************************************
 *   HtmlLinkList.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using Microsoft.Xna.Framework;
using System.Collections.Generic;
using UltimaXNA.Core.UI.HTML.Styles;

namespace UltimaXNA.Core.UI.HTML
{
    public class HtmlLinkList
    {
        public static HtmlLinkList Empty => new HtmlLinkList();

        readonly List<HtmlLink> _Links = new List<HtmlLink>();

        public HtmlLink this[int index]
        {
            get
            {
                if (_Links.Count == 0)
                    return null;
                if (index >= _Links.Count)
                    index = _Links.Count - 1;
                if (index < 0)
                    index = 0;
                return _Links[index];
            }
        }

        public int Count
        {
            get { return _Links.Count; }
        }

        public HtmlLink AddLink(StyleState style, Rectangle area)
        {
            HtmlLink matched = null;
            foreach (HtmlLink link in _Links)
            {
                if (link.Style.HREF == style.HREF && link.Area.Right == area.Left)
                {
                    bool overlap = link.Area.Top < area.Bottom && area.Top < link.Area.Bottom;
                    if (overlap)
                    {
                        matched = link;
                        matched.Area.Width = matched.Area.Width + area.Width;
                        if (matched.Area.Y > area.Y)
                            matched.Area.Y = area.Y;
                        if (matched.Area.Bottom < area.Bottom)
                            matched.Area.Height += (area.Bottom - matched.Area.Bottom);
                        break;
                    }
                }
            }

            if (matched == null)
            {
                _Links.Add(new HtmlLink(_Links.Count, style));
                matched = _Links[_Links.Count - 1];
                matched.Area = area;
            }
            return matched;
        }

        public void Clear()
        {
            _Links.Clear();
        }

        public HtmlLink RegionfromPoint(Point p)
        {
            int index = -1;
            for (int i = 0; i < _Links.Count; i++)
            {
                if (_Links[i].Area.Contains(p))
                    index = i;
            }
            if (index == -1)
                return null;
            else
                return _Links[index];
        }

		public HtmlLink Region(int index)
		{
			if (index > _Links.Count)
				return null;

			return _Links[index];
		}
    }
}
