﻿/***************************************************************************
 *   StyleState.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using Microsoft.Xna.Framework;
using UltimaXNA.Core.Resources;

namespace UltimaXNA.Core.UI.HTML.Styles
{
    public class StyleState
    {
        private StyleValue _IsUnderlined = StyleValue.Default;
        public bool IsUnderlined
        {
            set
            {
                if (value)
                {
                    _IsUnderlined = StyleValue.True;
                }
                else
                {
                    _IsUnderlined = StyleValue.False;
                }
            }
            get
            {
                if (_IsUnderlined == StyleValue.False)
                    return false;
                else if (_IsUnderlined == StyleValue.True)
                    return true;
                else // _IsUnderlined == TagValue.Default
                {
                    if (HREF != null)
                        return true;
                    else
                        return false;
                }
            }
        }

        public bool IsBold;
        public bool IsItalic;
        public bool IsOutlined;

        public bool DrawOutline
        {
            get { return IsOutlined && !Font.HasBuiltInOutline; }
        }

        public IFont Font; // default value set in manager ctor.
        public Color Color = Color.White;
        public int ColorHue = 2;
        public int ActiveColorHue = 12;
        public int HoverColorHue = 24;

        public string HREF;
        public bool IsHREF { get { return HREF != null; } }

        public int ExtraWidth
        {
            get
            {
                int extraWidth = 0;
                if (IsItalic)
                {
                    extraWidth = Font.Height / 2;
                }
                if (DrawOutline)
                {
                    extraWidth += 2;
                }
                return extraWidth;
            }
        }

        public StyleState(IResourceProvider provider)
        {
            Font = provider.GetUnicodeFont((int)Fonts.Default);
        }

        public StyleState(StyleState parent)
        {
            _IsUnderlined = parent._IsUnderlined;
            IsBold = parent.IsBold;
            IsItalic = parent.IsItalic;
            IsOutlined = parent.IsOutlined;
            Font = parent.Font;
            Color = parent.Color;
            ColorHue = parent.ColorHue;
            ActiveColorHue = parent.ActiveColorHue;
            HoverColorHue = parent.HoverColorHue;
            HREF = parent.HREF;
        }
    }
}
