﻿/***************************************************************************
 *   HuedTexture.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UltimaXNA.Core.Graphics;
#endregion

namespace UltimaXNA.Ultima.Resources
{
    public class HuedTexture
    {
        private readonly Texture2D _Texture;
        private readonly Rectangle _SourceRect = Rectangle.Empty;

        private Point _Offset;
        public Point Offset
        {
            set { _Offset = value; }
            get { return _Offset; }
        }
        
        private int _Hue;
        public int Hue
        {
            set { _Hue = value; }
            get { return _Hue; }
        }

        public HuedTexture(Texture2D texture, Point offset, Rectangle source, int hue)
        {
            _Texture = texture;
            _Offset = offset;
            _SourceRect = source;
            _Hue = hue;
        }

        public void Draw(SpriteBatchUI sb, Point position)
        {
            Vector3 v = new Vector3(position.X - _Offset.X, position.Y - _Offset.Y, 0);
            sb.Draw2D(_Texture, v, _SourceRect, Utility.GetHueVector(_Hue));
        }
    }
}
