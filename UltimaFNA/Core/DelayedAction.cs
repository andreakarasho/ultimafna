﻿using System;
using UltimaXNA.Core.Diagnostics.Tracing;
using System.Threading;

namespace UltimaXNA.Core
{
    public class DelayedAction
    {
        private volatile Action _Action;

        private DelayedAction(Action action, int msDelay)
        {
            _Action = action;
			Timer timer = new Timer(TimerProc);
            timer.Change(msDelay, Timeout.Infinite);
        }

        private void TimerProc(object state)
        {
            try
            {
                // The state object is the Timer object. 
                ((Timer)state).Dispose();
                _Action.Invoke();
            }
            catch (Exception ex)
            {
                Tracer.Error(ex);
            }
        }

        public static DelayedAction Start(Action callback, int msDelay)
        {
            return new DelayedAction(callback, msDelay);
        }
    }
}
