/***************************************************************************
 *   SettingsFile.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System;
using System.Collections.Generic;
using System.IO;
using System.Timers;
using UltimaXNA.Core.Diagnostics.Tracing;
#endregion

namespace UltimaXNA.Core.Configuration
{
    public class SettingsFile
    {
        private readonly Dictionary<string, ASettingsSection> _SectionCache;
        private static readonly object _SyncRoot = new object();

        private readonly string _Filename;
        private readonly Timer _SaveTimer;

        public SettingsFile(string filename)
        {
            _SectionCache = new Dictionary<string, ASettingsSection>();
            _SaveTimer = new Timer
            {
                Interval = 10000, // save settings every 10 seconds
                AutoReset = true
            };
            _SaveTimer.Elapsed += OnTimerElapsed;
            _Filename = filename;
        }

        public bool Exists
        {
            get { return File.Exists(_Filename); }
        }
        
        public void Save()
        {
            try
            {
                lock(_SyncRoot)
                {
                    TextSettingsFileWriter serializer = new TextSettingsFileWriter(_Filename);
                    serializer.Serialize(_SectionCache);
                }
            }
            catch(Exception e)
            {
                Tracer.Error(e);
            }
        }

        internal void InvalidateDirty()
        {
            // possibly save the file here?
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            // Lock the timer so we dont start it till its done save.
            lock(_SaveTimer)
            {
                Save();
            }
        }

        public void Load()
        {
            try
            {
                lock (_SyncRoot)
                {
                    if (LoadFromFile(_Filename))
                    {
                        Tracer.Debug("Read settings from settings file.");
                    }
                    else
                    {
                        if (File.Exists(_Filename + ".bak"))
                        {
                            Tracer.Error("Unable to read settings file.  Trying backup file");
                            if (LoadFromFile(_Filename + ".bak"))
                            {
                                Tracer.Debug("Read settings from backup settings file.", null);
                            }
                            else
                            {
                                Tracer.Error("Unable to read backup settings file. All settings are set to default values.");
                            }
                        }
                        else
                        {
                            Tracer.Error("Unable to read settings file. All settings are set to default values.");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Tracer.Error(e);
            }

            _SaveTimer.Enabled = true;
        }

        private bool LoadFromFile(string fileName)
        {
            if (!File.Exists(fileName))
            {
                return false;
            }

            try
            {
                TextSettingsFileWriter serializer = new TextSettingsFileWriter(fileName);
                serializer.Deserialize(_SectionCache);
                return true;
            }
            catch (Exception e)
            {
                Tracer.Error(e);
                return false;
            }
        }

        internal T CreateOrOpenSection<T>(string sectionName)
            where T : ASettingsSection, new()
        {
            ASettingsSection section;

            // If we've already deserialized the section, just return it.
            if(_SectionCache.TryGetValue(sectionName, out section))
            {
                return (T)section;
            }

            section = new T();
            InvalidateDirty();
            _SectionCache[sectionName] = section;
            return (T)section;
        }
    }
}