﻿/***************************************************************************
 *   LoginClient.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System;
using System.Security;
using System.Threading;
using UltimaFNA.Core.Network.SocketAsync;
using UltimaXNA.Core.Diagnostics.Tracing;
using UltimaXNA.Core.Network;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.Data;
using UltimaXNA.Ultima.Input;
using UltimaXNA.Ultima.Login.Accounts;
using UltimaXNA.Ultima.Network.Client;
using UltimaXNA.Ultima.Network.Server;
using UltimaXNA.Ultima.Player;
using UltimaXNA.Ultima.UI;
using UltimaXNA.Ultima.World;
using UltimaXNA.Ultima.World.Entities.Mobiles;
#endregion

namespace UltimaXNA.Ultima.Login
{
    public class LoginClient : IDisposable
    {
        readonly NetworkClient _Network;
        readonly UltimaGame _Engine;
        readonly UserInterfaceService _UserInterface;
        Timer _KeepAliveTimer;
        string _UserName;
        SecureString _Password;

        public LoginClient()
        {
            _Network = Service.Get<NetworkClient>();
            _Engine = Service.Get<UltimaGame>();
            _UserInterface = Service.Get<UserInterfaceService>();
            Initialize();
        }

        // ============================================================================================================
        // Packet registration and unregistration
        // ============================================================================================================
        void Initialize()
        {
            Register<LoginConfirmPacket>(0x1B, 37, ReceiveLoginConfirmPacket);
            Register<LoginCompletePacket>(0x55, 1, ReceiveLoginComplete);
            Register<ServerPingPacket>(0x73, 2, ReceivePingPacket);
            Register<LoginRejectionPacket>(0x82, 2, ReceiveLoginRejection);
            Register<DeleteResultPacket>(0x85, 2, ReceiveDeleteCharacterResponse);
            Register<CharacterListUpdatePacket>(0x86, -1, ReceiveCharacterListUpdate);
            Register<ServerRelayPacket>(0x8C, 11, ReceiveServerRelay);
            Register<ServerListPacket>(0xA8, -1, ReceiveServerList);
            Register<CharacterCityListPacket>(0xA9, -1, ReceiveCharacterList);
            Register<SupportedFeaturesPacket>(0xB9, 5, ReceiveEnableFeatures);
            Register<VersionRequestPacket>(0xBD, -1, ReceiveVersionRequest);
        }

        public void Dispose()
        {
            StopKeepAlivePackets();
            _Network.Unregister(this);
        }

        public void Register<T>(int id, int length, Action<T> onReceive) where T : IRecvPacket
        {
            _Network.Register(this, id, length, onReceive);
        }

        public void Unregister(int id)
        {
            _Network.Unregister(this, id);
        }

        // ============================================================================================================
        // Connection and Disconnect
        // ============================================================================================================
        public bool Connect(string host, int port, string account, SecureString password)
        {
            Disconnect();
            if (_Network.Connect(host, port))
            {
                _UserName = account;
                _Password = password;
                _Network.Send(new SeedPacket(0x1337BEEF, Settings.UltimaOnline.PatchVersion));
                Login(_UserName, _Password);
                return true;
            }
            return false;
        }

        public void Disconnect()
        {
            StopKeepAlivePackets();
            if (_Network.IsConnected)
            {
                _Network.Disconnect();
            }
        }

        // ============================================================================================================
        // Login sequence routines
        // ============================================================================================================
        public void Login(string account, SecureString password)
        {
            _Network.Send(new LoginPacket(account, password.ConvertToUnsecureString()));
        }

        public void SelectShard(int index)
        {
            _Network.Send(new SelectServerPacket(index));
        }

        public void LoginWithCharacter(int index)
        {
            if (Characters.List[index].Name != string.Empty)
            {
				if (!Service.Has<WorldModel>())
					_Engine.Models.Next = new WorldModel();

                _Network.Send(new LoginCharacterPacket(Characters.List[index].Name, index, Utility.IPAddress));
                Macros.Player.Load(Characters.List[index].Name);
            }
        }

        public void CreateCharacter(CreateCharacterPacket packet)
        {
            _Engine.Models.Next = new WorldModel();
            _Network.Send(packet);
        }

        public void DeleteCharacter(int index)
        {
            if (index == -1) {
                return;
            }
            if (Characters.List[index].Name != string.Empty) {
                _Network.Send(new DeleteCharacterPacket(index, Utility.IPAddress));
            }
        }

        public void SendClientVersion()
        {
            if (ClientVersion.HasExtendedFeatures(Settings.UltimaOnline.PatchVersion))
            {
                Tracer.Info("Client version is greater than 6.0.14.2, enabling extended 0xB9 packet.");
                Unregister(0xB9);
                Register<SupportedFeaturesPacket>(0xB9, 5, ReceiveEnableFeatures);
            }
            _Network.Send(new ClientVersionPacket(Settings.UltimaOnline.PatchVersion));
        }

        void ReceiveDeleteCharacterResponse(DeleteResultPacket packet)
        {
            MsgBoxGump.Show(packet.Result, MsgBoxTypes.OkOnly);
        }

        void ReceiveCharacterListUpdate(CharacterListUpdatePacket packet)
        {
            Characters.SetCharacterList(packet.Characters);
            (_Engine.Models.Current as LoginModel).ShowCharacterList();
        }

        void ReceiveCharacterList(CharacterCityListPacket packet)
        {
            Characters.SetCharacterList(packet.Characters);
            Characters.SetStartingLocations(packet.Locations);
            StartKeepAlivePackets();
            (_Engine.Models.Current as LoginModel).ShowCharacterList();
        }

        void ReceiveServerList(ServerListPacket packet)
        {
            (_Engine.Models.Current as LoginModel).ShowServerList((packet).Servers);
        }

        void ReceiveLoginRejection(LoginRejectionPacket packet)
        {
            Disconnect();
            (_Engine.Models.Current as LoginModel).ShowLoginRejection(packet.Reason);
        }

        void ReceiveServerRelay(ServerRelayPacket packet)
        {
            // On OSI, upon receiving this packet, the client would disconnect and
            // log in to the specified server. Since emulated servers use the same
            // server for both shard selection and world, we don't need to disconnect.
            _Network.Compression = true;
            _Network.Send(new GameLoginPacket(packet.AccountId, _UserName, _Password.ConvertToUnsecureString()));
        }

        void ReceiveEnableFeatures(SupportedFeaturesPacket packet)
        {
            PlayerState.ClientFeatures.SetFlags(packet.Flags);
        }

        void ReceiveVersionRequest(VersionRequestPacket packet)
        {
            SendClientVersion();
        }

        void ReceivePingPacket(ServerPingPacket packet)
        {

        }

        // ============================================================================================================
        // Login to World - Nominally, the server should send LoginConfirmPacket, followed by GeneralInfo0x08, and 
        // finally LoginCompletePacket. However, the legacy client finds it valid to receive the packets in any
        // order. The code below allows any of these possibilities.
        // ============================================================================================================
        LoginConfirmPacket _QueuedLoginConfirmPacket;
        bool _LoggingInToWorld;

        void ReceiveLoginConfirmPacket(LoginConfirmPacket packet)
        {
            _QueuedLoginConfirmPacket = packet;
            // set the player serial and create the player entity. Don't need to do anything with it yet.
            WorldModel.PlayerSerial = packet.Serial;
            PlayerMobile player = WorldModel.Entities.GetObject<PlayerMobile>(WorldModel.PlayerSerial, true);
            if (player == null) {
                Tracer.Critical("Could not create player object.");
            }
            CheckIfOkayToLogin();
        }

        void ReceiveLoginComplete(LoginCompletePacket packet)
        {
            CheckIfOkayToLogin();
        }

        void CheckIfOkayToLogin()
        {
            // Before the client logs in, we need to know the player entity's serial, and the
            // map the player will be loading on login. If we don't have either of these, we
            // delay loading until we do.
            if (!_LoggingInToWorld)
            {
                if ((_Engine.Models.Next as WorldModel).MapIndex != 0xffffffff)
                { // will be 0xffffffff if no map
                    _LoggingInToWorld = true; // stops double log in attempt caused by out of order packets.
                    _Engine.Models.ActivateNext();
                    if (_Engine.Models.Current is WorldModel)
                    {
                        (_Engine.Models.Current as WorldModel).LoginToWorld();
                        Mobile player = WorldModel.Entities.GetObject<Mobile>(_QueuedLoginConfirmPacket.Serial, true);
                        if (player == null)
                        {
                            Tracer.Critical("No player object ready in CheckIfOkayToLogin().");
                        }
                        player.Move_Instant(
                            _QueuedLoginConfirmPacket.X, _QueuedLoginConfirmPacket.Y,
                            _QueuedLoginConfirmPacket.Z, _QueuedLoginConfirmPacket.Direction);
                    }
                    else
                    {
                        Tracer.Critical("Not in world model at login.");
                    }
                }
            }
        }

        // ============================================================================================================
        // Keep-alive packets - These are necessary because the client does not otherwise send packets during character
        // creation, and the server will disconnect after a given length of inactivity.
        // ============================================================================================================
        void StartKeepAlivePackets()
        {
            if (_KeepAliveTimer == null)
            {
                _KeepAliveTimer = new Timer(e => SendKeepAlivePacket(), null, TimeSpan.Zero, TimeSpan.FromSeconds(60));
            }
        }

        void StopKeepAlivePackets()
        {
            if (_KeepAliveTimer != null)
            {
                _KeepAliveTimer.Dispose();
            }
        }

        void SendKeepAlivePacket()
        {
            if (_Network.IsConnected)
            {
                _Network.Send(new ClientPingPacket());
            }
            else
            {
                StopKeepAlivePackets();
            }
        }
    }
}