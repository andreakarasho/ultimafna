﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimaXNA.Core.Network;
using UltimaXNA.Core.Network.Packets;

namespace UltimaXNA.Ultima.Network.Server
{
    public class UO3DDisplayWayPoint : RecvPacket
    {
        public UO3DDisplayWayPoint(PacketReader r) : base(0xE5 , "UO3D Display waypoint")
        {

        }
    }
}
