﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimaXNA.Core.Network;
using UltimaXNA.Core.Network.Packets;
using UltimaXNA.Ultima.World;
using UltimaXNA.Ultima.World.Managers;
using UltimaXNA.Ultima.World.Entities.Items;

namespace UltimaXNA.Ultima.Network.Server
{
    public class NewWorldObjectPacket : RecvPacket
    {
        public NewWorldObjectPacket(PacketReader reader) : base(0xF3, "New WorldObject")
        {
            reader.ReadInt16();

            IsMulti = reader.ReadByte() == 2;
            Serial = reader.ReadInt32();
            ushort graphic = reader.ReadUInt16();
           /* if (IsMulti)
                graphic |= 0x4000;*/
            Graphic = graphic;
            byte dir = reader.ReadByte();
            Amount = reader.ReadInt16();
            reader.ReadInt16();
            X = reader.ReadInt16();
            Y = reader.ReadInt16();
            Z = reader.ReadSByte();
            reader.ReadByte();
            Hue = reader.ReadInt16();
            byte flag = reader.ReadByte();
        }

        public Serial Serial { get; }
        public ushort Graphic { get; }
        public bool IsMulti { get; }
        public Direction Direction { get; }
        public int Amount { get; }
        public short X { get; }
        public short Y { get; }
        public sbyte Z { get; }
        public short Hue { get; }
    }
}
