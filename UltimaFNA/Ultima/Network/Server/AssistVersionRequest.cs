﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimaXNA.Core.Network;
using UltimaXNA.Core.Network.Packets;

namespace UltimaXNA.Ultima.Network.Server
{
    public class AssistVersionRequest : RecvPacket
    {
        public AssistVersionRequest(PacketReader reader) : base(0xBE, "Assist version request")
        {
            AllowedVersion = reader.ReadInt32();
        }

        public int AllowedVersion { get; }
    }
}
