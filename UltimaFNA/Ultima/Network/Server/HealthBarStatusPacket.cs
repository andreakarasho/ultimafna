﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimaXNA.Core.Network;
using UltimaXNA.Core.Network.Packets;

namespace UltimaXNA.Ultima.Network.Server
{
    class HealthBarStatusPacket : RecvPacket
    {
        public HealthBarStatusPacket(PacketReader reader) : base(0x17, "HealthBar Status Update")
        {
            Serial = reader.ReadInt32();
            reader.Seek(2, System.IO.SeekOrigin.Current);
            Type = reader.ReadInt16();
            switch (Type)
            {
                case 1:
                    Flag = 0x04;
                    break;
                case 2:
                    Flag = 0x08;
                    break;
                default: return;
            }

            IsEnabled = reader.ReadBoolean();
        }

        public int Serial { get; }
        public short Type { get; }
        public int Flag { get; }
        public bool IsEnabled { get; }
    }
}
