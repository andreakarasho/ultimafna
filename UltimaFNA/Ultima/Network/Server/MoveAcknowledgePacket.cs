﻿/***************************************************************************
 *   MoveAcknowledgePacket.cs
 *   Copyright (c) 2009 UltimaXNA Development Team
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using UltimaXNA.Core.Network;
using UltimaXNA.Core.Network.Packets;
#endregion

namespace UltimaXNA.Ultima.Network.Server
{
    public class MoveAcknowledgePacket : RecvPacket
    {
        readonly byte _sequence;
        readonly byte _notoriety;

        public byte Sequence 
        {
            get { return _sequence; } 
        }

        public byte Notoriety
        {
            get { return _notoriety; }
        }

        public MoveAcknowledgePacket(PacketReader reader)
            : base(0x22, "Move Request Acknowledged")
        {
            _sequence = reader.ReadByte(); // (matches sent sequence)
            _notoriety = reader.ReadByte(); // Not sure why it sends 
        }
    }
}
