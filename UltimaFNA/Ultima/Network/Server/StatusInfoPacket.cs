﻿/***************************************************************************
 *   StatusInfoPacket.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using UltimaXNA.Core.Network;
using UltimaXNA.Core.Network.Packets;
using UltimaXNA.Ultima.Data;
#endregion


namespace UltimaXNA.Ultima.Network.Server
{
    public class StatusInfoPacket : RecvPacket
    {
        public readonly Serial Serial;
        public readonly string PlayerName;
        public readonly ushort CurrentHealth;
        public readonly ushort MaxHealth;
        public readonly bool NameChangeFlag;
        public readonly byte StatusTypeFlag;
        public readonly Genders Sex;
        public readonly ushort Strength;
        public readonly ushort Dexterity;
        public readonly ushort Intelligence;
        public readonly ushort CurrentStamina;
        public readonly ushort MaxStamina;
        public readonly ushort CurrentMana;
        public readonly ushort MaxMana;
        public readonly uint GoldInInventory;
        public readonly short ArmorRating;
        public readonly ushort Weight;
        public readonly ushort MaxWeight;
        public readonly Races Race;
        public readonly ushort StatCap;
        public readonly byte Followers;
        public readonly byte MaxFollowers;
        public readonly ushort ResistFire;
        public readonly ushort ResistCold;
        public readonly ushort ResistPoison;
        public readonly ushort ResistEnergy;
        public readonly ushort Luck;
        public readonly ushort DmgMin;
        public readonly ushort DmgMax;
        public readonly uint TithingPoints;
        public readonly short MaxPhysicRestistance;
        public readonly short MaxFireResistance;
        public readonly short MaxColdResistance;
        public readonly short MaxPoisonResistance;
        public readonly short MaxEnergyResistance;
        public readonly short DefenseChance;
        public readonly short MaxDefenceChance;
        public readonly short AttackChance;
        public readonly short WeaponSpeed;
        public readonly short WeaponDamage;
        public readonly short LowerReagCost;
        public readonly short SpellDamage;
        public readonly short FCR;
        public readonly short FC;
        public readonly short LowerManaCost;

		public StatusInfoPacket(PacketReader reader)
            : base(0x11, "StatusInfo")
        {
            Serial = reader.ReadInt32();
            PlayerName = reader.ReadString(30);
            CurrentHealth = reader.ReadUInt16();
            MaxHealth = reader.ReadUInt16();
            NameChangeFlag = reader.ReadBoolean(); // 0x1 = allowed, 0 = not allowed
            StatusTypeFlag = reader.ReadByte();
            if (StatusTypeFlag > 0)
            {
                Sex = (Genders)reader.ReadByte(); // 0=male, 1=female
                Strength = reader.ReadUInt16();
                Dexterity = reader.ReadUInt16();
                Intelligence = reader.ReadUInt16();
                CurrentStamina = reader.ReadUInt16();
                MaxStamina = reader.ReadUInt16();
                CurrentMana = reader.ReadUInt16();
                MaxMana = reader.ReadUInt16();
                GoldInInventory = reader.ReadUInt32();
                ArmorRating = reader.ReadInt16();
                Weight = reader.ReadUInt16();

                if (StatusTypeFlag >= 5)
                {
                    MaxWeight = reader.ReadUInt16();
                    Race = (Races)reader.ReadByte();
                }
                else
                    MaxWeight = (ushort)((Strength * 4) + 25);

                if (StatusTypeFlag >= 3)
                {
                    StatCap = reader.ReadUInt16();
                    Followers = reader.ReadByte();
                    MaxFollowers = reader.ReadByte();
                }

                if (StatusTypeFlag >= 4)
                {
                    ResistFire = reader.ReadUInt16();
                    ResistCold = reader.ReadUInt16();
                    ResistPoison = reader.ReadUInt16();
                    ResistEnergy = reader.ReadUInt16();
                    Luck = reader.ReadUInt16();
                    DmgMin = reader.ReadUInt16();
                    DmgMax = reader.ReadUInt16();
                    TithingPoints = reader.ReadUInt32();
				}
				if (StatusTypeFlag >= 6)
				{
                    MaxPhysicRestistance = reader.ReadInt16();
                    MaxFireResistance = reader.ReadInt16();
                    MaxColdResistance = reader.ReadInt16();
                    MaxPoisonResistance = reader.ReadInt16();
                    MaxEnergyResistance = reader.ReadInt16();
                    DefenseChance = reader.ReadInt16();
                    MaxDefenceChance = reader.ReadInt16();
                    AttackChance = reader.ReadInt16();
                    WeaponSpeed = reader.ReadInt16();
                    WeaponDamage = reader.ReadInt16();
                    LowerReagCost = reader.ReadInt16();
                    SpellDamage = reader.ReadInt16();
                    FCR = reader.ReadInt16();
                    FC = reader.ReadInt16();
                    LowerManaCost = reader.ReadInt16();
				}
			}
        }
    }
}
