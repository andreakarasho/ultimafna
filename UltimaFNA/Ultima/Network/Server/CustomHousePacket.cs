﻿/***************************************************************************
 *   CustomHousePacket.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using UltimaXNA.Core.Network;
using UltimaXNA.Core.Network.Packets;
using UltimaXNA.Ultima.World.Data;
#endregion

namespace UltimaXNA.Ultima.Network.Server
{
    public class CustomHousePacket : RecvPacket
    {
        readonly Serial _HouseSerial;
        public Serial HouseSerial => _HouseSerial;

        readonly int _RevisionHash;
        public int RevisionHash => _RevisionHash;

        readonly int _NumPlanes;
        public int PlaneCount => _NumPlanes;

        readonly CustomHousePlane[] _Planes;
        public CustomHousePlane[] Planes => _Planes;

        public bool EnableResponse { get; }

        public CustomHousePacket(PacketReader reader)
            : base(0xD8, "Custom House Packet")
        {
            byte CompressionType = reader.ReadByte();
           
            EnableResponse = reader.ReadBoolean(); // unknown, always 0?
            _HouseSerial = reader.ReadInt32();
            _RevisionHash = reader.ReadInt32();
            // this is for compression type 3 only
            int tilesLength = reader.ReadInt16();
            int bufferLength = reader.ReadInt16();
            _NumPlanes = reader.ReadByte();
            // end compression type 3
            _Planes = new CustomHousePlane[_NumPlanes];
            for (int i = 0; i < _NumPlanes; i++) {
                _Planes[i] = new CustomHousePlane(reader);
            }
        }
    }
}
