﻿/***************************************************************************
 *   CompressedGumpPacket.cs
 *   Copyright (c) 2009 UltimaXNA Development Team
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System;
using System.Collections.Generic;
using System.Text;
using UltimaXNA.Core.Network;
using UltimaXNA.Core.Network.Compression;
using UltimaXNA.Core.Network.Packets;
#endregion


namespace UltimaXNA.Ultima.Network.Server
{
    public class CompressedGumpPacket : RecvPacket
    {
        public readonly int GumpSerial;
        public readonly int GumpTypeID;
        public readonly int X;
        public readonly int Y;
        public readonly string GumpData;
        public readonly string[] TextLines;

        public bool HasData
        {
            get { return GumpData != null; }
        }

        public CompressedGumpPacket(PacketReader reader)
            : base(0xDD, "Compressed Gump")
        {
            GumpSerial = reader.ReadInt32();
            GumpTypeID = reader.ReadInt32();
            X = reader.ReadInt32();
            Y = reader.ReadInt32();
            
            int compressedLength = reader.ReadInt32() - 4;
            int decompressedLength = reader.ReadInt32();
            byte[] compressedData = reader.ReadBytes(compressedLength);
            byte[] decompressedData = new byte[decompressedLength];

            if (ZlibCompression.Unpack(decompressedData, ref decompressedLength, compressedData, compressedLength) != ZLibError.Okay)
            {
                // Problem decompressing, go ahead and quit.
                return;
            }
            else
            {
                GumpData = Encoding.ASCII.GetString(decompressedData);

                int numTextLines = reader.ReadInt32();
                if (numTextLines > 0)
                {
                    int compressedTextLength = reader.ReadInt32() - 4;
                    int decompressedTextLength = reader.ReadInt32();
                    byte[] decompressedText = new byte[decompressedTextLength];
                    byte[] compressedTextData = reader.ReadBytes(compressedTextLength);
                    ZlibCompression.Unpack(decompressedText, ref decompressedTextLength, compressedTextData, compressedTextLength);

                    string[] lines = new string[numTextLines];

                    int index = 0;
                    for (int i = 0; i < numTextLines; i++)
                    {
                        int length = decompressedText[index++] + decompressedText[index++];
                        byte[] text = new byte[length * 2];
                        Buffer.BlockCopy(decompressedText, index, text, 0, text.Length);
                        index += length * 2;
                        lines[i] = Encoding.BigEndianUnicode.GetString(text);
                    }

                    TextLines = lines;
                }
                else
                    TextLines = new string[0];
            }
        }
    }
}
