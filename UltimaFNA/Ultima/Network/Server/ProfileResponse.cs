﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimaXNA.Core.Network;
using UltimaXNA.Core.Network.Packets;
using UltimaXNA.Ultima.Data;

namespace UltimaXNA.Ultima.Network.Server
{
    class ProfileResponse : RecvPacket
    {
		public ProfileResponse(PacketReader reader) : base (0xB8, "Profile Response")
        {
            Serial = reader.ReadInt32();
			Header = reader.ReadStringSafe(); // Fisso
			Footer = reader.ReadUnicodeStringSafe(); // Fisso
			Body = reader.ReadUnicodeStringSafe(); // SI puo' editare
		}

        public int Serial { get; }
        public string Header { get; }
		public string Body { get; }
		public string Footer { get; }

	}
}
