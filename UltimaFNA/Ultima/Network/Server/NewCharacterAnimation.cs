﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimaXNA.Core.Network;
using UltimaXNA.Core.Network.Packets;

namespace UltimaXNA.Ultima.Network.Server
{
    class NewCharacterAnimation : RecvPacket
    {
        public NewCharacterAnimation(PacketReader reader) : base (0xE2, "New character animation")
        {
            Serial = reader.ReadInt32();
            Action = reader.ReadInt16();
            FrameCount = reader.ReadInt16();
            Delay = reader.ReadByte();
        }

        public Serial Serial { get; }
        public short Action { get; }
        public short FrameCount { get; }
        public byte Delay { get; }
    }
}
