﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimaXNA.Core.Network;
using UltimaXNA.Core.Network.Packets;

namespace UltimaXNA.Ultima.Network.Server
{

    public class DropItemApproved : RecvPacket
    {
        public DropItemApproved(PacketReader reader)
            : base(0x29, "Drop Item Approved")
        {

        }
    }
}

