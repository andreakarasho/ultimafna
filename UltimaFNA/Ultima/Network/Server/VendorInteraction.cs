﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimaXNA.Core.Network;
using UltimaXNA.Core.Network.Packets;
using UltimaXNA.Ultima.Data;

namespace UltimaXNA.Ultima.Network.Server
{
    class VendorInteraction : RecvPacket
    {
		/*
		// Packet che viene ricevuto a transazione eseguita da un vendor
		byte ID(3B)
		word Packet Size
		dword   Vendor Serial
		byte Flag(0x00 = no items, 0x02 = items list)
		loop Items
		byte Item Layer
		dword   Item Serial
		word Item Amount
		endloop Items
		// Nel caso di risposta dal server il packet si ferma dopo il flag con 0x00
		*/

		public VendorInteraction(PacketReader reader) : base (0x3B, "Vendor Interaction")
        {
		}
	}
}
