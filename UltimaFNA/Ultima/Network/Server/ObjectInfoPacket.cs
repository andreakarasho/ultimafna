﻿/***************************************************************************
 *   ObjectInfoPacket.cs
 *   Copyright (c) 2009 UltimaXNA Development Team
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using UltimaXNA.Core.Network;
using UltimaXNA.Core.Network.Packets;
using UltimaXNA.Ultima.IO;
#endregion

namespace UltimaXNA.Ultima.Network.Server
{
    public class ObjectInfoPacket : RecvPacket
    {
        public readonly Serial Serial;
        public readonly ushort ItemID;
        public readonly ushort Amount;
        public readonly short X;
        public readonly short Y;
        public readonly sbyte Z;
        public readonly byte Direction;
        public readonly ushort Hue;
        public readonly byte Flags;

        public ObjectInfoPacket(PacketReader reader)
            : base(0x1A, "ObjectInfoPacket")
        {
            int serial = reader.ReadInt32();
            ushort graphic = (ushort)(reader.ReadUInt16() & FileManager.ItemIDMask);

            Amount = (ushort)(((serial & 0x80000000) != 0) ? reader.ReadUInt16() : 1);

            if ((graphic & 0x8000) != 0)
                ItemID = (ushort)(graphic & 0x7FFF + reader.ReadSByte());
            else
                ItemID = (ushort)(graphic & 0x7FFF);

            short x = reader.ReadInt16();
            short y = reader.ReadInt16();
                        
            if ((x & 0x8000) != 0)
                Direction = reader.ReadByte();

            Z = reader.ReadSByte();

            if ((y & 0x8000) != 0)
                Hue = reader.ReadUInt16();

            if ((y & 0x4000) != 0)
                Flags = reader.ReadByte();

            Serial = (serial & 0x7FFFFFFF);
            X = (short)(x & 0x7FFF);
            Y = (short)(y & 0x3FFF);
        }
    }
}
