﻿/***************************************************************************
 *   CreateCharacterPacket.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System;
using Microsoft.Xna.Framework;
using UltimaXNA.Core.Network.Packets;
using UltimaXNA.Ultima.Data;
using UltimaXNA.Ultima.Login.Data;
using UltimaXNA.Ultima.Player;
#endregion

namespace UltimaXNA.Ultima.Network.Client
{
    public class CreateCharacterPacket : SendPacket
    {
        internal CreateCharacterPacket(CreateCharacterData data, short locationIndex, short slotNumber, int clientIp)
            : base(0xF8, "Create Character", 106)
        {

            bool isnew = ClientVersion.ClientInfo >= ClientVersionTypes.Version70160;

          /*  if (isnew)
                Resize(0xF8, 106);*/

            int str = (byte)MathHelper.Clamp(data.Attributes[0], 10, 60);
            int dex = (byte)MathHelper.Clamp(data.Attributes[1], 10, 60);
            int intel = (byte)MathHelper.Clamp(data.Attributes[2], 10, 60);
                
            if (str + dex + intel != 80)
                throw new Exception("Unable to create character with a combined stat total not equal to 80.");

            Stream.Write(0xedededed);
            Stream.Write(0xffffffff);
            Stream.Write((byte)0);
            Stream.WriteAsciiFixed(data.Name, 30);
            Stream.Write((short)0);
            //Stream.WriteAsciiFixed(string.Empty, 30);

            uint clientflag = 0;
            for (int i = 0; i < (int)PlayerState.ClientFeatures.Flags; i++)
                clientflag |= (uint)(1 << i);

            Stream.Write(clientflag);
            Stream.Write(1);
            Stream.Write(0);
            Stream.Write((byte)0); // profession
            Stream.WriteAsciiFixed(string.Empty, 15);

            int offset = 0;
            if (data.Race == 1)
                offset += 4;
            else if (data.Race == 2)
                offset += 6;

            int val = offset + data.Gender;

            Stream.Write((byte)val);
            Stream.Write((byte)str);
            Stream.Write((byte)dex);
            Stream.Write((byte)intel);

            Stream.Write((byte)data.SkillIndexes[0]);
            Stream.Write((byte)data.SkillValues[0]);
            Stream.Write((byte)data.SkillIndexes[1]);
            Stream.Write((byte)data.SkillValues[1]);
            Stream.Write((byte)data.SkillIndexes[2]);
            Stream.Write((byte)data.SkillValues[2]);
            if (isnew)
            {
                Stream.Write((byte)data.SkillIndexes[3]);
                Stream.Write((byte)data.SkillValues[3]);
            }

            Stream.Write((short)data.SkinHue);
            Stream.Write((short)data.HairStyleID);
            Stream.Write((short)data.HairHue);
            Stream.Write((short)data.FacialHairStyleID);
            Stream.Write((short)data.FacialHairHue);

            Stream.Write((short)locationIndex);
            Stream.Write((short)0);
            Stream.Write((short)slotNumber);
            Stream.Write((short)0);

            Stream.Write(clientIp);
            Stream.Write((short)data.ShirtColor);
            Stream.Write((short)data.PantsColor);
        }
    }
}
