﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimaXNA.Core.Network.Packets;

namespace UltimaXNA.Ultima.Network.Client
{
    public class AssistVersionResponse : SendPacket
    {
        public AssistVersionResponse(byte[] version) : base(0xBE,"Assist version response")
        {
            Stream.Write(1);
            Stream.WriteAsciiNull(string.Format("{0}.{1}.{2}.{3}", version[0], version[1], version[2], version[3]));
        }
    }
}
