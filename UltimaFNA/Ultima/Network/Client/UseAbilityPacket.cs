﻿/***************************************************************************
 *   QuestGumpRequestPacket.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using UltimaXNA.Core.Network.Packets;
#endregion

namespace UltimaXNA.Ultima.Network.Client
{
    public class UseAbilityPacket : SendPacket
    {
        public UseAbilityPacket(int serial, int index)
            : base(0xD7, "Use Ability Packet")
        {
            Stream.Write(serial);
            Stream.Write((ushort)0x0019); // Command
			if (index == 0)
			{
				Stream.Write(true);
			}
			else
			{
				Stream.Write(false);
				Stream.Write((int)index);
			}
		}
    }
}
