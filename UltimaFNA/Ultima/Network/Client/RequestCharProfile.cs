﻿#region usings
using UltimaXNA.Core.Network.Packets;
#endregion

namespace UltimaXNA.Ultima.Network.Client
{
    public class RequestCharProfile : SendPacket
    {
        public RequestCharProfile(Serial serial, bool requestupdate, string text = "")
            : base(0xB8, "Request Char Profile")
        {
			if (requestupdate)
			{
				Stream.Write((byte)0x01);
				Stream.Write(serial);
				Stream.Write((short)0x0001);
				Stream.Write((short)text.Length);
				Stream.WriteBigUniFixed(text, text.Length);
			}
			else
			{
				Stream.Write((byte)0x00);
				Stream.Write(serial);
			}
        }
    }
}
