﻿namespace UltimaXNA.Ultima.Input
{
    /// <summary>
    /// A single macro that performs one thing.
    /// </summary>
    public class Macro
    {
        public readonly MacroType Type;

        ValueTypes _ValueType =  ValueTypes.None;
        int _ValueInteger = -1;
        string _ValueString;

        public Macro(MacroType type)
        {
            Type = type;
        }

        public Macro(MacroType type, int value)
            : this(type)
        {
            _ValueInteger = value;
            _ValueType = ValueTypes.Integer;
        }

        public Macro(MacroType type, string value)
            : this(type)
        {
            _ValueString = value;
            _ValueType = ValueTypes.String;
        }

        public int ValueInteger
        {
            set
            {
                _ValueType =  ValueTypes.Integer;
                _ValueInteger = value;
            }
            get
            {
                if (_ValueType == ValueTypes.Integer)
                    return _ValueInteger;
                return 0;
            }
        }

        public string ValueString
        {
            set
            {
                _ValueType = ValueTypes.String;
                _ValueString = value;
            }
            get
            {
                if (_ValueType == ValueTypes.String)
                    return _ValueString;
                return null;
            }
        }

        public ValueTypes ValueType
        {
            get
            {
                return _ValueType;
            }
        }

        public override string ToString()
        {
            string value = (_ValueType == ValueTypes.None ? string.Empty : (_ValueType == ValueTypes.Integer ? _ValueInteger.ToString() : _ValueString));
            return string.Format("{0} ({1})", Type.ToString(), value);
        }

        public enum ValueTypes
        {
            None,
            Integer,
            String
        }
    }
}
