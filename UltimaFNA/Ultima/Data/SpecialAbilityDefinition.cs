﻿using System.Text;

namespace UltimaXNA.Ultima.Data
{
    public struct SpecialAbilityDefinition
	{
        public static SpecialAbilityDefinition EmptySpecialAbility = new SpecialAbilityDefinition();

        public readonly string Name;
        public readonly int ID;
        public readonly int GumpIconID;
		public readonly int DescClilocID;
		public readonly int NameClilocID;
		public readonly int[] WeaponID;

		public SpecialAbilityDefinition(string name, int index, int gumpIconID, int nameClilocID, int descriptionClilocID, params int[] weaponID)
		{
			Name = name;
			ID = index;
			GumpIconID = gumpIconID;
			DescClilocID = descriptionClilocID;
			NameClilocID = nameClilocID;
			WeaponID = weaponID;
		}
    }
}
