﻿/***************************************************************************
 *   Features.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

namespace UltimaXNA.Ultima.Data
{
    public class Features
    {
        FeatureFlags _Flags;
        public void SetFlags(FeatureFlags flags)
        {
            _Flags |= flags;
        }

        public FeatureFlags Flags => _Flags;

        public bool T2A => _Flags.HasFlag(FeatureFlags.TheSecondAge);
        public bool UOR => _Flags.HasFlag(FeatureFlags.Renaissance);
        public bool ThirdDawn => _Flags.HasFlag(FeatureFlags.ThirdDawn);
        public bool LBR => _Flags.HasFlag(FeatureFlags.LordBlackthornsRevenge);
        public bool AOS => _Flags.HasFlag(FeatureFlags.AgeOfShadows);
        public bool CharSlots6 => _Flags.HasFlag(FeatureFlags.CharacterSlot6);
        public bool SE => _Flags.HasFlag(FeatureFlags.SameraiEmpire);
        public bool ML => _Flags.HasFlag(FeatureFlags.MondainsLegacy);
        public bool Splash8th => _Flags.HasFlag(FeatureFlags.Splash8);
        public bool Splash9th => _Flags.HasFlag(FeatureFlags.Splash9);
        public bool TenthAge => _Flags.HasFlag(FeatureFlags.TenthAge);
        public bool MoreStorage => _Flags.HasFlag(FeatureFlags.MoreStorage);
        public bool CharSlots7 => _Flags.HasFlag(FeatureFlags.TheSecondAge);
        public bool TenthAgeFaces => _Flags.HasFlag(FeatureFlags.TenthAgeFaces);
        public bool TrialAccount => _Flags.HasFlag(FeatureFlags.TrialAccount);
        public bool EleventhAge => _Flags.HasFlag(FeatureFlags.EleventhAge);
        public bool SA => _Flags.HasFlag(FeatureFlags.StygianAbyss);

        public bool TooltipsEnabled => AOS;
    }
}