﻿using System.Diagnostics;
using System.IO;
using UltimaXNA.Ultima.IO;

namespace UltimaXNA.Ultima.Data {

    public enum ClientVersionTypes
    {
        NewSpellbook = 0x00000001,
        DamagePacket = 0x00000002,
        Unpack = 0x00000004,
        BuffIcon = 0x00000008,
        NewHaven = 0x00000010,
        ContainerGridLines = 0x00000020,
        ExtendedSupportedFeatures = 0x00000040,
        StygianAbyss = 0x00000080,
        HighSeas = 0x00000100,
        NewCharacterList = 0x00000200,
        NewCharacterCreation = 0x00000400,
        ExtendedStatus = 0x00000800,
        NewMobileIncoming = 0x00001000,
        NewSecureTrading = 0x00002000,

        Version400a = NewSpellbook,
        Version407a = Version400a | DamagePacket,
        Version500a = Version407a | Unpack,
        Version502b = Version500a | BuffIcon,
        Version6000 = Version502b | NewHaven,
        Version6017 = Version6000 | ContainerGridLines,
        Version60142 = Version6017 | ExtendedSupportedFeatures,
        Version7000 = Version60142 | StygianAbyss,
        Version7090 = Version7000 | HighSeas,
        Version70130 = Version7090 | NewCharacterList,
        Version70160 = Version70130 | NewCharacterCreation,
        Version70300 = Version70160 | ExtendedStatus,
        Version70331 = Version70300 | NewMobileIncoming,
        Version704565 = Version70331 | NewSecureTrading
    }


    public static class ClientVersion {
        // NOTE FROM ZaneDubya: DO NOT change DefaultVersion from 6.0.6.2.
        // We are focusing our efforts on getting a specific version of the client working.
        // Once we have this version working, we will attempt to support additional versions.
        // We will not support any issues you experience after changing this value.
        public static readonly byte[] DefaultVersion = { 7, 0, 35, 23 };

        static readonly byte[] _UnknownClientVersion = { 0, 0, 0, 0 };
        static readonly byte[] _ExtendedAddItemToContainer = { 6, 0, 1, 7 };
        static readonly byte[] _ExtendedFeaturesVersion = { 6, 0, 14, 2 };
        static readonly byte[] _ConvertedToUOPVersion = { 7, 0, 24, 0 };
        static byte[] _ClientExeVersion;


        public static bool NewSpellbook => ((_clientinfo & ClientVersionTypes.NewSpellbook) != 0);
        public static bool DamagePacket => ((_clientinfo & ClientVersionTypes.DamagePacket) != 0);
        public static bool Unpack => ((_clientinfo & ClientVersionTypes.Unpack) != 0);
        public static bool BuffIcon => ((_clientinfo & ClientVersionTypes.BuffIcon) != 0);
        public static bool NewHaven => ((_clientinfo & ClientVersionTypes.NewHaven) != 0);
        public static bool ContainerGridLines => ((_clientinfo & ClientVersionTypes.ContainerGridLines) != 0);
        public static bool ExtendedSupportedFeatures => ((_clientinfo & ClientVersionTypes.ExtendedSupportedFeatures) != 0);
        public static bool StygianAbyss => ((_clientinfo & ClientVersionTypes.StygianAbyss) != 0);
        public static bool HighSeas => ((_clientinfo & ClientVersionTypes.HighSeas) != 0);
        public static bool NewCharacterList => ((_clientinfo & ClientVersionTypes.NewCharacterList) != 0);
        public static bool NewCharacterCreation => ((_clientinfo & ClientVersionTypes.NewCharacterCreation) != 0);
        public static bool ExtendedStatus => ((_clientinfo & ClientVersionTypes.ExtendedStatus) != 0);
        public static bool NewMobileIncoming => ((_clientinfo & ClientVersionTypes.NewMobileIncoming) != 0);
        public static bool NewSecureTrading => ((_clientinfo & ClientVersionTypes.NewSecureTrading) != 0);

        public static byte[] ClientExe
        {
            get
            {
                if (_ClientExeVersion == null)
                {
                    string path = FileManager.GetPath("client.exe");
                    if (File.Exists(path))
                    {
                        FileVersionInfo exe = FileVersionInfo.GetVersionInfo(path);
                        _ClientExeVersion = new byte[]
                        {
                            (byte)exe.FileMajorPart, (byte)exe.FileMinorPart,
                            (byte)exe.FileBuildPart, (byte)exe.FilePrivatePart
                        };

                        if (exe.FileMajorPart == 7)
                        {
                            if (exe.FileBuildPart >= 45 && exe.FilePrivatePart >= 65)
                                _clientinfo = ClientVersionTypes.Version704565;
                            else if (exe.FileBuildPart >= 33 && exe.FilePrivatePart >= 1 && exe.FileBuildPart <= 45 && exe.FilePrivatePart < 65)
                                _clientinfo = ClientVersionTypes.Version70331;
                            else if (exe.FileBuildPart >= 30 && exe.FilePrivatePart >= 0 && exe.FileBuildPart <= 33 && exe.FilePrivatePart < 1)
                                _clientinfo = ClientVersionTypes.Version70300;
                            else if (exe.FileBuildPart >= 16 && exe.FilePrivatePart >= 0 && exe.FileBuildPart < 30)
                                _clientinfo = ClientVersionTypes.Version70160;
                            else if (exe.FileBuildPart >= 13 && exe.FilePrivatePart >= 0 && exe.FileBuildPart < 16)
                                _clientinfo = ClientVersionTypes.Version70130;
                            else if (exe.FileBuildPart >= 9 && exe.FilePrivatePart >= 0 && exe.FileBuildPart < 13)
                                _clientinfo = ClientVersionTypes.Version7090;
                            else if (exe.FileBuildPart >= 0 && exe.FilePrivatePart >= 0 && exe.FileBuildPart < 9)
                                _clientinfo = ClientVersionTypes.Version7000;
                        }
                        else if (exe.FileMajorPart == 6)
                        {
                            if (exe.FileBuildPart >= 14 && exe.FilePrivatePart >= 2)
                                _clientinfo = ClientVersionTypes.Version60142;
                            else if (exe.FileBuildPart >= 1 && exe.FilePrivatePart >= 7 && exe.FileBuildPart <= 14 && exe.FilePrivatePart < 2)
                                _clientinfo = ClientVersionTypes.Version6017;
                            else if (exe.FileBuildPart >= 0 && exe.FilePrivatePart >= 0 && exe.FileBuildPart <= 1 && exe.FilePrivatePart < 7)
                                _clientinfo = ClientVersionTypes.Version6000;
                        }
                       /* else if (exe.FileMajorPart == 5 && exe.FileMinorPart == 0)
                        {
                            if (exe.FileBuildPart >= 2 && patch == "b")
                                _clientinfo = ClientVersionTypes.Version502b;
                            else if (exe.FileBuildPart == 0 && patch == "a")
                                _clientinfo = ClientVersionTypes.Version500a;
                        }
                        else if (exe.FileMajorPart == 4 && exe.FileMinorPart == 0)
                        {
                            if (exe.FileBuildPart == 7 && patch == "a")
                                _clientinfo = ClientVersionTypes.Version407a;
                            else if (exe.FileBuildPart == 0 && patch == "a")
                                _clientinfo = ClientVersionTypes.Version400a;
                        }*/
                        else
                            throw new System.Exception("Wrong version");
                    }
                    else
                    {
                        _ClientExeVersion = _UnknownClientVersion;
                    }
                }
                return _ClientExeVersion;
            }
        }

        private static ClientVersionTypes _clientinfo;

        public static ClientVersionTypes ClientInfo => _clientinfo;

        public static bool InstallationIsUopFormat => GreaterThanOrEqualTo(ClientExe, _ConvertedToUOPVersion);

        public static bool HasExtendedFeatures(byte[] version) => GreaterThanOrEqualTo(version, _ExtendedFeaturesVersion);

        public static bool HasExtendedAddItemPacket(byte[] version) => GreaterThanOrEqualTo(version, _ExtendedAddItemToContainer);

        public static bool EqualTo(byte[] a, byte[] b) {
            if (a == null || b == null) {
                return false;
            }
            if (a.Length != b.Length) {
                return false;
            }
            int index = 0;
            while (index < a.Length) {
                if (a[index] != b[index]) {
                    return false;
                }
                index++;
            }
            return true;
        }

        /// <summary> Compare two arrays of equal size. Returns true if first parameter array is greater than or equal to second. </summary>
        static bool GreaterThanOrEqualTo(byte[] a, byte[] b) {
            if (a == null || b == null) {
                return false;
            }
            if (a.Length != b.Length) {
                return false;
            }
            int index = 0;
            while (index < a.Length) {
                if (a[index] > b[index]) {
                    return true;
                }
                if (a[index] < b[index]) {
                    return false;
                }
                index++;
            }
            return true;
        }
    }
}