﻿/***************************************************************************
 *   ContextMenuData.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using System.Collections.Generic;

namespace UltimaXNA.Ultima.Data {
    public class ContextMenuData {
        readonly List<ContextMenuItem> _Entries = new List<ContextMenuItem>();
        readonly Serial _Serial;

        public ContextMenuData(Serial serial) {
            _Serial = serial;
        }

        public Serial Serial => _Serial;

        public int Count => _Entries.Count;

        public ContextMenuItem this[int index] {
            get {
                if (index < 0 || index >= _Entries.Count)
                    return null;
                return _Entries[index];
            }
        }

        // Add a new context menu entry.
        internal void AddItem(int responseCode, int stringID, int flags, int hue) {
            _Entries.Add(new ContextMenuItem(responseCode, stringID, flags, hue));
        }
    }
}