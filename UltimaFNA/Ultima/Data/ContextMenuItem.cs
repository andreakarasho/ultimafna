﻿/***************************************************************************
 *   ContextMenu.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
 
using UltimaXNA.Core.Resources;

namespace UltimaXNA.Ultima.Data
{
    public class ContextMenuItem
    {
        readonly string _Caption;
        readonly int _ResponseCode;
		readonly int _Flags;

		public ContextMenuItem(int responseCode, int stringID, int flags, int hue)
        {
            // get the resource provider
            IResourceProvider provider = Service.Get<IResourceProvider>();
            _Caption = provider.GetString(stringID);
            _ResponseCode = responseCode;
			_Flags = flags;
		}

        public int ResponseCode => _ResponseCode;

        public string Caption => _Caption;

		public int Flags => _Flags;

		public override string ToString()
        {
            return string.Format("{0} [{1}]", _Caption, _ResponseCode);
        }
    }
}
