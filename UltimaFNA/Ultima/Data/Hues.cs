﻿namespace UltimaXNA.Ultima.Data
{
    static class Hues
    {
        public class Human
        {
            public static int[] SkinHue
            {
                get
                {
                    int max = 7 * 8;
                    int[] hues = new int[max];
                    for (int i = 0; i < max; i++)
                        hues[i] = (i < 37) ? i + 1002 : i + 1003;
                    return hues;
                }
            }

            public static int[] HairHues
            {
                get
                {
                    int max = 8 * 6;
                    int[] hues = new int[max];
                    for (int i = 0; i < max; i++)
                    {
                        hues[i] = i + 1102;
                    }
                    return hues;
                }
            }
        }

        public class Elf
        {
            private static readonly int[] _elfskintones = new int[] {
                    0x0BF, 0x24D, 0x24E, 0x24F, 0x353, 0x361, 0x367, 0x374,
                0x375, 0x376, 0x381, 0x382, 0x383, 0x384, 0x385, 0x389,
                0x3DE, 0x3E5, 0x3E6, 0x3E8, 0x3E9, 0x430, 0x4A7, 0x4DE,
                0x51D, 0x53F, 0x579, 0x76B, 0x76C, 0x76D, 0x835, 0x903};

            public static int[] SkinHues => _elfskintones;

            private static readonly int[] _elfHairHue = new int[]
            {
                0x034, 0x035, 0x036, 0x037, 0x038, 0x039, 0x058, 0x08E,
                0x08F, 0x090, 0x091, 0x092, 0x101, 0x159, 0x15A, 0x15B,
                0x15C, 0x15D, 0x15E, 0x128, 0x12F, 0x1BD, 0x1E4, 0x1F3,
                0x207, 0x211, 0x239, 0x251, 0x26C, 0x2C3, 0x2C9, 0x31D,
                0x31E, 0x31F, 0x320, 0x321, 0x322, 0x323, 0x324, 0x325,
                0x326, 0x369, 0x386, 0x387, 0x388, 0x389, 0x38A, 0x59D,
                0x6B8, 0x725, 0x853
            };

            public static int[] HairHues
            {
                get
                {
                    int max = 54;
                    int[] hues = new int[max];
                    for (int i = 0; i < max; i++)
                    {
                        if (i - _elfHairHue.Length >= 0)
                            hues[i] = _elfHairHue[_elfHairHue.Length - (max - i)];
                        else
                            hues[i] = _elfHairHue[i];
                    }
                    return hues;
                }
            }
        }

        public class Gargoyle
        {
            public static int[] SkinHues
            {
                get
                {
                    int max = 28;
                    int[] hues = new int[max];
                    for (int i = 0; i < max; i++)
                    {
                        if (max - i > 3)
                            hues[i] = i + 1755;
                        else
                            hues[i] = max - i + 1755;
                    }
                    return hues;
                }
            }

            private static readonly int[] _hornHues = new int[] { 0x709, 0x70B, 0x70D, 0x70F, 0x711, 0x763,
                0x765, 0x768, 0x76B, 0x6F3, 0x6F1, 0x6EF,
                0x6E4, 0x6E2, 0x6E0, 0x709, 0x70B, 0x70D};

            public static int[] HornHues => _hornHues;
        }

        public static int[] ClothesHues
        {
            get
            {
                int max = 1000;
                int[] hues = new int[max];
                for (int i = 0; i < max; i++)
                    hues[i] = i + 2;
                return hues;
            }
        }


        public static int[] TextTones
        {
            get
            {
                int max = 1024;
                int[] hues = new int[max];
                for (int i = 0; i < max; i++)
                {
                    hues[i] = i + 2;
                }
                return hues;
            }
        }
    }
}
