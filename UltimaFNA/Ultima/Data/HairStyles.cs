﻿/***************************************************************************
 *   HairStyles.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using UltimaXNA.Core.Resources;

namespace UltimaXNA.Ultima.Data
{
    class HairStyles
    {
        static readonly int[] m_maleStyles = { 3000340, 3000341, 3000342, 3000343, 3000344, 3000345, 3000346, 3000347, 3000348, 3000349 };
        static string[] m_male;
        public static string[] MaleHairNames
        {
            get
            {
                if (m_male == null)
                {
                    m_male = new string[m_maleStyles.Length];
                    for (int i = 0; i < m_maleStyles.Length; i++)
                    {
                        m_male[i] = Service.Get<IResourceProvider>().GetString(m_maleStyles[i]);
                        if (m_male[i] == "Pigtails")
                            m_male[i] = "2 Tails";
                    }
                }
                return m_male;
            }
        }
        static readonly int[] m_maleIDs = { 0, 8251, 8252, 8253, 8260, 8261, 8266, 8263, 8264, 8265 };
        public static int[] MaleIDs
        {
            get
            {
                return m_maleIDs;
            }
        }
        static readonly int[] m_maleIDsForCreation = { 0, 1875, 1876, 1879, 1877, 1871, 1874, 1873, 1880, 1870 };
        public static int MaleGumpIDForCharacterCreationFromItemID(int id)
        {
            int gumpID = 0;
            for (int i = 0; i < m_maleIDsForCreation.Length; i++)
                if (m_maleIDs[i] == id)
                    gumpID = m_maleIDsForCreation[i];
            return gumpID;
        }


        static readonly int[] m_facialStyles = { 3000340, 3000351, 3000352, 3000353, 3000354, 1011060, 1011061, 3000357 };
        static string[] m_facial;
        public static string[] FacialHair
        {
            get
            {
                if (m_facial == null)
                {
                    m_facial = new string[m_facialStyles.Length];
                    for (int i = 0; i < m_facialStyles.Length; i++)
                    {
                        m_facial[i] = Service.Get<IResourceProvider>().GetString(m_facialStyles[i]);
                    }
                }
                return m_facial;
            }
        }
        static readonly int[] m_facialIDs = { 0, 8256, 8254, 8255, 8257, 8267, 8268, 8269 };
        public static int[] FacialHairIDs
        {
            get
            {
                return m_facialIDs;
            }
        }
        static readonly int[] m_facialGumpIDsForCreation = { 0, 1881, 1883, 1885, 1884, 1886, 1882, 1887 };
        public static int FacialHairGumpIDForCharacterCreationFromItemID(int id)
        {
            int gumpID = 0;
            for (int i = 0; i < m_facialGumpIDsForCreation.Length; i++)
                if (m_facialIDs[i] == id)
                    gumpID = m_facialGumpIDsForCreation[i];
            return gumpID;
        }

        static readonly int[] m_femaleStyles = { 3000340, 3000341, 3000342, 3000343, 3000344, 3000345, 3000346, 3000347, 3000349, 3000350 };
        static string[] m_female;
        public static string[] FemaleHairNames
        {
            get
            {
                if (m_female == null)
                {
                    m_female = new string[m_femaleStyles.Length];
                    for (int i = 0; i < m_femaleStyles.Length; i++)
                    {
                        m_female[i] = Service.Get<IResourceProvider>().GetString(m_femaleStyles[i]);
                    }
                }
                return m_female;
            }
        }
        static readonly int[] m_femaleIDs = { 0, 8251, 8252, 8253, 8260, 8261, 8266, 8263, 8265, 8262 };
        public static int[] FemaleIDs
        {
            get
            {
                return m_femaleIDs;
            }
        }
        static readonly int[] m_femaleIDsForCreation = { 0, 1847, 1842, 1845, 1843, 1844, 1840, 1839, 1836, 1841 };
        public static int FemaleGumpIDForCharacterCreationFromItemID(int id)
        {
            int gumpID = 0;
            for (int i = 0; i < m_femaleIDsForCreation.Length; i++)
                if (m_femaleIDs[i] == id)
                    gumpID = m_femaleIDsForCreation[i];
            return gumpID;
        }

        // gargoyle
        private static readonly int[] _gargoyleMaleHairs = { 0, 0x76C, 0x76D, 0x773, 0x76E, 0x774, 0x775, 0x776, 0x777 };
        private static readonly int[] _gargoyleMaleBeards = { 0, 0x76F, 0x770, 0x771, 0x772 };
        private static readonly int[] _gargoyleFemaleHairs = { 0, 0x7A0, 0x7A1, 0x79E, 0x7A2, 0x79F, 0x77C, 0x77D, 0x77E };

        public static int[] GargoyleMaleHairs => _gargoyleMaleHairs;
        public static int[] GargoyleMaleBeards => _gargoyleMaleBeards;
        public static int[] GargoyleFemaleHairs => _gargoyleFemaleHairs;

        public static string[] GargoyleMaleFemaleHairsName
        {
            get
            {
                string[] _gargoyleMaleFemaleHairsName = new string[9];
                _gargoyleMaleFemaleHairsName[0] = Service.Get<IResourceProvider>().GetString(1112331);
                for (int i = 1; i < 9; i++)
                    _gargoyleMaleFemaleHairsName[i] = Service.Get<IResourceProvider>().GetString(1112309 + i);
                return _gargoyleMaleFemaleHairsName;
            }
        }

        public static string[] GargoyleMaleBeardsName
        {
            get
            {
                string[] _gargoyleMaleFemaleHairsName = new string[5];
                _gargoyleMaleFemaleHairsName[0] = Service.Get<IResourceProvider>().GetString(1112331);
                for (int i = 1; i < 5; i++)
                    _gargoyleMaleFemaleHairsName[i] = Service.Get<IResourceProvider>().GetString(1112309 + i);
                return _gargoyleMaleFemaleHairsName;
            }
        }

        public static int GetMaleGargoyleHairs(int id)
        {
            int gumpID = 0;
            for (int i = 0; i < _gargoyleMaleHairs.Length; i++)
                if (_gargoyleMaleHairs[i] == id)
                    gumpID = _gargoyleMaleHairs[i];
            return gumpID;
        }
        public static int GetMaleGargoyleBeards(int id)
        {
            int gumpID = 0;
            for (int i = 0; i < _gargoyleMaleBeards.Length; i++)
                if (_gargoyleMaleBeards[i] == id)
                    gumpID = _gargoyleMaleBeards[i];
            return gumpID;
        }
        public static int GetFemaleGargoyleHairs(int id)
        {
            int gumpID = 0;
            for (int i = 0; i < _gargoyleFemaleHairs.Length; i++)
                if (_gargoyleFemaleHairs[i] == id)
                    gumpID = _gargoyleFemaleHairs[i];
            return gumpID;
        }

        //elf
        private static readonly int[] _maleElfHairs = new int[] { 0, 0x06F8, 0x06F9, 0x06FA, 0x06FB, 0x06FD, 0x06FE, 0x06FF, 0x0701 };
        private static readonly int[] _femaleElfHairs = new int[] { 0, 0x06EF, 0x06F0, 0x06F1, 0x06F2, 0x06F4, 0x06F5, 0x06F6, 0x06F7 };

        public static int[] ElfMaleHairs => _maleElfHairs;
        public static int[] ElfFemaleHairs => _femaleElfHairs;


        private static readonly string[] _maleElfHairsName = { Service.Get<IResourceProvider>().GetString(1112331),
                                Service.Get<IResourceProvider>().GetString(1074385), // "Mid Long" ,
                                Service.Get<IResourceProvider>().GetString(1074386), // "Long Feather",
                                Service.Get<IResourceProvider>().GetString(1074387), // "Short" ,
                                Service.Get<IResourceProvider>().GetString(1074388), // "Mullet" ,
                                Service.Get<IResourceProvider>().GetString(1074390), // "Long" ,
                                Service.Get<IResourceProvider>().GetString(1074391), // "Topknot" ,
                                Service.Get<IResourceProvider>().GetString(1074392), // "Long Braid",
                                Service.Get<IResourceProvider>().GetString(1074394) };// "Spiked" 

        public static string[] MaleElfHairsName => _maleElfHairsName;


        private static readonly string[] _femaleElfHairsName = { Service.Get<IResourceProvider>().GetString(1112331),
                                Service.Get<IResourceProvider>().GetString(1074386), // "Long Feather" ,
                                Service.Get<IResourceProvider>().GetString(1074387), // "Short" ,
                                Service.Get<IResourceProvider>().GetString(1074388), // "Mullet" ,
                                Service.Get<IResourceProvider>().GetString(1074389), //  "Flower" ,
                                Service.Get<IResourceProvider>().GetString(1074391), // "Topknot" ,
                                Service.Get<IResourceProvider>().GetString(1074392), // "Long Braid",
                                Service.Get<IResourceProvider>().GetString(1074393), // "Buns" ,
                                Service.Get<IResourceProvider>().GetString(1074394) }; //  "Spiked" 

        public static string[] FemaleElfHairsName => _femaleElfHairsName;

        public static int GetMaleElfHairs(int id)
        {
            int gumpID = 0;
            for (int i = 0; i < _maleElfHairs.Length; i++)
                if (_maleElfHairs[i] == id)
                    gumpID = _maleElfHairs[i];
            return gumpID;
        }
        public static int GetFemalElfHairs(int id)
        {
            int gumpID = 0;
            for (int i = 0; i < _femaleElfHairs.Length; i++)
                if (_femaleElfHairs[i] == id)
                    gumpID = _femaleElfHairs[i];
            return gumpID;
        }
    }

    /* class HairStyles
     {
         static readonly int[] _maleStyles = { 3000340, 3000341, 3000342, 3000343, 3000344, 3000345, 3000346, 3000347, 3000348, 3000349 };
         static string[] _male;
         public static string[] MaleHairNames
         {
             get
             {
                 if (_male == null)
                 {
                     // get the resource provider
                     IResourceProvider provider = Service.Get<IResourceProvider>();

                     _male = new string[_maleStyles.Length];
                     for (int i = 0; i < _maleStyles.Length; i++)
                     {
                         _male[i] = provider.GetString(_maleStyles[i]);
                         if (_male[i] == "Pigtails")
                             _male[i] = "2 Tails";
                     }
                 }
                 return _male;
             }
         }
         static readonly int[] _maleIDs = { 0, 8251, 8252, 8253, 8260, 8261, 8266, 8263, 8264, 8265 };
         public static int[] MaleIDs
         {
             get
             {
                 return _maleIDs;
             }
         }
         static readonly int[] _maleIDsForCreation = { 0, 1875, 1876, 1879, 1877, 1871, 1874, 1873, 1880, 1870 };
         public static int MaleGumpIDForCharacterCreationFromItemID(int id)
         {
             int gumpID = 0;
             for (int i = 0; i < _maleIDsForCreation.Length; i++)
                 if (_maleIDs[i] == id)
                     gumpID = _maleIDsForCreation[i];
             return gumpID;
         }


         static readonly int[] _facialStyles = { 3000340, 3000351, 3000352, 3000353, 3000354, 1011060, 1011061, 3000357 };
         static string[] _facial;
         public static string[] FacialHair
         {
             get
             {
                 if (_facial == null)
                 {
                     // get the resource provider
                     IResourceProvider provider = Service.Get<IResourceProvider>();

                     _facial = new string[_facialStyles.Length];
                     for (int i = 0; i < _facialStyles.Length; i++)
                     {
                         _facial[i] = provider.GetString(_facialStyles[i]);
                     }
                 }
                 return _facial;
             }
         }
         static readonly int[] _facialIDs = { 0, 8256, 8254, 8255, 8257, 8267, 8268, 8269 };
         public static int[] FacialHairIDs
         {
             get
             {
                 return _facialIDs;
             }
         }
         static readonly int[] _facialGumpIDsForCreation = { 0, 1881, 1883, 1885, 1884, 1886, 1882, 1887 };
         public static int FacialHairGumpIDForCharacterCreationFromItemID(int id)
         {
             int gumpID = 0;
             for (int i = 0; i < _facialGumpIDsForCreation.Length; i++)
                 if (_facialIDs[i] == id)
                     gumpID = _facialGumpIDsForCreation[i];
             return gumpID;
         }

         static readonly int[] _femaleStyles = { 3000340, 3000341, 3000342, 3000343, 3000344, 3000345, 3000346, 3000347, 3000349, 3000350 };
         static string[] _female;
         public static string[] FemaleHairNames
         {
             get
             {
                 if (_female == null)
                 {
                     // get the resource provider
                     IResourceProvider provider = Service.Get<IResourceProvider>();

                     _female = new string[_femaleStyles.Length];
                     for (int i = 0; i < _femaleStyles.Length; i++)
                     {
                         _female[i] = provider.GetString(_femaleStyles[i]);
                     }
                 }
                 return _female;
             }
         }
         static readonly int[] _femaleIDs = { 0, 8251, 8252, 8253, 8260, 8261, 8266, 8263, 8265, 8262 };
         public static int[] FemaleIDs
         {
             get
             {
                 return _femaleIDs;
             }
         }
         static readonly int[] _femaleIDsForCreation = { 0, 1847, 1842, 1845, 1843, 1844, 1840, 1839, 1836, 1841 };
         public static int FemaleGumpIDForCharacterCreationFromItemID(int id)
         {
             int gumpID = 0;
             for (int i = 0; i < _femaleIDsForCreation.Length; i++)
                 if (_femaleIDs[i] == id)
                     gumpID = _femaleIDsForCreation[i];
             return gumpID;
         }
     }*/
}