﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace UltimaXNA.Ultima.Data
{
    static class SpellsBushido
	{
        static Dictionary<int, SpellDefinition> s_Spells;
        static ReadOnlyCollection<SpellDefinition> s_ReadOnlySpells;

        public static ReadOnlyCollection<SpellDefinition> Spells
        {
            get
            {
                if (s_ReadOnlySpells == null)
                {
                    List<SpellDefinition> spells = new List<SpellDefinition>();
                    for (int i = 1; i <= 6; i++)
                        spells.Add(s_Spells[i]);
                    s_ReadOnlySpells = new ReadOnlyCollection<SpellDefinition>(spells);
                }
                return s_ReadOnlySpells;
            }
        }

        public static SpellDefinition GetSpell(int spellIndex)
        {
            SpellDefinition spell;
            if (s_Spells.TryGetValue(spellIndex, out spell))
                return spell;

            return SpellDefinition.EmptySpell;
        }

        static SpellsBushido()
        {
            s_Spells = new Dictionary<int, SpellDefinition>()
            {
                // Spell List
                { 1, new SpellDefinition("Honorable Execution", 401, 0x5420, string.Empty, 0, 25, Reagents.None) },
				{ 2, new SpellDefinition("Confidence", 402, 0x5421, string.Empty, 10, 25, Reagents.None) },
				{ 3, new SpellDefinition("Evasion", 403, 0x5422, string.Empty, 10, 60, Reagents.None) },
				{ 4, new SpellDefinition("Counter Attack", 404, 0x5423, string.Empty, 5, 40, Reagents.None) },
				{ 5, new SpellDefinition("Lightning Strike", 405, 0x5424, string.Empty, 10, 50, Reagents.None) },
				{ 6, new SpellDefinition("Momentum Strike", 406, 0x5425, string.Empty, 10, 70, Reagents.None) }
			};
        }
    }
}
