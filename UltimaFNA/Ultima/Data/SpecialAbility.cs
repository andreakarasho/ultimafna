﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace UltimaXNA.Ultima.Data
{
    static class SpecialAbility
	{
        static Dictionary<int, SpecialAbilityDefinition> s_Abilities;
		static Dictionary<int, int[]> s_WeaponAbilities;
		static ReadOnlyCollection<SpecialAbilityDefinition> s_ReadOnlyAbilities;

        public static ReadOnlyCollection<SpecialAbilityDefinition> Abilities
        {
            get
            {
                if (s_ReadOnlyAbilities == null)
                {
                    List<SpecialAbilityDefinition> abilities = new List<SpecialAbilityDefinition>();
                    for (int i = 1; i <= 32; i++)
						abilities.Add(s_Abilities[i]);
					s_ReadOnlyAbilities = new ReadOnlyCollection<SpecialAbilityDefinition>(abilities);
                }
                return s_ReadOnlyAbilities;
            }
        }

        public static SpecialAbilityDefinition GetAbility(int abilityindex)
        {
			SpecialAbilityDefinition ability;
            if (s_Abilities.TryGetValue(abilityindex, out ability))
                return ability;

            return SpecialAbilityDefinition.EmptySpecialAbility;
        }

		public static SpecialAbilityDefinition GetWeaponPrimaryAbility(int itemID)
		{
			if (s_WeaponAbilities.ContainsKey(itemID))
				return GetAbility(s_WeaponAbilities[itemID][0]);

			return GetAbility(5); // Se non esiste in lista torna Disarm
		}

		public static SpecialAbilityDefinition GetWeaponSecondaryAbility(int itemID)
		{
			if (s_WeaponAbilities.ContainsKey(itemID))
				return GetAbility(s_WeaponAbilities[itemID][1]);

			return GetAbility(11); // Se non esiste in lista torna Paralyzing Blow
		}

		static SpecialAbility()
        {
			s_WeaponAbilities = new Dictionary<int, int[]>()  // Chiave ID arma, Array pozione 0 primay ability ID, 1 secondary ability ID.
			{
				{ 0x0901, new int[2]{ 10, 30 } }, // Gargish Cyclone
				{ 0x0902, new int[2]{ 8, 12 } }, // Gargish Dagger
				{ 0x0905, new int[2]{ 7, 9 } }, // Glass Staff
				{ 0x0906, new int[2]{ 4, 6 } }, // serpentstone staff
				{ 0x090C, new int[2]{ 2, 9 } }, // Glass Sword
				{ 0x0DF0, new int[2]{ 13, 11 } }, // Black Staves
				{ 0x0DF1, new int[2]{ 13, 11 } }, // Black Staves
				{ 0x0DF2, new int[2]{ 6, 5 } }, // Wands Type A-D
				{ 0x0DF3, new int[2]{ 6, 5 } }, // Wands Type A-D
				{ 0x0DF4, new int[2]{ 6, 5 } }, // Wands Type A-D
				{ 0x0DF5, new int[2]{ 6, 5 } }, // Wands Type A-D
				{ 0x0E81, new int[2]{ 4, 5 } }, // Shepherd's Crooks
				{ 0x0E82, new int[2]{ 4, 5 } }, // Shepherd's Crooks
				{ 0x0E85, new int[2]{ 7, 5 } }, // Pickaxes
				{ 0x0E86, new int[2]{ 7, 5 } }, // Pickaxes
				{ 0x0E87, new int[2]{ 2, 6 } }, // Pitchforks
				{ 0x0E88, new int[2]{ 2, 6 } }, // Pitchforks
				{ 0x0E89, new int[2]{ 7, 3 } }, // Quarter Staves
				{ 0x0E8A, new int[2]{ 7, 3 } }, // Quarter Staves
				{ 0x0EC2, new int[2]{ 2, 8 } }, // Cleavers
				{ 0x0EC3, new int[2]{ 2, 8 } }, // Cleavers
				{ 0x0EC4, new int[2]{ 12, 5 } }, // Skinning Knives
				{ 0x0EC5, new int[2]{ 12, 5 } }, // Skinning Knives
				{ 0x0F43, new int[2]{ 1, 5 } }, // Hatchets
				{ 0x0F44, new int[2]{ 1, 5 } }, // Hatchets
				{ 0x0F45, new int[2]{ 2, 9 } }, // Double Axes
				{ 0x0F46, new int[2]{ 2, 9 } }, // Double Axes
				{ 0x0F47, new int[2]{ 2, 3 } }, // Battle Axes
				{ 0x0F48, new int[2]{ 2, 3 } }, // Battle Axes
				{ 0x0F49, new int[2]{ 3, 6 } }, // Axes
				{ 0x0F4A, new int[2]{ 3, 6 } }, // Axes
				{ 0x0F4B, new int[2]{ 7, 13 } }, // ??
				{ 0x0F4C, new int[2]{ 7, 13 } }, // ??
				{ 0x0F4D, new int[2]{ 11, 6 } }, // Bardiches
				{ 0x0F4E, new int[2]{ 11, 6 } }, // Bardiches
				{ 0x0F4F, new int[2]{ 3, 9 } }, // Crossbows
				{ 0x0F50, new int[2]{ 3, 9 } }, // Crossbows
				{ 0x0F51, new int[2]{ 8, 12 } }, // Daggers
				{ 0x0F52, new int[2]{ 8, 12 } }, // Daggers
				{ 0x0F5C, new int[2]{ 3, 5 } }, // Maces
				{ 0x0F5D, new int[2]{ 3, 5 } }, // Maces
				{ 0x0F5E, new int[2]{ 4, 1 } }, // Broadswords
				{ 0x0F5F, new int[2]{ 4, 1 } }, // Broadswords
				{ 0x0F60, new int[2]{ 1, 3 } }, // Longswords
				{ 0x0F61, new int[2]{ 1, 3 } }, // Longswords
				{ 0x0F62, new int[2]{ 1, 11 } }, // Spears
				{ 0x0F63, new int[2]{ 1, 11 } }, // Spears
				{ 0x0FB5, new int[2]{ 3, 12 } }, // ??
				{ 0x13AF, new int[2]{ 1, 2 } }, // War Axes
				{ 0x13B0, new int[2]{ 1, 2 } }, // War Axes
				{ 0x13B1, new int[2]{ 11, 9 } }, // Bows
				{ 0x13B2, new int[2]{ 11, 9 } }, // Bows
				{ 0x13B3, new int[2]{ 12, 6 } }, // Clubs
				{ 0x13B4, new int[2]{ 12, 6 } }, // Clubs
				{ 0x13B7, new int[2]{ 7, 11 } }, // Scimitars
				{ 0x13B8, new int[2]{ 7, 11 } }, // Scimitars
				{ 0x13B9, new int[2]{ 11, 4 } }, // Viking Swords
				{ 0x13BA, new int[2]{ 11, 4 } }, // Viking Swords
				{ 0x13FD, new int[2]{ 10, 6 } }, // Heavy Crossbows
				{ 0x13E3, new int[2]{ 4, 12 } }, // Smith's Hammers
				{ 0x13F6, new int[2]{ 8, 5 } }, // Butcher Knives
				{ 0x13F8, new int[2]{ 3, 11 } }, // Gnarled Staves
				{ 0x13FB, new int[2]{ 13, 2 } }, // Large Battle Axes
				{ 0x13FF, new int[2]{ 7, 1 } }, // Katana
				{ 0x1401, new int[2]{ 1, 8 } }, // Kryss
				{ 0x1402, new int[2]{ 12, 9 } }, // Short Spears
				{ 0x1403, new int[2]{ 12, 9 } }, // Short Spears
				{ 0x1404, new int[2]{ 2, 5 } }, // War Forks
				{ 0x1405, new int[2]{ 2, 5 } }, // War Forks
				{ 0x1406, new int[2]{ 4, 2 } }, // War Maces
				{ 0x1407, new int[2]{ 4, 2 } }, // War Maces
				{ 0x1438, new int[2]{ 13, 4 } }, // War Hammers
				{ 0x1439, new int[2]{ 13, 4 } }, // War Hammers
				{ 0x143A, new int[2]{ 4, 3 } }, // Mauls
				{ 0x143B, new int[2]{ 4, 3 } }, // Mauls
				{ 0x143C, new int[2]{ 1, 9 } }, // Hammer Picks
				{ 0x143D, new int[2]{ 1, 9 } }, // Hammer Picks
				{ 0x143E, new int[2]{ 13, 3 } }, // Halberds
				{ 0x143F, new int[2]{ 13, 3 } }, // Halberds
				{ 0x1440, new int[2]{ 2, 12 } }, // Cutlasses
				{ 0x1441, new int[2]{ 2, 12 } }, // Cutlasses
				{ 0x1442, new int[2]{ 7, 12 } }, // Two Handed Axes
				{ 0x1443, new int[2]{ 7, 12 } }, // Two Handed Axes
				{ 0x26BA, new int[2]{ 2, 11 } }, // Scythes
				{ 0x26BB, new int[2]{ 11, 9 } }, // Bone Harvesters
				{ 0x26BC, new int[2]{ 4, 9 } }, // Scepters
				{ 0x26BD, new int[2]{ 1, 6 } }, // Bladed Staves
				{ 0x26BE, new int[2]{ 11, 8 } }, // Pikes
				{ 0x26BF, new int[2]{ 7, 8 } }, // Double Bladed Staff
				{ 0x26C0, new int[2]{ 6, 3 } }, // Lances
				{ 0x26C1, new int[2]{ 7, 9 } }, // Crescent Blades
				{ 0x26C2, new int[2]{ 1, 10 } }, // Composite Bows
				{ 0x26C3, new int[2]{ 7, 10 } }, // Repeating Crossbows
				{ 0x26C4, new int[2]{ 2, 11 } }, // Scythes
				{ 0x26C5, new int[2]{ 11, 9 } }, // Bone Harvesters
				{ 0x26C6, new int[2]{ 4, 9 } }, // Scepters
				{ 0x26C7, new int[2]{ 1, 6 } }, // Bladed Staves
				{ 0x26C8, new int[2]{ 11, 8 } }, // Pikes
				{ 0x26C9, new int[2]{ 7, 8 } }, // Double Bladed Staff
				{ 0x26CA, new int[2]{ 6, 3 } }, // Lances
				{ 0x26CB, new int[2]{ 7, 9 } }, // Crescent Blades
				{ 0x26CC, new int[2]{ 1, 10 } }, // Composite Bows
				{ 0x26CD, new int[2]{ 7, 10 } }, // Repeating Crossbows
				{ 0x27A2, new int[2]{ 4, 14 } }, // No-Dachi
				{ 0x27ED, new int[2]{ 4, 14 } }, // No-Dachi
				{ 0x27A3, new int[2]{ 20, 16 } }, // Tessen
				{ 0x27EE, new int[2]{ 20, 16 } }, // Tessen
				{ 0x27A4, new int[2]{ 15, 7 } }, // Wakizashi
				{ 0x27EF, new int[2]{ 15, 7 } }, // Wakizashi
				{ 0x27A5, new int[2]{ 23, 22 } }, // Yumi
				{ 0x27F0, new int[2]{ 23, 22 } }, // Yumi
				{ 0x27A6, new int[2]{ 15, 4 } }, // Tetsubo
				{ 0x27F1, new int[2]{ 15, 4 } }, // Tetsubo
				{ 0x27A7, new int[2]{ 17, 15 } }, // Lajatang
				{ 0x27F2, new int[2]{ 17, 15 } }, // Lajatang
				{ 0x27A8, new int[2]{ 20, 18 } }, // Bokuto
				{ 0x27F3, new int[2]{ 20, 18 } }, // Bokuto
				{ 0x27A9, new int[2]{ 20, 7 } }, // Daisho
				{ 0x27F4, new int[2]{ 20, 7 } }, // Daisho
				{ 0x27AA, new int[2]{ 5, 11 } }, // Fukya
				{ 0x27F5, new int[2]{ 5, 11 } }, // Fukya
				{ 0x27AB, new int[2]{ 21, 19 } }, // Tekagi
				{ 0x27F6, new int[2]{ 21, 19 } }, // Tekagi
				{ 0x27AD, new int[2]{ 13, 17 } }, // Kama
				{ 0x27F8, new int[2]{ 13, 17 } }, // Kama
				{ 0x27AE, new int[2]{ 16, 20 } }, // Nunchaku
				{ 0x27F9, new int[2]{ 16, 20 } }, // Nunchaku
				{ 0x27AF, new int[2]{ 16, 23 } }, // Sai
				{ 0x27FA, new int[2]{ 16, 23 } }, // Sai
				{ 0x2D1E, new int[2]{ 25, 28 } }, // Elven Composite Longbows
				{ 0x2D2A, new int[2]{ 25, 28 } }, // Elven Composite Longbows
				{ 0x2D1F, new int[2]{ 26, 27 } }, // Magical Shortbows
				{ 0x2D2B, new int[2]{ 26, 27 } }, // Magical Shortbows
				{ 0x2D20, new int[2]{ 27, 2 } }, // Elven Spellblades
				{ 0x2D2C, new int[2]{ 27, 2 } }, // Elven Spellblades
				{ 0x2D21, new int[2]{ 8, 12 } }, // Assassin Spikes
				{ 0x2D2D, new int[2]{ 8, 12 } }, // Assassin Spikes
				{ 0x2D22, new int[2]{ 20, 1 } }, // Leafblades
				{ 0x2D2E, new int[2]{ 20, 1 } }, // Leafblades
				{ 0x2D23, new int[2]{ 5, 24 } }, // War Cleavers
				{ 0x2D2F, new int[2]{ 5, 24 } }, // War Cleavers
				{ 0x2D24, new int[2]{ 3, 4 } }, // Diamond Maces
				{ 0x2D30, new int[2]{ 3, 4 } }, // Diamond Maces
				{ 0x2D25, new int[2]{ 16, 29 } }, // Wild Staves
				{ 0x2D31, new int[2]{ 16, 29 } }, // Wild Staves
				{ 0x2D26, new int[2]{ 5, 24 } }, // Rune Blades
				{ 0x2D32, new int[2]{ 5, 24 } }, // Rune Blades
				{ 0x2D27, new int[2]{ 13, 24 } }, // Radiant Scimitars
				{ 0x2D33, new int[2]{ 13, 24 } }, // Radiant Scimitars
				{ 0x2D28, new int[2]{ 5, 4 } }, // Ornate Axes
				{ 0x2D34, new int[2]{ 5, 4 } }, // Ornate Axes
				{ 0x2D29, new int[2]{ 17, 24 } }, // Elven Machetes
				{ 0x2D35, new int[2]{ 17, 24 } }, // Elven Machetes
				{ 0x4067, new int[2]{ 31, 3 } }, // Boomerang
				{ 0x4068, new int[2]{ 7, 8 } }, // Dual Short Axes
				{ 0x406B, new int[2]{ 1, 9 } }, // Soul Glaive
				{ 0x406C, new int[2]{ 10, 30 } }, // Cyclone
				{ 0x406D, new int[2]{ 7, 5 } }, // Dual Pointed Spear
				{ 0x406E, new int[2]{ 1, 5 } }, // Disc Mace
				{ 0x4072, new int[2]{ 2, 11 } }, // Blood Blade
				{ 0x4074, new int[2]{ 4, 3 } }, // Dread Sword
				{ 0x4075, new int[2]{ 13, 6 } }, // Gargish Talwar
				{ 0x4076, new int[2]{ 1, 9 } }, // Shortblade
				{ 0x48AE, new int[2]{ 2, 8 } }, // Gargish Cleaver
				{ 0x48B0, new int[2]{ 2, 3 } }, // Gargish Battle Axe
				{ 0x48B2, new int[2]{ 3, 6 } }, // Gargish Axe
				{ 0x48B4, new int[2]{ 11, 6 } }, // Gargish Bardiche
				{ 0x48B6, new int[2]{ 8, 5 } }, // Gargish Butcher Knife
				{ 0x48B8, new int[2]{ 3, 11 } }, // Gargish Gnarled Staff
				{ 0x48BA, new int[2]{ 7, 1 } }, // Gargish Katana
				{ 0x48BC, new int[2]{ 1, 8 } }, // Gargish Kryss
				{ 0x48BE, new int[2]{ 2, 5 } }, // Gargish War Fork
				{ 0x48CA, new int[2]{ 3, 6 } }, // Gargish Lance
				{ 0x48C0, new int[2]{ 13, 4 } }, // Gargish War Hammer
				{ 0x48C2, new int[2]{ 4, 3 } }, // Gargish Maul
				{ 0x48C4, new int[2]{ 2, 11 } }, // Gargish Scyte
				{ 0x48C6, new int[2]{ 11, 9 } }, // Gargish Bone Harvester
				{ 0x48C8, new int[2]{ 11, 8 } }, // Gargish Pike
				{ 0x48CD, new int[2]{ 20, 16 } }, // Gargish Tessen
				{ 0x48CE, new int[2]{ 21, 19 } }, // Gargish Tekagi
				{ 0x48D0, new int[2]{ 20, 6 } }, // Gargish Daisho
			};

			s_Abilities = new Dictionary<int, SpecialAbilityDefinition>()
            {
                // Ability List
                { 1, new SpecialAbilityDefinition("Armor Ignore", 1, 0x5200, 1028838, 1061693, 3908, 5048, 3935, 5119, 9927, 5181, 5040, 5121, 3939, 9932, 11554, 16497, 16502, 16494, 16491) },
				{ 2, new SpecialAbilityDefinition("Bleed Attack", 2, 0x5201, 1028839, 1061694, 3779, 5115, 3912, 3910, 5185, 9924, 5127, 5040, 3720, 5125, 11552, 16499, 16498) },
				{ 3, new SpecialAbilityDefinition("Concussion Blow", 3, 0x5202, 1028840, 1061695, 5048, 3912, 5183, 5179, 3933, 5113, 3722, 9930, 3920, 11556, 16487, 16500) },
				{ 4, new SpecialAbilityDefinition("Crushing Blow", 4, 0x5203, 1028841, 1061696, 5050, 3914, 3935, 3714, 5092, 5179, 5127, 5177, 9926, 4021, 10146, 11556, 11560, 5109, 16500, 16495) },
				{ 5, new SpecialAbilityDefinition("Disarm", 5, 0x5204, 1028838, 1061642, 5111, 3718, 3781, 3908, 3573, 3714, 3933, 5125, 11558, 11560, 5109, 9934, 16493, 16494) },
				{ 6, new SpecialAbilityDefinition("Dismount", 6, 0x5205, 1028838, 1061643, 3918, 3914, 9927, 3573, 5044, 3720, 9930, 5117, 16501, 16495) },
				{ 7, new SpecialAbilityDefinition("Double Strike", 7, 0x5206, 1028844, 1061699, 3718, 5187, 3916, 5046, 5119, 9931, 3722, 9929, 9933, 10148, 10153, 16488, 16493, 16496) },
				{ 8, new SpecialAbilityDefinition("Infecting", 8, 0x5207, 1028845, 1061700, 5111, 3779, 3922, 9928, 5121, 9929, 11553, 16490, 16488) },
				{ 9, new SpecialAbilityDefinition("Mortal Strike", 9, 0x5208, 1028846, 1061701, 3910, 9925, 9931, 5181, 9926, 5123, 3920, 5042, 16499, 16502, 16496, 16491) },
				{ 10, new SpecialAbilityDefinition("Moving Shot", 10, 0x5209, 1028847, 1061702, 5117, 9932, 9933, 16492) },
				{ 11, new SpecialAbilityDefinition("Paralyzing Blow", 11, 0x520A, 1028848, 1061703, 5050, 3918, 5046, 9924, 9925, 5113, 3569, 9928, 3939, 5042, 16497, 16498) },
				{ 12, new SpecialAbilityDefinition("Shadow Strike", 12, 0x520B, 1028849,  1061704, 3781, 5187, 5185, 5092, 5044, 3922, 5123, 4021, 11553, 16490) },
				{ 13, new SpecialAbilityDefinition("Whirlwind Attack", 13, 0x520C, 1028850, 1061705, 5115, 5183, 3916, 5177, 3569, 10157, 11559, 9934, 16501) },
				{ 14, new SpecialAbilityDefinition("Riding Swipe", 14, 0x520D, 1028851, 1061706, 10146) },
				{ 15, new SpecialAbilityDefinition("Frenzied Whirlwind", 15, 0x520E, 1028852, 1061707, 10148, 10150, 10151) },
				{ 16, new SpecialAbilityDefinition("Block", 16, 0x520F, 1028853, 1061708, 10147, 10158, 10159, 11557) },
				{ 17, new SpecialAbilityDefinition("Defense Mastery", 17, 0x5210, 1028854, 1061709, 10151, 10157, 11561) },
				{ 18, new SpecialAbilityDefinition("Nerve Strike", 18, 0x5211, 1028855, 1061710, 10152) },
				{ 19, new SpecialAbilityDefinition("Talon Strike", 19, 0x5212, 1028856, 1061711, 10155) },
				{ 20, new SpecialAbilityDefinition("Feint", 20, 0x5213, 1028857, 1061712, 10152, 10153, 10158, 11554) },
				{ 21, new SpecialAbilityDefinition("Dual Wield", 21, 0x5214, 1028858, 1061713, 10155) },
				{ 22, new SpecialAbilityDefinition("Double Shot", 22, 0x5215, 1028859, 1061714, 10149) },
				{ 23, new SpecialAbilityDefinition("Armor Pierce", 23, 0x5216, 1028860, 1061715, 10149, 10159) },
				{ 24, new SpecialAbilityDefinition("Bladeweave", 24, 0x5217, 1028861, 1061716, 11555, 11558, 11559, 11561) },
				{ 25, new SpecialAbilityDefinition("Force Arrow", 25, 0x5218, 1028862, 1061717, 11550) },
				{ 26, new SpecialAbilityDefinition("Lightning Arrow", 26, 0x5219, 1028863, 1061718, 11551) },
				{ 27, new SpecialAbilityDefinition("Psychic Attack", 27, 0x521A, 1028864, 1061719, 11551, 11552) },
				{ 28, new SpecialAbilityDefinition("Serpent Arrow", 28, 0x521B, 1028865, 1061720, 11550) },
				{ 29, new SpecialAbilityDefinition("Force of Nature", 29, 0x521C, 1028866, 1061721, 11557) },
				{ 30, new SpecialAbilityDefinition("Infused Throw", 30, 0x521D, 1113283, 1113288, 16492) },
				{ 31, new SpecialAbilityDefinition("Mystic Arc", 31, 0x521E, 1113282, 1113287, 16487) },
				//{ 32, new SpecialAbilityDefinition("Disrobe", 32, 0x521F, 0000000, 0) },
			};
        }
    }
}
