﻿/***************************************************************************
 *   WorldInteraction.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

#region usings
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using UltimaXNA.Core.Network;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.Data;
using UltimaXNA.Ultima.Network.Client;
using UltimaXNA.Ultima.Player;
using UltimaXNA.Ultima.UI.WorldGumps;
using UltimaXNA.Ultima.World.Entities;
using UltimaXNA.Ultima.World.Entities.Items;
using UltimaXNA.Ultima.World.Entities.Items.Containers;
using UltimaXNA.Ultima.World.Entities.Mobiles;
using UltimaXNA.Ultima.UI;
using UltimaFNA.Core.Network.SocketAsync;
#endregion

namespace UltimaXNA.Ultima.World {
    /// <summary>
    /// Hosts methods for interacting with the world.
    /// </summary>
    class WorldInteraction
    {
        private WorldModel _World;
        private NetworkClient _Network;
        private UserInterfaceService _UserInterface;

        public WorldInteraction(WorldModel world)
        {
            _Network = Service.Get<NetworkClient>();
            _UserInterface = Service.Get<UserInterfaceService>();

            _World = world;
        }

        private Serial _lastTarget;
        public Serial LastTarget
        {
            get { return _lastTarget; }
            set
            {
                _lastTarget = value;
                _Network.Send(new MobileQueryPacket(MobileQueryPacket.StatusType.BasicStatus, _lastTarget));
            }
        }

        /// <summary>
        /// For items, if server.expansion is less than AOS, sends single click packet.
        /// For mobiles, always sends a single click.
        /// Requests a context menu regardless of version.
        /// </summary>
        public void SingleClick(AEntity e)
        {
            if (!PlayerState.ClientFeatures.TooltipsEnabled)
            {
                _Network.Send(new SingleClickPacket(e.Serial));
            }
            _Network.Send(new RequestContextMenuPacket(e.Serial));
        }

        public void DoubleClick(AEntity e) // used by itemgumpling, paperdollinteractable, topmenu, worldinput.
        {
            if (e != null)
                _Network.Send(new DoubleClickPacket(e.Serial));
        }
        
        public void AttackRequest(Mobile mobile)
        {
            // Do nothing on Invulnerable
            if (mobile.Notoriety == 0x7){
            }
            // Attack Innocents, Reds and Greys
            else if (mobile.Notoriety == 0x1 || mobile.Notoriety == 0x3 || mobile.Notoriety == 0x4 || mobile.Notoriety == 0x5 || mobile.Notoriety == 0x6)
            {
                _Network.Send(new AttackRequestPacket(mobile.Serial));
            }
            // CrimeQuery is enabled, ask before attacking others
            else if (Settings.UserInterface.CrimeQuery)
            {
                _UserInterface.AddControl(new CrimeQueryGump(mobile), 0, 0);
            }
            // CrimeQuery is disabled, so attack without asking
            else
            {
                _Network.Send(new AttackRequestPacket(mobile.Serial));
            }
        }

        public void ToggleWarMode() // used by paperdollgump.
        {
            _Network.Send(new RequestWarModePacket(!((Mobile)WorldModel.Entities.GetPlayerEntity()).Flags.IsWarMode));
        }

		public void ToggleFlyMode() // used by gargoyle Fly ability.
		{
			_Network.Send(new ToggleFlyPacket());
		}

		public void UseSkill(int index) // used by WorldInteraction
        {
            _Network.Send(new RequestSkillUsePacket(index));
        }

        public void CastSpell(int index)
        {
            _Network.Send(new CastSpellPacket(index));
        }

		public void UseAbility(int index)
		{
			_Network.Send(new UseAbilityPacket(WorldModel.Entities.GetPlayerEntity().Serial, index));
		}

		public void ChangeSkillLock(SkillEntry skill)
        {
            if (skill == null)
                return;
            byte nextLockState = (byte)(skill.LockType + 1);
            if (nextLockState > 2)
                nextLockState = 0;
            _Network.Send(new SetSkillLockPacket((ushort)skill.Index, nextLockState));
            skill.LockType = nextLockState;
        }

        public void BookHeaderNewChange(Serial serial, string title, string author)
        {
            _Network.Send(new BookHeaderNewChangePacket(serial, title, author));
        }

        public void BookHeaderOldChange(Serial serial, string title, string author)
        {
            // Not yet implemented
            // _Network.Send(new BookHeaderOldChangePacket(serial, title, author));
        }

        public void BookPageChange(Serial serial, int page, string[] lines)
        {
            _Network.Send(new BookPageChangePacket(serial, page, lines));
        }

        public Gump OpenContainerGump(AEntity entity) // used by ultimaclient.
        {
            Gump gump;
            if ((gump = (Gump)_UserInterface.GetControl(entity.Serial)) != null)
            {
                gump.Dispose();
            }
            else
            {

                if (entity is Corpse)
                {
                    gump = new ContainerGump(entity, 0x2006);
                    _UserInterface.AddControl(gump, 96, 96);
                }
                else if (entity is SpellBook)
                {
                    gump = new SpellbookGump((SpellBook)entity);
                    _UserInterface.AddControl(gump, 96, 96);
                }
                else
                {
                    gump = new ContainerGump(entity, ((ContainerItem)entity).ItemID);
                    _UserInterface.AddControl(gump, 64, 64);
                }
            }
            return gump;
        }

        List<QueuedMessage> _ChatQueue = new List<QueuedMessage>();

        public void ChatMessage(string text) // used by gump
        {
            ChatMessage(text, 0, 0, true);
        }

        public void ChatMessage(string text, int font, int hue, bool asUnicode)
        {
            _ChatQueue.Add(new QueuedMessage(text, font, hue, asUnicode));

            ChatControl chat = Service.Get<ChatControl>();
            if (chat != null)
            {
                foreach (QueuedMessage msg in _ChatQueue)
                {
                    chat.AddLine(msg.Text, msg.Font, msg.Hue, msg.AsUnicode);
                }
                _ChatQueue.Clear();
            }
        }

        class QueuedMessage
        {
            public string Text;
            public int Hue;
            public int Font;
            public bool AsUnicode;

            public QueuedMessage(string text, int font, int hue, bool asUnicode)
            {
                Text = text;
                Hue = hue;
                Font = font;
                AsUnicode = asUnicode;
            }
        }

        public void CreateLabel(MessageTypes msgType, Serial serial, string text, int font, int hue, bool asUnicode)
        {
            if (serial.IsValid)
            {
                WorldModel.Entities.AddOverhead(msgType, serial, text, font, hue, asUnicode);
            }
            else
            {
                ChatMessage(text, font, hue, asUnicode);
            }
        }

        // ============================================================================================================
        // Cursor handling routines.
        // ============================================================================================================

        public Action<Item, int, int, int?> OnPickupItem;
        public Action OnClearHolding;

        internal void PickupItem(Item item, Point offset, int? amount = null)
        {
            if (item == null)
                return;
            if (OnPickupItem == null)
                return;

            OnPickupItem(item, offset.X, offset.Y, amount);
        }

        internal void ClearHolding()
        {
            if (OnClearHolding == null)
                return;

            OnClearHolding();
        }
    }
}
