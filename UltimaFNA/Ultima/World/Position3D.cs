﻿/***************************************************************************
 *   Position3D.cs
 *   Based on code from RunUO: http://www.runuo.com
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System;
using Microsoft.Xna.Framework;
#endregion

namespace UltimaXNA.Ultima.World
{
    public interface IPoint2D
    {
        int X { get; }
        int Y { get; }
    }

    public class Position3D : IPoint2D
    {
        public static Point NullTile = new Point(int.MinValue, int.MinValue);

        private Point _Tile;
        private int _Z;
        private Vector3 _Offset;

        public Point Tile
        {
            get { return _Tile; }
            set
            {
                if (_Tile != value)
                {
                    _Tile = value;
                    if (_Tile != NullTile && _OnTileChanged != null)
                        _OnTileChanged(_Tile.X, _Tile.Y);
                }
            }
        }

        public Vector3 Offset { get { return _Offset; } set { _Offset = value; } }

        public bool IsOffset { get { return _Offset != Vector3.Zero; } }
        public bool IsNullPosition { get { return _Tile == NullTile; } }

        public int X
        {
            get { return _Tile.X; }
        }
        public int Y
        {
            get { return _Tile.Y; }
        }
        public int Z
        {
            get { return _Z; }
        }

        public float X_offset { get { return _Offset.X % 1.0f; } }
        public float Y_offset { get { return _Offset.Y % 1.0f; } }
        public float Z_offset { get { return _Offset.Z; } }

        private Action<int, int> _OnTileChanged;

        public Position3D(Action<int, int> onTileChanged)
        {
            Tile = NullTile;
            _OnTileChanged = onTileChanged;
        }

        public Position3D(int x, int y, int z)
        {
            Tile = new Point(x, y);
            _Z = z;
        }

        internal void Set(int x, int y, int z)
        {
            _Z = z;
            Tile = new Point(x, y);
            _Offset = Vector3.Zero;
        }

        public override bool Equals(object o)
        {
            if (o == null) return false;
            if (o.GetType() != typeof(Position3D)) return false;
            if (X != ((Position3D)o).X) return false;
            if (Y != ((Position3D)o).Y) return false;
            if (Z != ((Position3D)o).Z) return false;
            return true;
        }

        // Equality operator. Returns dbNull if either operand is dbNull, 
        // otherwise returns dbTrue or dbFalse:
        public static bool operator ==(Position3D x, Position3D y)
        {
            if ((object)x == null)
                return ((object)y == null);
            return x.Equals(y);
        }

        // Inequality operator. Returns dbNull if either operand is
        // dbNull, otherwise returns dbTrue or dbFalse:
        public static bool operator !=(Position3D x, Position3D y)
        {
            if ((object)x == null)
                return ((object)y != null);
            return !x.Equals(y);
        }

        public override int GetHashCode()
        {
            return X ^ Y ^ Z;
        }

        public override string ToString()
        {
            return string.Format("X:{0} Y:{1} Z:{2}", X, Y, Z);
        }

        public string ToStringComplex()
        {
            return
                "P(Tile)=" + ToString() + Environment.NewLine +
                "P(Ofst)=" + string.Format("X:{0:0.00} Y:{1:0.00} Z:{2:0.00}", X_offset, Y_offset, Z_offset) + Environment.NewLine +
                "D(Tile)=" + string.Format("X:{0:0.00} Y:{1:0.00} Z:{2:0.00}", X, Y, Z) + Environment.NewLine +
                "D(Ofst)=" + string.Format("X:{0:0.00} Y:{1:0.00} Z:{2:0.00}", X_offset, Y_offset, Z_offset);
        }
    }
}
