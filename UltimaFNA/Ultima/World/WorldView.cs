﻿/***************************************************************************
 *   WorldView.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Patterns.MVC;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.UI.WorldGumps;
using UltimaXNA.Ultima.World.Entities;
using UltimaXNA.Ultima.World.Entities.Mobiles;
using UltimaXNA.Ultima.World.EntityViews;
using UltimaXNA.Ultima.World.WorldViews;
#endregion

namespace UltimaXNA.Ultima.World
{
    class WorldView : AView
    {
        public IsometricRenderer Isometric
        {
            get;
            private set;
        }

        public MiniMapTexture MiniMap
        {
            get;
            private set;
        }

        protected new WorldModel Model
        {
            get { return (WorldModel)base.Model; }
        }

        /// <summary>
        ///  When AllLabels is true, all entites should display their name above their object.
        /// </summary>
        public static bool AllLabels
        {
            get;
            set;
        }

        public static int MouseOverHue = 0x038;

        public WorldView(WorldModel model)
            : base(model)
        {
            Isometric = new IsometricRenderer();
            Isometric.Lighting.LightDirection = -0.6f;

            MiniMap = new MiniMapTexture();
            MiniMap.Initialize();

            _UI = Service.Get<UserInterfaceService>();
        }

        public override void Draw(double frameTime)
        {
            AEntity player = WorldModel.Entities.GetPlayerEntity();
            if (player == null)
                return;
            if (Model.Map == null)
                return;

            Position3D center = player.Position;
            if ((player as Mobile).IsAlive)
            {
                AEntityView.Technique = Techniques.Default;
                _ShowingDeathEffect = false;
                if (_YouAreDead != null)
                {
                    _YouAreDead.Dispose();
                    _YouAreDead = null;
                }
            }
            else
            {
                if (!_ShowingDeathEffect)
                {
                    _ShowingDeathEffect = true;
                    _DeathEffectTime = 0;
                    _LightingGlobal = Isometric.Lighting.OverallLightning;
                    _LightingPersonal = Isometric.Lighting.PersonalLightning;
                    _UI.AddControl(_YouAreDead = new YouAreDeadGump(), 0, 0);
                }

                double msFade = 2000d;
                double msHold = 1000d;

                if (_DeathEffectTime < msFade)
                {
                    AEntityView.Technique = Techniques.Default;
                    Isometric.Lighting.OverallLightning = (int)(_LightingGlobal + (0x1f - _LightingGlobal) * ((_DeathEffectTime / msFade)));
                    Isometric.Lighting.PersonalLightning = (int)(_LightingPersonal * (1d - (_DeathEffectTime / msFade)));
                }
                else if (_DeathEffectTime < msFade + msHold)
                {
                    Isometric.Lighting.OverallLightning = 0x1f;
                    Isometric.Lighting.PersonalLightning = 0x00;
                }
                else
                {
                    AEntityView.Technique = Techniques.Grayscale;
                    Isometric.Lighting.OverallLightning = (int)_LightingGlobal;
                    Isometric.Lighting.PersonalLightning = (int)_LightingPersonal;
                    if (_YouAreDead != null)
                    {
                        _YouAreDead.Dispose();
                        _YouAreDead = null;
                    }
                }

                _DeathEffectTime += frameTime;
            }

            Isometric.Update(Model.Map, center, Model.Input.MousePick);
            MiniMap.Update(Model.Map, center);
        }

        private bool _ShowingDeathEffect;
        private double _DeathEffectTime;
        private double _LightingGlobal;
        private double _LightingPersonal;
        private YouAreDeadGump _YouAreDead;

        private UserInterfaceService _UI;
    }
}
