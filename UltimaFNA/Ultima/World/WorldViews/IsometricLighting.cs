﻿/***************************************************************************
 *   IsometricLighting.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using System;
using Microsoft.Xna.Framework;

namespace UltimaXNA.Ultima.World.WorldViews
{
    public class IsometricLighting
    {
        private int _LightLevelPersonal = 9;
        private int _LightLevelOverall = 9;
        private float _LightDirection = 4.12f;
        private float _LightHeight = -0.75f;

        public IsometricLighting()
        {
            RecalculateLightningValues();
        }

        public int PersonalLightning
        {
            set { _LightLevelPersonal = value; RecalculateLightningValues(); }
            get { return _LightLevelPersonal; }
        }

        public int OverallLightning
        {
            set { _LightLevelOverall = value; RecalculateLightningValues(); }
            get { return _LightLevelOverall; }
        }

        public float LightDirection
        {
            set { _LightDirection = value; RecalculateLightningValues(); }
            get { return _LightDirection; }
        }

        public float LightHeight
        {
            set { _LightHeight = value; RecalculateLightningValues(); }
            get { return _LightHeight; }
        }

        private void RecalculateLightningValues()
        {
            float light = Math.Min(30 - OverallLightning + PersonalLightning, 30f);
            light = Math.Max(light, 0);
            IsometricLightLevel = light / 30; // bring it between 0-1

            // i'd use a fixed lightning direction for now - maybe enable this effect with a custom packet?
            _LightDirection = 1.2f;
            IsometricLightDirection = Vector3.Normalize(new Vector3((float)Math.Cos(_LightDirection), (float)Math.Sin(_LightDirection), 1f));
        }

        public float IsometricLightLevel
        {
            get;
            private set;
        }

        public Vector3 IsometricLightDirection
        {
            get;
            private set;
        }
    }
}
