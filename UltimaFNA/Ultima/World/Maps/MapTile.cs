﻿/***************************************************************************
 *   MapTile.cs
 *   Copyright (c) 2009 Chase Mosher
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System.Collections.Generic;
using UltimaXNA.Ultima.Resources;
using UltimaXNA.Ultima.World.Entities;
using UltimaXNA.Ultima.World.Entities.Items;
#endregion

namespace UltimaXNA.Ultima.World.Maps
{
    /// <summary>
    /// Represents a single tile on the Ultima Online map.
    /// </summary>
    public class MapTile
    {
        public MapTile()
        {
            _Entities = new List<AEntity>();
        }

        /// <summary>
        /// The Ground entity for this tile. Every tile has one and only one ground entity.
        /// </summary>
        public Ground Ground
        {
            get;
            private set;
        }

        public int X
        {
            get { return Ground.Position.X; }
        }

        public int Y
        {
            get { return Ground.Position.Y; }
        }

        // ============================================================================================================
        // Entity management
        // ============================================================================================================

        private List<AEntity> _Entities;

        /// <summary>
        /// Adds the passed entity to this Tile's entity collection, and forces a resort of the entities on this tile.
        /// </summary>
        /// <param name="entity"></param>
        public void OnEnter(AEntity entity)
        {
            // only allow one ground object.
            if (entity is Ground)
            {
                if (Ground != null)
                    Ground.Dispose();
                Ground = (Ground)entity;
            }

            // if we are receiving a Item with the same position and itemID as a static item, then replace the static item.
            if (entity is Item)
            {
                Item item = entity as Item;
                for (int i = 0; i < _Entities.Count; i++)
                {
                    if (_Entities[i] is Item)
                    {
                        Item comparison = _Entities[i] as Item;
                        if (comparison.ItemID == item.ItemID &&
                            comparison.Z == item.Z)
                        {
                            _Entities.RemoveAt(i);
                            i--;
                        }
                    }
                }
            }

            _Entities.Add(entity);
            _NeedsSorting = true;
        }

        public bool ItemExists(int itemID, int z)
        {
            for (int i = 0; i < _Entities.Count; i++)
            {
                if (_Entities[i] is Item)
                {
                    Item comparison = _Entities[i] as Item;
                    if (comparison.ItemID == itemID &&
                        comparison.Z == z)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Removes the passed entity from this Tile's entity collection.
        /// </summary>
        /// <param name="entity"></param>
        public void OnExit(AEntity entity)
        {
            _Entities.Remove(entity);
        }

        /// <summary>
        /// Checks if the specified z-height is under an item or a ground.
        /// </summary>
        /// <param name="z">The z value to check.</param>
        /// <param name="underEntity">Returns the first roof, surface, or wall that is over the specified z.
        ///                         If no such objects exist above the specified z, then returns null.</param>
        /// <param name="underGround">Returns the ground object of this tile if the specified z is under the ground.
        ///                         Returns null otherwise.</param>
        public bool IsZUnderEntityOrGround(int z, out AEntity underEntity, out AEntity underGround)
        {
            // getting the publicly exposed Entities collection will sort the entities if necessary.
            List<AEntity> entities = Entities;

            underEntity = null;
            underGround = null;

            for (int i = entities.Count - 1; i >= 0; i--)
            {
                if (entities[i].Z <= z)
                    continue;

                if (entities[i] is Item) // checks Item and StaticItem entities.
                {
                    ItemData data = ((Item)entities[i]).ItemData;
                    if (data.IsRoof || data.IsSurface || (data.IsWall && data.IsImpassable))
                    {
                        if (underEntity == null || entities[i].Z < underEntity.Z)
                            underEntity = entities[i];
                    }
                }
                else if (entities[i] is Ground && entities[i].GetView().SortZ >= z + 12)
                {
                    underGround = entities[i];
                }
            }
            return (underEntity != null) || (underGround != null);
        }

        private List<StaticItem> s_StaticItemList = new List<StaticItem>();
        /// <summary>
        /// Returns a list of static items in this tile. ONLY CALL ONCE AT A TIME. NOT THREAD SAFE.
        /// </summary>
        /// <returns></returns>
        public List<StaticItem> GetStatics()
        {
            List<StaticItem> items = s_StaticItemList;
            s_StaticItemList.Clear();

            for (int i = 0; i < _Entities.Count; i++)
            {
                if (_Entities[i] is StaticItem)
                    items.Add((StaticItem)_Entities[i]);
            }

            return items;
        }

        private static List<Item> s_ItemsAtZList = new List<Item>();
        /// <summary>
        /// Returns a list of items at the specified z in this tile. ONLY CALL ONCE AT A TIME. NOT THREAD SAFE.
        /// </summary>
        /// <returns></returns>
        public List<Item> GetItemsBetweenZ(int z0, int z1)
        {
            List<Item> items = s_ItemsAtZList;
            s_ItemsAtZList.Clear();

            for (int i = 0; i < _Entities.Count; i++)
            {
                if (_Entities[i] is Item && _Entities[i].Z >= z0 && _Entities[i].Z <= z1)
                    items.Add((Item)_Entities[i]);
            }

            return items;
        }

        private bool matchNames(ItemData m1, ItemData m2)
        {
            return (m1.Name == m2.Name);
        }

        private void InternalRemoveDuplicateEntities()
        {
            int[] itemsToRemove = new int[0x100];
            int removeIndex = 0;

            for (int i = 0; i < _Entities.Count; i++)
            {
                // !!! TODO: I think this is wrong...
                for (int j = 0; j < removeIndex; j++)
                {
                    if (itemsToRemove[j] == i)
                        continue;
                }

                if (_Entities[i] is StaticItem)
                {
                    // Make sure we don't double-add a static or replace an item with a static (like doors on multis)
                    for (int j = i + 1; j < _Entities.Count; j++)
                    {
                        if (_Entities[i].Z == _Entities[j].Z)
                        {
                            if (_Entities[j] is StaticItem && (
                                ((StaticItem)_Entities[i]).ItemID == ((StaticItem)_Entities[j]).ItemID))
                            {
                                itemsToRemove[removeIndex++] = i;
                                break;
                            }
                        }
                    }
                }
                else if (_Entities[i] is Item)
                {
                    // if we are adding an item, replace existing statics with the same *name*
                    // We could use same *id*, but this is more robust for items that can open ...
                    // an open door will have a different id from a closed door, but the same name.
                    // Also, don't double add an item.
                    for (int j = i + 1; j < _Entities.Count; j++)
                    {
                        if (_Entities[i].Z == _Entities[j].Z)
                        {
                            if ((_Entities[j] is StaticItem && matchNames(((Item)_Entities[i]).ItemData, ((StaticItem)_Entities[j]).ItemData)) ||
                                (_Entities[j] is Item && _Entities[i].Serial == _Entities[j].Serial))
                            {
                                itemsToRemove[removeIndex++] = j;
                                continue;
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < removeIndex; i++)
            {
                _Entities.RemoveAt(itemsToRemove[i] - i);
            }
        }

        public List<AEntity> Entities
        {
            get
            {
                if (_NeedsSorting)
                {
                    InternalRemoveDuplicateEntities();
                    TileSorter.Sort(_Entities);
                    _NeedsSorting = false;
                }
                return _Entities;
            }
        }

        // ============================================================================================================
        // Sorting
        // ============================================================================================================

        private bool _NeedsSorting;

        public void ForceSort()
        {
            _NeedsSorting = true;
        }
    }
}
