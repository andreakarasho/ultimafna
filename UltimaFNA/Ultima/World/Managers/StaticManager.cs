﻿/***************************************************************************
 *   WorldClient.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System.Collections.Generic;
using UltimaXNA.Ultima.World.Entities.Items;
#endregion

namespace UltimaXNA.Ultima.World.Managers
{
    public class StaticManager
    {
        private readonly List<StaticItem> _ActiveStatics = new List<StaticItem>();

        public void AddStaticThatNeedsUpdating(StaticItem item)
        {
            if (item.IsDisposed || item.Overheads.Count == 0)
                return;

            _ActiveStatics.Add(item);
        }

        public void Update(double frameMS)
        {
            for (int i = 0; i < _ActiveStatics.Count; i++)
            {
                _ActiveStatics[i].Update(frameMS);
                if (_ActiveStatics[i].IsDisposed || _ActiveStatics[i].Overheads.Count == 0)
                    _ActiveStatics.RemoveAt(i);
            }
        }
    }
}
