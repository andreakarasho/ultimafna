﻿/***************************************************************************
 *   WorldModel.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System;
using System.Collections.Generic;
using UltimaXNA.Core;
using UltimaXNA.Core.Diagnostics.Tracing;
using UltimaXNA.Core.Network;
using UltimaXNA.Core.Patterns.MVC;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.Login;
using UltimaXNA.Ultima.UI;
using UltimaXNA.Ultima.UI.WorldGumps;
using UltimaXNA.Ultima.World.Entities;
using UltimaXNA.Ultima.World.Input;
using UltimaXNA.Ultima.World.Managers;
using UltimaXNA.Ultima.World.Maps;
using UltimaXNA.Configuration.Properties;
using UltimaFNA.Core.Network.SocketAsync;
#endregion

namespace UltimaXNA.Ultima.World
{
    class WorldModel : AModel
    {
        // ============================================================================================================
        // Private variables
        // ============================================================================================================
        Map _Map;
        WorldCursor _Cursor;
        readonly NetworkClient _Network;
        readonly UserInterfaceService _UserInterface;
        readonly UltimaGame _Engine;

        // ============================================================================================================
        // Public Static Properties
        // ============================================================================================================
        public static Serial PlayerSerial
        {
            get;
            set;
        }

        public static EntityManager Entities
        {
            get;
            private set;
        }

        public static EffectManager Effects
        {
            get;
            private set;
        }

        public static StaticManager Statics
        {
            get;
            private set;
        }

        // ============================================================================================================
        // Public Properties
        // ============================================================================================================
        public WorldClient Client
        {
            get;
            private set;
        }

        public WorldInput Input
        {
            get;
            private set;
        }

        public WorldInteraction Interaction
        {
            get;
            private set;
        }
        
        public WorldCursor Cursor
        {
            get { return _Cursor; }
            set
            {
                if (_Cursor != null)
                {
                    _Cursor.Dispose();
                }
                _Cursor = value;
            }
        }
        
        public Map Map
        {
            get { return _Map; }
        }

        public uint MapIndex
        {
            get
            {
                return (_Map == null) ? 0xFFFFFFFF : _Map.Index;
            }
            set
            {
                if (value != MapIndex)
                {
                    // clear all entities
                    Entities.Reset(false);
                    if (_Map != null)
                    {
                        AEntity player = Entities.GetPlayerEntity();
                        // save current player position
                        int x = player.X, y = player.Y, z = player.Z;
                        // place the player in null space (allows the map to be reloaded when we return to the same location in a different map).
                        player.SetMap(null);
                        // dispose of map
                        _Map.Dispose();
                        _Map = null;
                        // add new map!
                        _Map = new Map(value);
                        player.SetMap(_Map);
                        // restore previous player position
                        player.Position.Set(x, y, z);
                    }
                    else
                    {
                        AEntity player = Entities.GetPlayerEntity();
                        _Map = new Map(value);
                        player.SetMap(_Map);
                    }
                }
            }
        }

        public static bool IsInWorld // InWorld allows us to tell when our character object has been loaded in the world.
        {
            get;
            set;
        }

        // ============================================================================================================
        // Ctor, Initialization, Dispose, Update
        // ============================================================================================================
        public WorldModel()
        {
            Service.Add<WorldModel>(this);

            _Engine = Service.Get<UltimaGame>();
            _Network = Service.Get<NetworkClient>();
            _UserInterface = Service.Get<UserInterfaceService>();

            Entities = new EntityManager(this);
            Entities.Reset(true);
            Effects = new EffectManager(this);
            Statics = new StaticManager();

            Input = new WorldInput(this);
            Interaction = new WorldInteraction(this);
            Client = new WorldClient(this);

         //   World.Entities.Multis.Multi.InitMulti();
        }

        protected override void OnInitialize()
        {
            _Engine.SetupWindowForWorld();
            _UserInterface.Cursor = Cursor = new WorldCursor(this);
            Client.Initialize();
            Player.PlayerState.Journaling.AddEntry("Welcome to Ultima Online!", 9, 0x3B4, string.Empty, false);
        }

        protected override void OnDispose()
        {
            _Engine.SaveResolution();

            Service.Remove<WorldModel>();

            _UserInterface.Reset();

            Entities.Reset();
            Entities = null;

            Effects = null;

            Input.Dispose();
            Input = null;

            Interaction = null;

            Client.Dispose();
            Client = null;
        }

        public override void Update(double totalMS, double frameMS)
        {
            if (!_Network.IsConnected)
            {
                if (_UserInterface.IsModalControlOpen == false)
                {
                    MsgBoxGump g = MsgBoxGump.Show("You have lost your connection with the server.", MsgBoxTypes.OkOnly);
                    g.OnClose = OnCloseLostConnectionMsgBox;
                }
            }
            else
            {
                Input.Update(frameMS);
                Entities.Update(frameMS);
                Effects.Update(frameMS);
                Statics.Update(frameMS);
            }
        }

        // ============================================================================================================
        // Public Methods
        // ============================================================================================================
        public void LoginToWorld()
        {
            _UserInterface.AddControl(new WorldViewGump(), 0, 0); // world gump will restore its position on load.
            if (!Settings.UserInterface.MenuBarDisabled)
            {
                _UserInterface.AddControl(new TopMenuGump(), 0, 0); // by default at the top of the screen.
            }

            Client.SendWorldLoginPackets();
            IsInWorld = true;
            Client.StartKeepAlivePackets();
            
            // wait until we've received information about the entities around us before restoring saved gumps.
            DelayedAction.Start(RestoreSavedGumps, 1000);
        }

        public void Disconnect()
        {
			SaveOpenGumps();
			_Network.Disconnect(); // stops keep alive packets.
            IsInWorld = false;
            _Engine.Models.Current = new LoginModel();
        }

        // ============================================================================================================
        // Private/Protected Methods
        // ============================================================================================================
        protected override AView CreateView()
        {
            return new WorldView(this);
        }

        void OnCloseLostConnectionMsgBox()
        {
            Disconnect();
        }

        void SaveOpenGumps()
        {
			Settings.Gumps.SavedGumps.Clear();
			
			foreach (AControl gump in _UserInterface.OpenControls)
			{
				if (gump is Gump)
				{
					if ((gump as Gump).SaveOnWorldStop)
					{
						System.Console.WriteLine("salvataggio per apertura di : " + gump.GetType());
						Dictionary<string, object> data;
						if ((gump as Gump).SaveGump(out data))
						{
							System.Console.WriteLine("salvataggio Confermato di : " + gump.GetType());
							Settings.Gumps.SavedGumps.Add(new SavedGumpProperty(gump.GetType(), data));
						}
					}
				}
			}
        }

        void RestoreSavedGumps()
        {
            foreach (SavedGumpProperty savedGump in Settings.Gumps.SavedGumps)
            {
               try
                {
                    Type type = Type.GetType(savedGump.GumpType);
                    object gump = System.Activator.CreateInstance(type);
                    if (gump is Gump)
                    {
                        if ((gump as Gump).RestoreGump(savedGump.GumpData))
                        {
                            _UserInterface.AddControl(gump as Gump, 0, 0);
                        }
                        else
                        {
                            Tracer.Error("Unable to restore saved gump with type {0}: Failed to restore gump.", savedGump.GumpType);
                        }
                    }
                    else
                    {
                        Tracer.Error("Unable to restore saved gump with type {0}: Type does not derive from Gump.", savedGump.GumpType);
                    }
                }
                catch
                {
                    Tracer.Error("Unable to restore saved gump with type {0}: Type cannot be Instanced.", savedGump.GumpType);
                }
            }
            //Settings.Gumps.SavedGumps.Clear();
        }
    }
}
