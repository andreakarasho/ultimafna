﻿/***************************************************************************
 *   BaseBook.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
using UltimaXNA.Ultima.World.Maps;

namespace UltimaXNA.Ultima.World.Entities.Items
{
    public class BaseBook : Item
    {
        string _Title;
        string _Author;
        BookPageInfo[] _Pages;
        bool _IsEditable;

        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        public string Author
        {
            get { return _Author; }
            set { _Author = value; }
        }

        public bool IsEditable
        {
            get { return _IsEditable; }
            set
            {
                _IsEditable = value;
            }
        }

        public int PageCount => _Pages.Length;

        public BookPageInfo[] Pages
        {
            get { return _Pages; }
            set
            {
                _Pages = value;
                _OnUpdated?.Invoke(this);
            }
        }

        public BaseBook(Serial serial, Map map) 
            : this(serial, map, true)
        {
        }

        public BaseBook(Serial serial, Map map, bool writable)
            : this(serial, map, writable, null, null)
        {
        }

        public BaseBook(Serial serial, Map map, bool writable, string title, string author)
            : base(serial, map)
        {
            _Title = title;
            _Author = author;
            _IsEditable = writable;
            _Pages = new BookPageInfo[0];
        }

        public class BookPageInfo
        {
            string[] _Lines;
            public string[] Lines
            {
                get
                {
                    return _Lines;
                }
                set
                {
                    _Lines = value;
                }
            }

            public BookPageInfo()
            {
                _Lines = new string[0];
            }

            public BookPageInfo(string[] lines)
            {
                _Lines = lines;
            }

            public string GetAllLines() => string.Join("\n", _Lines);
        }
    }
}