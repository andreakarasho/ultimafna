﻿/***************************************************************************
 *   AEffect.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System.Collections.Generic;
using UltimaXNA.Ultima.World.Maps;
#endregion

namespace UltimaXNA.Ultima.World.Entities.Effects
{
    public abstract class AEffect : AEntity
    {
        private List<AEffect> _Children;
        public List<AEffect> Children
        {
            get
            {
                if (_Children == null)
                    _Children = new List<AEffect>();
                return _Children;
            }
        }

        public int ChildrenCount
        {
            get
            {
                return (_Children == null) ? 0 : _Children.Count;
            }
        }

        protected AEntity _Source;
        protected AEntity _Target;

        protected int _xSource, _ySource, _zSource;
        protected int _xTarget, _yTarget, _zTarget;

        private double _TimeActiveMS;
        public int FramesActive
        {
            get
            {
                int frameOffset = (int)(_TimeActiveMS / 50d); // one frame every 20ms
                return frameOffset;
            }
        }

        public int BlendMode;

        public AEffect(Map map)
            : base(Serial.Null, map)
        {

        }

        public override void Update(double frameMS)
        {
            base.Update(frameMS);
            if (!IsDisposed)
            {
                _TimeActiveMS += frameMS;
            }
        }

        public override void Dispose()
        {
            _Source = null;
            _Target = null;
            base.Dispose();
        }

        /// <summary>
        /// Adds an effect which will display when the parent effect despawns.
        /// </summary>
        /// <param name="effect"></param>
        public void AddChildEffect(AEffect effect)
        {
            if (_Children == null)
                _Children = new List<AEffect>();
            _Children.Add(effect);
        }

        /// <summary>
        /// Returns the world position that is the source of the effect.
        /// </summary>
        protected void GetSource(out int x, out int y, out int z)
        {
            if (_Source == null)
            {
                x = _xSource;
                y = _ySource;
                z = _zSource;
            }
            else
            {
                x = _Source.X;
                y = _Source.Y;
                z = _Source.Z;
            }
        }

        /// <summary>
        /// Returns the world position that is the target of the effect.
        /// </summary>
        protected void GetTarget(out int X, out int Y, out int Z)
        {
            if (_Target == null)
            {
                X = _xTarget;
                Y = _yTarget;
                Z = _zTarget;
            }
            else
            {
                X = _Target.X;
                Y = _Target.Y;
                Z = _Target.Z;
            }
        }

        public void SetSource(AEntity source)
        {
            _Source = source;
        }

        public void SetSource(int x, int y, int z)
        {
            _Source = null;
            _xSource = x;
            _ySource = y;
            _zSource = z;
        }

        public void SetTarget(AEntity target)
        {
            _Target = target;
        }

        public void SetTarget(int x, int y, int z)
        {
            _Target = null;
            _xTarget = x;
            _yTarget = y;
            _zTarget = z;
        }
    }
}
