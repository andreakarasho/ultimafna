﻿/***************************************************************************
 *   Multi.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using UltimaXNA.Ultima.Resources;
using UltimaXNA.Ultima.World.Entities.Items;
using UltimaXNA.Ultima.World.Maps;
using UltimaXNA.Ultima.World.Data;
using System.Threading;
#endregion

namespace UltimaXNA.Ultima.World.Entities.Multis
{
    class Multi : AEntity
    {
        private MultiComponentList _Components;
        private int _customHouseRevision = 0x7FFFFFFF;
        private CustomHouse _customHouse;
        private int _MultiID = -1;


        public Multi(Serial serial, Map map)
            : base(serial, map)
        {
        }


        public int MultiID
        {
            get { return _MultiID; }
            set
            {
                if (_MultiID != value)
                {
                    _MultiID = value;
                    _Components = MultiData.GetComponents(_MultiID);
                }
            }
        }

        public MultiComponentList Components => _Components;
        public int CustomHouseRevision { get { return _customHouseRevision; } }

        public void AddCustomHousingTiles(CustomHouse house)
        {
            _customHouse = house;
            _customHouse.CreateHouse(this);
        }

        public override void Dispose()
        {
            base.Dispose();
        }

        public override int GetMaxUpdateRange()
        {
            return 50;
            return _customHouse == null ? 22 : 24;
        }
    }
}
