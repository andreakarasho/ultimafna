﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimaXNA.Ultima.World.Maps;

namespace UltimaXNA.Ultima.World.Entities.Mobiles
{
    public class PlayerMobile : Mobile
    {
        public PlayerMobile(Serial serial, Map map) : base(serial, map)
        {
        }

        public override void Update(double frameMS)
        {
            _movement.Check();
            base.Update(frameMS);
        }
    }
}
