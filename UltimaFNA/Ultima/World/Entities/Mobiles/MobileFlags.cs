﻿/***************************************************************************
 *   MobileFlags.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using System;

namespace UltimaXNA.Ultima.World.Entities.Mobiles
{
    [Flags]
    public enum MobileFlag
    {
        None = 0x00,
		Paralized = 0x01,
        Female = 0x02,
        Poisoned = 0x04,
        Blessed = 0x08,
		Fly = 0x16,
        Warmode = 0x40,
        Hidden = 0x80,
    }

    public class MobileFlags
    {
        /// <summary>
        /// 0x02 = female
        /// 0x04 = poisoned 
        /// 0x08 = blessed/yellow health bar
        /// 0x40 = warmode
        /// 0x80 = hidden
		/// 0x16 = Fly
        /// </summary>
        private MobileFlag _flags;

        public bool IsFemale { get { return ((_flags & MobileFlag.Female) != 0); } }
        public bool IsPoisoned { get { return ((_flags & MobileFlag.Poisoned) != 0); } }
		public bool IsBlessed { get { return ((_flags & MobileFlag.Blessed) != 0); } }
		public bool IsFlying { get { return ((_flags & MobileFlag.Fly) != 0); } }
		public bool IsParalized { get { return ((_flags & MobileFlag.Paralized) != 0); } }
		public bool IsWarMode
        {
            get { return ((_flags & MobileFlag.Warmode) != 0); }
            set
            {
                if (value == true)
                {
                    _flags |= MobileFlag.Warmode;
                }
                else
                {
                    _flags &= ~MobileFlag.Warmode;
                }
            }
        }
        public bool IsHidden { get { return ((_flags & MobileFlag.Hidden) != 0); } }

        public MobileFlags(MobileFlag flags)
        {
			if (Player.PlayerState.ClientFeatures.SA) // in caso di Protocollo SA il flag poison diventa il flag fly, poison gestito da altro packet
			{
				if (flags.HasFlag(MobileFlag.Poisoned))
					_flags |= MobileFlag.Fly;
				else
					_flags &= ~MobileFlag.Fly;

			}

			_flags = flags;
		}

        public MobileFlags()
        {
            _flags = (MobileFlag)0;
        }
    }
}
