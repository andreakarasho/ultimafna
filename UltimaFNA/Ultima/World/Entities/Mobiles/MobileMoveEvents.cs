﻿/***************************************************************************
 *   MobileMoveEvents.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
 using System;
using UltimaXNA.Ultima.Network.Client;

namespace UltimaXNA.Ultima.World.Entities.Mobiles
{
    /// <summary>
    /// Queues moves and maintains the fastwalk key and current sequence value.
    /// TODO: This class needs a serious refactor.
    /// </summary>
    class MobileMoveEvents
    {
        private int _LastSequenceAck;
        private int _SequenceQueued;
        private int _SequenceNextSend;
        private int _FastWalkKey;
        MobileMoveEvent[] _History;

        public bool SlowSync => (_SequenceNextSend > _LastSequenceAck + 4);
        public int NextSeq => _SequenceNextSend;

        public MobileMoveEvents()
        {
            ResetMoveSequence();
        }

        public void ResetMoveSequence()
        {
            _SequenceQueued = 0;
            _LastSequenceAck = -1;
            _SequenceNextSend = 0;
            _FastWalkKey = new Random().Next(int.MinValue, int.MaxValue);
            _History = new MobileMoveEvent[256];
        }

        public MobileMoveEvent AddMoveEvent(int x, int y, int z, int facing, bool createdByPlayerInput)
        {
            MobileMoveEvent moveEvent = new MobileMoveEvent(x, y, z, facing, _FastWalkKey);
            moveEvent.CreatedByPlayerInput = createdByPlayerInput;

            _History[_SequenceQueued] = moveEvent;

            _SequenceQueued++;
            if (_SequenceQueued > byte.MaxValue)
                _SequenceQueued = 1;
            return moveEvent;
        }

        public MobileMoveEvent GetNextMoveEvent(out int sequence)
        {
            if (_History[_SequenceNextSend] != null)
            {
                MobileMoveEvent m = _History[_SequenceNextSend];
                _History[_SequenceNextSend] = null;
                sequence = _SequenceNextSend;
                _SequenceNextSend++;
                if (_SequenceNextSend > byte.MaxValue)
                    _SequenceNextSend = 1;
                return m;
            }
            else
            {
                sequence = 0;
                return null;
            }
        }

        public MobileMoveEvent GetAt(int seq)
        {
            if (_History[seq] != null)
            {
                MobileMoveEvent m = _History[seq];
                _History[seq] = null;
                _SequenceNextSend++;
                if (_SequenceNextSend > byte.MaxValue)
                    _SequenceNextSend = 1;
                return m;
            }
            return null;
        }

        public MobileMoveEvent PeekNextMoveEvent()
        {
            if (_History[_SequenceNextSend] != null)
            {
                MobileMoveEvent m = _History[_SequenceNextSend];
                return m;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Retrieves the last move event received. If more than one move event is in the queue, forwards the mobile to the
        /// second-to-last move event destination.
        /// </summary>
        /// <param name="sequence">The sequence index of the final move event. The player mobile needs to track this so
        /// it can send it with each move event and keep in sync with the server.</param>
        /// <returns>The final move event! Null if no move events in the queue</returns>
        public MobileMoveEvent GetAndForwardToFinalMoveEvent(out int sequence)
        {
            MobileMoveEvent moveEvent = null;
            MobileMoveEvent moveEventNext, moveEventLast;

            while ((moveEventNext = GetNextMoveEvent(out sequence)) != null)
            {
                // save the last moveEvent.
                moveEventLast = moveEvent;
                // get the next move event, erasing it from the queued move events.
                moveEvent = moveEventNext;
                // get the next move event, peeking to see if it is null (this does not erase it from the queue).
                moveEventNext = PeekNextMoveEvent();

                // we want to save move events that are the last move event in the queue, and are only facing changes.
                if ((moveEventNext == null) && (moveEventLast != null) &&
                    (moveEvent.X == moveEventLast.X) && (moveEvent.Y == moveEventLast.Y) && (moveEventLast.Z == moveEvent.Z) &&
                    (moveEvent.Facing != moveEventLast.Facing))
                {
                    // re-queue the final facing change, and return the second-to-last move event.
                    AddMoveEvent(moveEvent.X, moveEvent.Y, moveEvent.Z, moveEvent.Facing, false);
                    return moveEventLast;
                }
            }
            return moveEvent;
        }
        
        public void AcknowledgeMoveRequest(int sequence)
        {
            _History[sequence] = null;
            _LastSequenceAck = sequence;
        }

        public void RejectMoveRequest(int sequence, out int x, out int y, out int z, out int facing)
        {
            if (_History[sequence] != null)
            {
                MobileMoveEvent e = _History[sequence];
                x = e.X;
                y = e.Y;
                z = e.Z;
                facing = e.Facing;
            }
            else
            {
                x = y = z = facing = -1;
            }
            ResetMoveSequence();
        }
    }
}
