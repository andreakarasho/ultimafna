﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimaXNA.Ultima.Network.Client;
using UltimaXNA.Ultima.World;
using UltimaXNA.Ultima.World.Entities.Mobiles;

namespace UltimaXNA.Ultima.World.Entities.Mobiles
{
    public class Step
    {
        private static int _sequence = -1;

        public static void Reset() => _sequence = -1;

        private int _fastWalk;
        public Step(Position3D p, Direction direction)
        {
            _sequence++;
            if (_sequence > byte.MaxValue)
                _sequence = 1;
            Sequence = _sequence;
            DesideredPosition = p;
            DesideredDirection = direction;
            _fastWalk = new Random().Next(int.MinValue, int.MaxValue);
        }

        public int Sequence { get; set; }
        public Position3D DesideredPosition { get; }
        public Direction DesideredDirection { get; set; }
        public int FastWalk => _fastWalk;
        public bool Accepted { get; set; }

        public override string ToString()
        {
            return $"Position: {DesideredPosition}\tSeq: {Sequence}";
        }
    }

    public class StepsQueue
    {
        private Queue<Step> _steps = new Queue<Step>();

        public bool IsEmpty => _steps.Count == 0;
        public int Count => _steps.Count;

        public void Enqueue(Step step)
        {
            _steps.Enqueue(step);
        }

        public Step Dequeue()
        {
            Step step = _steps.Dequeue();
            return step;
        }

        public Step Peek()
        {
            return _steps.Peek();
        }

        public Step PeekAt(int i)
        {
           return _steps.ElementAt(i);
        }

        public void Clear(bool isplayer = false) { _steps.Clear(); if (isplayer) Step.Reset(); }
    }

    public class Walker
    {
        public static Action<MoveRequestPacket> SendMoveRequestPacket;

        private object _sync = new object();

        private Mobile _mobile;
        private Position3D _currentPosition, _nextPosition;
        private StepsQueue _stepsQueue;
        private double _moveSeq;
        private Direction _nextDirection;
        private bool _nowalk;

        private int _stepsCount;


        public Walker(Mobile mobile)
        {
            _mobile = mobile;
            _currentPosition = mobile.Position;
            _stepsQueue = new StepsQueue();
        }


        public bool IsRunning => (Facing & Direction.Running) == Direction.Running;
        public bool IsMoving
        {
            get
            {
                if (_nextPosition == null) return false;
                if (_currentPosition.Tile == _nextPosition.Tile && !_currentPosition.IsOffset)
                    return false;
                return true;
            }
        }
        public Direction Facing { get; set; }

        public Position3D Position => _currentPosition;
        public Position3D GoalPosition => _nextPosition;


        public void PlayerMobile_MoveEventAck(int seq) => AcceptedMovement(seq);
        public void PlayerMobile_MoveEventRej(int sequenceID, int x, int y, int z, int direction)
        {
           Move_Instant(x, y, z, direction);
            RejectedMovement();
        }
        public void PlayerMobile_ChangeFacing(Direction facing) { RequestMovement(facing, true); }
        public void PlayerMobile_Move(Direction facing) => RequestMovement(facing);
        public void Mobile_ServerAddMoveEvent(int x, int y, int z, int facing)
        {
            /*Step step = new Step(new Position3D(x, y, z),(Direction) facing);
            _stepsQueue.Enqueue(step);*/
        }
        public void Move_Instant(int x, int y, int z, int facing)
        {
            Reset();
            Facing = (Direction)facing;
            _currentPosition.Set(x, y, z);
            _nextPosition = null;
        }

        public void Reset()
        {
            _stepsQueue.Clear(_mobile.IsClientEntity);
            _stop = true;
        }

        public void RejectedMovement()
        {
            _stepsQueue.Clear();
            _stop = true;
        }

        public void AcceptedMovement(int seq)
        {
            Step step = _stepsQueue.Dequeue();
            if (step.Sequence == seq)
            {
                Console.WriteLine("NEW: {0},{1} - OLD: {2}, {3}", step.DesideredPosition.X, step.DesideredPosition.Y, _currentPosition.X, _currentPosition.Y);
                _stop = true;
            }
            else
            {
                Console.WriteLine("NEED RESYNC: {0}\t SEQ RECV: {1}", step, seq);
            }            
        }

        private bool _stop = true;

        public void RequestMovement(Direction nextDir, bool nowalk = false)
        {
            _nextDirection = nextDir;
            _nowalk = nowalk;

            Check();
        }

        public void DoWalk(Step step)
        {
            if (_currentPosition == step.DesideredPosition && Facing == step.DesideredDirection)
                return;

            _nextPosition = step.DesideredPosition;
            Facing = step.DesideredDirection;
        }

        private DateTime _lastStepRequestTime;

        private bool Check()
        {
            if (_nextDirection == Direction.Nothing  || _lastStepRequestTime > DateTime.Now)
                return false;

            Direction oldDirection = Facing &  Direction.FacingMask;
            Direction nextDir = _nextDirection & Direction.FacingMask;
            _nextDirection = Direction.Nothing;

            Direction direction = nextDir;


            if (/*CanWalk(ref direction, ref x, ref y, ref z)*/ PlayerMovementAdjustment(ref direction, out Position3D nextPosition) || _nowalk)
            {
              //  Position3D nextPosition = new Position3D(x, y, z);

                /* if ((nextDir &  Direction.Running) < Facing - 1 || (nextDir & Direction.Running) > Facing + 1)
                     _nowalk = true;
                     */

                

                if ((direction ) < oldDirection - 1 || (direction ) > oldDirection + 1)
                    _nowalk = true;

                if (_currentPosition != nextPosition || _nowalk)
                {
                    if ((nextDir & Direction.Running) != 0)
                        direction |= Direction.Running;
                    else
                        direction &= Direction.FacingMask;

                    _stop = false;
                    Step step = new Step(_nowalk? _currentPosition : nextPosition, direction);
                    _stepsQueue.Enqueue(step);
                  
                    SendMoveRequestPacket(new MoveRequestPacket((byte)direction, (byte)step.Sequence, step.FastWalk));
                    DoWalk(step);

                    double timecomplete = MovementSpeed.TimeToCompleteMove(_mobile, direction);
                    double diff = _ticks / timecomplete;

                    _lastStepRequestTime = DateTime.Now.AddMilliseconds(timecomplete - diff);

                    return true;
                }
            }
            return false;
        }


        private bool CanWalk(ref Direction direction, ref int x, ref int y, ref int z)
        {
            int newX = x;
            int newY = y;
            int newZ = z;
            Direction newDirection = direction;

            GetNewXY(ref direction, ref newX, ref newY);

            bool passes = MobileMovementCheck.CheckMovement(_mobile, _currentPosition/* new Position3D(newX, newY, newZ)*/, direction, out newZ);

            if ((int)direction % 2 == 1)
            {
                int[] dirOffset = new int[2] { 1, -1 };

                if (passes)
                {
                    for (int i = 0; i < 2 && passes; i++)
                    {
                        int testX = x;
                        int testY = y;
                        int testZ = z;

                        Direction testDir = (Direction)(((int)direction + dirOffset[i]) % 8);
                        GetNewXY(ref testDir, ref testX, ref testY);
                        passes = MobileMovementCheck.CheckMovement(_mobile, _currentPosition/* new Position3D(testX, testY, testZ)*/, testDir, out testZ);
                    }
                }

                if (!passes)
                {
                    for (int i = 0; i < 2 && !passes; i++)
                    {
                        newX = x;
                        newY = y;
                        newZ = z;

                        newDirection = (Direction)(((int)direction + dirOffset[i]) % 8);
                        GetNewXY(ref newDirection, ref newX, ref newY);
                        passes = MobileMovementCheck.CheckMovement(_mobile, _currentPosition/*new Position3D(newX, newY, newZ)*/, newDirection, out newZ);
                    }
                }
            }

            if (passes)
            {
                x = newX;
                y = newY;
                z = newZ;
                direction = newDirection;
            }

            return passes;
        }

        private void GetNewXY(ref Direction direction, ref int x, ref int y)
        {
            switch ((int)(direction & Direction.FacingMask))
            {
                case 0:
                    y--;
                    break;
                case 1:
                    x++; y--;
                    break;
                case 2:
                    x++;
                    break;
                case 3:
                    x++; y++;
                    break;
                case 4:
                    y++;
                    break;
                case 5:
                    x--; y++;
                    break;
                case 6:
                    x--;
                    break;
                case 7:
                    x--; y--;
                    break;
            }

        }

        private double _ticks;
        public void Update(double frameMS)
        {
            ///* if (!IsMoving )
            // {             
            //    // bool check = _mobile.IsClientEntity ? Check() : false;

            //     if (!_stepsQueue.IsEmpty)
            //     {
            //         Step step;
            //         if (check)
            //         {
            //            // while (!_stepsQueue.IsEmpty)
            //            // {
            //             /*    step = _stepsQueue.Peek();
            //             Console.WriteLine("SEND");
            //                 SendMoveRequestPacket(new MoveRequestPacket((byte)step.DesideredDirection, (byte)step.Sequence, step.FastWalk));
            //                 DoWalk(step);*/
            //            // }
            //         }
            //         else
            //         {
            //            /* if (!_mobile.IsClientEntity)
            //             {
            //                 while (!_stepsQueue.IsEmpty)
            //                 {
            //                     step = _stepsQueue.Dequeue();
            //                     DoWalk(step);
            //                 }
            //             }*/
            //         }

            //     }            
            // }

           // _ticks += frameMS;
            if (IsMoving)
            {
                double timecomplete = MovementSpeed.TimeToCompleteMove(_mobile, Facing);
                double diff = frameMS / timecomplete;
                _moveSeq += diff;

                _ticks = frameMS;


                if (_moveSeq < 1f) // animaione in corso
                {
                    _currentPosition.Offset = new Vector3(_nextPosition.X - _currentPosition.X, _nextPosition.Y - _currentPosition.Y, _nextPosition.Z - _currentPosition.Z) * (float)(_moveSeq);
                }
                else
                {
                    //if (_stop)
                    {
                        _currentPosition.Set(_nextPosition.X, _nextPosition.Y, _nextPosition.Z);
                        _currentPosition.Offset = Vector3.Zero;
                        _moveSeq = 0;
                    }
                }
            }
            else
            {
                if (_moveSeq > 0)
                    _moveSeq = 0;
            }
        }

        private bool PlayerMovementAdjustment(ref Direction direction, out Position3D nextPosition)
        {
            Point nextPoint = MobileMovementCheck.OffsetTile(_currentPosition, direction);
            direction = CalculateNextDirection(_currentPosition, nextPoint);
            bool ok = MobileMovementCheck.CheckMovement(_mobile, _currentPosition, direction, out int nextZ);
            if ((int)direction % 2 == 1)
            {
                if (!ok)
                {
                    direction = (direction - 1) & Direction.ValueMask;
                    nextPoint = MobileMovementCheck.OffsetTile(_currentPosition, direction);
                    ok = MobileMovementCheck.CheckMovement(_mobile, _currentPosition, direction, out nextZ);
                }

                if (!ok)
                {
                    direction = (direction + 2) & Direction.ValueMask;
                    nextPoint = MobileMovementCheck.OffsetTile(_currentPosition, direction);
                    ok = MobileMovementCheck.CheckMovement(_mobile, _currentPosition, direction, out nextZ);
                }
           }
            
            nextPosition = new Position3D(nextPoint.X, nextPoint.Y, nextZ);

            if (ok)
            {
                if (IsRunning)
                    direction |= Direction.Running;
                return true;
            }
            return false;
        }

        private Direction CalculateNextDirection(Position3D current, Point goal)
        {
            Direction facing;

            if (goal.X < current.X)
            {
                if (goal.Y < current.Y)
                    facing = Direction.Up;
                else if (goal.Y > current.Y)
                    facing = Direction.Left;
                else
                    facing = Direction.West;
            }
            else if (goal.X > current.X)
            {
                if (goal.Y < current.Y)
                    facing = Direction.Right;
                else if (goal.Y > current.Y)
                    facing = Direction.Down;
                else
                    facing = Direction.East;
            }
            else
            {
                if (goal.Y < current.Y)
                    facing = Direction.North;
                else if (goal.Y > current.Y)
                    facing = Direction.South;
                else
                {
                    // We should never reach 
                    facing = Facing & Direction.FacingMask;
                }
            }

            return facing;
        }
    }

    public class WalkerAdv
    {


        public WalkerAdv()
        {
            Steps = new List<Step>();
            /*for (int i = 0; i < 255; i++)
                _steps[i] = new Step();*/
        }

        public List<Step> Steps { get; }
        public int UnAcceptedPacketsCount { get; set; }
        public int StepsCount { get; set; }
        public int CurrentWalkSequence { get; set; }
        public bool ResendPacketSended { get; set; }
        public bool WalkingFailed { get; set; }
        public int WalkSequence { get; set; }
        public double LastStepRequestTime { get; set; }
        public Mobile Player => WorldModel.Entities.GetPlayerEntity();

        // quando arriva il pacchetto di ACK
        public void ConfirmWalk(int seq)
        {
            if (UnAcceptedPacketsCount > 0)
                UnAcceptedPacketsCount--;

            int stepindex = 0;

            for (int i = 0; i < StepsCount; i++)
            {
                if (Steps[i].Sequence == seq)
                    break;
                stepindex++;
            }

            bool isbadstep = stepindex == StepsCount;

            if (!isbadstep)
            {
                if (stepindex >= CurrentWalkSequence)
                    Steps[stepindex].Accepted = true;
                else if (stepindex <= 0)
                {
                    for (int i = 1; i < StepsCount; i++)
                        Steps[i - 1] = Steps[i];

                    StepsCount--;
                    CurrentWalkSequence--;
                }
                else
                    isbadstep = true;
            }

            if (isbadstep)
            {
                if (!ResendPacketSended)
                {
                    //send
                    ResendPacketSended = true;
                }
                WalkingFailed = true;
                StepsCount = 0;
                CurrentWalkSequence = 0;
            }
        }

        public void DenyWalk(int seq, int x, int y, int z)
        {
            Steps.Clear();
            Player.Position.Offset = Vector3.Zero;
            Reset();

            if (x != -1)
            {
                Player.Position.Set(x, y, z);
            }
        }

        public void Reset()
        {
            UnAcceptedPacketsCount = 0;
            StepsCount = 0;
            WalkSequence = 0;
            WalkingFailed = false;
            ResendPacketSended = false;
            LastStepRequestTime = 0;
        }
    }

    public class Mover
    {
        public static Action<MoveRequestPacket> SendMoveRequestPacket;

        private WalkerAdv _walker;
        private double _ticks;
        public Mover()
        {
            _walker = new WalkerAdv();
        }

        public WalkerAdv Walker => _walker;

        public bool IsWalking => LastStepTime > (_ticks - 200);

        public double LastStepTime { get; private set; }

        public bool Walk(bool run, Direction direction)
        {
            if (_walker.WalkingFailed || _walker.LastStepRequestTime > _ticks || _walker.StepsCount >= 4)
                return false;

            int x = _walker.Player.Position.X;
            int y = _walker.Player.Position.Y;
            int z = _walker.Player.Position.Z;
            Direction oldDirection = _walker.Player.Facing;

            bool onmount = false;

            bool emptystack = _walker.Steps.Count == 0;

            if (!emptystack)
            {
                Step step1 = _walker.Steps[_walker.StepsCount];
                x = step1.DesideredPosition.X;
                y = step1.DesideredPosition.Y;
                z = step1.DesideredPosition.Z;
                oldDirection = step1.DesideredDirection;
            }

            int oldZ = z;
            int walktime = 100;

            // fastrotation

            if (((int)oldDirection & 7) == ((int)direction & 7))
            {
                Direction newDir = direction;
                int newX = x;
                int newY = y;
                int newZ = z;

                if (!CanWalk(ref newDir, ref newX, ref newY, ref newZ))
                    return false;

                if (((int)direction % 7) != (int)newDir)
                    direction = newDir;
                else
                {
                    direction = newDir;
                    x = newX;
                    y = newY;
                    z = newZ;
                    walktime = MovementSpeed.TimeToCompleteMove(_walker.Player, direction);
                }
            }
            else
            {
                Direction newDir = direction;
                int newX = x;
                int newY = y;
                int newZ = z;

                if (!CanWalk(ref newDir, ref newX, ref newY, ref newZ))
                {
                    if (((int)oldDirection & 7) == (int)newDir)
                        return false;
                }

                if (((int)oldDirection & 7) == (int)newDir)
                {
                    x = newX;
                    y = newY;
                    z = newZ;

                    walktime = MovementSpeed.TimeToCompleteMove(_walker.Player, direction);
                }
                direction = newDir;
            }


            if (emptystack)
            {
                if (!IsWalking)
                    _walker.Player.Animation.Animate(Animations.MobileAction.Stand);
                LastStepTime = _ticks;
            }

            if (_walker.StepsCount > 0)
            {
                Step step = _walker.Steps[_walker.StepsCount - 1];
                step.Sequence = _walker.WalkSequence;
                step.Accepted = false;
                step.DesideredDirection = direction;
            }

            _walker.StepsCount++;

            _walker.Steps.Add(new Step(new Position3D(x, y, z), direction));

            SendMoveRequestPacket(new MoveRequestPacket((byte)direction, (byte)_walker.WalkSequence, 1000));

            if (_walker.WalkSequence == 0xFF)
                _walker.WalkSequence = 1;
            else
                _walker.WalkSequence++;

            _walker.UnAcceptedPacketsCount++;


            _walker.LastStepRequestTime = _ticks + walktime;

            _walker.Player.Animation.Animate(Animations.MobileAction.Walk);

            return true;
        }

        private bool CanWalk(ref Direction direction, ref int x, ref int y, ref int z)
        {
            int newX = x;
            int newY = y;
            int newZ = z;
            Direction newDirection = direction;

            Point newpoint = MobileMovementCheck.OffsetTile(new Position3D(newX, newY, newZ), newDirection);
            newX = newpoint.X;
            newY = newpoint.Y;

            bool passes = MobileMovementCheck.CheckMovement(_walker.Player, new Position3D(newX, newY, newZ), newDirection, out z);

            if ((int)direction % 2 != 0)
            {
                int[] dirOffset = new int[2] { 1, -1 };

                if (passes)
                {
                    for (int i = 0; i < 2 && passes; i++)
                    {
                        int testX = x;
                        int testY = y;
                        int testZ = z;

                        Direction testDir = (Direction)(((int)direction & dirOffset[i]) % 8);
                        Point newpoint1 = MobileMovementCheck.OffsetTile(new Position3D(testX, testY, testZ), testDir);
                        testX = newpoint1.X;
                        testY = newpoint1.Y;

                        passes = MobileMovementCheck.CheckMovement(_walker.Player, new Position3D(testX, testY, testZ), testDir, out testZ);
                    }
                }

                if (!passes)
                {
                    for (int i = 0; i < 2 && !passes; i++)
                    {
                        newX = x;
                        newY = y;
                        newZ = z;

                        newDirection = (Direction)(((int)direction + dirOffset[i]) %8);
                        Point newpoint2 = MobileMovementCheck.OffsetTile(new Position3D(newX, newY, newZ), newDirection);
                        newX = newpoint2.X;
                        newY = newpoint2.Y;

                        passes = MobileMovementCheck.CheckMovement(_walker.Player, new Position3D(newX, newY, newZ), newDirection, out newZ);
                    }
                }
            }

            if (passes)
            {
                x = newX;
                y = newY;
                z = newZ;
                direction = newDirection;
            }

            return passes;
        }


        public void Update(double frameMS)
        {
            _ticks += frameMS;
        }
    }
}
