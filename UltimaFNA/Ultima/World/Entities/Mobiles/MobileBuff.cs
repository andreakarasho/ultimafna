﻿using System.Collections.Generic;
using System;

namespace UltimaXNA.Ultima.World.Entities.Mobiles
{
    public class PlayerBuff
    {
		public class BuffData
		{
			private int _icon;
			public int Icon { get { return _icon; } set { _icon = value; } }

			private int _duration;
			public int Duration { get { return _duration; } set { _duration = value; } }

			private int _titlecliloc;
			public int TitleCliloc { get { return _titlecliloc; } set { _titlecliloc = value; } }

			private int _descriptioncliloc1;
			public int DescriptionCliloc1 { get { return _descriptioncliloc1; } set { _descriptioncliloc1 = value; } }

			private int _descriptioncliloc2;
			public int DescriptionCliloc2 { get { return _descriptioncliloc2; } set { _descriptioncliloc2 = value; } }

			private DateTime _activationtime;
			public DateTime ActivationTime { get { return _activationtime; } set { _activationtime = value; } }

			public BuffData(int duration, int titlecliloc, int descriptioncliloc1, int descriptioncliloc2, int icon)
			{
				_icon = icon;
				_duration = duration;
				_titlecliloc = titlecliloc;
				_descriptioncliloc1 = descriptioncliloc1;
				_descriptioncliloc2 = descriptioncliloc2;
				_activationtime = DateTime.Now;
			}
		}

		private static Dictionary<int, BuffData> _data = new Dictionary<int, BuffData>();

		public static Dictionary<int, BuffData> Data { get { return _data; } }
		
		public static void Add(int icon, int duration, int cliloc1, int cliloc2, int cliloc3)
		{
			if (_data.ContainsKey(icon))
				return;
            BuffData data = new BuffData(duration, cliloc1, cliloc2, cliloc3, icon);
            _data.Add(icon, data);
		}

		public static void Remove(int icon)
		{
			if (_data.ContainsKey(icon))
				_data.Remove(icon);
		}
	}
}
