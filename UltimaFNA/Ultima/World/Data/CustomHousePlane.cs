﻿using UltimaXNA.Core.Network;
using UltimaXNA.Core.Network.Compression;

namespace UltimaXNA.Ultima.World.Data
{
    public class CustomHousePlane
    {
        public readonly int Index;
        public readonly bool IsFloor;
        public readonly byte[] ItemData;
        public int PlaneMode { get; }
        public int PlaneZ { get; }

        public CustomHousePlane(PacketReader reader)
        {
               uint header = reader.ReadUInt32();
               int dlen = (int)( ((header & 0xFF0000) >> 16) | ((header & 0xF0) << 4));
               uint clen = ((header & 0xFF00) >> 8) | ((header & 0x0F)<< 8);
               uint planeZ = (header & 0x0F000000) >> 24;
               uint planeMode = (header & 0xF0000000) >> 28;

               if (clen <= 0)
                   return;

               ItemData = new byte[dlen];

               if (ZlibCompression.Unpack(ItemData, ref dlen, reader.ReadBytes((int)clen), (int)clen) != ZLibError.Okay)
               {
                   return;
               }


               PlaneMode = (int)planeMode;
               PlaneZ = (int)planeZ;              
             /*byte[] data = reader.ReadBytes(4);
             Index = data[0];
             int uncompressedsize = data[1] + ((data[3] & 0xF0) << 4);
             int compressedLength = data[2] + ((data[3] & 0xF) << 8);
             ItemData = new byte[uncompressedsize];
             ZlibCompression.Unpack(ItemData, ref uncompressedsize, reader.ReadBytes(compressedLength), compressedLength);

             IsFloor = ((Index & 0x20) == 0x20);
             Index &= 0x1F;*/
        }
    }
}
