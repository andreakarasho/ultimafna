﻿/***************************************************************************
 *   CustomHouse.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using Microsoft.Xna.Framework;
using UltimaXNA.Ultima.World.Entities.Items;
using UltimaXNA.Ultima.World.Entities.Multis;

namespace UltimaXNA.Ultima.World.Data
{
    class CustomHouse
    {
        private int _planeCount;
        private CustomHousePlane[] _planes;
        private Multi _foundation;

        public CustomHouse(Serial serial)
        {
            Serial = serial;
        }

        public Serial Serial { get; }
        public int Hash { get; private set; }

        public bool HasTiles => _foundation != null;

        public void Update(int hash, int planecount, CustomHousePlane[] planes)
        {
            Hash = hash;
            _planeCount = planecount;
            _planes = planes;
        }
     
        public void CreateHouse(Multi foundation)
        {
            _foundation = foundation;

            if (_foundation == null)
                return;

            short minX = (short)_foundation.Components.Min.X;
            short minY = (short)_foundation.Components.Min.Y;
            short maxY = (short)_foundation.Components.Max.Y;
            Point center = _foundation.Components.Center;

            foreach (CustomHousePlane plane in _planes)
            {
                short id = 0;
                byte x, y;
                sbyte z;
                int index = 0;
                switch (plane.PlaneMode)
                {
                    case 0:
                        {
                            for (uint i = 0; i < plane.ItemData.Length / 5; i++)
                            {
                                id = (short)((plane.ItemData[index++] << 8) + plane.ItemData[index++]);
                                x = plane.ItemData[index++];
                                y = plane.ItemData[index++];
                                z = (sbyte)plane.ItemData[index++];

                                x = (byte)(x + center.X);//((width >> 1) + x- 1);
                                y = (byte)(y + center.Y);//((height >> 1) + y - 1);

                                if (id != 0)
                                    AddTile(id, x, y, z); //statics.Add(new StaticTile() { ID = id, X = x, Y = y, Z = z });
                            }
                            break;
                        }
                    case 1:
                        {
                            if (plane.PlaneZ > 0)
                                z = (sbyte)(((plane.PlaneZ - 1) % 4) * 20 + 7);
                            else
                                z = 0;

                            for (uint i = 0; i < plane.ItemData.Length / 4; i++)
                            {
                                id = (short)((plane.ItemData[index++] << 8) + plane.ItemData[index++]);
                                x = plane.ItemData[index++];
                                y = plane.ItemData[index++];

                                if (id != 0)
                                    AddTile(id, x, y, z);// statics.Add(new StaticTile() { ID = id, X = x, Y = y, Z = z });
                            }

                            break;
                        }
                    case 2:
                        {
                            short xOffs = 0;
                            short yOffs = 0;
                            short multiHeight = 0;

                            if (plane.PlaneZ > 0)
                                z = (sbyte)(((plane.PlaneZ - 1) % 4) * 20 + 7); // Z=7,27,47,67
                            else
                                z = 0;

                            if (plane.PlaneZ <= 0)
                            {
                                xOffs = minX;
                                yOffs = minY;
                                multiHeight = (short)((maxY - minY) + 2);
                            }
                            else if (plane.PlaneZ <= 4)
                            {
                                xOffs = (short)(minX + 1);
                                yOffs = (short)(minY + 1);
                                multiHeight = (short)(maxY - minY);
                            }
                            else
                            {
                                xOffs = minX;
                                yOffs = minY;
                                multiHeight = (short)((maxY - minY) + 1);
                            }

                            for (uint i = 0; i < plane.ItemData.Length / 2; i++)
                            {
                                id = (short)((plane.ItemData[index++] << 8) + plane.ItemData[index++]);
                                x = (byte)((i / multiHeight + xOffs) + center.X);
                                y = (byte)((i % multiHeight + yOffs) + center.Y);

                                if (id != 0)
                                    AddTile(id, x, y, z); //statics.Add( new StaticTile() { ID = id, X = x, Y = y, Z = z });
                            }

                            break;
                        }
                    default: break;
                }
            }
        }

        private void AddTile(int id, int x, int y, int z)
        {
            StaticItem item = new StaticItem(id, 0, 0, _foundation.Map);
            item.Position.Set(_foundation.Components.Min.X + _foundation.X + x, _foundation.Components.Min.Y + _foundation.Y + y, _foundation.Z + z);
        }

    }
}
