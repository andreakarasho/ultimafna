﻿/***************************************************************************
 *   CustomHousing.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using System.Collections.Generic;

namespace UltimaXNA.Ultima.World.Data
{
    class CustomHousing
    {
        private static readonly Dictionary<Serial, CustomHouse> _CustomHouses = new Dictionary<Serial, CustomHouse>();

        public static bool IsHashCurrent(Serial serial, int hash)
        {
            if (_CustomHouses.ContainsKey(serial))
            {
                CustomHouse h = _CustomHouses[serial];
                return (h.Hash == hash);
            }
            return false;
        }

        public static CustomHouse GetCustomHouseData(Serial serial) => _CustomHouses[serial];
        public static bool Contains(Serial serial) => _CustomHouses.ContainsKey(serial);

        public static void UpdateCustomHouseData(Serial serial, int hash, int planecount, CustomHousePlane[] planes)
        {
            CustomHouse house;
            if (_CustomHouses.ContainsKey(serial))
            {
                house = _CustomHouses[serial];
            }
            else
            {
                house = new CustomHouse(serial);
                _CustomHouses.Add(serial, house);
            }
            house.Update(hash, planecount, planes);
        }
    }
}