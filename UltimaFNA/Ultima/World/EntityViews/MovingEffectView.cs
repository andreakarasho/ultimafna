﻿using Microsoft.Xna.Framework;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Ultima.IO;
using UltimaXNA.Ultima.Resources;
using UltimaXNA.Ultima.World.Entities.Effects;
using UltimaXNA.Ultima.World.Input;
using UltimaXNA.Ultima.World.Maps;
using UltimaXNA.Ultima.World.WorldViews;

namespace UltimaXNA.Ultima.World.EntityViews
{
    public class MovingEffectView : AEntityView
    {
        MovingEffect Effect => (MovingEffect)Entity;

        EffectData _AnimData;
        readonly bool _Animated;
        int _DisplayItemID = -1;

        public MovingEffectView(MovingEffect effect)
            : base(effect)
        {
            _Animated = TileData.ItemData[Effect.ItemID & FileManager.ItemIDMask].IsAnimation;
            if (_Animated)
            {
                _AnimData = Provider.GetResource<EffectData>(Effect.ItemID);
                _Animated = _AnimData.FrameCount > 0;
            }
        }

        public override bool Draw(SpriteBatch3D spriteBatch, Vector3 drawPosition, MouseOverList mouseOver, Map map, bool roofHideFlag)
        {
            CheckDefer(map, drawPosition);
            return DrawInternal(spriteBatch, drawPosition, mouseOver, map, roofHideFlag);
        }

        public override bool DrawInternal(SpriteBatch3D spriteBatch, Vector3 drawPosition, MouseOverList mouseOver, Map map, bool roofHideFlag)
        {
            int displayItemdID = (_Animated) ? Effect.ItemID + ((Effect.FramesActive / _AnimData.FrameInterval) % _AnimData.FrameCount) : Effect.ItemID;
            if (displayItemdID != _DisplayItemID)
            {
                _DisplayItemID = displayItemdID;
                DrawTexture = Provider.GetItemTexture(_DisplayItemID);
                DrawArea = new Rectangle(DrawTexture.Width / 2 - IsometricRenderer.TILE_SIZE_INTEGER_HALF, DrawTexture.Height - IsometricRenderer.TILE_SIZE_INTEGER, DrawTexture.Width, DrawTexture.Height);
                PickType = PickType.PickNothing;
                DrawFlip = false;
            }
            DrawArea.X = 0 - (int)((Entity.Position.X_offset - Entity.Position.Y_offset) * IsometricRenderer.TILE_SIZE_INTEGER_HALF);
            DrawArea.Y = 0 + (int)((Entity.Position.Z_offset + Entity.Z) * 4) - (int)((Entity.Position.X_offset + Entity.Position.Y_offset) * IsometricRenderer.TILE_SIZE_INTEGER_HALF);
            Rotation = Effect.AngleToTarget;
            HueVector = Utility.GetHueVector(Entity.Hue);
            return base.Draw(spriteBatch, drawPosition, mouseOver, map, roofHideFlag);
        }
    }
}
