﻿/***************************************************************************
 *   DeferredView.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
using Microsoft.Xna.Framework;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Ultima.World.Input;
using UltimaXNA.Ultima.World.Maps;
using UltimaXNA.Ultima.World.Entities.Mobiles;
using UltimaXNA.Ultima.World.Entities;

namespace UltimaXNA.Ultima.World.EntityViews
{
    public class DeferredView : AEntityView
    {
        readonly Vector3 _DrawPosition;
        readonly AEntityView _BaseView;

        public DeferredView(DeferredEntity entity, Vector3 drawPosition, AEntityView baseView)
            : base(entity)
        {
            _DrawPosition = drawPosition;
            _BaseView = baseView;
        }

        public override bool Draw(SpriteBatch3D spriteBatch, Vector3 drawPosition, MouseOverList mouseOver, Map map, bool roofHideFlag)
        {
            if (_BaseView.Entity is Mobile)
            { 
                Mobile mobile = _BaseView.Entity as Mobile;
                if (!mobile.IsAlive || mobile.IsDisposed || mobile.Body == 0)
                {
                    Entity.Dispose();
                    return false;
                }
            }
            /*_BaseView.SetYClipLine(_DrawPosition.Y - 22 -
                ((_BaseView.Entity.Position.Z + _BaseView.Entity.Position.Z_offset) * 4) +
                ((_BaseView.Entity.Position.X_offset + _BaseView.Entity.Position.Y_offset) * IsometricRenderer.TILE_SIZE_INTEGER_HALF));*/
            bool success = _BaseView.DrawInternal(spriteBatch, _DrawPosition, mouseOver, map, roofHideFlag);
            /*_BaseView.ClearYClipLine();*/
            return success;
        }
    }
}
