﻿using System;
using System.Collections.Generic;

namespace UltimaXNA.Ultima.Player
{
    public class JournalData
    {
        private readonly List<JournalEntry> _JournalEntries = new List<JournalEntry>();
        public List<JournalEntry> JournalEntries
        {
            get { return _JournalEntries; }
        }

        public event Action<JournalEntry> OnJournalEntryAdded;

        public void AddEntry(string text, int font, ushort hue, string speakerName, bool asUnicode)
        {
            while (_JournalEntries.Count > 99)
                _JournalEntries.RemoveAt(0);
            _JournalEntries.Add(new JournalEntry(text, font, hue, speakerName, asUnicode));
            OnJournalEntryAdded?.Invoke(_JournalEntries[_JournalEntries.Count - 1]);
        }
    }

    public class JournalEntry
    {
        public readonly string Text;
        public readonly int Font;
        public readonly ushort Hue;
        public readonly string SpeakerName;
        public readonly bool AsUnicode;

        public JournalEntry(string text, int font, ushort hue, string speakerName, bool asUnicode)
        {
            Text = text;
            Font = font;
            Hue = hue;
            SpeakerName = speakerName;
            AsUnicode = asUnicode;
        }
    }
}
