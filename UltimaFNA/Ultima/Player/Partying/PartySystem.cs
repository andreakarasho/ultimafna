﻿/***************************************************************************
 *   PartySystem.cs
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using System.Collections.Generic;
using UltimaFNA.Core.Network.SocketAsync;
using UltimaXNA.Core.Network;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.Network.Client;
using UltimaXNA.Ultima.Network.Client.Partying;
using UltimaXNA.Ultima.Network.Server.GeneralInfo;
using UltimaXNA.Ultima.UI;
using UltimaXNA.Ultima.UI.WorldGumps;
using UltimaXNA.Ultima.World;

namespace UltimaXNA.Ultima.Player.Partying
{
    public class PartySystem
    {
        Serial _LeaderSerial;
        Serial _InvitingPartyLeader;
        List<PartyMember> _PartyMembers = new List<PartyMember>();
        bool _AllowPartyLoot;

        public Serial LeaderSerial => _LeaderSerial;
        public List<PartyMember> Members => _PartyMembers;
        public bool InParty => _PartyMembers.Count > 1;
        public bool PlayerIsLeader => InParty && PlayerState.Partying.LeaderSerial == WorldModel.PlayerSerial;

        public bool AllowPartyLoot
        {
            get
            {
                return _AllowPartyLoot;
            }
            set
            {
                _AllowPartyLoot = value;
                NetworkClient network = Service.Get<NetworkClient>();
                network.Send(new PartyCanLootPacket(_AllowPartyLoot));
            }
        }

        public void ReceivePartyMemberList(PartyMemberListInfo info)
        {
            bool wasInParty = InParty;
            _PartyMembers.Clear();
            for (int i = 0; i < info.Count; i++)
                AddMember(info.Serials[i]);
            RefreshPartyGumps();
            if (InParty && !wasInParty)
            {
                AllowPartyLoot = _AllowPartyLoot;
            }
        }

        public void ReceiveRemovePartyMember(PartyRemoveMemberInfo info)
        {
            _PartyMembers.Clear();
            for (int i = 0; i < info.Count; i++)
                AddMember(info.Serials[i]);
            RefreshPartyGumps();
        }

        public void ReceiveInvitation(PartyInvitationInfo info)
        {
            _InvitingPartyLeader = info.PartyLeaderSerial;
        }

        public void AddMember(Serial serial)
        {
            int index = _PartyMembers.FindIndex(p => p.Serial == serial);
            if (index != -1)
            {
                _PartyMembers.RemoveAt(index);
            }
            _PartyMembers.Add(new PartyMember(serial));
            NetworkClient network = Service.Get<NetworkClient>();
            network.Send(new MobileQueryPacket(MobileQueryPacket.StatusType.BasicStatus, serial));
        }

        public PartyMember GetMember(int index)
        {
            if (index >= 0 && index < _PartyMembers.Count)
                return _PartyMembers[index];
            return null;
        }

        public PartyMember GetMember(Serial serial)
        {
            return _PartyMembers.Find(p => p.Serial == serial);
        }

        public void LeaveParty()
        {
            NetworkClient network = Service.Get<NetworkClient>();
            network.Send(new PartyLeavePacket());
            _PartyMembers.Clear();
            _LeaderSerial = Serial.Null;
            UserInterfaceService ui = Service.Get<UserInterfaceService>();
            ui.RemoveControl<PartyHealthTrackerGump>();
        }

        public void DoPartyCommand(string text)
        {
            // I do this a little differently than the legacy client. With legacy, if you type "/add this other player,
            // please ?" the client will detect the first word is "add" and request an add player target. Instead, I
            // interpret this as a message, and send the message "add this other player, please?" as a party message.
            NetworkClient network = Service.Get<NetworkClient>();
            WorldModel world = Service.Get<WorldModel>();
            string command = text.ToLower();
            bool commandHandled = false;
            switch (command)
            {
                case "help":
                    ShowPartyHelp();
                    commandHandled = true;
                    break;
                case "add":
                    RequestAddPartyMemberTarget();
                    commandHandled = true;
                    break;
                case "rem":
                case "remove":
                    if (InParty && PlayerIsLeader)
                    {
                        world.Interaction.ChatMessage("Who would you like to remove from your party?", 3, 10, false);
                        network.Send(new PartyRequestRemoveTargetPacket());
                    }
                    commandHandled = true;
                    break;
                case "accept":
                    if (!InParty && _InvitingPartyLeader.IsValid)
                    {
                        network.Send(new PartyAcceptPacket(_InvitingPartyLeader));
                        _LeaderSerial = _InvitingPartyLeader;
                        _InvitingPartyLeader = Serial.Null;
                    }
                    commandHandled = true;
                    break;
                case "decline":
                    if (!InParty && _InvitingPartyLeader.IsValid)
                    {
                        network.Send(new PartyDeclinePacket(_InvitingPartyLeader));
                        _InvitingPartyLeader = Serial.Null;
                    }
                    commandHandled = true;
                    break;
                case "quit":
                    LeaveParty();
                    commandHandled = true;
                    break;
            }
            if (!commandHandled)
            {
                network.Send(new PartyPublicMessagePacket(text));
            }
        }

        internal void BeginPrivateMessage(int serial)
        {
            PartyMember member = GetMember((Serial)serial);
            if (member != null)
            {
                ChatControl chat = Service.Get<ChatControl>();
                chat.SetModeToPartyPrivate(member.Name, member.Serial);
            }
        }

        public void SendPartyPrivateMessage(Serial serial, string text)
        {
            WorldModel world = Service.Get<WorldModel>();
            PartyMember recipient = GetMember(serial);
            if (recipient != null)
            {
                NetworkClient network = Service.Get<NetworkClient>();
                network.Send(new PartyPrivateMessagePacket(serial, text));
                world.Interaction.ChatMessage($"To {recipient.Name}: {text}", 3, Settings.UserInterface.PartyPrivateMsgColor, false);
            }
            else
            {
                world.Interaction.ChatMessage("They are no longer in your party.", 3, Settings.UserInterface.PartyPrivateMsgColor, false);
            }
        }

        internal void RequestAddPartyMemberTarget()
        {
            NetworkClient network = Service.Get<NetworkClient>();
            WorldModel world = Service.Get<WorldModel>();
            if (!InParty)
            {
                _LeaderSerial = WorldModel.PlayerSerial;
                network.Send(new PartyRequestAddTargetPacket());
            }
            else if (InParty && PlayerIsLeader)
            {
                network.Send(new PartyRequestAddTargetPacket());
            }
            else if (InParty && !PlayerIsLeader)
            {
                world.Interaction.ChatMessage("You may only add members to the party if you are the leader.", 3, 10, false);
            }
        }

        public void RefreshPartyGumps()
        {
            UserInterfaceService ui = Service.Get<UserInterfaceService>();
            ui.RemoveControl<PartyHealthTrackerGump>();
            for (int i = 0; i < Members.Count; i++)
            {
                ui.AddControl(new PartyHealthTrackerGump(Members[i]), 5, 40 + (48 * i));
            }
            Gump gump;
            if ((gump = ui.GetControl<PartyGump>()) != null)
            {
                int x = gump.X;
                int y = gump.Y;
                ui.RemoveControl<PartyGump>();
                ui.AddControl(new PartyGump(), x, y);
            }
        }

        public void RemoveMember(Serial serial)
        {
            NetworkClient network = Service.Get<NetworkClient>();
            network.Send(new PartyRemoveMemberPacket(serial));
            int index = _PartyMembers.FindIndex(p => p.Serial == serial);
            if (index != -1)
            {
                _PartyMembers.RemoveAt(index);
            }
        }

        public void ShowPartyHelp()
        {
            WorldModel _world = Service.Get<WorldModel>();
            _world.Interaction.ChatMessage("/add       - add a new member or create a party", 3, 51, true);
            _world.Interaction.ChatMessage("/rem       - kick a member from your party", 3, 51, true);
            _world.Interaction.ChatMessage("/accept    - join a party", 3, 51, true);
            _world.Interaction.ChatMessage("/decline   - decline a party invitation", 3, 51, true);
            _world.Interaction.ChatMessage("/quit      - leave your current party", 3, 51, true);
        }
    }
}