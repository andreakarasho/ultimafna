﻿/***************************************************************************
 *   SkillData.cs
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System;
using System.Collections.Generic;
using UltimaXNA.Ultima.Resources;
#endregion

namespace UltimaXNA.Ultima.Player
{
    public class SkillData
    {
        public Action<SkillEntry> OnSkillChanged;

        private readonly Dictionary<int, SkillEntry> _Skills = new Dictionary<int, SkillEntry>();
        private bool _SkillsLoaded;

        public Dictionary<int, SkillEntry> List
        {
            get
            {
                if (!_SkillsLoaded)
                {
                    _SkillsLoaded = true;
                    foreach (Skill skill in SkillsData.List)
                        if (skill.Index == -1)
                        {
                            // do nothing.
                        }
                        else
                        {
                            _Skills.Add(skill.ID, new SkillEntry(this, skill.ID, skill.Index, skill.UseButton, skill.Name, 0.0f, 0.0f, 0, 0.0f));
                        }
                }
                return _Skills;
            }
        }

        public SkillEntry SkillEntry(int skillID)
        {
            if (List.Count > skillID)
                return List[skillID];
            else
                return null;
        }

        public SkillEntry SkillEntryByIndex(int index)
        {
            foreach (SkillEntry skill in _Skills.Values)
                if (skill.Index == index)
                    return skill;
            return null;
        }
    }

    public class SkillEntry
    {
        private readonly SkillData _DataParent;

        private int _id;
        private int _index;
        private bool _hasUseButton;
        private string _name;
        private float _value;
        private float _valueUnmodified;
        private byte _lockType;
        private float _cap;

        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        public int Index
        {
            get { return _index; }
            set { _index = value; }
        }
        public bool HasUseButton
        {
            get { return _hasUseButton; }
            set { _hasUseButton = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public float Value
        {
            get { return _value; }
            set
            {
                _value = value;
                _DataParent.OnSkillChanged?.Invoke(this);
            }
        }
        public float ValueUnmodified
        {
            get { return _valueUnmodified; }
            set
            {
                _valueUnmodified = value;
                _DataParent.OnSkillChanged?.Invoke(this);
            }
        }
        public byte LockType
        {
            get { return _lockType; }
            set
            {
                _lockType = value;
                _DataParent.OnSkillChanged?.Invoke(this);
            }
        }
        public float Cap
        {
            get { return _cap; }
            set
            {
                _cap = value;
                _DataParent.OnSkillChanged?.Invoke(this);
            }
        }

        public SkillEntry(SkillData dataParent, int id, int index, bool useButton, string name, float value, float unmodified, byte locktype, float cap)
        {
            _DataParent = dataParent;
            ID = id;
            Index = index;
            HasUseButton = useButton;
            Name = name;
            Value = value;
            ValueUnmodified = unmodified;
            LockType = locktype;
            Cap = cap;
        }
    }
}
