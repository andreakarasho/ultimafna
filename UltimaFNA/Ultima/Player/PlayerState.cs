﻿using UltimaXNA.Ultima.Data;
using UltimaXNA.Ultima.Player.Partying;

namespace UltimaXNA.Ultima.Player {
    class PlayerState {
        static readonly PlayerState _Instance;

        JournalData _Journal;
        SkillData _Skills;
        StatLockData _StatLocks;
        PartySystem _Partying;
        Features _Features;

        static PlayerState() {
            _Instance = new PlayerState();
            _Instance._Journal = new JournalData();
            _Instance._Skills = new SkillData();
            _Instance._StatLocks = new StatLockData();
            _Instance._Partying = new PartySystem();
            _Instance._Features = new Features();
        }

        public static JournalData Journaling => _Instance._Journal;
        public static SkillData Skills => _Instance._Skills;
        public static StatLockData StatLocks => _Instance._StatLocks;
        public static PartySystem Partying => _Instance._Partying;
        public static Features ClientFeatures => _Instance._Features;
        public static AssistantFeatures DisabledFeatures = AssistantFeatures.None;
    }
}
