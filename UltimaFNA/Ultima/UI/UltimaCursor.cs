﻿/***************************************************************************
 *   UltimaCursor.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Resources;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.Resources;
using UltimaXNA.Ultima.World;
#endregion

namespace UltimaXNA.Ultima.UI
{
    class UltimaCursor : ICursor
    {
        private HuedTexture _CursorSprite;
        private int _CursorSpriteArtIndex = -1;
        protected Tooltip _Tooltip;

        public int CursorSpriteArtIndex
        {
            get { return _CursorSpriteArtIndex; }
            set
            {
                if (value != _CursorSpriteArtIndex)
                {
                    _CursorSpriteArtIndex = value;

                    IResourceProvider provider = Service.Get<IResourceProvider>();
                    Texture2D art = provider.GetItemTexture(_CursorSpriteArtIndex);
                    if (art == null)
                    {
                        // shouldn't we have a debug texture to show that we are missing this cursor art? !!!
                        _CursorSprite = null;
                    }
                    else
                    {
                        Rectangle sourceRect = new Rectangle(1, 1, art.Width - 2, art.Height - 2);
                        _CursorSprite = new HuedTexture(art, Point.Zero, sourceRect, 0);
                    }
                }
            }
        }

        public Point CursorOffset
        {
            get;
            protected set;
        }

        public int CursorHue
        {
            get;
            protected set;
        }

        private UserInterfaceService _UserInterface;

        public UltimaCursor()
        {
            _UserInterface = Service.Get<UserInterfaceService>();
        }

        public virtual void Dispose()
        {
            _UserInterface = null;
        }

        public virtual void Update()
        {

        }

        protected virtual void BeforeDraw(SpriteBatchUI spriteBatch, Point position)
        {
            // Over the interface or not in world. Display a default cursor.
            int artworkIndex = 8305;

            if (WorldModel.IsInWorld && WorldModel.Entities.GetPlayerEntity().Flags.IsWarMode)
            {
                // if in warmode, show the red-hued cursor.
                artworkIndex -= 23;
            }

            CursorSpriteArtIndex = artworkIndex;
            CursorOffset = new Point(-1, 1);
        }

        public void Draw(SpriteBatchUI spriteBatch, Point position)
        {
            BeforeDraw(spriteBatch, position);
            if (_CursorSprite != null)
            {
                _CursorSprite.Hue = CursorHue;
                _CursorSprite.Offset = CursorOffset;
                _CursorSprite.Draw(spriteBatch, position);
            }

            DrawTooltip(spriteBatch, position);
        }

        protected virtual void DrawTooltip(SpriteBatchUI spritebatch, Point position)
        {
            if (_UserInterface.IsMouseOverUI && _UserInterface.MouseOverControl != null && _UserInterface.MouseOverControl.HasTooltip)
            {
                if (_Tooltip != null && _Tooltip.Caption != _UserInterface.MouseOverControl.Tooltip)
                {
                    _Tooltip.Dispose();
                    _Tooltip = null;
                }
                if (_Tooltip == null)
                {
                    _Tooltip = new Tooltip(_UserInterface.MouseOverControl.Tooltip);
                }
                _Tooltip.Draw(spritebatch, position.X, position.Y + 24);
            }
            else
            {
                if (_Tooltip != null)
                {
                    _Tooltip.Dispose();
                    _Tooltip = null;
                }
            }
        }
    }
}
