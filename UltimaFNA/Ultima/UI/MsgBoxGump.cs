﻿/***************************************************************************
 *   MsgBoxGump.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System;
using UltimaXNA.Ultima.UI.Controls;
using UltimaXNA.Ultima.World;
#endregion

namespace UltimaXNA.Ultima.UI
{
    public enum MsgBoxTypes
    {
        OkOnly,
        OkCancel
    }

    public class MsgBoxGump : Gump
    {
        /// <summary>
        /// Opens a modal message box with either 'OK' or 'OK and Cancel' buttons.
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="type"></param>
        public static MsgBoxGump Show(string msg, MsgBoxTypes type, bool isloginmessage = false)
        {
            MsgBoxGump gump = new MsgBoxGump(msg, type, isloginmessage);
			return gump;
        }

        private string _msg;
        private HtmlGumpling _text;
        private MsgBoxTypes _type;
		private bool _IsLoginMessage;

        public Action OnClose;
        public Action OnCancel;

        private MsgBoxGump(string msg, MsgBoxTypes msgBoxType, bool isloginmessage)
            : base(0, 0)
        {
            _msg = "<big color=000000>" + msg;
            _type = msgBoxType;
			_IsLoginMessage = isloginmessage;
			UserInterface.AddControl(this, 0, 0);
            MetaData.IsModal = true;
        }

        public override void Update(double totalMS, double frameMS)
        {
            if (IsInitialized && _text == null)
            {
                ResizePic resize;

                _text = new HtmlGumpling(this, 10, 10, 200, 200, 0, 0, _msg);
                int width = _text.Width + 20;
                AddControl(resize = new ResizePic(this, 0, 0, 9200, width, _text.Height + 45));
                AddControl(_text);
                // Add buttons
                switch (_type)
                {
                    case MsgBoxTypes.OkOnly:
                        AddControl(new Button(this, (_text.Width + 20) / 2 - 23, _text.Height + 15, 2074, 2075, ButtonTypes.Activate, 0, 0));
                        ((Button)LastControl).GumpOverID = 2076;
                        break;
                    case MsgBoxTypes.OkCancel:
                        AddControl(new Button(this, (width / 2) - 46 - 10, _text.Height + 15, 0x817, 0x818, ButtonTypes.Activate, 0, 1));
                        ((Button)LastControl).GumpOverID = 0x819;
                        AddControl(new Button(this, (width / 2) + 10, _text.Height + 15, 2074, 2075, ButtonTypes.Activate, 0, 0));
                        ((Button)LastControl).GumpOverID = 2076;
                        break;
                }
                
                base.Update(totalMS, frameMS);
                CenterThisControlOnScreen();
            }
            base.Update(totalMS, frameMS);
        }

        public override void OnButtonClick(int buttonID)
        {
            switch (buttonID)
            {
                case 0:
					if (_IsLoginMessage) // Esegue in caso di messaggio al login per ritornare allo stato iniziale
					{
						Service.Get<Ultima.Login.LoginModel>().ResetToLogin();
					}
                    OnClose?.Invoke();
                    break;
                case 1:
                    OnCancel?.Invoke();
                    break;
            }
            Dispose();
        }
    }
}
