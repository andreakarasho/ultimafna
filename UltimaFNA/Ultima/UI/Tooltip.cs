﻿/***************************************************************************
 *   Tooltip.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using Microsoft.Xna.Framework;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Resources;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.UI.Controls;
using UltimaXNA.Ultima.World.Entities;
#endregion

namespace UltimaXNA.Ultima.UI
{
    class Tooltip
    {
        public string Caption
        {
            get;
            protected set;
        }

        RenderedText _RenderedText;

        int _PropertyListHash;
        AEntity _Entity;

        public Tooltip(string caption)
        {
            _Entity = null;
            Caption = caption;
        }

        public Tooltip(AEntity entity)
        {
            _Entity = entity;
            _PropertyListHash = _Entity.PropertyList.Hash;
            Caption = _Entity.PropertyList.Properties;

           
        }

        public void Dispose()
        {
            Caption = null;
        }

        public void Draw(SpriteBatchUI spriteBatch, int x, int y)
        {
            // determine if properties need to be updated.
            if (_Entity != null && _PropertyListHash != _Entity.PropertyList.Hash)
            {
                _PropertyListHash = _Entity.PropertyList.Hash;
                Caption = _Entity.PropertyList.Properties;
            }

            // update text if necessary.
            if (_RenderedText == null)
            {
                _RenderedText = new RenderedText("<center>" + Caption, 300, true);
            }
            else if (_RenderedText.Text != "<center>" + Caption)
            {
                _RenderedText = null;
                _RenderedText = new RenderedText("<center>" + Caption, 300, true);
            }

            // draw checkered trans underneath.
            spriteBatch.Draw2DTiled(CheckerTrans.CheckeredTransTexture /*Service.Get<IResourceProvider>().GetUITexture(0xA2C)*/, new Rectangle(x - 4, y - 4, _RenderedText.Width + 8, _RenderedText.Height + 8), /*Utility.GetHueVector(0, false, true, true)*/ Vector3.Zero);
            // draw tooltip contents
            _RenderedText.Draw(spriteBatch, new Point(x, y));
        }

        internal void UpdateEntity(AEntity entity)
        {
            if (_Entity == null || _Entity != entity || _PropertyListHash != _Entity.PropertyList.Hash)
            {
                _Entity = entity;
                _PropertyListHash = _Entity.PropertyList.Hash;
                Caption = _Entity.PropertyList.Properties;
            }
        }

        internal void UpdateCaption(string caption)
        {
            _Entity = null;
            Caption = caption;
        }
    }
}
