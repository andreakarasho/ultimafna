﻿/***************************************************************************
 *   CreateCharAppearanceGump.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using Microsoft.Xna.Framework;
using System;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.Resources;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.Data;
using UltimaXNA.Ultima.Login.Data;
using UltimaXNA.Ultima.UI.Controls;
#endregion

namespace UltimaXNA.Ultima.UI.LoginGumps
{
    class CreateCharAppearanceGump : Gump
    {
        enum Buttons
        {
            BackButton,
            ForwardButton,
            QuitButton
        }

        event Action _OnForward;
        event Action _OnBackward;

        public string Name { get { return _TxtName.Text; } set { _TxtName.Text = value; } }
        public int Gender { get; private set; }
        public int Race { get; set; } 
        public int HairID
        {
            get
            {
                switch (Race)
                {
                    case 0:
                        return (Gender == 0) ? HairStyles.MaleIDs[_HairMale.Index] : HairStyles.FemaleIDs[_hairFemale.Index];
                    case 1:
                        return (Gender == 0) ? HairStyles.ElfMaleHairs[_maleElfHair.Index] : HairStyles.ElfFemaleHairs[_femaleElfHair.Index];
                    case 2:
                        return HairStyles.GargoyleMaleHairs[_male_female_GargHair.Index];
                }
                return 0;
            }
            set
            {
                switch (Race)
                {
                    case 0:
                        if (Gender == 0)
                            HairStyles.MaleIDs[_HairMale.Index] = value;
                        else
                            HairStyles.FemaleIDs[_hairFemale.Index] = value;
                        break;
                    case 1:
                        if (Gender == 0)
                            HairStyles.ElfMaleHairs[_maleElfHair.Index] = value;
                        else
                            HairStyles.ElfFemaleHairs[_femaleElfHair.Index] = value;
                        break;
                    case 2:
                        HairStyles.GargoyleMaleHairs[_male_female_GargHair.Index] = value;
                        break;
                }
            }
        }
        public int FacialHairID
        {
            get
            {
                switch (Race)
                {
                    case 0:
                        return (Gender == 0) ? HairStyles.FacialHairIDs[_FacialHairMale.Index] : 0;
                    case 2:
                        return (Gender == 0) ? HairStyles.GargoyleMaleBeards[_beardsGarg.Index] : 0;
                }
                return 0;
            }
            set
            {
                switch (Race)
                {
                    case 0:
                        if (Gender == 0)
                            HairStyles.FacialHairIDs[_FacialHairMale.Index] = value;
                        break;
                    case 2:
                        if (Gender == 0)
                            HairStyles.GargoyleMaleBeards[_beardsGarg.Index] = value;
                        break;
                }

            }
        }
        public int SkinHue
        {
            get
            {
                switch (Race)
                {
                    case 0:
                        return _SkinHue.HueValue;
                    case 1:
                        return _ElfSkinHue.HueValue;
                    case 2:
                        return _GargSkinHue.HueValue;
                }
                return 0;
            }
            set
            {
                switch (Race)
                {
                    case 0: _SkinHue.HueValue = value; break;
                    case 1: _ElfSkinHue.HueValue = value; break;
                    case 2: _GargSkinHue.HueValue = value; break;
                }
            }
        }
        public int HairHue
        {
            get
            {
                switch (Race)
                {
                    case 0:
                        return _HairHue.HueValue;
                    case 1:
                        return _elfHairHue.HueValue;
                    case 2:
                        return _gargHairHue.HueValue;
                }
                return 0;
            }
            set
            {
                switch (Race)
                {
                    case 0: _HairHue.HueValue = value; break;
                    case 1: _elfHairHue.HueValue = value; break;
                    case 2: _gargHairHue.HueValue = value; break;
                }
            }
        }
        public int FacialHairHue
        {
            get
            {
                switch (Race)
                {
                    case 0:
                        return _FacialHairHue.HueValue;
                    case 2:
                        return _gargBeardsHue.HueValue;
                }
                return 0;
            }
            set
            {
                switch (Race)
                {
                    case 0: _FacialHairHue.HueValue = value; break;
                    case 1: _gargBeardsHue.HueValue = value; break;
                }

            }
        }

        public int ShirtHue
        {
            get { return _ShirtHue.HueValue; }
            set { _ShirtHue.HueValue = value; }
        }

        public int PantsHue
        {
            get { return _PantsHue.HueValue; }
            set { _PantsHue.HueValue = value; }
        }


     
        private TextEntry _TxtName;
        private PaperdollLargeUninteractable _paperdoll;
        private DropDownList _HairMale, _hairFemale, _maleElfHair, _femaleElfHair, _male_female_GargHair;
        private DropDownList _FacialHairMale, _beardsGarg;
        private ColorPicker _SkinHue, _ElfSkinHue, _GargSkinHue;
        private ColorPicker _HairHue, _elfHairHue, _gargHairHue;
        private ColorPicker _FacialHairHue, _gargBeardsHue;
        private ColorPicker _ShirtHue;
        private ColorPicker _PantsHue;

        private TextLabelAscii _HairTitle;
        private TextLabelAscii _FacialHairTitle, _labelFacialHair;
        private TextLabelAscii _ShirtColorTitle;
        private TextLabelAscii _PantsColorTitle;
        private RadioButton _maleRadio, _femaleRadio, _humanRadio, _elfRadio, _gargoyleRadio;

        private IResourceProvider provider;

        public CreateCharAppearanceGump(Action onForward, Action onBackward)
            : base(0, 0)
        {
            _OnForward = onForward;
            _OnBackward = onBackward;

            // get the resource provider
            provider = Service.Get<IResourceProvider>();

            _HairMale = new DropDownList(this, 97, 154, 122, HairStyles.MaleHairNames, 6, 0, false);
            _hairFemale = new DropDownList(this, 97, 154, 122, HairStyles.FemaleHairNames, 6, 0, false) { IsVisible = false };
            _maleElfHair = new DropDownList(this, 97, 154, 122, HairStyles.MaleElfHairsName, 6, 0, false) { IsVisible = false };
            _femaleElfHair = new DropDownList(this, 97, 154, 122, HairStyles.FemaleElfHairsName, 6, 0, false) { IsVisible = false };
            _male_female_GargHair = new DropDownList(this, 97, 154, 122, HairStyles.GargoyleMaleFemaleHairsName, 6, 0, false) { IsVisible = false };

            _beardsGarg = new DropDownList(this, 97, 204, 122, HairStyles.GargoyleMaleBeardsName, 6, 0, false) { IsVisible = false };
            _FacialHairMale = new DropDownList(this, 97, 204, 122, HairStyles.FacialHair, 6, 0, false);


            _HairTitle = new TextLabelAscii(this, 100, 140, 9, 2036, provider.GetString(3000121));
            _FacialHairTitle = new TextLabelAscii(this, 100, 190, 9, 2036, provider.GetString(3000122));
            _ShirtColorTitle = new TextLabelAscii(this, 489, 276, 9, 2036, provider.GetString(3000440));
            _PantsColorTitle = new TextLabelAscii(this, 489, 321, 9, 2036, provider.GetString(3000441));


            AddControl(new GumpPicTiled(this, 0, 0, 640, 480, 0x0588));
            AddControl(new GumpPic(this, 0,0, 5500, 0));
            AddControl(new GumpPic(this, 0, 0, 5536, 0));

            AddControl(new GumpPic(this, 280, 53, 0x0709, 0));
            AddControl(new GumpPic(this, 280, 73, 0x070A, 0));
            AddControl(new GumpPic(this, 463, 73, 0x070C, 0));

            _TxtName = new TextEntry(this, 248, 73, 220, 16, 0, 0, 30, string.Empty)
            {
                FocusedColor = 29,
                LeadingHtmlTag = "<span color='#000' style='font-family:uni0;'>",
                LegacyCarat = true
            };

            AddControl(new ResizePic(this, _TxtName));
            AddControl(_TxtName);

            AddControl(new GumpPic(this, 238, 98, 1800, 0));

            _paperdoll = new PaperdollLargeUninteractable(this, 237, 97);
            _paperdoll.IsCharacterCreation = true;
            AddControl(_paperdoll);

            AddControl(new ResizePic(this, 82, 125, 3600, 151, 310));

            AddControl(_HairTitle);
            AddControl(_HairMale);
            AddControl(_hairFemale);

            AddControl(_maleElfHair); AddControl(_femaleElfHair); AddControl(_male_female_GargHair); AddControl(_beardsGarg);
            AddControl(_FacialHairTitle); AddControl(_FacialHairMale);

            AddControl(new ResizePic(this, 475, 125, 3600, 151, 310));

            AddControl(new TextLabelAscii(this, 489, 141, 9, 2036, provider.GetString(3000183)));
            AddControl(_SkinHue = new ColorPicker(this, new Rectangle(490, 154, 120, 24), new Rectangle(490, 140, 120, 280), 7, 8, Hues.Human.SkinHue));

            AddControl(new TextLabelAscii(this, 489, 186, 9, 2036, provider.GetString(3000184)));
            AddControl(_HairHue = new ColorPicker(this, new Rectangle(490, 199, 120, 24), new Rectangle(490, 140, 120, 280), 8, 6, Hues.Human.HairHues));

            AddControl(_labelFacialHair = new TextLabelAscii(this, 489, 231, 9, 2036, provider.GetString(3000185)));
            AddControl(_FacialHairHue = new ColorPicker(this, new Rectangle(490, 244, 120, 24), new Rectangle(490, 140, 120, 280), 8, 6, Hues.Human.HairHues));

            AddControl(_ShirtColorTitle);
            AddControl(_ShirtHue = new ColorPicker(this, new Rectangle(490, 289, 120, 24), new Rectangle(490, 140, 120, 280), 20, 45, Hues.ClothesHues));

            AddControl(_PantsColorTitle);
            AddControl(_PantsHue = new ColorPicker(this, new Rectangle(490, 334, 120, 24), new Rectangle(490, 140, 120, 280), 20, 45, Hues.ClothesHues));

            AddControl(_ElfSkinHue = new ColorPicker(this, new Rectangle(490, 154, 120, 24), new Rectangle(490, 140, 120, 280), 4, 8, Hues.Elf.SkinHues) { IsVisible = false });
            AddControl(_GargSkinHue = new ColorPicker(this, new Rectangle(490, 154, 120, 24), new Rectangle(490, 140, 120, 280), 4, 7, Hues.Gargoyle.SkinHues) { IsVisible = false });

            AddControl(_elfHairHue = new ColorPicker(this, new Rectangle(490, 199, 120, 24), new Rectangle(490, 140, 120, 280), 6, 9, Hues.Elf.HairHues) { IsVisible = false });
            AddControl(_gargHairHue = new ColorPicker(this, new Rectangle(490, 199, 120, 24), new Rectangle(490, 140, 120, 280), 6, 3, Hues.Gargoyle.HornHues) { IsVisible = false });
            AddControl(_gargBeardsHue = new ColorPicker(this, new Rectangle(490, 244, 120, 24), new Rectangle(490, 140, 120, 280), 6, 3, Hues.Gargoyle.HornHues) { IsVisible = false });


            // SEX BUTTONS
            AddControl(new Button(this, 445, 435, 0x710, 0x712, 0x711, ButtonTypes.Activate, 0, -1)).MouseClickEvent += (sender, x, y, button) => { MaleRadioButton_MouseClick(null, x, y, button); }; // maschio
            AddControl(new Button(this, 445, 455, 0x070D, 0x070F, 0x070E, ButtonTypes.Activate, 0, -1)).MouseClickEvent += (sender, x, y, button) => { FemaleRadioButton_MouseClick(null, x, y, button); }; // femmina
            AddControl(_maleRadio = new RadioButton(this, 425, 435, 0x0768, 0x0767, true, 0, 0)).MouseClickEvent += MaleRadioButton_MouseClick;
            AddControl(_femaleRadio = new RadioButton(this, 425, 455, 0x0768, 0x0767, false, 0, 0)).MouseClickEvent += FemaleRadioButton_MouseClick;


            // RACE BUTTONS
            AddControl(new Button(this, 200, 435, 0x702, 0x703, 0x704, ButtonTypes.Activate, 0, -1)).MouseClickEvent += (sender, x, y, button) => { HumanRadioButton_MouseClick(null, x, y, button); };
            AddControl(new Button(this, 200, 455, 0x705, 0x707, 0x706, ButtonTypes.Activate, 0, -1)).MouseClickEvent += (sender, x, y, button) => { ElfRadioButton_MouseClick(null, x, y, button); };
            AddControl(new Button(this, 80, 435, 0x7D3, 0x7D5, 0x7D4, ButtonTypes.Activate, 0, -1)).MouseClickEvent += (sender, x, y, button) => { GarogyleRadioButton_MouseClick(null, x, y, button); };
            AddControl(_humanRadio = new RadioButton(this, 180, 435, 0x0768, 0x0767, true, 0, 1)).MouseClickEvent += HumanRadioButton_MouseClick;
            AddControl(_elfRadio = new RadioButton(this, 180, 455, 0x0768, 0x0767, false, 0, 1)).MouseClickEvent += ElfRadioButton_MouseClick;
            AddControl(_gargoyleRadio = new RadioButton(this, 60, 435, 0x0768, 0x0767, false, 0, 1)).MouseClickEvent += GarogyleRadioButton_MouseClick;


            // back button
            AddControl(new Button(this, 586, 435, 5537, 5539, 5538, ButtonTypes.Activate, 0, (int)Buttons.BackButton));

            // forward button
            AddControl(new Button(this, 610, 435, 5540, 5542, 5541, ButtonTypes.Activate, 0, (int)Buttons.ForwardButton));

            // quit button
            AddControl(new Button(this, 554, 2, 5513, 5515, 5514, ButtonTypes.Activate, 0, (int)Buttons.QuitButton));

            /*// backdrop
            AddControl(new GumpPicTiled(this, 0, 0, 800, 600, 9274));
            AddControl(new GumpPic(this, 0, 0, 5500, 0));
            // character name 
            AddControl(new GumpPic(this, 280, 53, 1801, 0));
            _TxtName = new TextEntry(this, 238, 70, 234, 20, 0, 0, 29, string.Empty);
            _TxtName.LeadingHtmlTag = "<span color='#000' style='font-family:uni0;'>";
            AddControl(new ResizePic(this, _TxtName));
            AddControl(_TxtName);
            // character window
            AddControl(new GumpPic(this, 238, 98, 1800, 0));
            // paperdoll
            _paperdoll = new PaperdollLargeUninteractable(this, 237, 97);
            _paperdoll.IsCharacterCreation = true;
            AddControl(_paperdoll);

            // left option window
            AddControl(new ResizePic(this, 82, 125, 3600, 151, 310));
            // this is the place where you would put the race selector.
            // if you do add it, move everything else in this left window down by 45 pixels
            // gender
            AddControl(new TextLabelAscii(this, 100, 141, 9, 2036, provider.GetString(3000120)), 1);
            AddControl(_Gender = new DropDownList(this, 97, 154, 122, new string[] { provider.GetString(3000118), provider.GetString(3000119) }, 2, 0, false));
            // hair (male)
            AddControl(new TextLabelAscii(this, 100, 186, 9, 2036, provider.GetString(3000121)), 1);
            AddControl(_HairMale = new DropDownList(this, 97, 199, 122, HairStyles.MaleHairNames, 6, 0, false), 1);
            // facial hair (male)
            AddControl(new TextLabelAscii(this, 100, 231, 9, 2036, provider.GetString(3000122)), 1);
            AddControl(_FacialHairMale = new DropDownList(this, 97, 244, 122, HairStyles.FacialHair, 6, 0, false), 1);
            // hair (female)
            AddControl(new TextLabelAscii(this, 100, 186, 9, 2036, provider.GetString(3000121)), 2);
            AddControl(_HairFemale = new DropDownList(this, 97, 199, 122, HairStyles.FemaleHairNames, 6, 0, false), 2);

            // right option window
            AddControl(new ResizePic(this, 475, 125, 3600, 151, 310));
            // skin tone
            AddControl(new TextLabelAscii(this, 489, 141, 9, 2036, provider.GetString(3000183)));
            AddControl(_SkinHue = new ColorPicker(this, new Rectangle(490, 154, 120, 24), new Rectangle(490, 140, 120, 280), 7, 8, Data.Hues.SkinTones));
            // hair color
            AddControl(new TextLabelAscii(this, 489, 186, 9, 2036, provider.GetString(3000184)));
            AddControl(_HairHue = new ColorPicker(this, new Rectangle(490, 199, 120, 24), new Rectangle(490, 140, 120, 280), 8, 6, Data.Hues.HairTones));
            // facial hair color (male)
            AddControl(new TextLabelAscii(this, 489, 231, 9, 2036, provider.GetString(3000185)), 1);
            AddControl(_FacialHairHue = new ColorPicker(this, new Rectangle(490, 244, 120, 24), new Rectangle(490, 140, 120, 280), 8, 6, Data.Hues.HairTones), 1);

            // back button
            AddControl(new Button(this, 586, 435, 5537, 5539, ButtonTypes.Activate, 0, (int)Buttons.BackButton), 0);
            ((Button)LastControl).GumpOverID = 5538;
            // forward button
            AddControl(new Button(this, 610, 435, 5540, 5542, ButtonTypes.Activate, 0, (int)Buttons.ForwardButton), 0);
            ((Button)LastControl).GumpOverID = 5541;
            // quit button
            AddControl(new Button(this, 554, 2, 5513, 5515, ButtonTypes.Activate, 0, (int)Buttons.QuitButton));
            ((Button)LastControl).GumpOverID = 5514;
            */

            IsUncloseableWithRMB = true;
        }

        internal void SaveData(CreateCharacterData data)
        {
              data.HasAppearanceData = true;
              data.Name = Name;
              data.Gender = Gender;
              data.Race = Race;
              data.HairStyleID = HairID;
              data.FacialHairStyleID = FacialHairID;
              data.SkinHue = SkinHue;
              data.HairHue = HairHue;
              data.FacialHairHue = FacialHairHue;
        }

        internal void RestoreData(CreateCharacterData _Data)
        {
               Name = _Data.Name;
               Gender = _Data.Gender;
               Race = _Data.Race;
               HairID = _Data.HairStyleID;
               FacialHairID = _Data.FacialHairStyleID;
               SkinHue = _Data.SkinHue;
               HairHue = _Data.HairHue;
               FacialHairHue = _Data.FacialHairHue;
        }


        public override void Update(double totalMS, double frameMS)
        {
            base.Update(totalMS, frameMS);

            // show different controls based on what gender we're looking at.
            // Also copy over the hair id to facilitate easy switching between male and female appearances.
            /*if (_Gender.Index == 0)
            {
                ActivePage = 1;
                _HairFemale.Index = _HairMale.Index;
            }
            else
            {
                ActivePage = 2;
                _HairMale.Index = _HairFemale.Index;
            }*/

            // update the paperdoll
            _paperdoll.Race = Race;
            _paperdoll.Gender = Gender;

            _paperdoll.SetSlotEquipment(PaperdollLargeUninteractable.EquipSlots.Hair, HairID);
            _paperdoll.SetSlotHue(PaperdollLargeUninteractable.EquipSlots.Hair, HairHue);

            _paperdoll.SetSlotEquipment(PaperdollLargeUninteractable.EquipSlots.FacialHair, FacialHairID);
            _paperdoll.SetSlotHue(PaperdollLargeUninteractable.EquipSlots.FacialHair, FacialHairHue);
            _paperdoll.SetSlotHue(PaperdollLargeUninteractable.EquipSlots.Body, SkinHue);


            if (Race == 2) // se gargoyle
            {
                _paperdoll.SetSlotHue(PaperdollLargeUninteractable.EquipSlots.Robe, ShirtHue);
            }
            else // Elfo e umano
            {
                _paperdoll.SetSlotHue(PaperdollLargeUninteractable.EquipSlots.Shirt, ShirtHue);
                _paperdoll.SetSlotHue(PaperdollLargeUninteractable.EquipSlots.Legging, PantsHue);
            }
        }

        private void MaleRadioButton_MouseClick(AControl sender, int x, int y, MouseButton button)
        {
            if (sender == null)
                _maleRadio.IsChecked = true;

            Gender = 0;

            switch (Race)
            {
                case 0:
                    _hairFemale.IsVisible = false;
                    _FacialHairMale.IsVisible = _FacialHairTitle.IsVisible = _FacialHairHue.IsVisible = _labelFacialHair.IsVisible = true;
                    _HairMale.Index = _hairFemale.Index;
                    _PantsColorTitle.Text = Gender == 1 ? provider.GetString(3000439) : provider.GetString(3000441);
                    break;
                case 1:
                    _femaleElfHair.IsVisible = false;
                    _maleElfHair.Index = _femaleElfHair.Index;
                    _PantsColorTitle.Text = Gender == 1 ? provider.GetString(3000439) : provider.GetString(3000441);
                    break;
                case 2:
                    _FacialHairTitle.IsVisible = _gargBeardsHue.IsVisible = _labelFacialHair.IsVisible = true;
                    _beardsGarg.IsVisible = true;
                    _PantsColorTitle.Text = "Robe Color";
                    break;
            }
        }

        private void FemaleRadioButton_MouseClick(AControl sender, int x, int y, MouseButton button)
        {
            if (sender == null)
                _femaleRadio.IsChecked = true;

            Gender = 1;

            switch (Race)
            {
                case 0:
                    _hairFemale.IsVisible = true;
                    _FacialHairMale.IsVisible = _FacialHairTitle.IsVisible = _FacialHairHue.IsVisible = _labelFacialHair.IsVisible = false;
                    _hairFemale.Index = _HairMale.Index;
                    _PantsColorTitle.Text = Gender == 1 ? provider.GetString(3000439) : provider.GetString(3000441);
                    break;
                case 1:
                    _femaleElfHair.IsVisible = true;
                    _maleElfHair.Index = _femaleElfHair.Index;
                    _PantsColorTitle.Text = Gender == 1 ? provider.GetString(3000439) : provider.GetString(3000441);
                    break;
                case 2:
                    _FacialHairTitle.IsVisible = _gargBeardsHue.IsVisible = _labelFacialHair.IsVisible = false;
                    _beardsGarg.IsVisible = false;
                    _PantsColorTitle.Text = "Robe Color";
                    break;
            }
        }

        private void HumanRadioButton_MouseClick(AControl sender, int x, int y, MouseButton button)
        {
            if (sender == null)
                _humanRadio.IsChecked = true;

            _ShirtColorTitle.Text = provider.GetString(3000440);
            _PantsColorTitle.Text = Gender == 1 ? provider.GetString(3000439) : provider.GetString(3000441);
            _PantsColorTitle.IsVisible = true;
            _PantsHue.IsVisible = true;
            _SkinHue.IsVisible = true;

            switch (Race)
            {
                case 1:
                    if (Gender == 0)
                    {
                        _maleElfHair.IsVisible = false;
                        _elfHairHue.IsVisible = false;
                        _HairMale.IsVisible = true;
                        _FacialHairTitle.IsVisible = true;
                        _FacialHairMale.IsVisible = true;
                        _labelFacialHair.IsVisible = _FacialHairHue.IsVisible = true;
                    }
                    else
                    {
                        _femaleElfHair.IsVisible = false;
                        _elfHairHue.IsVisible = false;
                        _hairFemale.IsVisible = true;
                    }
                    _ElfSkinHue.IsVisible = false;
                    break;
                case 2:
                    _male_female_GargHair.IsVisible = false;
                    if (Gender == 0)
                    {
                        _gargBeardsHue.IsVisible = false;
                        _beardsGarg.IsVisible = false;
                        _HairMale.IsVisible = true;
                        _FacialHairTitle.IsVisible = true;
                        _labelFacialHair.IsVisible = _FacialHairMale.IsVisible = true;
                    }
                    else
                    {
                        _hairFemale.IsVisible = true;
                    }
                    _gargHairHue.IsVisible = false;
                    _GargSkinHue.IsVisible = false;
                    break;
            }

            Race = 0;
        }

        private void ElfRadioButton_MouseClick(AControl sender, int x, int y, MouseButton button)
        {
            if (sender == null)
                _elfRadio.IsChecked = true;

            _ShirtColorTitle.Text = provider.GetString(3000440);
            _PantsColorTitle.Text = Gender == 1 ? provider.GetString(3000439) : provider.GetString(3000441);
            _PantsColorTitle.IsVisible = true;
            _PantsHue.IsVisible = true;
            _ElfSkinHue.IsVisible = true;
            _elfHairHue.IsVisible = true;

            switch (Race)
            {
                case 0:
                    if (Gender == 0)
                    {
                        _labelFacialHair.IsVisible = _FacialHairMale.IsVisible = _HairMale.IsVisible = _FacialHairTitle.IsVisible = _FacialHairHue.IsVisible = false;
                        _maleElfHair.IsVisible = true;
                    }
                    else
                    {
                        _hairFemale.IsVisible = false;
                        _femaleElfHair.IsVisible = true;
                    }
                    _SkinHue.IsVisible = false;
                    break;
                case 2:
                    if (Gender == 0)
                    {
                        _labelFacialHair.IsVisible = _gargBeardsHue.IsVisible = false;
                        _FacialHairTitle.IsVisible = false;
                        _beardsGarg.IsVisible = false;
                        _maleElfHair.IsVisible = true;
                    }
                    else
                        _femaleElfHair.IsVisible = true;
                    _male_female_GargHair.IsVisible = false;
                    _GargSkinHue.IsVisible = false;
                    break;
            }
            Race = 1;
        }

        private void GarogyleRadioButton_MouseClick(AControl sender, int x, int y, MouseButton button)
        {
            if (sender == null)
                _gargoyleRadio.IsChecked = true;

            _ShirtColorTitle.Text = "Robe Color";
            _PantsColorTitle.IsVisible = false;
            _PantsHue.IsVisible = false;
            _GargSkinHue.IsVisible = true;
            _gargHairHue.IsVisible = true;

            switch (Race)
            {
                case 0:
                    if (Gender == 0)
                    {
                        _FacialHairMale.IsVisible = _HairMale.IsVisible = false;
                        _FacialHairHue.IsVisible = false;
                        _beardsGarg.IsVisible = true;
                        _FacialHairTitle.IsVisible = true;
                        _labelFacialHair.IsVisible = _gargBeardsHue.IsVisible = true;
                    }
                    else
                    {
                        _hairFemale.IsVisible = false;
                        _labelFacialHair.IsVisible = _FacialHairTitle.IsVisible = false;
                    }
                    _SkinHue.IsVisible = _HairHue.IsVisible = false;
                    _male_female_GargHair.IsVisible = true;
                    break;
                case 1:
                    if (Gender == 0)
                    {
                        _maleElfHair.IsVisible = false;
                        _labelFacialHair.IsVisible = _beardsGarg.IsVisible = true; _FacialHairTitle.IsVisible = true;
                        _gargBeardsHue.IsVisible = true;
                    }
                    else
                    {
                        _femaleElfHair.IsVisible = false;
                        _labelFacialHair.IsVisible = _FacialHairTitle.IsVisible = false;
                    }
                    _ElfSkinHue.IsVisible = false;
                    _elfHairHue.IsVisible = false;
                    _male_female_GargHair.IsVisible = true;

                    break;
            }
            Race = 2;
        }




        public override void OnButtonClick(int buttonID)
        {
            switch ((Buttons)buttonID)
            {
                case Buttons.BackButton:
                    _OnBackward();
                    break;
                case Buttons.ForwardButton:
                    _OnForward();
                    break;
                case Buttons.QuitButton:
                    Service.Get<UltimaGame>().Quit();
                    break;
            }
        }
    }
}
