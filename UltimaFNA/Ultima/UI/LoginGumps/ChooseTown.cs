﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.Resources;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.Data;
using UltimaXNA.Ultima.Login.Accounts;
using UltimaXNA.Ultima.Resources.Fonts;
using UltimaXNA.Ultima.UI.Controls;

namespace UltimaXNA.Ultima.UI.LoginGumps
{
    class ChooseTown : Gump
    {
        enum Buttons
        {
            QuitButton,
            BackButton,
            ForwardButton,
            SwitchDescription
        }

        private HtmlGumpling _descriptionGump;
        private string[] _facet = new string[6] { "Felucca", "Trammel", "Ilshenar", "Malas", "Tokuno", "Ter Mur" };

        private Dictionary<string, int> _descriptions = new Dictionary<string, int>()
        {
            {"Minoc", 1075073 },
            {"Yew", 1075072 },
            {"Vesper", 1075080 },
            {"Moonglow", 1075075 },
            {"Britain", 1075074 },
            {"Skara Brae", 1075079 },
            {"New Haven", 1150168 },
            {"Trinsic", 1075076 },
            {"Jhelom", 1075078 }
        };

        private int _currentFacet = -1;
        private int _currentTown = -1;
        private Action _loginChar, _onBack;

        public ChooseTown(Action loginChar, Action onback) : base(0 ,0)
        {
            _loginChar = loginChar; _onBack = onback;

            AddControl(new GumpPicTiled(this, 0, 0, 640, 480, 0xE14));
            AddControl(new GumpPic(this, 0, 0, 0x157C, 0));
            AddControl(new GumpPic(this, 0, 4, 0x15A0, 0));
            AddControl(new Button(this, 554, 2, 5513, 5515, 5514, ButtonTypes.Activate, 0, (int)Buttons.QuitButton));

            int facet = _currentFacet = Characters.StartingLocations[0].Facet;

            if (ClientVersion.ClientInfo >= ClientVersionTypes.NewCharacterCreation)
            {
                AddControl(new GumpPic(this, 65, 54, 0x15D9 + facet, 0));
                AddControl(new GumpPic(this, 57, 49, 0x15DF, 0));
                AddControl(new TextLabelAscii(this, 240, 440, 3, 0x0481, _facet[_currentFacet]));
            }
            else AddControl(new GumpPic(this, 57, 49, 0x1598, 0));

            AddControl(_descriptionGump = new HtmlGumpling(this, 452,60,175,365,1,1, "<basefont color=#000000><big>"));

            foreach (var city in Characters.StartingLocations)
            {
                int x = 62 + Calculate(7168 - 2048, city.X, 383);
                int y = 54 + Calculate(4096, city.Y, 384);

                IResourceProvider provider = Service.Get<IResourceProvider>();

                AddControl(new Button(this, x, y, 0x4B9, 0x4BA, 0x4BA, ButtonTypes.Activate, 0, (int)Buttons.SwitchDescription)).MouseClickEvent += (sender, xx,yy,button) =>
                {
                    _descriptionGump.Text = "<basefont color=#000000><big>" + provider.GetString(_descriptions[city.CityName]).Remove(0, 4);
                    _currentTown = city.Index;
                };

                y -= 20;

                if (city.CityName == "Moonglow")
                    x -= 60;

                AddControl(new TextEntry(this, x, y, (provider.GetAsciiFont(3) as FontAscii).GetWidth(city.CityName), 20, 0x58, 0, 32, string.Format("<span style=\"font-family:ascii{0}\">{1}", 3, city.CityName)) { FocusedColor = 0x481, HoveredColor = 0x99, CaretVisible = false}).MouseClickEvent += (sender, xx, yy, button) =>
                {
                    _descriptionGump.Text = "<basefont color=#000000><big>" + provider.GetString(_descriptions[city.CityName]).Remove(0, 4);
                    _currentTown = city.Index;
                };
            }

            AddControl(new Button(this, 586, 435, 5537,5539,5538, ButtonTypes.Activate, 0, (int)Buttons.BackButton));
            AddControl(new Button(this, 610, 435, 5540, 5542, 5541, ButtonTypes.Activate, 0, (int)Buttons.ForwardButton));
        }

        public int TownIndex => _currentTown;

        private int Calculate(int max, int current, int maxvalue)
        {
            if (max > 0)
            {
                max = (current * 100) / max;
                if (max > 100) max = 100;
                if (max > 1) max = (maxvalue * max) / 100;
            }
            return max;
        }

        public override void OnButtonClick(int buttonID)
        {
            switch ((Buttons)buttonID)
            {
                case Buttons.BackButton:
                    break;
                case Buttons.ForwardButton:
                    _loginChar.Invoke();
                    break;
                case Buttons.QuitButton:
                    Service.Get<UltimaGame>().Quit();
                    break;
                case Buttons.SwitchDescription: //nel client normale mi pare che se clicchi su una gemma della città ti crea il pg
                    break;
            }
        }
    }
}
