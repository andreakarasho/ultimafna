﻿/***************************************************************************
 *   ContainerGump.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System.Collections.Generic;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.Resources;
using UltimaXNA.Ultima.UI.Controls;
using UltimaXNA.Ultima.World.Entities;
using UltimaXNA.Ultima.World.Entities.Mobiles;
using UltimaXNA.Ultima.World.Entities.Items;
using UltimaXNA.Ultima.World.Entities.Items.Containers;
using UltimaFNA.Core.Network.SocketAsync;
using UltimaXNA.Ultima.Network.Client;
#endregion

namespace UltimaXNA.Ultima.UI.WorldGumps
{
    class ContainerGump : Gump
    {
        ContainerData _data;
        ContainerItem _item;

		public ContainerGump()
			: base(0, 0)
		{
		}

		public ContainerGump(AEntity containerItem, int gumpID)
        : base(containerItem.Serial, 0)
        {
			_data = ContainerData.Get(gumpID);
			_item = (ContainerItem)containerItem;
			_item.SetCallbacks(OnItemUpdated, OnItemDisposed);

			IsMoveable = true;

			if (_item.Parent is Mobile) // Abilita salvataggio se backpack player
			{
				Mobile mob = (Mobile)_item.Parent;
				if (mob.IsPlayer)
				{
					if (mob.Backpack.Serial == _item.Serial)
						SaveOnWorldStop = true;
				}
			}


			AddControl(new GumpPicContainer(this, 0, 0, _data.GumpID, 0, _item));
		}

		public override void Dispose()
        {
			if (_item != null)
				_item.ClearCallBacks(OnItemUpdated, OnItemDisposed);

            base.Dispose();
        }
		protected override void OnInitialize()
		{
			if (_item != null && _item.Parent is Mobile)  // salva dati se backpack player
			{
				Mobile mob = (Mobile)_item.Parent;
				if (mob.IsPlayer)
				{
					if (mob.Backpack.Serial == _item.Serial)
						SetSavePositionName("backpack");
				}
			}

			base.OnInitialize();
		}

		public override bool SaveGump(out Dictionary<string, object> data)
		{
			data = new Dictionary<string, object>();
			data.Add("serial", (int)_item.Serial);
			return true;
		}

		public override bool RestoreGump(Dictionary<string, object> data)
		{
			int serial;
			if (data.ContainsKey("serial"))
			{


				serial = (int)data["serial"];

				NetworkClient client = Service.Get<NetworkClient>();
				client.Send(new DoubleClickPacket(serial));
				return true;
			}
			return false;
		}

		public override void Update(double totalMS, double frameMS)
        {
            base.Update(totalMS, frameMS);
        }

        void OnItemUpdated(AEntity entity)
        {
            // delete any items in our pack that are no longer in the container.
            List<AControl> ControlsToRemove = new List<AControl>();
            foreach (AControl c in Children)
            {
                if (c is ItemGumpling && !_item.Contents.Contains(((ItemGumpling)c).Item))
                {
                    ControlsToRemove.Add(c);
                }
            }
            foreach (AControl c in ControlsToRemove)
                Children.Remove(c);

            // add any items in the container that are not in our pack.
            foreach (Item item in _item.Contents)
            {
                bool controlForThisItem = false;
                foreach (AControl c in Children)
                {
                    if (c is ItemGumpling && ((ItemGumpling)c).Item == item)
                    {
                        controlForThisItem = true;
                        break;
                    }
                }
                if (!controlForThisItem)
                {
                    AddControl(new ItemGumpling(this, item));
                }
            }
        }

        void OnItemDisposed(AEntity entity)
        {
            Dispose();
        }
    }
}
