#region usings
using Microsoft.Xna.Framework;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Resources;
using UltimaXNA.Ultima.UI.Controls;
using UltimaXNA.Ultima.World;
using UltimaXNA.Ultima.World.Entities.Mobiles;
using UltimaXNA.Core.Network;
using UltimaXNA.Ultima.Network.Client;
using System;
using System.Collections.Generic;
#endregion

namespace UltimaXNA.Ultima.UI.WorldGumps
{
	class BuffGump : Gump
	{
		private bool _NeedUpdate;
		private int _BackGroundID;
		private GumpPic _BackGroud;
		private Button _RotateButton;
		private List<GumpPic> _BuffList = new List<GumpPic>();
		private IResourceProvider _Provider = Service.Get<IResourceProvider>();
		private double _RefreshTime;

		public bool NeedUpdate { get { return _NeedUpdate; } set { _NeedUpdate = value; } }

		public BuffGump()
			: base(0, 0)
		{
			IsMoveable = true;
			_BackGroud = (GumpPic)AddControl(new GumpPic(this, 0, 0, 0x757F, 0));
			_BackGroundID = 0x757F;

			_RotateButton = (Button)AddControl(new Button(this, 0, 0, 0x7583, 0x7583, ButtonTypes.Activate, 0, 0));
			_RotateButton.GumpOverID = 0x7589;

			UpdateGump(0x757F);
		}

		protected override void OnInitialize()
		{
			base.OnInitialize();
		}

		public override void Update(double totalMS, double frameMS)
		{
			if (_NeedUpdate) // aggiornamento necessario per nuovo buff ricevuto
			{
				UpdateGump(_BackGroud.GumpID);
				_NeedUpdate = false;				
			}
			else
			{
				if (_RefreshTime + 1000.0d < totalMS) // Aggiorna timer tooltip buff (ogni secondo piu' veloce non ha senso)
				{
					_RefreshTime = totalMS;
					UpdateTimerToolTip();
				}
			}


			base.Update(totalMS, frameMS);
		}

		public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
		{
			base.Draw(spriteBatch, position, frameMS);
		}

		public override void OnButtonClick(int buttonID)
		{
			_BackGroundID++;
			if (_BackGroundID > 0x7582)
				_BackGroundID = _BackGroud.GumpID = 0x757F;
			else
				_BackGroud.GumpID = _BackGroundID;

			switch (_BackGroud.GumpID)
			{
				case 0x757F:  // Orientamento Verticale Sinistra
					_RotateButton.Position = new Point(0, 0);
					UpdateGump(_BackGroud.GumpID);
					break;
				case 0x7580:  // Orientamento Orizzontale Sinistra
					_RotateButton.Position = new Point(-2, 36);
					UpdateGump(_BackGroud.GumpID);
					break;
				case 0x7581:  // Orientamento Verticale Destra
					_RotateButton.Position = new Point(34, 78);
					UpdateGump(_BackGroud.GumpID);
					break;
				case 0x7582:  // Orientamento Orizzontale Destra
					_RotateButton.Position = new Point(76, 36);
					UpdateGump(_BackGroud.GumpID);
					break;
			}

			Core.Diagnostics.Tracing.Tracer.Warn("- Manca salvataggio Orientamento Buff Gump");
		}

		private void UpdateTimerToolTip()
		{
			int i = 0;
			string tooltip = string.Empty;
			
			foreach (PlayerBuff.BuffData b in PlayerBuff.Data.Values) // lista buff attibi
			{
				TimeSpan durationleft = (TimeSpan.FromSeconds(b.Duration) - (DateTime.Now - b.ActivationTime));

				tooltip = string.Format("{0} <br> {1} <br> Time left: {2} seconds",
												_Provider.GetString(b.TitleCliloc), _Provider.GetString(b.DescriptionCliloc1), (durationleft.Minutes * 60) + durationleft.Seconds);

				_BuffList[i].SetTooltip(tooltip);
				i++;
			}
		}

		private string GetToolTipString(PlayerBuff.BuffData b)
		{
			TimeSpan durationleft = (TimeSpan.FromSeconds(b.Duration) - (DateTime.Now - b.ActivationTime));
			return string.Format("{0} <br> {1} <br> Time left: {2} seconds</font>",
				   _Provider.GetString(b.TitleCliloc), _Provider.GetString(b.DescriptionCliloc1), (durationleft.Minutes * 60) + durationleft.Seconds);
		}

		private void UpdateGump(int backid)
		{
			foreach (GumpPic buff in _BuffList) // Rimuovo vecchi
			{
				buff.Dispose();
			}
			_BuffList.Clear();

			int offset = 0;
			string tooltip = string.Empty;

			switch (backid)
			{
				case 0x757F:  // Orientamento Verticale Sinistra
					foreach (PlayerBuff.BuffData b in PlayerBuff.Data.Values) // Aggiorno con nuovi attivi
					{

						GumpPic buff = (GumpPic)AddControl(new GumpPic(this, 26, 25 + offset, b.Icon, 0));
						buff.SetTooltip(GetToolTipString(b));
						_BuffList.Add(buff);
						offset += 31;
					}
					break;
				case 0x7580:  // Orientamento Orizzontale Sinistra
					foreach (PlayerBuff.BuffData b in PlayerBuff.Data.Values) // Aggiorno con nuovi attivi
					{
						GumpPic buff = (GumpPic)AddControl(new GumpPic(this, 26 + offset, 5, b.Icon, 0));
						buff.SetTooltip(GetToolTipString(b));
						_BuffList.Add(buff);
						offset += 31;
					}
					break;
				case 0x7581:  // Orientamento Verticale Destra
					foreach (PlayerBuff.BuffData b in PlayerBuff.Data.Values) // Aggiorno con nuovi attivi
					{
						GumpPic buff = (GumpPic)AddControl(new GumpPic(this, 5, 48 + offset, b.Icon, 0));
						buff.SetTooltip(GetToolTipString(b));
						_BuffList.Add(buff);
						offset -= 31;
					}
					break;
				case 0x7582:  // Orientamento Orizzontale Destra
					foreach (PlayerBuff.BuffData b in PlayerBuff.Data.Values) // Aggiorno con nuovi attivi
					{
						GumpPic buff = (GumpPic)AddControl(new GumpPic(this, 48 + offset, 5, b.Icon, 0));
						buff.SetTooltip(GetToolTipString(b));
						_BuffList.Add(buff);
						offset -= 31;
					}
					break;
			}
		}
	}
}