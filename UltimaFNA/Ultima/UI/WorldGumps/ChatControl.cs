﻿/***************************************************************************
 *   ChatControl.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.Windows;
using UltimaXNA.Core.Resources;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.UI.Controls;
using UltimaXNA.Ultima.World;
using UltimaXNA.Ultima.Data;
using UltimaXNA.Ultima.Player;
using UltimaXNA.Ultima.Network.Client;
using UltimaXNA.Core.Network;
using UltimaFNA.Core.Network.SocketAsync;
#endregion

namespace UltimaXNA.Ultima.UI.WorldGumps
{
    class ChatControl : AControl
    {
        private const int MaxChatMessageLength = 96;

        private TextEntry _TextEntry;
        private List<ChatLineTimed> _TextEntries;
        private List<Tuple<ChatMode, string>> _MessageHistory;
        private InputManager _Input;
        private WorldModel _World;
        private int _MessageHistoryIndex = -1;
        private Serial _PrivateMessageSerial = Serial.Null;
        private string _PrivateMessageName;

        private ChatMode _Mode = ChatMode.Default;
        private ChatMode Mode
        {
            get { return _Mode; }
            set
            {
                _Mode = value;
                switch (value)
                {
                    case ChatMode.Default:
                        _TextEntry.LeadingHtmlTag = string.Format("<outline color='#{0}' style='font-family: uni0;'>",
                            Utility.GetColorFromUshort(Resources.HueData.GetHue(Settings.UserInterface.SpeechColor, 1)));
                        _TextEntry.LeadingText = string.Empty;
                        _TextEntry.Text = string.Empty;
                        break;
                    case ChatMode.Whisper:
                        _TextEntry.LeadingHtmlTag = string.Format("<outline color='#{0}' style='font-family: uni0;'>",
                            Utility.GetColorFromUshort(Resources.HueData.GetHue(Settings.UserInterface.SpeechColor, 1)));
                        _TextEntry.LeadingText = "Whisper: ";
                        _TextEntry.Text = string.Empty;
                        break;
                    case ChatMode.Emote:
                        _TextEntry.LeadingHtmlTag = string.Format("<outline color='#{0}' style='font-family: uni0;'>",
                            Utility.GetColorFromUshort(Resources.HueData.GetHue(Settings.UserInterface.EmoteColor, 1)));
                        _TextEntry.LeadingText = "Emote: ";
                        _TextEntry.Text = string.Empty;
                        break;
                    case ChatMode.Party:
                        _TextEntry.LeadingHtmlTag = string.Format("<outline color='#{0}' style='font-family: uni0;'>",
                            Utility.GetColorFromUshort(Resources.HueData.GetHue(Settings.UserInterface.PartyMsgColor, 1)));
                        _TextEntry.LeadingText = "Party: ";
                        _TextEntry.Text = string.Empty;
                        break;
                    case ChatMode.PartyPrivate:
                        _TextEntry.LeadingHtmlTag = string.Format("<outline color='#{0}' style='font-family: uni0;'>",
                            Utility.GetColorFromUshort(Resources.HueData.GetHue(Settings.UserInterface.PartyPrivateMsgColor, 1)));
                        _TextEntry.LeadingText = $"To {_PrivateMessageName}: ";
                        _TextEntry.Text = string.Empty;
                        break;
                    case ChatMode.Guild:
                        _TextEntry.LeadingHtmlTag = string.Format("<outline color='#{0}' style='font-family: uni0;'>",
                            Utility.GetColorFromUshort(Resources.HueData.GetHue(Settings.UserInterface.GuildMsgColor, 1)));
                        _TextEntry.LeadingText = "Guild: ";
                        _TextEntry.Text = string.Empty;
                        break;
                    case ChatMode.Alliance:
                        _TextEntry.LeadingHtmlTag = string.Format("<outline color='#{0}' style='font-family: uni0;'>",
                            Utility.GetColorFromUshort(Resources.HueData.GetHue(Settings.UserInterface.AllianceMsgColor, 1)));
                        _TextEntry.LeadingText = "Alliance: ";
                        _TextEntry.Text = string.Empty;
                        break;
                }
            }
        }

        public ChatControl(AControl parent, int x, int y, int width, int height)
            : base(parent)
        {
            Position = new Point(x, y);
            Size = new Point(width, height);

            _TextEntries = new List<ChatLineTimed>();
            _MessageHistory = new List<Tuple<ChatMode, string>>();

            _Input = Service.Get<InputManager>();
            _World = Service.Get<WorldModel>();

            IsUncloseableWithRMB = true;
        }

        public TextEntry TextEntry { get => _TextEntry; set => _TextEntry = value; }

        public void SetModeToPartyPrivate(string name, Serial serial)
        {
            _PrivateMessageName = name;
            _PrivateMessageSerial = serial;
            Mode = ChatMode.PartyPrivate;
        }

        public void SetSize(int w, int h)
        {
            Width = w;
            Height = h;

            _TextEntry.Width = w;
            _checker.Width = w;

            _TextEntry.Position = new Point(_TextEntry.X, h - _font.Height);
            _checker.Position = new Point(_checker.X, h - 20);
        }

        private CheckerTrans _checker;
        private IFont _font;

        public override void Update(double totalMS, double frameMS)
        {
            if (_TextEntry == null)
            {
                IResourceProvider provider = Service.Get<IResourceProvider>();
                IFont font = provider.GetUnicodeFont(0);
                _TextEntry = new TextEntry(this, 1, Height - font.Height, Width, font.Height, 0, 0, MaxChatMessageLength, string.Empty);
                _TextEntry.LegacyCarat = true;
                Mode = ChatMode.Default;

                AddControl(_checker = new CheckerTrans(this, 0, Height - 20, Width, 20));
                AddControl(_TextEntry);
                _font = font;
            }

            for (int i = 0; i < _TextEntries.Count; i++)
            {
                _TextEntries[i].Update(totalMS, frameMS);
                if (_TextEntries[i].IsExpired)
                {
                    _TextEntries[i].Dispose();
                    _TextEntries.RemoveAt(i);
                    i--;
                }
            }

            // Ctrl-Q = Cycle backwards through the things you have said today
            // Ctrl-W = Cycle forwards through the things you have said today
            if (_Input.HandleKeyboardEvent(KeyboardEvent.Down, WinKeys.Q, false, false, true) && _MessageHistoryIndex > -1)
            {
                if (_MessageHistoryIndex > 0)
                    _MessageHistoryIndex -= 1;
                {
                    Mode = _MessageHistory[_MessageHistoryIndex].Item1;
                    _TextEntry.Text = _MessageHistory[_MessageHistoryIndex].Item2;
                }
            }
            else if (_Input.HandleKeyboardEvent(KeyboardEvent.Down, WinKeys.W, false, false, true))
            {
                if (_MessageHistoryIndex < _MessageHistory.Count - 1)
                {
                    _MessageHistoryIndex += 1;
                    Mode = _MessageHistory[_MessageHistoryIndex].Item1;
                    _TextEntry.Text = _MessageHistory[_MessageHistoryIndex].Item2;
                }
                else
                    _TextEntry.Text = string.Empty;
            }
            // backspace when mode is not default and Text is empty = clear mode.
            else if (_Input.HandleKeyboardEvent(KeyboardEvent.Down, WinKeys.Back, false, false, false) && _TextEntry.Text == string.Empty)
            {
                Mode = ChatMode.Default;
            }

            // only switch mode if the single command char is the only char entered.
            if ((Mode == ChatMode.Default && _TextEntry.Text.Length == 1) ||
                (Mode != ChatMode.Default && _TextEntry.Text.Length == 1))
            {
                switch (_TextEntry.Text[0])
                {
                    case ':':
                        Mode = ChatMode.Emote;
                        break;
                    case ';':
                        Mode = ChatMode.Whisper;
                        break;
                    case '/':
                        Mode = ChatMode.Party;
                        break;
                    case '\\':
                        Mode = ChatMode.Guild;
                        break;
                    case '|':
                        Mode = ChatMode.Alliance;
                        break;
                }
            }

            base.Update(totalMS, frameMS);
        }

        public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
        {
            int y = _TextEntry.Y + position.Y - 6;
            for (int i = _TextEntries.Count - 1; i >= 0; i--)
            {
                y -= _TextEntries[i].TextHeight;
                _TextEntries[i].Draw(spriteBatch, new Point(position.X + 2, y));
            }
            base.Draw(spriteBatch, position, frameMS);
        }

        public override void OnKeyboardReturn(int textID, string text)
        {
            // local variables
            ChatMode sentMode = Mode;
            MessageTypes speechType = MessageTypes.Normal;
            int hue = 0;
            // save this message and reset chat for next entry
            _TextEntry.Text = string.Empty;
            _MessageHistory.Add(new Tuple<ChatMode, string>(Mode, text));
            _MessageHistoryIndex = _MessageHistory.Count;
            Mode = ChatMode.Default;
            // send the message and display it locally.
            switch (sentMode)
            {
                case ChatMode.Default:
                    speechType = MessageTypes.Normal;
                    hue = Settings.UserInterface.SpeechColor;
                    break;
                case ChatMode.Whisper:
                    speechType = MessageTypes.Whisper;
                    hue = Settings.UserInterface.SpeechColor;
                    break;
                case ChatMode.Emote:
                    speechType = MessageTypes.Emote;
                    hue = Settings.UserInterface.EmoteColor;
                    break;
                case ChatMode.Party:
                    PlayerState.Partying.DoPartyCommand(text);
                    return;
                case ChatMode.PartyPrivate:
                    PlayerState.Partying.SendPartyPrivateMessage(_PrivateMessageSerial, text);
                    return;
                case ChatMode.Guild:
                    speechType = MessageTypes.Guild;
                    hue = Settings.UserInterface.GuildMsgColor;
                    break;
                case ChatMode.Alliance:
                    speechType = MessageTypes.Alliance;
                    hue = Settings.UserInterface.AllianceMsgColor;
                    break;
            }
            NetworkClient network = Service.Get<NetworkClient>();
            network.Send(new AsciiSpeechPacket(speechType, 0, hue + 2, "ENU", text));
        }

        public void AddLine(string text, int font, int hue, bool asUnicode)
        {
            _TextEntries.Add(new ChatLineTimed(string.Format("<outline color='#{3}' style='font-family:{1}{2};'>{0}",
                text, asUnicode ? "uni" : "ascii", font, Utility.GetColorFromUshort(Resources.HueData.GetHue(hue, -1))),
                Width));
        }

        class ChatLineTimed
        {
            readonly string _text;
            public string Text { get { return _text; } }
            float _createdTime = float.MinValue;
            bool _isExpired;
            public bool IsExpired { get { return _isExpired; } }
            float _alpha;
            public float Alpha { get { return _alpha; } }
            private int _width;

            const float Time_Display = 10000.0f;
            const float Time_Fadeout = 4000.0f;

            private RenderedText _Texture;
            public int TextHeight { get { return _Texture.Height; } }

            public ChatLineTimed(string text, int width)
            {
                _text = text;
                _isExpired = false;
                _alpha = 1.0f;
                _width = width;

                _Texture = new RenderedText(_text, _width);
            }

            public void Update(double totalMS, double frameMS)
            {
                if (_createdTime == float.MinValue)
                    _createdTime = (float)totalMS;
                float time = (float)totalMS - _createdTime;
                if (time > Time_Display)
                    _isExpired = true;
                else if (time > (Time_Display - Time_Fadeout))
                {
                    _alpha = 1.0f - ((time) - (Time_Display - Time_Fadeout)) / Time_Fadeout;
                }
            }

            public void Draw(SpriteBatchUI sb, Point position)
            {
                _Texture.Draw(sb, position, Utility.GetHueVector(0, false, (_alpha < 1.0f), true));
            }

            public void Dispose()
            {
                _Texture = null;
            }

            public override string ToString()
            {
                return _text;
            }
        }
    }
}
