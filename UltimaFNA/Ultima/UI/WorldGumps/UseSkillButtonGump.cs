﻿/***************************************************************************
 *   UseSkillButtonGump.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using Microsoft.Xna.Framework;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.Player;
using UltimaXNA.Ultima.UI.Controls;
using UltimaXNA.Ultima.World;
#endregion

namespace UltimaXNA.Ultima.UI.WorldGumps
{
    public class UseSkillButtonGump : Gump
    {
        // private variables
        private readonly SkillEntry _Skill;
        private ResizePic[] _BG;
        private HtmlGumpling _Caption;
        private bool _IsMouseDown;
        // services
        private readonly WorldModel _World;

        public UseSkillButtonGump(SkillEntry skill)
            : base(skill.ID, 0)
        {
            while (UserInterface.GetControl<UseSkillButtonGump>(skill.ID) != null)
            {
                UserInterface.GetControl<UseSkillButtonGump>(skill.ID).Dispose();
            }

            _Skill = skill;
            _World = Service.Get<WorldModel>();

            IsMoveable = true;
            HandlesMouseInput = true;

            _BG = new ResizePic[3];
            _BG[0] = (ResizePic)AddControl(new ResizePic(this, 0, 0, 0x24B8, 120, 40));
            _BG[1] = (ResizePic)AddControl(new ResizePic(this, 0, 0, 0x24EA, 120, 40));
            _BG[2] = (ResizePic)AddControl(new ResizePic(this, 0, 0, 0x251C, 120, 40));
            _Caption = (HtmlGumpling)AddControl(new HtmlGumpling(this, 0, 10, 120, 20, 0, 0, "<center>" + _Skill.Name));

            for (int i = 0; i < 3; i++)
            {
                _BG[i].MouseDownEvent += EventMouseDown;
                _BG[i].MouseUpEvent += EventMouseUp;
                _BG[i].MouseClickEvent += EventMouseClick;
            }
        }

        public override void Dispose()
        {
            for (int i = 0; i < 3; i++)
            {
                _BG[i].MouseDownEvent -= EventMouseDown;
                _BG[i].MouseUpEvent -= EventMouseUp;
                _BG[i].MouseClickEvent -= EventMouseClick;
            }
            base.Dispose();
        }

        public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
        {
            bool isMouseOver = (_BG[0].IsMouseOver || _BG[1].IsMouseOver || _BG[2].IsMouseOver);
            if (_IsMouseDown)
            {
                _BG[0].IsVisible = false;
                _BG[1].IsVisible = false;
                _BG[2].IsVisible = true;

            }
            else if (isMouseOver)
            {
                _BG[0].IsVisible = false;
                _BG[1].IsVisible = true;
                _BG[2].IsVisible = false;
            }
            else
            {
                _BG[0].IsVisible = true;
                _BG[1].IsVisible = false;
                _BG[2].IsVisible = false;
            }

            if (_IsMouseDown)
                _Caption.Position = new Point(_Caption.Position.X, _Caption.Position.Y + 1);

            base.Draw(spriteBatch, position, frameMS);

            if (_IsMouseDown)
                _Caption.Position = new Point(_Caption.Position.X, _Caption.Position.Y - 1);
        }

        private void EventMouseDown(AControl sender, int x, int y, MouseButton button)
        {
            OnMouseDown(x, y, button);
        }

        private void EventMouseUp(AControl sender, int x, int y, MouseButton button)
        {
            OnMouseUp(x, y, button);
        }

        private void EventMouseClick(AControl sender, int x, int y, MouseButton button)
        {
            if (button == MouseButton.Left)
                OnMouseClick(x, y, button);
        }

        protected override void OnMouseDown(int x, int y, MouseButton button)
        {
            if (button != MouseButton.Left)
                return;
            _IsMouseDown = true;
        }

        protected override void OnMouseUp(int x, int y, MouseButton button)
        {
            _IsMouseDown = false;
        }

        protected override void OnMouseClick(int x, int y, MouseButton button)
        {
            if (button != MouseButton.Left)
                return;
            _World.Interaction.UseSkill(_Skill.Index);
        }
    }
}