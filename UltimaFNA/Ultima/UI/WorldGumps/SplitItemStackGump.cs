﻿/***************************************************************************
 *   SplitItemStackGump.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using Microsoft.Xna.Framework;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.UI.Controls;
using UltimaXNA.Ultima.World;
using UltimaXNA.Ultima.World.Entities.Items;
#endregion

namespace UltimaXNA.Ultima.UI.WorldGumps
{
    class SplitItemStackGump : Gump
    {
        public Item Item
        {
            get;
            private set;
        }

        private readonly Point _PickupOffset;

        private readonly HSliderBar _Slider;
        private TextEntry _AmountEntry;
        private Button _OKButton;
        private int _LastValue;

        public SplitItemStackGump(Item item, Point pickupOffset)
            : base(0, 0)
        {
            Item = item;
            _PickupOffset = pickupOffset;

            IsMoveable = true;

            // Background
            AddControl(new GumpPic(this, 0, 0, 0x085c, 0));
            // Slider
            _Slider = (HSliderBar)AddControl(new HSliderBar(this, 30, 16, 104, 0, item.Amount, item.Amount, HSliderBarStyle.BlueWidgetNoBar));
            _LastValue = _Slider.Value;
            // Ok button
            AddControl(_OKButton = new Button(this, 102, 38, 0x085d, 0x085e, ButtonTypes.Default, 0, 0));
            _OKButton.GumpOverID = 0x085f;
            _OKButton.MouseClickEvent += ClickOkayButton;
            // Text entry field
            _AmountEntry = (TextEntry)AddControl(new TextEntry(this, 30, 39, 60, 16, 0, 0, 5, item.Amount.ToString()));
            _AmountEntry.LeadingHtmlTag = "<big>";
            _AmountEntry.LegacyCarat = true;
            _AmountEntry.Hue = 1001;
            _AmountEntry.ReplaceDefaultTextOnFirstKeypress = true;
            _AmountEntry.NumericOnly = true;
        }

        public override void Dispose()
        {
            _OKButton.MouseClickEvent -= ClickOkayButton;
            base.Dispose();
        }

        public override void Update(double totalMS, double frameMS)
        {
            // update children controls first
            base.Update(totalMS, frameMS);

            // update strategy: if slider != last value, then set text equal to slider value. else try parsing text.
            //                  if text is empty, value = minvalue.
            //                  if can't parse text, then set text equal to slider value.
            //                  if can parse text, and text != slider, then set slider = text.
            if (_Slider.Value != _LastValue)
            {
                _AmountEntry.Text = _Slider.Value.ToString();
            }
            else
            {
                int textValue;
                if (_AmountEntry.Text.Length == 0)
                {
                    _Slider.Value = _Slider.MinValue;
                }
                else if (!int.TryParse(_AmountEntry.Text, out textValue))
                {
                    _AmountEntry.Text = _Slider.Value.ToString();
                }
                else
                {
                    if (textValue != _Slider.Value)
                    {
                        if (textValue <= _Slider.MaxValue)
                            _Slider.Value = textValue;
                        else
                        {
                            _Slider.Value = _Slider.MaxValue;
                            _AmountEntry.Text = _Slider.Value.ToString();
                        }
                    }
                }
            }
            _LastValue = _Slider.Value;
        }

        private void ClickOkayButton(AControl sender, int x, int y, MouseButton button)
        {
            if (_Slider.Value > 0)
            {
                WorldModel world = Service.Get<WorldModel>();
                world.Interaction.PickupItem(Item, _PickupOffset, _Slider.Value);
            }
            Dispose();
        }

        public override void OnKeyboardReturn(int textID, string text)
        {
            if (_Slider.Value > 0)
            {
                WorldModel world = Service.Get<WorldModel>();
                world.Interaction.PickupItem(Item, _PickupOffset, _Slider.Value);
            }
            Dispose();
        }
    }
}
