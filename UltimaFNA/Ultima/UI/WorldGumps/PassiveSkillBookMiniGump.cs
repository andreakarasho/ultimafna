﻿#region usings
using System.Collections.Generic;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.Data;
using UltimaXNA.Ultima.UI.Controls;
using UltimaXNA.Ultima.World;
using UltimaXNA.Ultima.World.Entities.Mobiles;
#endregion

namespace UltimaXNA.Ultima.UI.WorldGumps
{
    class PassiveSkillBookMiniGump : Gump
    {
		private Mobile _Mobile;
		public PassiveSkillBookMiniGump(Mobile mobile)
			: base(mobile.Serial, 0)
		{
			IsMoveable = true;
			_Mobile = mobile;
			AddControl(new GumpPic(this, 0, 0, 0x2B27, 0)).MouseDoubleClickEvent += MaximizeBook;
		}

		public override void Update(double totalMS, double frameMS)
		{
			base.Update(totalMS, frameMS);
		}

		private void MaximizeBook(AControl sender, int x, int y, MouseButton button)
		{
			UserInterface.AddControl(new PassiveSkillBookGump(_Mobile), this.X, this.Y);
			this.Dispose(); // Rimuovo libro piccolo e apro libro grande
		}
	}
}
