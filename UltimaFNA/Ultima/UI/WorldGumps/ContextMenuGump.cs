﻿/***************************************************************************
 *   ContextMenuGump.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System.Text;
using UltimaXNA.Core.Network;
using UltimaXNA.Core.Resources;
using UltimaXNA.Core.UI.Fonts;
using UltimaXNA.Ultima.Data;
using UltimaXNA.Ultima.Network.Client;
using UltimaXNA.Ultima.UI.Controls;
using UltimaXNA.Core.Input;
using UltimaFNA.Core.Network.SocketAsync;
#endregion

namespace UltimaXNA.Ultima.UI.WorldGumps
{
    /// <summary>
    /// A context menu with a number of choices.
    /// </summary>
    class ContextMenuGump : Gump
    {
        private readonly ContextMenuData _Data;

        private readonly ResizePic _Background;
        private readonly HtmlGumpling _MenuItems;

        public ContextMenuGump(ContextMenuData data)
            : base(0, 0)
        {
            MetaData.IsModal = true;
            MetaData.ModalClickOutsideAreaClosesThisControl = true;

            _Data = data;

            IResourceProvider provider = Service.Get<IResourceProvider>();
            AFont font = (AFont)provider.GetUnicodeFont(1);

            _Background = (ResizePic)AddControl(new ResizePic(this, 0, 0, 0x0A3C, 50, font.Height * _Data.Count + 20));

            StringBuilder htmlContextItems = new StringBuilder();
            for (int i = 0; i < _Data.Count; i++)
            {
				if ((_Data[i].Flags & 0x01) == 0x01) // Context disabilitato 
				{
					htmlContextItems.Append(string.Format("<a href='{0}' color='#4d4d4d' hovercolor='#FFF' activecolor='#BBB' style='text-decoration:none;'>{1}</a><br/>", _Data[i].ResponseCode, _Data[i].Caption));
				}
				else if ((_Data[i].Flags & 0x04) == 0x04) // Context Highlight
				{
					htmlContextItems.Append(string.Format("<a href='{0}' color='#e6ac00' hovercolor='#FFF' activecolor='#BBB' style='text-decoration:none;'>{1}</a><br/>", _Data[i].ResponseCode, _Data[i].Caption));
				}
				else
				{
					htmlContextItems.Append(string.Format("<a href='{0}' color='#DDD' hovercolor='#FFF' activecolor='#BBB' style='text-decoration:none;'>{1}</a><br/>", _Data[i].ResponseCode, _Data[i].Caption));
				}
            }
            _MenuItems = (HtmlGumpling)AddControl(new HtmlGumpling(this, 10, 10, 200, font.Height * _Data.Count, 0, 0, htmlContextItems.ToString()));
		}

        public override void Update(double totalMS, double frameMS)
        {
            base.Update(totalMS, frameMS);
            _Background.Width = _MenuItems.Width + 20;
        }

        protected override void OnMouseOut(int x, int y)
        {
            // Dispose();
        }

        public override void OnHtmlInputEvent(string href, MouseEvent e)
        {
            if (e != MouseEvent.Click)
                return;

            int contextMenuItemSelected;
            if (int.TryParse(href, out contextMenuItemSelected))
            {
                NetworkClient network = Service.Get<NetworkClient>();
                network.Send(new ContextMenuResponsePacket(_Data.Serial, (short)contextMenuItemSelected));
                this.Dispose();
            }
        }
    }
}
