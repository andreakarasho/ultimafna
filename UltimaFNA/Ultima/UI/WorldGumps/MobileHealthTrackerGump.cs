﻿/***************************************************************************
 *   MobileHealthTrackerGump.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using UltimaXNA.Core.Input;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.UI.Controls;
using UltimaXNA.Ultima.World;
using UltimaXNA.Ultima.World.Entities.Mobiles;
using UltimaXNA.Core.Network;
using UltimaXNA.Ultima.Network.Client;
using Microsoft.Xna.Framework;
using UltimaFNA.Core.Network.SocketAsync;
#endregion

namespace UltimaXNA.Ultima.UI.WorldGumps
{
    class MobileHealthTrackerGump : Gump
    {
        public Mobile Mobile
        {
            get;
            private set;
        }

        private GumpPic _Background;
        private GumpPicWithWidth[] _Bars;
        private GumpPic[] _BarBGs;
        private TextEntry _NameEntry;
        private readonly WorldModel _World;

        public MobileHealthTrackerGump(Mobile mobile)
            : base(mobile.Serial, 0)
        {
            while (UserInterface.GetControl<MobileHealthTrackerGump>(mobile.Serial) != null)
            {
                UserInterface.GetControl<MobileHealthTrackerGump>(mobile.Serial).Dispose();
            }

            IsMoveable = true;

            Mobile = mobile;
            _World = Service.Get<WorldModel>();

            if (Mobile.IsClientEntity)
            {
                AddControl(_Background = new GumpPic(this, 0, 0, 0x0803, 0));
                _Background.MouseDoubleClickEvent += Background_MouseDoubleClickEvent;
                _BarBGs = new GumpPic[3];
                AddControl(_BarBGs[0] = new GumpPic(this, 34, 10, 0x0805, 0));
                AddControl(_BarBGs[1] = new GumpPic(this, 34, 24, 0x0805, 0));
                AddControl(_BarBGs[2] = new GumpPic(this, 34, 38, 0x0805, 0));
                _Bars = new GumpPicWithWidth[3];
                AddControl(_Bars[0] = new GumpPicWithWidth(this, 34, 10, 0x0806, 0, 1f));
                AddControl(_Bars[1] = new GumpPicWithWidth(this, 34, 24, 0x0806, 0, 1f));
                AddControl(_Bars[2] = new GumpPicWithWidth(this, 34, 38, 0x0806, 0, 1f));
            }
            else
            {
                AddControl(_Background = new GumpPic(this, 0, 0, 0x0804, 0));
                _Background.MouseDoubleClickEvent += Background_MouseDoubleClickEvent;
                _BarBGs = new GumpPic[1];
                AddControl(_BarBGs[0] = new GumpPic(this, 34, 38, 0x0805, 0));
                _Bars = new GumpPicWithWidth[1];
                AddControl(_Bars[0] = new GumpPicWithWidth(this, 34, 38, 0x0806, 0, 1f));
                AddControl(_NameEntry = new TextEntry(this, 17, 16, 124, 20, 0, 0, 99, mobile.Name));
                SetupMobileNameEntry();
            }

            // bars should not handle mouse input, pass it to the background gump.
            for (int i = 0; i < _BarBGs.Length; i++)
            {
                _BarBGs[i].HandlesMouseInput = false;
                _Bars[i].HandlesMouseInput = false;
            }
        }

        public override void Dispose()
        {
            _Background.MouseDoubleClickEvent -= Background_MouseDoubleClickEvent;
            base.Dispose();
        }

        public override void Update(double totalMS, double frameMS)
        {
			_Bars[0].PercentWidthDrawn = ((float)Mobile.Health.Current / Mobile.Health.Max);

			if (Mobile.Flags.IsBlessed)
                _Bars[0].GumpID = 0x0809;
            else if (Mobile.Flags.IsPoisoned)
                _Bars[0].GumpID = 0x0808;

            if (Mobile.IsClientEntity)
            {
                if (Mobile.Flags.IsWarMode)
                    _Background.GumpID = 0x0807;
                else
                    _Background.GumpID = 0x0803;
                _Bars[1].PercentWidthDrawn = ((float)Mobile.Stamina.Current / Mobile.Stamina.Max);
                _Bars[2].PercentWidthDrawn = ((float)Mobile.Mana.Current / Mobile.Mana.Max);
            }
            else
            {
				// Distanza e colore notoriety
				if (Utility.IsPointThisDistanceAway(new Point(Mobile.X, Mobile.Y), new Point(WorldModel.Entities.GetPlayerEntity().X, WorldModel.Entities.GetPlayerEntity().Y), 20))
				{
					_Background.Hue = 0x0386;
					_Bars[0].Hue = 0x0386;
				}
				else
				{
					if (WorldModel.Entities.GetObject<Mobile>(Mobile.Serial, false) == null) // Rimozione barra se si assiste alla morte del mob
					{
						Dispose();
						return;
					}
					_Background.GumpID = 0x0804;
					_Bars[0].Hue = 0;
					_Background.Hue = Mobile.NotorietyHue;
				}

                if (Mobile.PlayerCanChangeName != _NameEntry.IsEditable)
                    SetupMobileNameEntry();
            }

            base.Update(totalMS, frameMS);
        }

        private void Background_MouseDoubleClickEvent(AControl caller, int x, int y, MouseButton button)
        {
            if (Mobile.IsClientEntity)
            {
				this.Dispose(); // Rimuovo vecchio e creo nuovo grande
				StatusGump.Toggle(Mobile.Serial);
            }
            else
            {
                _World.Interaction.LastTarget = Mobile.Serial;

                // Attack
                if (WorldModel.Entities.GetPlayerEntity().Flags.IsWarMode)
                {
                    _World.Interaction.AttackRequest(Mobile);
                }
                // Open Paperdoll
                else
                {
                    _World.Interaction.DoubleClick(Mobile);
                }
            }
        }

        private void SetupMobileNameEntry()
        {
            if (Mobile.PlayerCanChangeName)
            {
                _NameEntry.IsEditable = true;
                _NameEntry.LeadingHtmlTag = "<span color='#808' style='font-family:ascii9;'>";
            }
            else
            {
                _NameEntry.IsEditable = false;
                _NameEntry.LeadingHtmlTag = "<span color='#444' style='font-family=ascii9;'>";
            }
        }

        public override void OnKeyboardReturn(int textID, string text)
        {
            NetworkClient client = Service.Get<NetworkClient>();
            client.Send(new RenameCharacterPacket(Mobile.Serial, text));
        }
    }
}
