﻿/***************************************************************************
 *   BookGump.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using System.Collections.Generic;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.Audio;
using UltimaXNA.Ultima.Data;
using UltimaXNA.Ultima.UI.Controls;
using UltimaXNA.Ultima.World;
using UltimaXNA.Ultima.World.Entities;
using UltimaXNA.Ultima.World.Entities.Items;

namespace UltimaXNA.Ultima.UI.WorldGumps
{
    public class BookGump : Gump
    {
        BaseBook _Book;
        GumpPic _BookBackground;
        GumpPic _PageCornerLeft;
        GumpPic _PageCornerRight;
        List<TextEntryPage> _Pages = new List<TextEntryPage>();
        TextEntry _TitleTextEntry;
        TextEntry _AuthorTextEntry;
        int _LastPage;
        WorldModel _World;

        // ================================================================================
        // Ctor, Dispose, BuildGump, and SetActivePage
        // ================================================================================
        public BookGump(BaseBook entity)
            : base(entity.Serial, 0)
        {
            _Book = entity;
            _Book.SetCallbacks(OnEntityUpdate, OnEntityDispose);
            _LastPage = (_Book.PageCount + 2) / 2;
            IsMoveable = true;
            _World = Service.Get<WorldModel>(false);
            BuildGump();
        }

        public override void Dispose()
        {
            _Book.ClearCallBacks(OnEntityUpdate, OnEntityDispose);
            if (_PageCornerLeft != null)
            {
                _PageCornerLeft.MouseClickEvent -= PageCorner_MouseClickEvent;
                _PageCornerLeft.MouseDoubleClickEvent -= PageCorner_MouseDoubleClickEvent;
            }
            if (_PageCornerRight != null)
            {
                _PageCornerRight.MouseClickEvent -= PageCorner_MouseClickEvent;
                _PageCornerRight.MouseDoubleClickEvent -= PageCorner_MouseDoubleClickEvent;
            }
            base.Dispose();
        }

        void BuildGump()
        {
            ClearControls();
           // if (_Book.ItemID >= 0xFEF && _Book.ItemID <= 0xFF2)
           if (Books.IsBookItem((ushort)_Book.ItemID))
            {
                _BookBackground = new GumpPic(this, 0, 0, 0x1FE, 0);
                _PageCornerLeft = new GumpPic(this, 0, 0, 0x1FF, 0);
                _PageCornerRight = new GumpPic(this, 356, 0, 0x200, 0);
            }
            AddControl(_BookBackground);   // book background gump
            AddControl(_PageCornerLeft);   // page turn left
            _PageCornerLeft.GumpLocalID = 0;
            _PageCornerLeft.MouseClickEvent += PageCorner_MouseClickEvent;
            _PageCornerLeft.MouseDoubleClickEvent += PageCorner_MouseDoubleClickEvent;
            AddControl(_PageCornerRight);  // page turn right
            _PageCornerRight.GumpLocalID = 1;
            _PageCornerRight.MouseClickEvent += PageCorner_MouseClickEvent;
            _PageCornerRight.MouseDoubleClickEvent += PageCorner_MouseDoubleClickEvent;
            // Draw the title and author page
            _TitleTextEntry = new TextEntry(this, 45, 50, 155, 300, 1, 0, 0, _Book.Title);
            _TitleTextEntry.MakeThisADragger();
            _TitleTextEntry.IsEditable = _Book.IsEditable;
            _AuthorTextEntry = new TextEntry(this, 45, 110, 160, 300, 1, 0, 0, _Book.Author);
            _AuthorTextEntry.MakeThisADragger();
            _AuthorTextEntry.IsEditable = _Book.IsEditable;
            AddControl(_TitleTextEntry, 1);
            AddControl(new HtmlGumpling(this, 45, 90, 155, 300, 0, 0, "<font color=#444>By"), 1);
            AddControl(_AuthorTextEntry, 1);
            // Add book pages to active pages
            bool isRight = true;
            string color = _Book.IsEditable ? "800" : "000";
            for (int i = 0; i < _Book.PageCount; i++)
            {
                int onGumpPage = (i + 3) / 2;
                int x = isRight ? 235 : 45;
                _Pages.Add(new TextEntryPage(this, x, 32, 155, 300, i));
                _Pages[i].SetMaxLines(8, OnPageOverflow, OnPageUnderflow);
                _Pages[i].SetKeyboardPageControls(OnPreviousPage, OnNextPage);
                _Pages[i].MakeThisADragger();
                _Pages[i].IsEditable = _Book.IsEditable;
                _Pages[i].LeadingHtmlTag = $"<font color=#{color}>";
                _Pages[i].Text = _Book.Pages[i].GetAllLines();
                AddControl(_Pages[i], onGumpPage);
                AddControl(new HtmlGumpling(this, x, 195, 135, 20, 0, 0, $"<center><font color=#444>{i + 1}"), onGumpPage);
                isRight = !isRight;
            }
            AudioService service = Service.Get<AudioService>();
            service.PlaySound(0x058);
            SetActivePage(1);
            UserInterface.KeyboardFocusControl = _Pages[0];
            _Pages[0].CaratAt = _Pages[0].Text.Length;
        }

        void SetActivePage(int page)
        {
            if (page == ActivePage)
            {
                return;
            }
            CheckForContentChanges();
            if (page < 1)
            {
                page = 1;
            }
            if (page > _LastPage)
            {
                page = _LastPage;
            }
            ActivePage = page;
            // Hide the page corners if we're at the first or final page.
            _PageCornerLeft.Page = (page != 1) ? 0 : int.MaxValue;
            _PageCornerRight.Page = (page != _LastPage) ? 0 : int.MaxValue;
            int textEntryPageIndex = (page - 1) * 2 - 1;
            if (textEntryPageIndex == -1)
            {
                textEntryPageIndex = 0;
            }
            if (_Pages[textEntryPageIndex] != null)
            {
                UserInterface.KeyboardFocusControl = _Pages[textEntryPageIndex];
                _Pages[textEntryPageIndex].CaratAt = _Pages[textEntryPageIndex].Text.Length;
            }
        }

        // ================================================================================
        // OnEntityUpdate - called when book entity is updated by server.
        // OnEntityDispose - called when book entity is disposed by server.
        // ================================================================================
        void OnEntityUpdate(AEntity entity)
        {
            _Book = entity as BaseBook;
            BuildGump();
        }

        void OnEntityDispose(AEntity entity)
        {
            Dispose();
        }

        // ================================================================================
        // Mouse Control
        // ================================================================================

        void PageCorner_MouseClickEvent(AControl sender, int x, int y, MouseButton button)
        {
            if (button != MouseButton.Left)
            {
                return;
            }
            if (sender.GumpLocalID == 0)
            {
                SetActivePage(ActivePage - 1);
            }
            else
            {
                SetActivePage(ActivePage + 1);
            }
            AudioService service = Service.Get<AudioService>();
            service.PlaySound(0x055);
        }

        void PageCorner_MouseDoubleClickEvent(AControl sender, int x, int y, MouseButton button)
        {
            if (button != MouseButton.Left)
            {
                return;
            }
            if (sender.GumpLocalID == 0)
            {
                SetActivePage(1);
            }
            else
            {
                SetActivePage(_LastPage);
            }
        }

        protected override void CloseWithRightMouseButton()
        {
            CheckForContentChanges();
            AudioService service = Service.Get<AudioService>();
            service.PlaySound(0x058);
            base.CloseWithRightMouseButton();
        }

        // ================================================================================
        // Keyboard/Text Control
        // ================================================================================

            
        void OnNextPage(int pageIndex)
        {
            if (pageIndex < _Pages.Count - 1)
            {
                int nextPage = pageIndex + 1;
                SetActivePage((nextPage + 1) / 2 + 1);
                UserInterface.KeyboardFocusControl = _Pages[nextPage];
                _Pages[nextPage].CaratAt = 0;
            }
        }

        void OnPreviousPage(int pageIndex)
        {
            if (pageIndex > 0)
            {
                int prevPage = pageIndex - 1;
                SetActivePage((prevPage + 1) / 2 + 1);
                UserInterface.KeyboardFocusControl = _Pages[prevPage];
                _Pages[prevPage].CaratAt = _Pages[prevPage].Text.Length;
            }
        }

        /// <summary>
        /// Called when the user hits backspace at index 0 on a page. 
        /// </summary>
        void OnPageUnderflow(int pageIndex)
        {
            if (pageIndex <= 0)
            {
                return;
            }
            int underflowFrom = pageIndex;
            int underflowTo = pageIndex - 1;
            string underflowFromText = _Pages[underflowFrom].Text;
            string underflowToText = _Pages[underflowTo].Text.Substring(0, (_Pages[underflowTo].Text.Length > 0 ? _Pages[underflowTo].Text.Length - 1 : 0));
            int carat = underflowToText.Length - _Pages[underflowFrom].CaratAt;
            _Pages[underflowFrom].Text = string.Empty;
            _Pages[underflowTo].Text = $"{underflowToText}{underflowFromText}";
            if (carat <= _Pages[underflowTo].Text.Length)
            {
                SetActivePage((underflowTo + 1) / 2 + 1);
                UserInterface.KeyboardFocusControl = _Pages[underflowTo];
                _Pages[underflowTo].CaratAt = carat;
            }
            else
            {
                SetActivePage((underflowFrom + 1) / 2 + 1);
                UserInterface.KeyboardFocusControl = _Pages[underflowFrom];
                _Pages[underflowFrom].CaratAt = carat - _Pages[underflowTo].Text.Length;
            }
        }

        /// <summary>
        /// Called when text on a page is too large to be held in the page. The text overflows to the next page.
        /// </summary>
        void OnPageOverflow(int page, string overflow)
        {
            int overflowFrom = page;
            int overflowTo = page + 1;
            if (overflowTo < _Pages.Count)
            {
                _Pages[overflowTo].Text = _Pages[overflowTo].Text.Insert(0, overflow);
                SetActivePage((overflowTo + 1) / 2 + 1);
                UserInterface.KeyboardFocusControl = _Pages[overflowTo];
                _Pages[overflowTo].CaratAt = overflow.Length;
            }
        }

        void CheckForContentChanges()
        {
            if (ActivePage < 1)
            {
                return;
            }
            int leftIndex = ActivePage * 2 - 3;
            int rightIndex = leftIndex + 1;
            // Check title, author, and the first page if leftPageIndex < 0
            // Else if leftPageIndex >= 0, they are all pages
            if (leftIndex < 0)
            {
                if (_TitleTextEntry.Text != _Book.Title || _AuthorTextEntry.Text != _Book.Author)
                {
                    _World?.Interaction.BookHeaderNewChange(_Book.Serial, _TitleTextEntry.Text, _AuthorTextEntry.Text);
                }
                if (rightIndex < _Pages.Count && _Pages[rightIndex].TextWithLineBreaks != _Book.Pages[rightIndex].GetAllLines())
                {
                    _World?.Interaction.BookPageChange(_Book.Serial, rightIndex, GetTextEntryAsArray(_Pages[rightIndex]));
                }
            }
            else
            {
                if (_Pages[leftIndex].TextWithLineBreaks != _Book.Pages[leftIndex].GetAllLines())
                {
                    _World?.Interaction.BookPageChange(_Book.Serial, leftIndex, GetTextEntryAsArray(_Pages[leftIndex]));
                }
                if (rightIndex < _Pages.Count - 1 && _Pages[rightIndex].TextWithLineBreaks != _Book.Pages[rightIndex].GetAllLines())
                {
                    _World?.Interaction.BookPageChange(_Book.Serial, rightIndex, GetTextEntryAsArray(_Pages[rightIndex]));
                }
            }
        }

        string[] GetTextEntryAsArray(TextEntryPage text) => text.TextWithLineBreaks.Split('\n');
    }
}
