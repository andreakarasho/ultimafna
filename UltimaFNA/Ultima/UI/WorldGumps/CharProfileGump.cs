﻿#region usings
using System.Collections.Generic;
using System.Text;
using UltimaXNA.Core.Input;
using UltimaXNA.Ultima.Player;
using UltimaXNA.Ultima.UI.Controls;
using UltimaXNA.Ultima.World;
#endregion

namespace UltimaXNA.Ultima.UI.WorldGumps
{
    class CharProfileGump : Gump
    {
        // Private variables
        private ExpandableScroll _Background;
        private HtmlGumpling _ProfileHtml;
		private StringBuilder _Html = new StringBuilder();

		// Services
		private WorldModel _World;

        public CharProfileGump(string header, string body, string footer)
            : base(0, 0)
        {
			IsMoveable = true;

            _World = Service.Get<WorldModel>();

            AddControl(_Background = new ExpandableScroll(this, 0, 0, 200));
            _Background.TitleGumpID = 0x820;

            AddControl(_ProfileHtml = new HtmlGumpling(this, 42, 42, 220, 155, 0, 2, string.Empty));

			_Html.Append(string.Format("<span color='#000000' style='font-family=uni1;'>{0}</span>", header)); // testo header
			_Html.Append("<br><br>"); // Spazio
			_Html.Append("<gumpimg src = '0x0060'/>"); // Divisore

			if (body != string.Empty)
			{
				_Html.Append("<br>"); // Spazio
				_Html.Append(string.Format("<span color='#000000' style='font-family=uni1;'>{0}</span>", body)); // testo body
				_Html.Append("<br><br>"); // Spazio
				_Html.Append("<gumpimg src = '0x0060'/>"); // Divisore
			}

			_Html.Append("<br>"); // Spazio
			_Html.Append(string.Format("<span color='#000000' style='font-family=uni1;'>{0}</span>", footer)); // testo footer

			_ProfileHtml.Text = _Html.ToString();
		}
    }
}
