﻿/***************************************************************************
 *   MiniMapGump.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.Resources;
using UltimaXNA.Ultima.World;
using UltimaXNA.Ultima.World.Entities;
using UltimaXNA.Core.UI;
#endregion

namespace UltimaXNA.Ultima.UI.WorldGumps
{
    class MiniMapGump : Gump
    {
        const float ReticleBlinkMS = 250f;

        float _TimeMS;
        bool _UseLargeMap;
        WorldModel _World;
        Texture2D _GumpTexture;
        Texture2D _PlayerIndicator;

        public static bool MiniMap_LargeFormat
        {
            get;
            set;
        }

        public static void Toggle()
        {
            UserInterfaceService ui = Service.Get<UserInterfaceService>();
            if (ui.GetControl<MiniMapGump>() == null)
            {
                ui.AddControl(new MiniMapGump(), 566, 25);
            }
            else
            {
                if (MiniMapGump.MiniMap_LargeFormat == false)
                {
                    MiniMapGump.MiniMap_LargeFormat = true;
                }
                else
                {
                    ui.RemoveControl<MiniMapGump>();
                    MiniMapGump.MiniMap_LargeFormat = false;
                }
            }
        }

        public MiniMapGump()
            : base(0, 0)
        {
            _World = Service.Get<WorldModel>();

            _UseLargeMap = MiniMap_LargeFormat;

            IsMoveable = true;
            MakeThisADragger();
        }

        protected override void OnInitialize()
        {
            SetSavePositionName("minimap");
            base.OnInitialize();
        }

        public override void Update(double totalMS, double frameMS)
        {
            if (_GumpTexture == null || _UseLargeMap != MiniMap_LargeFormat)
            {
                _UseLargeMap = MiniMap_LargeFormat;
                if (_GumpTexture != null)
                {
                    _GumpTexture = null;
                }
                IResourceProvider provider = Service.Get<IResourceProvider>();
                _GumpTexture = provider.GetUITexture((_UseLargeMap ? 5011 : 5010), true);
                Size = new Point(_GumpTexture.Width, _GumpTexture.Height);
            }

            base.Update(totalMS, frameMS);
        }

        public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
        {
            AEntity player = WorldModel.Entities.GetPlayerEntity();
            float x = (float)Math.Round((player.Position.X % 256) + player.Position.X_offset) / 256f;
            float y = (float)Math.Round((player.Position.Y % 256) + player.Position.Y_offset) / 256f;
            Vector3 playerPosition = new Vector3(x - y, x + y, 0f);
            float minimapU = (_GumpTexture.Width / 256f) / 2f;
            float minimapV = (_GumpTexture.Height / 256f) / 2f;

            VertexPositionNormalTextureHue[] v = 
            {
                new VertexPositionNormalTextureHue(new Vector3(position.X, position.Y, 0), playerPosition + new Vector3(-minimapU, -minimapV, 0), new Vector3(0, 0, 0)),
                new VertexPositionNormalTextureHue(new Vector3(position.X + Width, position.Y, 0), playerPosition + new Vector3(minimapU, -minimapV, 0), new Vector3(1, 0, 0)),
                new VertexPositionNormalTextureHue(new Vector3(position.X, position.Y + Height, 0), playerPosition + new Vector3(-minimapU, minimapV, 0), new Vector3(0, 1, 0)),
                new VertexPositionNormalTextureHue(new Vector3(position.X + Width, position.Y + Height, 0), playerPosition + new Vector3(minimapU, minimapV, 0), new Vector3(1, 1, 0))
            };

            spriteBatch.DrawSprite(_GumpTexture, v, Techniques.MiniMap);

            _TimeMS += (float)frameMS;
            if (_TimeMS >= ReticleBlinkMS)
            {
                if (_PlayerIndicator == null)
                {
                    _PlayerIndicator = new Texture2D(spriteBatch.GraphicsDevice, 1, 1);
                    _PlayerIndicator.SetData(new uint[1] { 0xFFFFFFFF });
                }
                spriteBatch.Draw2D(_PlayerIndicator, new Vector3(position.X + Width / 2, position.Y + Height / 2 - 8, 0), Vector3.Zero);
            }
            if (_TimeMS >= ReticleBlinkMS * 2)
            {
                _TimeMS -= ReticleBlinkMS * 2;
            }
        }

        protected override void OnMouseDoubleClick(int x, int y, MouseButton button)
        {
            if (button == MouseButton.Left)
            {
                MiniMap_LargeFormat = !MiniMap_LargeFormat;
            }
        }
    }
}
