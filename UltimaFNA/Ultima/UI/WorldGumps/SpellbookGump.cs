﻿/***************************************************************************
 *   SpellbookGump.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System.Collections.Generic;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.Data;
using UltimaXNA.Ultima.UI.Controls;
using UltimaXNA.Ultima.World;
using UltimaXNA.Ultima.World.Entities;
using UltimaXNA.Ultima.World.Entities.Items.Containers;
using UltimaXNA.Core.Resources;
#endregion

namespace UltimaXNA.Ultima.UI.WorldGumps
{
    class SpellbookGump : Gump
    {
		// Descrizioni spell inizio ID
		const int _MageClilocStart = 1061290;
		const int _NecromancerClilocStart = 1061390;
		const int _ChivalryClilocStart = 1061490;
		const int _BushidoClilocStart = 1063263;
		const int _NinjitsuClilocStart = 1063279;
		const int _SpellweavingClilocStart = 1072042;
		const int _MysticismClilocStart = 1095193;

		// ============================================================================================================
		// Private variables
		// ============================================================================================================
		private SpellBook _Spellbook;
        private HtmlGumpling[] _CircleHeaders;
        private HtmlGumpling[] _Indexes;

        // ============================================================================================================
        // Private services 
        // ============================================================================================================
        private WorldModel _World;
		private IResourceProvider _Provider;

		// ============================================================================================================
		// Ctor, Update, Dispose
		// ============================================================================================================
		public SpellbookGump(SpellBook entity)
            : base(entity.Serial, 0)
        {
            _World = Service.Get<WorldModel>();
			_Provider = Service.Get<IResourceProvider>();

			_Spellbook = entity;
            _Spellbook.SetCallbacks(OnEntityUpdate, OnEntityDispose);

            IsMoveable = true;

			switch (_Spellbook.BookType)
			{
				case SpellBookTypes.Magic:
					CreateMageryGumplings();
					break;
				case SpellBookTypes.Bushido:
					CreateBushidoGumplings();
					break;
				case SpellBookTypes.Chivalry:
					CreateChivalryGumplings();
					break;
				case SpellBookTypes.Necromancer:
					CreateNecromancerGumplings();
					break;
				case SpellBookTypes.Ninjitsu:
					CreateNinjitsuGumplings();
					break;
				case SpellBookTypes.Spellweaving:
					CreateSpellweavingGumplings();
					break;
				case SpellBookTypes.Mysticism:
					CreateMysticismGumplings();
					break;
				default:
					break;
			}
        }

        public override void Update(double totalMS, double frameMS)
        {
            base.Update(totalMS, frameMS);
        }

        public override void Dispose()
        {
            _Spellbook.ClearCallBacks(OnEntityUpdate, OnEntityDispose);
            if (_PageCornerLeft != null)
            {
                _PageCornerLeft.MouseClickEvent -= PageCorner_MouseClickEvent;
                _PageCornerLeft.MouseDoubleClickEvent -= PageCorner_MouseDoubleClickEvent;
            }
            if (_PageCornerRight != null)
            {
                _PageCornerRight.MouseClickEvent -= PageCorner_MouseClickEvent;
                _PageCornerRight.MouseDoubleClickEvent -= PageCorner_MouseDoubleClickEvent;
            }
            base.Dispose();
        }

        // ============================================================================================================
        // OnEntityUpdate - called when spellbook entity is updated by server.
        // ============================================================================================================
        void OnEntityUpdate(AEntity entity)
        {
			switch (_Spellbook.BookType)
			{
				case SpellBookTypes.Magic:
					CreateMageryGumplings();
					break;
				case SpellBookTypes.Bushido:
					CreateBushidoGumplings();
					break;
				case SpellBookTypes.Chivalry:
					CreateChivalryGumplings();
					break;
				case SpellBookTypes.Necromancer:
					CreateNecromancerGumplings();
					break;
				case SpellBookTypes.Ninjitsu:
					CreateNinjitsuGumplings();
					break;
				case SpellBookTypes.Spellweaving:
					CreateSpellweavingGumplings();
					break;
				case SpellBookTypes.Mysticism:
					CreateMysticismGumplings();
					break;
				default:
					break;
			}
        }

        void OnEntityDispose(AEntity entity)
        {
            Dispose();
        }

        // ============================================================================================================
        // Child control creation
        // The spellbook is laid out as follows:
        // 1. A list of all spells in the book. Clicking on a spell will turn to that spell's page.
        // 2. One page per spell in the book. Icon, runes, reagents, etc.
        // ============================================================================================================
        GumpPic _PageCornerLeft;
        GumpPic _PageCornerRight;
        int _MaxPage;
        readonly List<KeyValuePair<int, int>> _SpellList = new List<KeyValuePair<int, int>>();

	    void CreateMysticismGumplings()
		{
			ClearControls();

			AddControl(new GumpPic(this, 0, 0, 0x2B32, 0)).MouseClickEvent += MinimizeBook; // spellbook background

			AddControl(_PageCornerLeft = new GumpPic(this, 50, 8, 0x08BB, 0)); // page turn left
			_PageCornerLeft.GumpLocalID = 0;
			_PageCornerLeft.MouseClickEvent += PageCorner_MouseClickEvent;
			_PageCornerLeft.MouseDoubleClickEvent += PageCorner_MouseDoubleClickEvent;
			_PageCornerLeft.Page = int.MaxValue; // Nascondo gira pagina di sinistra se appena aperto

			AddControl(_PageCornerRight = new GumpPic(this, 321, 8, 0x08BC, 0)); // page turn right
			_PageCornerRight.GumpLocalID = 1;
			_PageCornerRight.MouseClickEvent += PageCorner_MouseClickEvent;
			_PageCornerRight.MouseDoubleClickEvent += PageCorner_MouseDoubleClickEvent;

			// Titolo libro
			AddControl(new HtmlGumpling(this, 55, 15, 130, 200, 0, 0,
						string.Format("<span color='#004' style='font-family=uni0;'><center>INDEX</center></span>")),
						1);

			AddControl(new HtmlGumpling(this, 220, 15, 130, 200, 0, 0,
						string.Format("<span color='#004' style='font-family=uni0;'><center>INDEX</center></span>")),
						1);

			// creo spazio per lista spell
			_Indexes = new HtmlGumpling[16];
			for (int i = 0; i < 8; i++) // Lista pagina Sinistra
			{
				_Indexes[i] = (HtmlGumpling)AddControl(
					new HtmlGumpling(this, 60, 45 + (i * 18), 130, 200, 0, 0, string.Empty),
					1);
			}

			for (int i = 0; i < 8; i++) // Lista pagina destra
			{
				_Indexes[i + 8] = (HtmlGumpling)AddControl(
					new HtmlGumpling(this, 220, 45 + (i * 18), 130, 200, 0, 0, string.Empty),
					1);
			}

			// COntrollo le spell nel libro
			int totalSpells = 0;
			_SpellList.Clear();

            
            for (int spellIndex = 1; spellIndex <= 16; spellIndex++)
            {
                if (_Spellbook.HasSpell(1, spellIndex))
                {
                    _SpellList.Add(new KeyValuePair<int, int>(1, spellIndex));
                    totalSpells++;
                }
            }
            

			int h = 0;

			List<int> presentspell = new List<int>();

			// Genero Indice spell
			foreach (KeyValuePair<int, int> spell in _SpellList)
			{
				_Indexes[h].Text += string.Format("<a href='page={1}' hovercolor='#800' style='font-family=ascii9; text-decoration=none;'>{0}</a><br/>",
					SpellsMysticism.GetSpell(spell.Value).Name, 2 + (h / 2));
				h++;
				presentspell.Add(spell.Value);
			}

			int y = 2;
			h = 0;
			bool right = false; 
			// Genero pagine descrizione spell
			foreach (int id in presentspell)
			{
				CreateSpellPage(y, right, 1, SpellsMysticism.GetSpell(id));

				if (right) // Seleziono su quale pagina mettere la descrizione
					right = false;
				else
					right = true;

				// Conto le pagine necessarie 1 per ogni 2 spell
				if ((h % 2) == 1)
				{
					y++;
				}
				h++;
			}

			_MaxPage = y - 1;
		}
		void CreateBushidoGumplings()
		{
			ClearControls();

			AddControl(new GumpPic(this, 0, 0, 0x2B07, 0)).MouseClickEvent += MinimizeBook; // spellbook background

			AddControl(_PageCornerLeft = new GumpPic(this, 50, 8, 0x08BB, 0)); // page turn left
			_PageCornerLeft.GumpLocalID = 0;
			_PageCornerLeft.MouseClickEvent += PageCorner_MouseClickEvent;
			_PageCornerLeft.MouseDoubleClickEvent += PageCorner_MouseDoubleClickEvent;
			_PageCornerLeft.Page = int.MaxValue; // Nascondo gira pagina di sinistra se appena aperto

			AddControl(_PageCornerRight = new GumpPic(this, 321, 8, 0x08BC, 0)); // page turn right
			_PageCornerRight.GumpLocalID = 1;
			_PageCornerRight.MouseClickEvent += PageCorner_MouseClickEvent;
			_PageCornerRight.MouseDoubleClickEvent += PageCorner_MouseDoubleClickEvent;

			// Titolo libro
			AddControl(new HtmlGumpling(this, 55, 15, 130, 200, 0, 0,
						string.Format("<span color='#004' style='font-family=uni0;'><center>INDEX</center></span>")),
						1);

			AddControl(new HtmlGumpling(this, 220, 15, 130, 200, 0, 0,
						string.Format("<span color='#004' style='font-family=uni0;'><center>INDEX</center></span>")),
						1);

			// creo spazio per lista spell
			_Indexes = new HtmlGumpling[6];
			for (int i = 0; i < 3; i++) // Lista pagina Sinistra
			{
				_Indexes[i] = (HtmlGumpling)AddControl(
					new HtmlGumpling(this, 60, 45 + (i * 18), 160, 200, 0, 0, string.Empty),
					1);
			}

			for (int i = 0; i < 3; i++) // Lista pagina destra
			{
				_Indexes[i + 3] = (HtmlGumpling)AddControl(
					new HtmlGumpling(this, 220, 45 + (i * 18), 160, 200, 0, 0, string.Empty),
					1);
			}

			// Carico lista spell bushido inutile ? in quanto le ha già tutte di base vero?
			int totalSpells = 0;
			_SpellList.Clear();
			for (int spellIndex = 1; spellIndex <= 6; spellIndex++)
			{
				_SpellList.Add(new KeyValuePair<int, int>(1, spellIndex));
				totalSpells++;
			}

			int h = 0;

			List<int> presentspell = new List<int>();

			// Genero Indice spell
			foreach (KeyValuePair<int, int> spell in _SpellList)
			{
				_Indexes[h].Text += string.Format("<a href='page={1}' hovercolor='#800' style='font-family=ascii9; text-decoration=none;'>{0}</a><br/>",
					SpellsBushido.GetSpell(spell.Value).Name, 2 + (h / 2));
				h++;
				presentspell.Add(spell.Value);
			}

			int y = 2;
			h = 0;
			bool right = false; 

			// Genero pagine descrizione spell
			foreach (int id in presentspell)
			{
				CreateSpellPage(y, right, 1, SpellsBushido.GetSpell(id));

				if (right) // Seleziono su quale pagina mettere la descrizione
					right = false;
				else
					right = true;

				// Conto le pagine necessarie 1 per ogni 2 spell
				if ((h % 2) == 1)
				{
					y++;
				}
				h++;
			}
			_MaxPage = y - 1;
		}

		void CreateChivalryGumplings()
		{
			ClearControls();

			AddControl(new GumpPic(this, 0, 0, 0x2B01, 0)).MouseClickEvent += MinimizeBook; // spellbook background

			AddControl(_PageCornerLeft = new GumpPic(this, 50, 8, 0x08BB, 0)); // page turn left
			_PageCornerLeft.GumpLocalID = 0;
			_PageCornerLeft.MouseClickEvent += PageCorner_MouseClickEvent;
			_PageCornerLeft.MouseDoubleClickEvent += PageCorner_MouseDoubleClickEvent;
			_PageCornerLeft.Page = int.MaxValue; // Nascondo gira pagina di sinistra se appena aperto

			AddControl(_PageCornerRight = new GumpPic(this, 321, 8, 0x08BC, 0)); // page turn right
			_PageCornerRight.GumpLocalID = 1;
			_PageCornerRight.MouseClickEvent += PageCorner_MouseClickEvent;
			_PageCornerRight.MouseDoubleClickEvent += PageCorner_MouseDoubleClickEvent;

			// Titolo libro
			AddControl(new HtmlGumpling(this, 55, 15, 130, 200, 0, 0,
						string.Format("<span color='#004' style='font-family=uni0;'><center>INDEX</center></span>")),
						1);

			AddControl(new HtmlGumpling(this, 220, 15, 130, 200, 0, 0,
						string.Format("<span color='#004' style='font-family=uni0;'><center>INDEX</center></span>")),
						1);

			// creo spazio per lista spell
			_Indexes = new HtmlGumpling[10];
			for (int i = 0; i < 5; i++) // Lista pagina Sinistra
			{
				_Indexes[i] = (HtmlGumpling)AddControl(
					new HtmlGumpling(this, 60, 45 + (i * 18), 160, 200, 0, 0, string.Empty),
					1);
			}

			for (int i = 0; i < 5; i++) // Lista pagina destra
			{
				_Indexes[i + 5] = (HtmlGumpling)AddControl(
					new HtmlGumpling(this, 220, 45 + (i * 18), 160, 200, 0, 0, string.Empty),
					1);
			}

			// tithing Totali
			AddControl(new HtmlGumpling(this, 56, 155, 146, 106, 0, 0, string.Format(
				"<span style='font-family=ascii6;'>Tithing Points<br>Available: {0}", WorldModel.Entities.GetPlayerEntity().TithingPoints)),
				1);

			// Carico lista spell paladino (in quanto le ha già tutte di base vero?
			int totalSpells = 0;
			_SpellList.Clear();
			for (int spellIndex = 1; spellIndex <= 10; spellIndex++)
			{
					_SpellList.Add(new KeyValuePair<int, int>(1, spellIndex));
					totalSpells++;
			}

			int h = 0;

			List<int> presentspell = new List<int>();

			// Genero Indice spell
			foreach (KeyValuePair<int, int> spell in _SpellList)
			{
				_Indexes[h].Text += string.Format("<a href='page={1}' hovercolor='#800' style='font-family=ascii9; text-decoration=none;'>{0}</a><br/>",
					SpellsChivalry.GetSpell(spell.Value).Name, 2 + (h / 2));
				h++;
				presentspell.Add(spell.Value);
			}

			int y = 2;
			h = 0;
			bool right = false;

			// Genero pagine descrizione spell
			foreach (int id in presentspell)
			{
				CreateSpellPage(y, right, 1, SpellsChivalry.GetSpell(id));

				if (right) // Seleziono su quale pagina mettere la descrizione
					right = false;
				else
					right = true;

				// Conto le pagine necessarie 1 per ogni 2 spell
				if ((h % 2) == 1)
				{
					y++;
				}
				h++;
			}
			_MaxPage = y - 1;
		}

		void CreateNecromancerGumplings()
		{
			ClearControls();

			AddControl(new GumpPic(this, 0, 0, 0x2B00, 0)).MouseClickEvent += MinimizeBook; // spellbook background

			AddControl(_PageCornerLeft = new GumpPic(this, 50, 8, 0x08BB, 0)); // page turn left
			_PageCornerLeft.GumpLocalID = 0;
			_PageCornerLeft.MouseClickEvent += PageCorner_MouseClickEvent;
			_PageCornerLeft.MouseDoubleClickEvent += PageCorner_MouseDoubleClickEvent;
			_PageCornerLeft.Page = int.MaxValue; // Nascondo gira pagina di sinistra se appena aperto

			AddControl(_PageCornerRight = new GumpPic(this, 321, 8, 0x08BC, 0)); // page turn right
			_PageCornerRight.GumpLocalID = 1;
			_PageCornerRight.MouseClickEvent += PageCorner_MouseClickEvent;
			_PageCornerRight.MouseDoubleClickEvent += PageCorner_MouseDoubleClickEvent;

			// Titolo libro
			AddControl(new HtmlGumpling(this, 55, 15, 130, 200, 0, 0,
						string.Format("<span color='#004' style='font-family=uni0;'><center>INDEX</center></span>")),
						1);

			AddControl(new HtmlGumpling(this, 220, 15, 130, 200, 0, 0,
						string.Format("<span color='#004' style='font-family=uni0;'><center>INDEX</center></span>")),
						1);

			// creo spazio per lista spell
			_Indexes = new HtmlGumpling[17];
			for (int i = 0; i < 8; i++) // Lista pagina Sinistra
			{
					_Indexes[i] = (HtmlGumpling)AddControl(
						new HtmlGumpling(this, 60, 45 + (i*18), 130, 200, 0, 0, string.Empty),
						1);
			}

			for (int i = 0; i < 8; i++) // Lista pagina destra
			{
					_Indexes[i+8] = (HtmlGumpling)AddControl(
						new HtmlGumpling(this, 220, 45 + (i * 18), 130, 200, 0, 0, string.Empty),
						1);
			}

			// COntrollo le spell nel libro
			int totalSpells = 0;
			_SpellList.Clear();
			for (int spellIndex = 1; spellIndex <= 17; spellIndex++)
			{
				if (_Spellbook.HasSpell(1, spellIndex))
				{
					_SpellList.Add(new KeyValuePair<int, int>(1, spellIndex));
					totalSpells++;
				}
			}

			int h = 0;

			List<int> presentspell = new List<int>();

			// Genero Indice spell
			foreach (KeyValuePair<int, int> spell in _SpellList)
			{
				_Indexes[h].Text += string.Format("<a href='page={1}' hovercolor='#800' style='font-family=ascii9; text-decoration=none;'>{0}</a><br/>",
					SpellsNecromancer.GetSpell(spell.Value).Name, 2 + (h / 2));
				h++;
				presentspell.Add(spell.Value);
			}

			int y = 2;
			h = 0;
			bool right = false;

			// Genero pagine descrizione spell
			foreach (int id in presentspell)
			{
				CreateSpellPage(y, right, 1, SpellsNecromancer.GetSpell(id));

				if (right) // Seleziono su quale pagina mettere la descrizione
					right = false;
				else
					right = true;

				// Conto le pagine necessarie 1 per ogni 2 spell
				if ((h % 2) == 1)
				{
					y++;
				}
				h++;
			}
			_MaxPage = y - 1;
		}

		void CreateNinjitsuGumplings()
		{
			ClearControls();

			AddControl(new GumpPic(this, 0, 0, 0x2B06, 0)).MouseClickEvent += MinimizeBook; // spellbook background

			AddControl(_PageCornerLeft = new GumpPic(this, 50, 8, 0x08BB, 0)); // page turn left
			_PageCornerLeft.GumpLocalID = 0;
			_PageCornerLeft.MouseClickEvent += PageCorner_MouseClickEvent;
			_PageCornerLeft.MouseDoubleClickEvent += PageCorner_MouseDoubleClickEvent;
			_PageCornerLeft.Page = int.MaxValue; // Nascondo gira pagina di sinistra se appena aperto

			AddControl(_PageCornerRight = new GumpPic(this, 321, 8, 0x08BC, 0)); // page turn right
			_PageCornerRight.GumpLocalID = 1;
			_PageCornerRight.MouseClickEvent += PageCorner_MouseClickEvent;
			_PageCornerRight.MouseDoubleClickEvent += PageCorner_MouseDoubleClickEvent;

			// Titolo libro
			AddControl(new HtmlGumpling(this, 55, 15, 130, 200, 0, 0,
						string.Format("<span color='#004' style='font-family=uni0;'><center>INDEX</center></span>")),
						1);

			AddControl(new HtmlGumpling(this, 220, 15, 130, 200, 0, 0,
						string.Format("<span color='#004' style='font-family=uni0;'><center>INDEX</center></span>")),
						1);

			// creo spazio per lista spell
			_Indexes = new HtmlGumpling[8];
			for (int i = 0; i < 4; i++) // Lista pagina Sinistra
			{
				_Indexes[i] = (HtmlGumpling)AddControl(
					new HtmlGumpling(this, 60, 45 + (i * 18), 160, 200, 0, 0, string.Empty),
					1);
			}

			for (int i = 0; i < 4; i++) // Lista pagina destra
			{
				_Indexes[i + 4] = (HtmlGumpling)AddControl(
					new HtmlGumpling(this, 220, 45 + (i * 18), 160, 200, 0, 0, string.Empty),
					1);
			}

			// Carico lista spell Ninjitsu inutile ? in quanto le ha già tutte di base vero?
			int totalSpells = 0;
			_SpellList.Clear();
			for (int spellIndex = 1; spellIndex <= 8; spellIndex++)
			{
				_SpellList.Add(new KeyValuePair<int, int>(1, spellIndex));
				totalSpells++;
			}

			int h = 0;

			List<int> presentspell = new List<int>();

			// Genero Indice spell
			foreach (KeyValuePair<int, int> spell in _SpellList)
			{
				_Indexes[h].Text += string.Format("<a href='page={1}' hovercolor='#800' style='font-family=ascii9; text-decoration=none;'>{0}</a><br/>",
					SpellsNinjitsu.GetSpell(spell.Value).Name, 2 + (h / 2));
				h++;
				presentspell.Add(spell.Value);
			}

			int y = 2;
			h = 0;
			bool right = false;

			// Genero pagine descrizione spell
			foreach (int id in presentspell)
			{
				CreateSpellPage(y, right, 1, SpellsNinjitsu.GetSpell(id));

				if (right) // Seleziono su quale pagina mettere la descrizione
					right = false;
				else
					right = true;

				// Conto le pagine necessarie 1 per ogni 2 spell
				if ((h % 2) == 1)
				{
					y++;
				}
				h++;
			}
			_MaxPage = y - 1;
		}
		void CreateSpellweavingGumplings()
		{
			ClearControls();

			AddControl(new GumpPic(this, 0, 0, 0x2B2F, 0)).MouseClickEvent += MinimizeBook; // spellbook background

			AddControl(_PageCornerLeft = new GumpPic(this, 50, 8, 0x08BB, 0)); // page turn left
			_PageCornerLeft.GumpLocalID = 0;
			_PageCornerLeft.MouseClickEvent += PageCorner_MouseClickEvent;
			_PageCornerLeft.MouseDoubleClickEvent += PageCorner_MouseDoubleClickEvent;
			_PageCornerLeft.Page = int.MaxValue; // Nascondo gira pagina di sinistra se appena aperto

			AddControl(_PageCornerRight = new GumpPic(this, 321, 8, 0x08BC, 0)); // page turn right
			_PageCornerRight.GumpLocalID = 1;
			_PageCornerRight.MouseClickEvent += PageCorner_MouseClickEvent;
			_PageCornerRight.MouseDoubleClickEvent += PageCorner_MouseDoubleClickEvent;

			// Titolo libro
			AddControl(new HtmlGumpling(this, 55, 15, 130, 200, 0, 0,
						string.Format("<span color='#004' style='font-family=uni0;'><center>INDEX</center></span>")),
						1);

			AddControl(new HtmlGumpling(this, 220, 15, 130, 200, 0, 0,
						string.Format("<span color='#004' style='font-family=uni0;'><center>INDEX</center></span>")),
						1);

			// creo spazio per lista spell
			_Indexes = new HtmlGumpling[16];
			for (int i = 0; i < 7; i++) // Lista pagina Sinistra
			{
				_Indexes[i] = (HtmlGumpling)AddControl(
					new HtmlGumpling(this, 60, 45 + (i * 18), 160, 200, 0, 0, string.Empty),
					1);
			}

			for (int i = 0; i < 7; i++) // Lista pagina destra
			{
				_Indexes[i + 7] = (HtmlGumpling)AddControl(
					new HtmlGumpling(this, 220, 45 + (i * 18), 160, 200, 0, 0, string.Empty),
					1);
			}

			// Carico lista spell Spellweaving presenti nel libro
			int totalSpells = 0;
			_SpellList.Clear();
			for (int spellIndex = 1; spellIndex <= 16; spellIndex++)
			{
				if (_Spellbook.HasSpell(1, spellIndex))
				{
					_SpellList.Add(new KeyValuePair<int, int>(1, spellIndex));
					totalSpells++;
				}
			}

			int h = 0;

			List<int> presentspell = new List<int>();

			// Genero Indice spell
			foreach (KeyValuePair<int, int> spell in _SpellList)
			{
				_Indexes[h].Text += string.Format("<a href='page={1}' hovercolor='#800' style='font-family=ascii9; text-decoration=none;'>{0}</a><br/>",
					SpellsSpellweaving.GetSpell(spell.Value).Name, 2 + (h / 2));
				h++;
				presentspell.Add(spell.Value);
			}

			int y = 2;
			h = 0;
			bool right = false;

			// Genero pagine descrizione spell
			foreach (int id in presentspell)
			{
				CreateSpellPage(y, right, 1, SpellsSpellweaving.GetSpell(id));

				if (right) // Seleziono su quale pagina mettere la descrizione
					right = false;
				else
					right = true;

				// Conto le pagine necessarie 1 per ogni 2 spell
				if ((h % 2) == 1)
				{
					y++;
				}
				h++;
			}
			_MaxPage = y - 1;
		}
		
		void CreateMageryGumplings()
        {
            ClearControls();

            AddControl(new GumpPic(this, 0, 0, 0x08AC, 0)).MouseClickEvent += MinimizeBook; // spellbook background

			AddControl(_PageCornerLeft = new GumpPic(this, 50, 8, 0x08BB, 0)); // page turn left
            _PageCornerLeft.GumpLocalID = 0;
            _PageCornerLeft.MouseClickEvent += PageCorner_MouseClickEvent;
            _PageCornerLeft.MouseDoubleClickEvent += PageCorner_MouseDoubleClickEvent;
			_PageCornerLeft.Page = int.MaxValue; // Nascondo gira pagina di sinistra se appena aperto

			AddControl(_PageCornerRight = new GumpPic(this, 321, 8, 0x08BC, 0)); // page turn right
            _PageCornerRight.GumpLocalID = 1;
            _PageCornerRight.MouseClickEvent += PageCorner_MouseClickEvent;
            _PageCornerRight.MouseDoubleClickEvent += PageCorner_MouseDoubleClickEvent;

            for (int i = 0; i < 4; i++) // spell circles 1 - 4
            {
                AddControl(new GumpPic(this, 60 + i * 35, 174, 0x08B1 + i, 0));
                LastControl.GumpLocalID = i;
                LastControl.MouseClickEvent += SpellCircle_MouseClickEvent; // unsubscribe from these?
            }
            for (int i = 0; i < 4; i++) // spell circles 5 - 8
            {
                AddControl(new GumpPic(this, 226 + i * 34, 174, 0x08B5 + i, 0));
                LastControl.GumpLocalID = i + 4;
                LastControl.MouseClickEvent += SpellCircle_MouseClickEvent; // unsubscribe from these?
            }

            // indexes are on pages 1 - 4. Spells are on pages 5+.
            _CircleHeaders = new HtmlGumpling[8];
            for (int i = 0; i < 8; i++)
            {
                _CircleHeaders[i] = (HtmlGumpling)AddControl(
                    new HtmlGumpling(this, 64 + (i % 2) * 148, 10, 130, 200, 0, 0,
                        string.Format("<span color='#004' style='font-family=uni0;'><center>{0}</center></span>", SpellsMagery.CircleNames[i])),
                        1 + (i / 2));
            }
            _Indexes = new HtmlGumpling[8];
            for (int i = 0; i < 8; i++)
            {
                _Indexes[i] = (HtmlGumpling)AddControl(
                    new HtmlGumpling(this, 64 + (i % 2) * 156, 28, 130, 200, 0, 0, string.Empty),
                    1 + (i / 2));
            }

            _MaxPage = 4;

            // Begin checking which spells are in the spellbook and add them to _Spells list

            int totalSpells = 0;
            _SpellList.Clear();
            for (int spellCircle = 0; spellCircle < 8; spellCircle++)
            {
                for (int spellIndex = 1; spellIndex <= 8; spellIndex++)
                {
                    if (_Spellbook.HasSpell(spellCircle, spellIndex))
                    {
                        _SpellList.Add(new KeyValuePair<int, int>(spellCircle, spellIndex));
                        totalSpells++;
                    }
                }
            }

            _MaxPage = _MaxPage + ((totalSpells + 1) / 2); // The number of additional spell info pages needed

            SetActivePage(1);
        }

        void CreateSpellPage(int page, bool rightPage, int circle, SpellDefinition spell)
        {
			if (_Spellbook.BookType == SpellBookTypes.Necromancer)  // Necro
			{
				// Nome spell
				AddControl(new HtmlGumpling(this, 84 + (rightPage ? 130 : 0), 10, 130, 200, 0, 0,
					string.Format("<span style='font-family=ascii6;'>{0}</span>", spell.Name)),
					page);

				// Icona e PowerWords
				AddControl(new HtmlGumpling(this, 56 + (rightPage ? 156 : 0), 38, 130, 44, 0, 0,
					string.Format("<a href='spellicon={0}'><gumpimg src='{1}'/></a>",
					spell.ID, spell.GumpIconID)),
					page).SetTooltip(_Provider.GetString(_NecromancerClilocStart + (spell.ID - 101)));
				AddControl(new HtmlGumpling(this, 104 + (rightPage ? 156 : 0), 38, 88, 40, 0, 0,
					string.Format("<span color='#542' style='font-family=uni0;'>{0}</span>", spell.PowerWords)), page);

				// Divisore
				AddControl(new GumpPicTiled(this, 56 + (rightPage ? 156 : 0), 88, 130, 4, 0x238D), page);


				// Reagenti
				AddControl(new HtmlGumpling(this, 56 + (rightPage ? 156 : 0), 94, 146, 106, 0, 0, string.Format(
					"<span color='#004' style='font-family=uni0;'>Reagents:</span>")),
					page);

				for (int i = 0; i < spell.Regs.Length; i++)
				{
					AddControl(new HtmlGumpling(this, 56 + (rightPage ? 156 : 0), 114 + (i*13), 146, 106, 0, 0, string.Format(
					"<span color ='#542' style ='font-family=ascii9;' >{0}</span> ", spell.Regs[i])),
					page);
				}

				//  Mana e Skill
				AddControl(new HtmlGumpling(this, 56 + (rightPage ? 156 : 0), 155, 146, 106, 0, 0, string.Format(
					"<span style='font-family=ascii6;'>Mana Cost:{0}<br>Min. Skill:{1}</span><br/>", spell.ManaCost, spell.MinSkill)),
					page);
			}
			else if (_Spellbook.BookType == SpellBookTypes.Mysticism)  // Mysticism
			{
				// Nome spell
				AddControl(new HtmlGumpling(this, 84 + (rightPage ? 130 : 0), 10, 130, 200, 0, 0,
					string.Format("<span style='font-family=ascii6;'>{0}</span>", spell.Name)),
					page);

				// Icona e PowerWords
				AddControl(new HtmlGumpling(this, 56 + (rightPage ? 156 : 0), 38, 130, 44, 0, 0,
					string.Format("<a href='spellicon={0}'><gumpimg src='{1}'/></a>",
					spell.ID, spell.GumpIconID)),
					page).SetTooltip(_Provider.GetString(_MysticismClilocStart + (spell.ID - 678)));
				AddControl(new HtmlGumpling(this, 104 + (rightPage ? 156 : 0), 38, 88, 40, 0, 0,
					string.Format("<span color='#542' style='font-family=uni0;'>{0}</span>", spell.PowerWords)), page);

				// Divisore
				AddControl(new GumpPicTiled(this, 56 + (rightPage ? 156 : 0), 88, 130, 4, 0x238D), page);


				// Reagenti
				AddControl(new HtmlGumpling(this, 56 + (rightPage ? 156 : 0), 94, 146, 106, 0, 0, string.Format(
					"<span color='#004' style='font-family=uni0;'>Reagents:</span>")),
					page);

				for (int i = 0; i < spell.Regs.Length; i++)
				{
					AddControl(new HtmlGumpling(this, 56 + (rightPage ? 156 : 0), 114 + (i * 13), 146, 106, 0, 0, string.Format(
					"<span color ='#542' style ='font-family=ascii9;' >{0}</span> ", spell.Regs[i])),
					page);
				}

				//  Mana e Skill
				AddControl(new HtmlGumpling(this, 56 + (rightPage ? 156 : 0), 165, 146, 106, 0, 0, string.Format(
					"<span style='font-family=ascii6;'>Mana Cost:{0}<br>Min. Skill:{1}</span><br/>", spell.ManaCost, spell.MinSkill)),
					page);
			}
			else if (_Spellbook.BookType == SpellBookTypes.Spellweaving)  // Spellweaving
			{
				// Nome spell
				AddControl(new HtmlGumpling(this, 84 + (rightPage ? 130 : 0), 10, 130, 200, 0, 0,
					string.Format("<span style='font-family=ascii6;'>{0}</span>", spell.Name)),
					page);

				// Icona e PowerWords
				AddControl(new HtmlGumpling(this, 56 + (rightPage ? 156 : 0), 38, 130, 44, 0, 0,
					string.Format("<a href='spellicon={0}'><gumpimg src='{1}'/></a>",
					spell.ID, spell.GumpIconID)),
					page).SetTooltip(_Provider.GetString(_SpellweavingClilocStart + (spell.ID - 601)));
				AddControl(new HtmlGumpling(this, 104 + (rightPage ? 156 : 0), 38, 88, 40, 0, 0,
					string.Format("<span color='#542' style='font-family=uni0;'>{0}</span>", spell.PowerWords)), page);

				//  Mana e Skill
				AddControl(new HtmlGumpling(this, 56 + (rightPage ? 156 : 0), 155, 146, 106, 0, 0, string.Format(
					"<span style='font-family=ascii6;'>Mana Cost:{0}<br>Min. Skill:{1}</span><br/>", spell.ManaCost, spell.MinSkill)),
					page);
			}

			else if (_Spellbook.BookType == SpellBookTypes.Bushido || _Spellbook.BookType == SpellBookTypes.Ninjitsu)  // Bushido e Ninjitsu
			{
				// Nome spell
				AddControl(new HtmlGumpling(this, 84 + (rightPage ? 130 : 0), 10, 100, 200, 0, 0,
					string.Format("<span style='font-family=ascii6;'>{0}</span>", spell.Name)),
					page);

				// Icona 
				if (_Spellbook.BookType == SpellBookTypes.Bushido)
				{
					AddControl(new HtmlGumpling(this, 56 + (rightPage ? 156 : 0), 52, 130, 70, 0, 0,
					string.Format("<a href='spellicon={0}'><gumpimg src='{1}'/></a>",
					spell.ID, (spell.GumpIconID - 0x20))), // Cambio ID icona per modella grande
					page).SetTooltip(_Provider.GetString(_BushidoClilocStart + (spell.ID - 401)));
				}
				else
				{
					AddControl(new HtmlGumpling(this, 56 + (rightPage ? 156 : 0), 52, 130, 70, 0, 0,
					string.Format("<a href='spellicon={0}'><gumpimg src='{1}'/></a>",
					spell.ID, (spell.GumpIconID - 0x20))), // Cambio ID icona per modella grande
					page).SetTooltip(_Provider.GetString(_NinjitsuClilocStart + (spell.ID - 501)));
				}

				//  Mana e Skill
				AddControl(new HtmlGumpling(this, 56 + (rightPage ? 156 : 0), 155, 146, 106, 0, 0, string.Format(
					"<span style='font-family=ascii6;'>Mana Cost:{0}<br>Min. Skill:{1}</span><br/>", spell.ManaCost, spell.MinSkill)),
					page);
			}

			else if (_Spellbook.BookType == SpellBookTypes.Chivalry)  // Chivalry
			{
				// Nome spell
				AddControl(new HtmlGumpling(this, 84 + (rightPage ? 130 : 0), 10, 130, 200, 0, 0,
					string.Format("<span style='font-family=ascii6;'>{0}</span>", spell.Name)),
					page);

				// Icona e PowerWords
				AddControl(new HtmlGumpling(this, 56 + (rightPage ? 156 : 0), 38, 130, 44, 0, 0,
					string.Format("<a href='spellicon={0}'><gumpimg src='{1}'/></a>",
					spell.ID, spell.GumpIconID)),
					page).SetTooltip(_Provider.GetString(_ChivalryClilocStart + (spell.ID - 201)));
				AddControl(new HtmlGumpling(this, 104 + (rightPage ? 156 : 0), 38, 88, 40, 0, 0,
					string.Format("<span color='#542' style='font-family=uni0;'>{0}</span>", spell.PowerWords)), page);
				 
				//  Mana e Skill e tithing
				AddControl(new HtmlGumpling(this, 56 + (rightPage ? 156 : 0), 135, 146, 106, 0, 0, string.Format(
					"<span style='font-family=ascii6;'>Tithing Cost: {2}<br>Mana Cost: {0}<br>Min. Skill: {1}</span><br/>", spell.ManaCost, spell.MinSkill, spell.TithingCost)),
					page);
			}

			else // Magery
			{
				// header: "NTH CIRCLE"
				AddControl(new HtmlGumpling(this, 64 + (rightPage ? 148 : 0), 10, 130, 200, 0, 0,
					string.Format("<span color='#004' style='font-family=uni0;'><center>{0}</center></span>", SpellsMagery.CircleNames[circle])),
					page);
				// icon and spell name
				AddControl(new HtmlGumpling(this, 56 + (rightPage ? 156 : 0), 38, 130, 44, 0, 0,
					string.Format("<a href='spellicon={0}'><gumpimg src='{1}'/></a>",
					spell.ID, spell.GumpIconID - 0x1298)),
					page).SetTooltip(_Provider.GetString(_MageClilocStart + (spell.ID -1)));
				AddControl(new HtmlGumpling(this, 104 + (rightPage ? 156 : 0), 38, 88, 40, 0, 0, string.Format(
					"<a href='spell={0}' color='#542' hovercolor='#875' activecolor='#420' style='font-family=uni0; text-decoration=none;'>{1}</a>",
					spell.ID, spell.Name)),
					page);

				// Divisore
				AddControl(new GumpPicTiled(this, 56 + (rightPage ? 156 : 0), 88, 130, 4, 0x23BF), page);

				// Reagenti
				AddControl(new HtmlGumpling(this, 56 + (rightPage ? 156 : 0), 94, 146, 106, 0, 0, string.Format(
					"<span color='#004' style='font-family=uni0;'>Reagents:</span>")),
					page);

				for (int i = 0; i < spell.Regs.Length; i++)
				{
					AddControl(new HtmlGumpling(this, 56 + (rightPage ? 156 : 0), 114 + (i * 13), 146, 106, 0, 0, string.Format(
					"<span color ='#542' style ='font-family=ascii9;' >{0}</span> ", spell.Regs[i])),
					page);
				}
			}
        }

		public override void OnHtmlInputEvent(string href, MouseEvent e)
        {
            if (e == MouseEvent.DoubleClick)
            {
                string[] hrefs = href.Split('=');
				if (hrefs.Length != 2)
					return;
				else if (hrefs[0] == "page")
				{
					int page;
					if (int.TryParse(hrefs[1], out page))
						_World.Interaction.CastSpell(page - 4);
				}
				else if (hrefs[0] == "spell")
				{
					int spell;
					if (int.TryParse(hrefs[1], out spell))
						_World.Interaction.CastSpell(spell);
				}
				else if (hrefs[0] == "spellicon")
				{
					int spell;
					if (int.TryParse(hrefs[1], out spell))
					{
						_World.Interaction.CastSpell(spell);
					}
				}
            }
            else if (e == MouseEvent.Click)
            {
                string[] hrefs = href.Split('=');
                if (hrefs.Length != 2)
                    return;
                if (hrefs[0] == "page")
                {
					int page;
					if (int.TryParse(hrefs[1], out page))
						SetActivePage(page);
				
				}
            }
            else if (e == MouseEvent.DragBegin)
            {
                string[] hrefs = href.Split('=');
                if (hrefs.Length != 2)
                    return;

				if (hrefs[0] == "spellicon")
                {
					int spellIndex;
                    if (!int.TryParse(hrefs[1], out spellIndex))
                        return;

					SpellDefinition spell = SpellDefinition.EmptySpell;

					switch (_Spellbook.BookType)
					{
						case SpellBookTypes.Mysticism:
							spell = SpellsMysticism.GetSpell(spellIndex - 677); // index +677 per packet cast
							break;
						case SpellBookTypes.Spellweaving:
							spell = SpellsSpellweaving.GetSpell(spellIndex -600); // index +600 per packet cast
							break;
						case SpellBookTypes.Ninjitsu:
							spell = SpellsNinjitsu.GetSpell(spellIndex - 500); // index +500 per packet cast
							break;
						case SpellBookTypes.Bushido:
							spell = SpellsBushido.GetSpell(spellIndex - 400); // index +400 per packet cast
							break;
						case SpellBookTypes.Chivalry:
							spell = SpellsChivalry.GetSpell(spellIndex - 200); // index +200 per packet cast
							break;
						case SpellBookTypes.Necromancer:
							spell = SpellsNecromancer.GetSpell(spellIndex-100); // index +100 per packet cast
							break;
						default:
							spell = SpellsMagery.GetSpell(spellIndex);
							break;
					}

					if (spell.ID == spellIndex)
                    {
						Core.Diagnostics.Tracing.Tracer.Warn("- Manca salvataggio posizione");
						InputManager input = Service.Get<InputManager>();
                        UseSpellButtonGump gump = new UseSpellButtonGump(spell);
                        UserInterface.AddControl(gump, input.MousePosition.X - 22, input.MousePosition.Y - 22);
                        UserInterface.AttemptDragControl(gump, input.MousePosition, true);
                    }
                }
            }
        }

        void SpellCircle_MouseClickEvent(AControl sender, int x, int y, MouseButton button)
        {
            if (button != MouseButton.Left)
                return;
            SetActivePage(sender.GumpLocalID / 2 + 1);
        }

        void PageCorner_MouseClickEvent(AControl sender, int x, int y, MouseButton button)
        {
            if (button != MouseButton.Left)
                return;

            if (sender.GumpLocalID == 0)
            {
                SetActivePage(ActivePage - 1);
            }
            else
            {
                SetActivePage(ActivePage + 1);
            }
        }

        void PageCorner_MouseDoubleClickEvent(AControl sender, int x, int y, MouseButton button)
        {
            if (button != MouseButton.Left)
                return;

            if (sender.GumpLocalID == 0)
            {
                SetActivePage(1);
            }
            else
            {
                SetActivePage(_MaxPage);
            }
        }

        void SetActivePage(int page)
        {
			if (_Spellbook.BookType == SpellBookTypes.Unknown)
				return;

			if (page < 1)
				page = 1;
			if (page > _MaxPage)
				page = _MaxPage;

			if (_Spellbook.BookType == SpellBookTypes.Magic) // magery
			{
				int currentPage = page;
				int currentSpellCircle = currentPage * 2 - 2; // chooses the right spell circle to print on index page
				int currentSpellInfoIndex = currentPage * 2 - 10; // keeps track of which spell info page to print
				for (int currentCol = 0; currentCol < 2; currentCol++)
				{
					bool isRightPage = (currentCol + 1 == 2);
					currentSpellInfoIndex += currentCol;

					// Create Spell Index page
					if (currentPage <= 4)
					{
						_Indexes[currentSpellCircle].Text = "";
						foreach (KeyValuePair<int, int> spell in _SpellList)
						{
							if (spell.Key == currentSpellCircle)
							{
								int currentSpellInfoPage = _SpellList.IndexOf(spell) / 2;
								_Indexes[currentSpellCircle].Text += string.Format("<a href='page={1}' hovercolor='#800' activecolor='#611' style='font-family=ascii9; text-decoration=none;'>{0}</a><br/>",
									SpellsMagery.GetSpell(currentSpellCircle * 8 + spell.Value).Name,
									5 + currentSpellInfoPage);
							}
						}
						currentSpellCircle++;
					}
					else
					{
						// Create Spell Info Page
						if (currentSpellInfoIndex < _SpellList.Count)
						{
							CreateSpellPage(page, isRightPage, _SpellList[currentSpellInfoIndex].Key, SpellsMagery.GetSpell(_SpellList[currentSpellInfoIndex].Key * 8 + _SpellList[currentSpellInfoIndex].Value));
						}
					}
				}
				ActivePage = page;
			}
			else
				ActivePage = page;

			// hide the page corners if we're at the first or final page.
			_PageCornerLeft.Page = (ActivePage != 1) ? 0 : int.MaxValue;
			_PageCornerRight.Page = (ActivePage != _MaxPage) ? 0 : int.MaxValue;

		}

		private void MinimizeBook(AControl sender, int x, int y, MouseButton button)
		{
			if (x >= 3 && x <= 19 && y >= 101 && y <= 120) // Area del resize
			{
				UserInterface.AddControl(new SpellbookMiniGump(_Spellbook), this.X, this.Y);
				this.Dispose(); // Rimuovo libro e creo libro piccolo
			}
		}
	}
}
