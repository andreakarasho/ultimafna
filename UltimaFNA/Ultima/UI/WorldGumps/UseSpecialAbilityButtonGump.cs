﻿#region usings
using Microsoft.Xna.Framework;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.Data;
using UltimaXNA.Ultima.UI.Controls;
using UltimaXNA.Ultima.World;
using UltimaXNA.Ultima.World.Entities.Mobiles;
using UltimaXNA.Ultima.World.Entities.Items;
using UltimaXNA.Core.Resources;
#endregion

namespace UltimaXNA.Ultima.UI.WorldGumps
{
    public class UseSpecialAbilityButtonGump : Gump
    {
        // private variables
        private int _Primary;
		private bool _AbilityEnabled = false;
        private GumpPic _AbilityButton;
		private SpecialAbilityDefinition _Ability;
		private IResourceProvider provider = Service.Get<IResourceProvider>();
		private Mobile _Mobile = WorldModel.Entities.GetPlayerEntity();
		private double _RefreshTime;
		private readonly WorldModel _World;

		public bool AbilityEnabled { get { return _AbilityEnabled; } set { _AbilityEnabled = value; } }

		public UseSpecialAbilityButtonGump(int primary)  // 0 for primary 1 for secondary
            : base(primary, 0)
        {
            while (UserInterface.GetControl<UseSpecialAbilityButtonGump>(primary) != null)
            {
                UserInterface.GetControl<UseSpecialAbilityButtonGump>(primary).Dispose();
            }

			_Primary = primary;
			_World = Service.Get<WorldModel>();

			IsMoveable = true;
            HandlesMouseInput = true;

			_AbilityButton = new GumpPic(this, 0, 0, 0x5200, 0);
			UpdateAbilityIcon(primary);
			AddControl(_AbilityButton);
			_AbilityButton.HandlesMouseInput = true;
			_AbilityButton.MouseDoubleClickEvent += EventMouseDoubleClick;
        }

		private void UpdateAbilityIcon(int primary)
		{
			Item itemslot1 = (_Mobile).GetItem(1);
			Item itemslot2 = (_Mobile).GetItem(2);

			if (primary == 0) // 0 for primary 1 for secondary
			{
				if (itemslot1 != null)
				{
					_Ability = SpecialAbility.GetWeaponPrimaryAbility(itemslot1.ItemID);
				}
				else
				{
					if (itemslot2 != null)
					{
						_Ability = SpecialAbility.GetWeaponPrimaryAbility(itemslot2.ItemID);
					}
					else
					{
						_Ability = SpecialAbility.GetWeaponPrimaryAbility(0x0001);
					}
				}
			}
			else
			{
				if (itemslot1 != null)
				{
					_Ability = SpecialAbility.GetWeaponSecondaryAbility(itemslot1.ItemID);
				}
				else
				{
					if (itemslot2 != null)
					{
						_Ability = SpecialAbility.GetWeaponSecondaryAbility(itemslot2.ItemID);
					}
					else
					{
						_Ability = SpecialAbility.GetWeaponSecondaryAbility(0x0001);
					}
				}
			}

			_AbilityButton.GumpID = _Ability.GumpIconID;
			_AbilityButton.SetTooltip(provider.GetString(_Ability.NameClilocID));

			if (_AbilityEnabled)
				_AbilityButton.Hue = 0x26;
			else
				_AbilityButton.Hue = 0x0;

		}

		public override void Dispose()
        {
			_AbilityButton.MouseDoubleClickEvent -= EventMouseDoubleClick;
            base.Dispose();
        }

		public override void Update(double totalMS, double frameMS)
		{
			if (_RefreshTime + 0.5d < totalMS) //need to update
			{
				_RefreshTime = totalMS;
				UpdateAbilityIcon(_Primary);
			}
			base.Update(totalMS, frameMS);
		}

		public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
        {
            base.Draw(spriteBatch, position, frameMS);
        }

        private void EventMouseDoubleClick(AControl sender, int x, int y, MouseButton button)
        {
            if (button != MouseButton.Left)
                return;

			if (_AbilityEnabled) // Se bottone già attivo disattivo e esco
			{
				_AbilityEnabled = false;
				_World.Interaction.UseAbility(0);
				return;
			}

			// Disattivazione primaria o secondaria in caso sia già attiva
			if (_Primary == 0)
			{
				if (UserInterface.GetControl<UseSpecialAbilityButtonGump>(1) != null)
				{
					if (UserInterface.GetControl<UseSpecialAbilityButtonGump>(1).AbilityEnabled)
					{
						UserInterface.GetControl<UseSpecialAbilityButtonGump>(1).AbilityEnabled = false;
					}
				}

			}
			else
			{
				if (UserInterface.GetControl<UseSpecialAbilityButtonGump>(0) != null)
				{
					if (UserInterface.GetControl<UseSpecialAbilityButtonGump>(0).AbilityEnabled)
					{
						UserInterface.GetControl<UseSpecialAbilityButtonGump>(0).AbilityEnabled = false;
					}
				}
			}

			// abilito abilità
			_AbilityEnabled = true;
			_World.Interaction.UseAbility(_Ability.ID);
		}
    }
}