/***************************************************************************
 *   StatusGump.cs
 *   Based on code by surcouf94
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using Microsoft.Xna.Framework;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.UI.Controls;
using UltimaXNA.Ultima.World;
using UltimaXNA.Ultima.World.Entities.Mobiles;
using UltimaXNA.Core.Network;
using UltimaXNA.Ultima.Network.Client;
using UltimaXNA.Ultima.Player;
using System.Collections.Generic;
using UltimaFNA.Core.Network.SocketAsync;
#endregion

namespace UltimaXNA.Ultima.UI.WorldGumps
{
    class StatusGump : Gump
    {
        public static void Toggle(Serial serial)
        {
            UserInterfaceService ui = Service.Get<UserInterfaceService>();
            if (ui.GetControl<StatusGump>() == null)
            {
                NetworkClient client = Service.Get<NetworkClient>();
                client.Send(new MobileQueryPacket(MobileQueryPacket.StatusType.BasicStatus, serial));
                ui.AddControl(new StatusGump(), 200, 400);
            }
            else
                ui.RemoveControl<StatusGump>();
        }

        internal Mobile _Mobile = WorldModel.Entities.GetPlayerEntity();
        double _RefreshTime;

        private TextLabelAscii[] _Labels = new TextLabelAscii[(int)MobileStats.Max];
		private HtmlGumpling _Name;
		private Button _Buff, _Str, _Dex, _Int;

		private enum StatID
		{
			Buff,
			Str,
			Dex,
			Int
		}

		private enum MobileStats
        {
            Name,
            Strength,
            Dexterity,
            Intelligence,
            HealthCurrent,
            HealthMax,
            StaminaCurrent,
            StaminaMax,
            ManaCurrent,
            ManaMax,
			Damage,
			Followers,
            WeightCurrent,
            WeightMax,
            StatCap,
            Luck,
            Gold,
            AR,
            RF,
            RC,
            RP,
            RE,
			WeaponDamageIncrease,
			SwingSpeedIncrease,
			LowReagentCost,
			SpellDamageIncrease,
			FasterCasting,
			FasterCastRecovery,
			Sex,
			HitChanceIncrease,
			DefenseChanceIncrease,
			LowerManaCost,
			Max
        }

        public StatusGump()
            : base(0, 0)
        {
            IsMoveable = true;
			SaveOnWorldStop = true;


			if (PlayerState.ClientFeatures.AOS)
            {
				AddControl(new GumpPic(this, 0, 0, 0x2A6C, 0)).MouseClickEvent += ResizeStatus;

				_Name = (HtmlGumpling)AddControl(new HtmlGumpling(this, 54, 50, 484, 20, 0, 0, string.Format(
				"<center><span color='#000' style ='font-family=ascii9;'>{0}", _Mobile.Name)));

                _Labels[(int)MobileStats.Strength] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 80, 76, 1, 997, _Mobile.Strength.ToString()));
                _Labels[(int)MobileStats.Dexterity] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 80, 104, 1, 997, _Mobile.Dexterity.ToString()));
                _Labels[(int)MobileStats.Intelligence] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 80, 132, 1, 997, _Mobile.Intelligence.ToString()));

                _Labels[(int)MobileStats.HealthCurrent] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 160, 71, 1, 997, _Mobile.Health.Current.ToString()));
				AddControl(new GumpPicTiled(this, 150, 82, 44, 1, 0x1412));// Divisore
				_Labels[(int)MobileStats.HealthMax] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 160, 82, 1, 997, _Mobile.Health.Max.ToString()));

                _Labels[(int)MobileStats.StaminaCurrent] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 160, 99, 1, 997, _Mobile.Stamina.Current.ToString()));
				AddControl(new GumpPicTiled(this, 150, 110, 44, 1, 0x1412));// Divisore
				_Labels[(int)MobileStats.StaminaMax] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 160, 110, 1, 997, _Mobile.Stamina.Max.ToString()));

                _Labels[(int)MobileStats.ManaCurrent] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 160, 127, 1, 997, _Mobile.Mana.Current.ToString()));
				AddControl(new GumpPicTiled(this, 150, 138, 44, 1, 0x1412));// Divisore
				_Labels[(int)MobileStats.ManaMax] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 160, 138, 1, 997, _Mobile.Mana.Max.ToString()));

				_Labels[(int)MobileStats.Damage] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 315, 75, 1, 997, ConcatCurrentMax(_Mobile.DamageMin, _Mobile.DamageMax)));
				_Labels[(int)MobileStats.Followers] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 317, 132, 1, 997, ConcatCurrentMax(_Mobile.Followers.Current, _Mobile.Followers.Max)));

				_Labels[(int)MobileStats.WeightCurrent] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 242, 127, 1, 997, _Mobile.Weight.Current.ToString()));
				AddControl(new GumpPicTiled(this, 234, 138, 40, 1, 0x1412));// Divisore
				_Labels[(int)MobileStats.WeightMax] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 242, 138, 1, 997, _Mobile.Weight.Max.ToString()));

                _Labels[(int)MobileStats.StatCap] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 242, 74, 1, 997, _Mobile.StatCap.ToString()));
                _Labels[(int)MobileStats.Luck] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 242, 102, 1, 997, _Mobile.Luck.ToString()));
				
				_Labels[(int)MobileStats.Gold] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 475, 161, 1, 997, _Mobile.Gold.ToString()));

				_Labels[(int)MobileStats.AR] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 475, 73, 1, 997, ConcatCurrentMax(_Mobile.ArmorRating, _Mobile.MaxPhysicRestistance)));
                _Labels[(int)MobileStats.RF] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 475, 90, 1, 997, ConcatCurrentMax(_Mobile.ResistFire, _Mobile.MaxFireResistance)));
                _Labels[(int)MobileStats.RC] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 475, 105, 1, 997, ConcatCurrentMax(_Mobile.ResistCold, _Mobile.MaxColdResistance)));
                _Labels[(int)MobileStats.RP] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 475, 119, 1, 997, ConcatCurrentMax(_Mobile.ResistPoison, _Mobile.MaxPoisonResistance)));
                _Labels[(int)MobileStats.RE] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 475, 135, 1, 997, ConcatCurrentMax(_Mobile.ResistEnergy, _Mobile.MaxEnergyResistance)));

				_Labels[(int)MobileStats.HitChanceIncrease] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 80, 161, 1, 997, _Mobile.HitChanceIncrease.ToString()));
				_Labels[(int)MobileStats.DefenseChanceIncrease] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 150, 161, 1, 997, ConcatCurrentMax(_Mobile.DefenseChanceIncrease, 45)));

				_Labels[(int)MobileStats.LowerManaCost] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 232, 161, 1, 997, _Mobile.LowerManaCost.ToString()));

				_Labels[(int)MobileStats.WeaponDamageIncrease] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 317, 102, 1, 997, _Mobile.WeaponDamageIncrease.ToString()));
				_Labels[(int)MobileStats.SwingSpeedIncrease] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 317, 161, 1, 997, _Mobile.SwingSpeedIncrease.ToString()));

				_Labels[(int)MobileStats.LowReagentCost] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 400, 74, 1, 997, _Mobile.LowReagentCost.ToString()));
				_Labels[(int)MobileStats.SpellDamageIncrease] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 400, 102, 1, 997, _Mobile.SpellDamageIncrease.ToString()));
				_Labels[(int)MobileStats.FasterCasting] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 400, 132, 1, 997, _Mobile.FasterCasting.ToString()));
				_Labels[(int)MobileStats.FasterCastRecovery] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 400, 161, 1, 997, _Mobile.FasterCastRecovery.ToString()));

				// Bottoni
				_Buff = (Button)AddControl(new Button(this, 41, 49, 0x7538, 0x7538, ButtonTypes.Activate, 0, (int)StatID.Buff));
				_Str = (Button)AddControl(new Button(this, 27, GetStatGumpYCorrection(StatID.Str), GetStatGumpStatus(StatID.Str), GetStatGumpStatus(StatID.Str), ButtonTypes.Activate, 0, (int)StatID.Str));
				_Dex = (Button)AddControl(new Button(this, 27, GetStatGumpYCorrection(StatID.Dex), GetStatGumpStatus(StatID.Dex), GetStatGumpStatus(StatID.Dex), ButtonTypes.Activate, 0, (int)StatID.Dex));
				_Int = (Button)AddControl(new Button(this, 27, GetStatGumpYCorrection(StatID.Int), GetStatGumpStatus(StatID.Int), GetStatGumpStatus(StatID.Int), ButtonTypes.Activate, 0, (int)StatID.Int));
			}
			else
            {
                AddControl(new GumpPic(this, 0, 0, 0x802, 0));

                _Labels[(int)MobileStats.Name] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 84, 37, 6, 0, _Mobile.Name));
                _Labels[(int)MobileStats.Strength] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 84, 57, 6, 0, _Mobile.Strength.ToString()));
                _Labels[(int)MobileStats.Dexterity] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 84, 69, 6, 0, _Mobile.Dexterity.ToString()));
                _Labels[(int)MobileStats.Intelligence] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 84, 81, 6, 0, _Mobile.Intelligence.ToString()));
                _Labels[(int)MobileStats.Sex] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 84, 93, 6, 0, _Mobile.Flags.IsFemale ? "F" : "M"));
                _Labels[(int)MobileStats.AR] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 84, 105, 6, 0, _Mobile.ArmorRating.ToString()));

                _Labels[(int)MobileStats.HealthCurrent] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 72, 57, 6, 0, _Mobile.Health.Current.ToString() + '/' + _Mobile.Health.Max.ToString()));
                _Labels[(int)MobileStats.ManaCurrent] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 172, 69, 6, 0, _Mobile.Mana.Current.ToString() + '/' + _Mobile.Mana.Max.ToString()));
                _Labels[(int)MobileStats.StaminaCurrent] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 172, 81, 6, 0, _Mobile.Stamina.Current.ToString() + '/' + _Mobile.Stamina.Max.ToString()));
                _Labels[(int)MobileStats.Gold] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 172, 93, 6, 0, _Mobile.Gold.ToString()));
                _Labels[(int)MobileStats.WeightCurrent] = (TextLabelAscii)AddControl(new TextLabelAscii(this, 172, 105, 6, 0, _Mobile.Weight.Current.ToString() + '/' + _Mobile.Weight.Max.ToString()));
            }
        }

		private int GetStatGumpYCorrection(StatID statid)
		{
			int y = 0;
			int correction = 3;
			int gumptype = GetStatGumpStatus(statid);
			switch (statid)
			{
				case StatID.Str:
					if (gumptype != 0x082C)
						y = 77;
					else
						y = 77 - correction;
					break;

				case StatID.Dex:
					if (gumptype != 0x082C)
						y = 104;
					else
						y = 104 - correction;
					break;

				case StatID.Int:
					if (gumptype != 0x082C)
						y = 135;
					else
						y = 135 - correction;
					break;
			}

			return y;
		}

		private int GetStatGumpStatus(StatID id)
		{
			// PlayerState // 0 Up // 1 down // 2 Lock 
			switch (id)
				{
				case StatID.Str:
					if (PlayerState.StatLocks.StrengthLock == 0)
						return 0x0983;
					else if (PlayerState.StatLocks.StrengthLock == 1)
						return 0x0985;
					else
						return 0x082C;

				case StatID.Dex:
					if (PlayerState.StatLocks.DexterityLock == 0)
						return 0x0983;
					else if (PlayerState.StatLocks.DexterityLock == 1)
						return 0x0985;
					else
						return 0x082C;

				case StatID.Int:
					if (PlayerState.StatLocks.IntelligenceLock == 0)
						return 0x0983;
					else if (PlayerState.StatLocks.IntelligenceLock == 1)
						return 0x0985;
					else
						return 0x082C;

				default:
					return 0x082C;
			}
		}

        protected override void OnInitialize()
        {
            SetSavePositionName("status");
            base.OnInitialize();
        }

        public override void Update(double totalMS, double frameMS)
        {
            if (_RefreshTime + 0.5d < totalMS) //need to update
            {
                _RefreshTime = totalMS;
                // we can just set these without checking if they've changed.
                // The label will only update if the value has changed.
                if (PlayerState.ClientFeatures.AOS)
                {
					_Name.Text = string.Format("<center><span color='#000' style ='font-family=ascii9;'>{0}", _Mobile.Name);
					_Labels[(int)MobileStats.Strength].Text = _Mobile.Strength.ToString();
                    _Labels[(int)MobileStats.Dexterity].Text = _Mobile.Dexterity.ToString();
                    _Labels[(int)MobileStats.Intelligence].Text = _Mobile.Intelligence.ToString();

                    _Labels[(int)MobileStats.HealthCurrent].Text = _Mobile.Health.Current.ToString();
                    _Labels[(int)MobileStats.HealthMax].Text = _Mobile.Health.Max.ToString();

                    _Labels[(int)MobileStats.StaminaCurrent].Text = _Mobile.Stamina.Current.ToString();
                    _Labels[(int)MobileStats.StaminaMax].Text = _Mobile.Stamina.Max.ToString();

                    _Labels[(int)MobileStats.ManaCurrent].Text = _Mobile.Mana.Current.ToString();
                    _Labels[(int)MobileStats.ManaMax].Text = _Mobile.Mana.Max.ToString();

					_Labels[(int)MobileStats.Damage].Text = ConcatCurrentMax(_Mobile.DamageMin, _Mobile.DamageMax);
					_Labels[(int)MobileStats.Followers].Text = ConcatCurrentMax(_Mobile.Followers.Current, _Mobile.Followers.Max);

                    _Labels[(int)MobileStats.WeightCurrent].Text = _Mobile.Weight.Current.ToString();
                    _Labels[(int)MobileStats.WeightMax].Text = _Mobile.Weight.Max.ToString();

                    _Labels[(int)MobileStats.StatCap].Text = _Mobile.StatCap.ToString();
                    _Labels[(int)MobileStats.Luck].Text = _Mobile.Luck.ToString();
                    _Labels[(int)MobileStats.Gold].Text = _Mobile.Gold.ToString();

					_Labels[(int)MobileStats.AR].Text = ConcatCurrentMax(_Mobile.ArmorRating, _Mobile.MaxPhysicRestistance);
					_Labels[(int)MobileStats.RF].Text = ConcatCurrentMax(_Mobile.ResistFire, _Mobile.MaxFireResistance);
					_Labels[(int)MobileStats.RC].Text = ConcatCurrentMax(_Mobile.ResistCold, _Mobile.MaxColdResistance);
					_Labels[(int)MobileStats.RP].Text = ConcatCurrentMax(_Mobile.ResistPoison, _Mobile.MaxPoisonResistance);
					_Labels[(int)MobileStats.RE].Text = ConcatCurrentMax(_Mobile.ResistEnergy, _Mobile.MaxEnergyResistance);

					_Labels[(int)MobileStats.HitChanceIncrease].Text = _Mobile.HitChanceIncrease.ToString();
					_Labels[(int)MobileStats.DefenseChanceIncrease].Text = ConcatCurrentMax(_Mobile.DefenseChanceIncrease, 45);

					_Labels[(int)MobileStats.LowerManaCost].Text = _Mobile.LowerManaCost.ToString();

					_Labels[(int)MobileStats.WeaponDamageIncrease].Text = _Mobile.WeaponDamageIncrease.ToString();
					_Labels[(int)MobileStats.SwingSpeedIncrease].Text = _Mobile.SwingSpeedIncrease.ToString();

					_Labels[(int)MobileStats.LowReagentCost].Text = _Mobile.LowReagentCost.ToString();
					_Labels[(int)MobileStats.SpellDamageIncrease].Text = _Mobile.SpellDamageIncrease.ToString();
					_Labels[(int)MobileStats.FasterCasting].Text =  _Mobile.SpellDamageIncrease.ToString();
					_Labels[(int)MobileStats.FasterCastRecovery].Text = _Mobile.FasterCastRecovery.ToString();
				}
                else
                {
                    _Labels[(int)MobileStats.Name].Text = _Mobile.Name;
                    _Labels[(int)MobileStats.Strength].Text = _Mobile.Strength.ToString();
                    _Labels[(int)MobileStats.Dexterity].Text = _Mobile.Dexterity.ToString();
                    _Labels[(int)MobileStats.Intelligence].Text = _Mobile.Intelligence.ToString();

                    _Labels[(int)MobileStats.HealthCurrent].Text = ConcatCurrentMax(_Mobile.Health.Current, _Mobile.Health.Max);
                    _Labels[(int)MobileStats.StaminaCurrent].Text = ConcatCurrentMax(_Mobile.Stamina.Current, _Mobile.Stamina.Max);
                    _Labels[(int)MobileStats.ManaCurrent].Text = ConcatCurrentMax(_Mobile.Mana.Current, _Mobile.Mana.Max);

                    _Labels[(int)MobileStats.WeightCurrent].Text = _Mobile.Weight.Current.ToString();

                    _Labels[(int)MobileStats.Gold].Text = _Mobile.Gold.ToString();

                    _Labels[(int)MobileStats.AR].Text = _Mobile.ArmorRating.ToString();

                    _Labels[(int)MobileStats.Sex].Text = _Mobile.Flags.IsFemale ? "F" : "M";
                }
            }

            base.Update(totalMS, frameMS);
        }

        public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
        {
            base.Draw(spriteBatch, position, frameMS);
        }

        private string ConcatCurrentMax(int min, int max)
        {
            return string.Format("{0}/{1}", min, max);
        }

		private void ResizeStatus(AControl sender, int x, int y,  Core.Input.MouseButton button)
		{
			if (x >= 537 && x <= 554 && y >= 173 && y <= 196) // Area del resize
			{
				UserInterface.AddControl(new MobileHealthTrackerGump(_Mobile), this.X, this.Y);
				this.Dispose(); // Rimuovo vecchio e creo nuovo piccolo
			}
		}

		public override bool SaveGump(out Dictionary<string, object> data)
		{
			data = new Dictionary<string, object>();
			data.Add("serial", (int)_Mobile.Serial);
			return true;
		}

		public override bool RestoreGump(Dictionary<string, object> data)
		{
			int serial;
			if (data.ContainsKey("serial"))
			{
				serial = (int)data["serial"];
				NetworkClient client = Service.Get<NetworkClient>();
				client.Send(new MobileQueryPacket(MobileQueryPacket.StatusType.BasicStatus, serial));
				return true;
			}
			return false;
		}

		public override void OnButtonClick(int buttonID)
		{
			switch ((StatID)buttonID)
			{
				case StatID.Buff:
					if (UserInterface.GetControl<BuffGump>() == null)
						UserInterface.AddControl(new BuffGump(), this.X, this.Y);
					else
						UserInterface.GetControl<BuffGump>().Position = Position;
					break;
				case StatID.Str:
					PlayerState.StatLocks.StrengthLock++;
					if (PlayerState.StatLocks.StrengthLock > 2)
						PlayerState.StatLocks.StrengthLock = 0;
					_Str.Dispose();
					_Str = (Button)AddControl(new Button(this, 27, GetStatGumpYCorrection(StatID.Str), GetStatGumpStatus(StatID.Str), GetStatGumpStatus(StatID.Str), ButtonTypes.Activate, 0, (int)StatID.Str));
					Service.Get<NetworkClient>().Send(new StatLockChange(0, (byte)PlayerState.StatLocks.StrengthLock));
					break;
				case StatID.Dex:
					PlayerState.StatLocks.DexterityLock++;
					if (PlayerState.StatLocks.DexterityLock > 2)
						PlayerState.StatLocks.DexterityLock = 0;
					_Dex.Dispose();
					_Dex = (Button)AddControl(new Button(this, 27, GetStatGumpYCorrection(StatID.Dex), GetStatGumpStatus(StatID.Dex), GetStatGumpStatus(StatID.Dex), ButtonTypes.Activate, 0, (int)StatID.Dex));
					Service.Get<NetworkClient>().Send(new StatLockChange(1, (byte)PlayerState.StatLocks.DexterityLock));
					break;
				case StatID.Int:
					PlayerState.StatLocks.IntelligenceLock++;
					if (PlayerState.StatLocks.IntelligenceLock > 2)
						PlayerState.StatLocks.IntelligenceLock = 0;
					_Int.Dispose();
					_Int = (Button)AddControl(new Button(this, 27, GetStatGumpYCorrection(StatID.Int), GetStatGumpStatus(StatID.Int), GetStatGumpStatus(StatID.Int), ButtonTypes.Activate, 0, (int)StatID.Int));
					Service.Get<NetworkClient>().Send(new StatLockChange(2, (byte)PlayerState.StatLocks.IntelligenceLock));
					break;
			}
		}
	}
}