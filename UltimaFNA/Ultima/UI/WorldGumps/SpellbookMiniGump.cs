﻿#region usings
using System.Collections.Generic;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.Data;
using UltimaXNA.Ultima.UI.Controls;
using UltimaXNA.Ultima.World;
using UltimaXNA.Ultima.World.Entities;
using UltimaXNA.Ultima.World.Entities.Items.Containers;
#endregion

namespace UltimaXNA.Ultima.UI.WorldGumps
{
    class SpellbookMiniGump : Gump
    {
		private SpellBook _Spellbook;
		public SpellbookMiniGump(SpellBook entity)
            : base(entity.Serial, 0)
        {
			_Spellbook = entity;
			IsMoveable = true;

			switch (_Spellbook.BookType)
			{
				case SpellBookTypes.Magic:
					AddControl(new GumpPic(this, 0, 0, 0x2B32, 0)).MouseDoubleClickEvent += MaximizeBook;
					break;
				case SpellBookTypes.Bushido:
					AddControl(new GumpPic(this, 0, 0, 0x2B09, 0)).MouseDoubleClickEvent += MaximizeBook;
					break;
				case SpellBookTypes.Chivalry:
					AddControl(new GumpPic(this, 0, 0, 0x2B04, 0)).MouseDoubleClickEvent += MaximizeBook;
					break;
				case SpellBookTypes.Necromancer:
					AddControl(new GumpPic(this, 0, 0, 0x2B03, 0)).MouseDoubleClickEvent += MaximizeBook;
					break;
				case SpellBookTypes.Ninjitsu:
					AddControl(new GumpPic(this, 0, 0, 0x2B08, 0)).MouseDoubleClickEvent += MaximizeBook;
					break;
				case SpellBookTypes.Spellweaving:
					AddControl(new GumpPic(this, 0, 0, 0x2B2D, 0)).MouseDoubleClickEvent += MaximizeBook;
					break;
				case SpellBookTypes.Mysticism:
					AddControl(new GumpPic(this, 0, 0, 0x2B30, 0)).MouseDoubleClickEvent += MaximizeBook;
					break;
				default:
					break;
			}

		}

		public override void Update(double totalMS, double frameMS)
		{
			base.Update(totalMS, frameMS);
		}

		private void MaximizeBook(AControl sender, int x, int y, MouseButton button)
		{
			UserInterface.AddControl(new SpellbookGump(_Spellbook), this.X, this.Y);
			this.Dispose(); // Rimuovo libro piccolo e apro libro grande
		}
	}
}
