﻿/***************************************************************************
 *   VendorSellGump.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System;
using System.Collections.Generic;
using UltimaFNA.Core.Network.SocketAsync;
using UltimaXNA.Core.Diagnostics.Tracing;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.Network;
using UltimaXNA.Core.Resources;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.Network.Client;
using UltimaXNA.Ultima.Network.Server;
using UltimaXNA.Ultima.UI.Controls;
#endregion

namespace UltimaXNA.Ultima.UI.WorldGumps
{
    class VendorSellGump : Gump
    {
        private ExpandableScroll _Background;
        private IScrollBar _ScrollBar;
        private HtmlGumpling _TotalCost;

        private int _VendorSerial;
        private VendorItemInfo[] _Items;
        private RenderedTextList _ShopContents;

        private MouseState _MouseState = MouseState.None;
        private int _MouseDownOnIndex;
        private double _MouseDownMS;
        private Button _OKButton;

		private bool _ShiftPressed = false;

		public VendorSellGump(VendorSellListPacket packet)
            : base(0, 0)
        {
            IsMoveable = true;
            // note: original gumplings start at index 0x0870.
            AddControl(_Background = new ExpandableScroll(this, 0, 0, 360, false));
            AddControl(new HtmlGumpling(this, 0, 6, 300, 20, 0, 0, " <center><span color='#004' style='font-family:ascii4;'>My Inventory"));

			// Eventi per gestione shift per vendita in blocco
			_Background.HandlesKeyboardFocus = true;
			_Background.KeyboardUpEvent += KeyEventUp;
			_Background.KeyboardDownEvent += KeyEventDown;

			// Eventi gestione scroll da mouse
			_Background.HandlesMouseInput = true;
			_Background.MouseWheelEvent += Scroll_MouseWheelEvent;

			_ScrollBar = (IScrollBar)AddControl(new ScrollFlag(this));
            AddControl(_ShopContents = new RenderedTextList(this, 22, 32, 250, 260, _ScrollBar));
            BuildShopContents(packet);

            AddControl(_TotalCost = new HtmlGumpling(this, 44, 334, 200, 30, 0, 0, string.Empty));
            UpdateEntryAndCost();

            AddControl(_OKButton = new Button(this, 220, 333, 0x907, 0x908, ButtonTypes.Activate, 0, 0));
            _OKButton.GumpOverID = 0x909;
            _OKButton.MouseClickEvent += okButton_MouseClickEvent;
        }

        public override void Dispose()
        {
            _OKButton.MouseClickEvent -= okButton_MouseClickEvent;
            base.Dispose();
        }

        private void okButton_MouseClickEvent(AControl control, int x, int y, MouseButton button)
        {
            if (button != MouseButton.Left)
                return;

            List<Tuple<int, short>> itemsToBuy = new List<Tuple<int, short>>();
            for (int i = 0; i < _Items.Length; i++)
            {
                if (_Items[i].AmountToSell > 0)
                {
                    itemsToBuy.Add(new Tuple<int, short>(_Items[i].Serial, (short)_Items[i].AmountToSell));
                }
            }

            if (itemsToBuy.Count == 0)
                return;

            NetworkClient network = Service.Get<NetworkClient>();
            network.Send(new SellItemsPacket(_VendorSerial, itemsToBuy.ToArray()));
            this.Dispose();
        }

        public override void Update(double totalMS, double frameMS)
        {
            _ShopContents.Height = Height - 69;
            base.Update(totalMS, frameMS);

            if (_MouseState != MouseState.None)
            {
                _MouseDownMS += frameMS;
                if (_MouseDownMS >= 350d)
                {
                    _MouseDownMS -= 120d;
                    if (_MouseState == MouseState.MouseDownOnAdd)
                    {
                        AddItem(_MouseDownOnIndex);
                    }
                    else
                    {
                        RemoveItem(_MouseDownOnIndex);
                    }
                }
            }
        }

        private void BuildShopContents(VendorSellListPacket packet)
        {
            _VendorSerial = packet.VendorSerial;

            _Items = new VendorItemInfo[packet.Items.Length];

            for (int i = 0; i < packet.Items.Length; i++)
            {
                VendorSellListPacket.VendorSellItem item = packet.Items[i];
                if (item.Amount > 0)
                {
                    string cliLocAsString = packet.Items[i].Name;
                    int clilocDescription;
                    string description;
                    if (!(int.TryParse(cliLocAsString, out clilocDescription)))
                    {
                        description = cliLocAsString;
                    }
                    else
                    {
                        // get the resource provider
                        IResourceProvider provider = Service.Get<IResourceProvider>();
                        description = Utility.CapitalizeAllWords(provider.GetString(clilocDescription));
                    }

                    string html = string.Format(c_Format, description, item.Price.ToString(), item.ItemID, item.Amount, i, 0, 0);
                    _ShopContents.AddEntry(html);

                    _Items[i] = new VendorItemInfo(item.ItemSerial, item.ItemID, item.Hue, description, item.Price, item.Amount);
                }
            }

            // list starts displaying first item.
            _ScrollBar.Value = 0;
        }

        private const string c_Format =
		    "<right><a href='add={4}'><gumpimg src='0x037'/></a><div width='4'/> <a href='remove={4}'><gumpimg src='0x038'/></a></right>" + // Tasti quantità
			"<left><itemimg src='{2}' width='62' height='44'/></left>" + // Immagine item
			"<left><span color='#400'>{0}<br/></span>" + // Nome Item
			"<span color='#2952a3'>{1}gp, {3} to sell.<br/></span>" + // Totale da vendere
			"<span color='#006600'>{5} sell, for {6}gp</span></left>" + // Totale Venduti
			"<br>"; // Spazio fra item

		public override void OnHtmlInputEvent(string href, MouseEvent e)
        {
            string[] hrefs = href.Split('=');
            bool isAdd;
            int index;
            if (hrefs[0] == "add")
            {
                isAdd = true;
            }
            else if (hrefs[0] == "remove")
            {
                isAdd = false;
            }
            else
            {
                Tracer.Error("Bad HREF in VendorBuyGump: {0}", href);
                return;
            }

            // parse item index
            if (!(int.TryParse(hrefs[1], out index)))
            {
                Tracer.Error("Unknown vendor item index in VendorBuyGump: {0}", href);
                return;
            }

            if (e == MouseEvent.Down)
            {
                if (isAdd)
                {
                    AddItem(index);
                }
                else
                {
                    RemoveItem(index);
                }
                _MouseState = isAdd ? MouseState.MouseDownOnAdd : MouseState.MouseDownOnRemove;
                _MouseDownMS = 0;
                _MouseDownOnIndex = index;
            }
            else if (e == MouseEvent.Up)
            {
                _MouseState = MouseState.None;
            }

            UpdateEntryAndCost(index);

            if (isAdd)
            {
                if (_Items[index].AmountToSell < _Items[index].AmountTotal)
                    _Items[index].AmountToSell++;
            }
            else
            {
                if (_Items[index].AmountToSell > 0)
                    _Items[index].AmountToSell--;
            }

            UpdateEntryAndCost();
        }

        private void AddItem(int index)
        {
			if (_ShiftPressed)
			{
				_Items[index].AmountToSell = _Items[index].AmountTotal;
			}
			else
			{
				if (_Items[index].AmountToSell < _Items[index].AmountTotal)
					_Items[index].AmountToSell++;
			}

            UpdateEntryAndCost(index);
        }

        private void RemoveItem(int index)
        {
			if (_ShiftPressed)
			{
				_Items[index].AmountToSell = 0;
			}
			else
			{
				if (_Items[index].AmountToSell > 0)
					_Items[index].AmountToSell--;
			}

			UpdateEntryAndCost(index);
        }

        private void UpdateEntryAndCost(int index = -1)
        {
            if (index >= 0)
            {
                _ShopContents.UpdateEntry(index, string.Format(c_Format,
                   _Items[index].Description,
                   _Items[index].Price.ToString(),
                   _Items[index].ItemID,
                   _Items[index].AmountTotal - _Items[index].AmountToSell, index, _Items[index].AmountToSell, (_Items[index].AmountToSell * _Items[index].Price)));
            }

            int totalCost = 0;
            if (_Items != null)
            {
                for (int i = 0; i < _Items.Length; i++)
                {
                    totalCost += _Items[i].AmountToSell * _Items[i].Price;
                }
            }
            _TotalCost.Text = string.Format("<span style='font-family:uni0;' color='#008'>Total: </span><span color='#400'>{0}gp</span>", totalCost);
        }

        private class VendorItemInfo
        {
            public readonly Serial Serial;
            public readonly ushort ItemID;
            public readonly ushort Hue;
            public readonly string Description;
            public readonly int Price;
            public readonly int AmountTotal;
            public int AmountToSell;

            public VendorItemInfo(Serial serial, ushort itemID, ushort hue, string description, int price, int amount)
            {
                Serial = serial;
                ItemID = itemID;
                Hue = hue;
                Description = description;
                Price = price;
                AmountTotal = amount;
                AmountToSell = 0;
            }
        }

		private void KeyEventDown(AControl sender, InputEventKeyboard e)
		{
			if (e.Shift)
				_ShiftPressed = true;
		}

		private void KeyEventUp(AControl sender, InputEventKeyboard e)
		{
			if (_ShiftPressed)
			{
				if (!e.Shift)
					_ShiftPressed = false;
			}
		}

		private void Scroll_MouseWheelEvent(AControl arg1, int arg2, int arg3, int arg4)
		{
			if (arg4 < 0)
				_ScrollBar.Value += 20;
			else
				_ScrollBar.Value -= 20;
		}

		enum MouseState
        {
            None,
            MouseDownOnAdd,
            MouseDownOnRemove
        }
    }
}
