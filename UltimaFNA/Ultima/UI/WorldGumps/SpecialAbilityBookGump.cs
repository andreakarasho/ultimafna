﻿#region usings
using System.Collections.Generic;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.UI;
using UltimaXNA.Core.Resources;
using UltimaXNA.Ultima.World;
using UltimaXNA.Ultima.World.Entities.Mobiles;
using UltimaXNA.Ultima.UI.Controls;
using UltimaXNA.Ultima.Data;
using UltimaXNA.Ultima.World.Entities;
using UltimaXNA.Ultima.World.Entities.Items;
using UltimaXNA.Ultima.World.Entities.Items.Containers;
#endregion

namespace UltimaXNA.Ultima.UI.WorldGumps
{
	class SpecialAbilityBookGump : Gump
	{
		private GumpPic _PageCornerLeft;
		private GumpPic _PageCornerRight;
		private int _MaxPage;
		private HtmlGumpling[] _Primary = new HtmlGumpling[3];
		private HtmlGumpling[] _Secondary = new HtmlGumpling[3];
		private Mobile _Mobile;
		private double _RefreshTime;
		private readonly WorldModel _World;
		private SpecialAbilityDefinition _PrimaryAbility;
		private SpecialAbilityDefinition _SecondaryAbility;


		private IResourceProvider provider = Service.Get<IResourceProvider>();

		public SpecialAbilityBookGump(Mobile mobile)
			: base(mobile.Serial, 0)
		{
			ClearControls();

			if (UserInterface.GetControl<SpecialAbilityBookMiniGump>() != null) // Rimuovo vechio libro per evitare doppie aperture
				UserInterface.GetControl<SpecialAbilityBookMiniGump>().Dispose();

			_World = Service.Get<WorldModel>();
			_Mobile = mobile;
			IsMoveable = true;

			AddControl(new GumpPic(this, 0, 0, 0x2B02, 0)).MouseClickEvent += MinimizeBook; // spellbook background

			AddControl(_PageCornerLeft = new GumpPic(this, 50, 8, 0x08BB, 0)); // page turn left
			_PageCornerLeft.GumpLocalID = 0;
			_PageCornerLeft.MouseClickEvent += PageCorner_MouseClickEvent;
			_PageCornerLeft.MouseDoubleClickEvent += PageCorner_MouseDoubleClickEvent;
			_PageCornerLeft.Page = int.MaxValue; // Nascondo gira pagina di sinistra se appena aperto

			AddControl(_PageCornerRight = new GumpPic(this, 321, 8, 0x08BC, 0)); // page turn right
			_PageCornerRight.GumpLocalID = 1;
			_PageCornerRight.MouseClickEvent += PageCorner_MouseClickEvent;
			_PageCornerRight.MouseDoubleClickEvent += PageCorner_MouseDoubleClickEvent;

			// Titolo libro
			for (int i = 1; i < 4; i++)
			{
				AddControl(new HtmlGumpling(this, 55, 10, 130, 200, 0, 0,
							string.Format("<span color='#004' style='font-family=uni0;'><center>INDEX</center></span>")),
							i);

				AddControl(new HtmlGumpling(this, 220, 10, 130, 200, 0, 0,
							string.Format("<span color='#004' style='font-family=uni0;'><center>INDEX</center></span>")),
							i);

				// Abilita testo
				AddControl(new HtmlGumpling(this, 265, 105, 80, 200, 0, 0, "<span color='#648' style='font-family=ascii6;'>Primary Ability Icon</span>"), i);
				AddControl(new HtmlGumpling(this, 265, 150, 80, 200, 0, 0, "<span color='#648' style='font-family=ascii6;'>Secondary Ability Icon</span>"), i);

				// Bottoni abilità primaria e secondaria
				_Primary[i - 1] = (HtmlGumpling)AddControl(new HtmlGumpling(this, 215, 105, 130, 44, 0, 0, string.Empty), i);
				_Secondary[i - 1] = (HtmlGumpling)AddControl(new HtmlGumpling(this, 215, 150, 130, 44, 0, 0, string.Empty), i);
			}

			UpdateAbilityIcon();

			int pageoffset = 3;
			int currentpage = 1;

			// Lista abilità
			// Pagina 1 lato sinistro
			for (int i = 1; i < 10; i++)
			{
				string link = string.Format("<a href='page={1}' hovercolor='#800' style='font-family=ascii9; text-decoration=none;'>{0}</a><br/>",
											SpecialAbility.GetAbility(i).Name, pageoffset + i);
				AddControl(new HtmlGumpling(this, 60, 15 + (i * 18), 130, 200, 0, 0, link),	currentpage);
			}

			// Pagina 1 lato Destro
			for (int i = 10; i < 14; i++)
			{
				string link = string.Format("<a href='page={1}' hovercolor='#800' style='font-family=ascii9; text-decoration=none;'>{0}</a><br/>",
											SpecialAbility.GetAbility(i).Name, pageoffset + i);
				AddControl(new HtmlGumpling(this, 220, 15 + ((i-9) * 18), 130, 200, 0, 0, link), currentpage);
			}

			currentpage = 2;
			// Pagina 2 lato sinistro
			for (int i = 14; i < 23; i++)
			{
				string link = string.Format("<a href='page={1}' hovercolor='#800' style='font-family=ascii9; text-decoration=none;'>{0}</a><br/>",
											SpecialAbility.GetAbility(i).Name, pageoffset + i);
				AddControl(new HtmlGumpling(this, 60, 15 + ((i-13) * 18), 140, 200, 0, 0, link), currentpage);
			}

			// Pagina 2 lato Destro
			for (int i = 23; i < 27; i++)
			{
				string link = string.Format("<a href='page={1}' hovercolor='#800' style='font-family=ascii9; text-decoration=none;'>{0}</a><br/>",
											SpecialAbility.GetAbility(i).Name, pageoffset + i);
				AddControl(new HtmlGumpling(this, 220, 15 + ((i - 22) * 18), 130, 200, 0, 0, link), currentpage);
			}

			currentpage = 3;
			// Pagina 3 lato sinistro
			for (int i = 27; i < 32; i++)
			{
				string link = string.Format("<a href='page={1}' hovercolor='#800' style='font-family=ascii9; text-decoration=none;'>{0}</a><br/>",
											SpecialAbility.GetAbility(i).Name, pageoffset + i);
				AddControl(new HtmlGumpling(this, 60, 15 + ((i - 26) * 18), 140, 200, 0, 0, link), currentpage);
			}


			int y = 4;
			// Creo pagine abilità
			for (int i = 1; i < 32; i++)
			{
				CreateSkillPage(y, SpecialAbility.GetAbility(i));
				y++;
			}

				
			_MaxPage = y - 1;
		}

		void CreateSkillPage(int page, SpecialAbilityDefinition ability)
		{
			// Nome ability
			AddControl(new HtmlGumpling(this, 104, 35, 100, 200, 0, 0,
				string.Format("<span style='font-family=ascii6;'>{0}</span>", ability.Name)),
				page);

			// Icona
			AddControl(new GumpPic(this, 56, 38, ability.GumpIconID, 0), page).SetTooltip(provider.GetString(ability.DescClilocID)); 

			// Divisore
			AddControl(new GumpPicTiled(this, 56, 86, 130, 4, 0x23BF), page);


			// Lista delle weapon
			World.Entities.Items.StaticItem wep = new World.Entities.Items.StaticItem(0x0001, 0, 0, new World.Maps.Map(0));

			if (ability.WeaponID.Length < 7)
			{
				for (int i = 0; i < ability.WeaponID.Length; i++)
				{
					wep.ItemID = ability.WeaponID[i];

					AddControl(new HtmlGumpling(this, 56, 93 + (i * 15), 146, 106, 0, 0, string.Format(
					"<span color ='#542' style ='font-family=ascii9;' >{0}</span> ", Utility.CapitalizeAllWords(wep.Name))),
					page);
				}
			}
			else
			{
				for (int i = 0; i < 7; i++)
				{
					wep.ItemID = ability.WeaponID[i];

					AddControl(new HtmlGumpling(this, 56, 93 + (i * 15), 146, 106, 0, 0, string.Format(
					"<span color ='#542' style ='font-family=ascii9;' >{0}</span> ", Utility.CapitalizeAllWords(wep.Name))),
					page);
				}

				for (int i = 7; i < ability.WeaponID.Length; i++)
				{
					wep.ItemID = ability.WeaponID[i];

					AddControl(new HtmlGumpling(this, 60 + 156, 33 + ((i - 7) * 15), 146, 106, 0, 0, string.Format(
					"<span color ='#542' style ='font-family=ascii9;' >{0}</span> ", Utility.CapitalizeAllWords(wep.Name))),
					page);
				}
			}

		}

		private void UpdateAbilityIcon()
		{
			Item itemslot1 = (_Mobile).GetItem(1);
			Item itemslot2 = (_Mobile).GetItem(2);

			if (itemslot1 != null) // Estraggo quale abilità in base alle armi equippate
			{
				_PrimaryAbility = SpecialAbility.GetWeaponPrimaryAbility(itemslot1.ItemID);
				_SecondaryAbility = SpecialAbility.GetWeaponSecondaryAbility(itemslot1.ItemID);
			}
			else
			{
				if (itemslot2 != null)
				{
					_PrimaryAbility = SpecialAbility.GetWeaponPrimaryAbility(itemslot2.ItemID);
					_SecondaryAbility = SpecialAbility.GetWeaponSecondaryAbility(itemslot2.ItemID);
				}
				else
				{
					_PrimaryAbility = SpecialAbility.GetWeaponPrimaryAbility(0x0001);
					_SecondaryAbility = SpecialAbility.GetWeaponSecondaryAbility(0x0001);
				}
			}

			for (int i = 0; i < 3; i++) // Aggiorno icone
			{
				_Primary[i].Text = string.Format("<a href='spellicon={0}'><gumpimg src='{1}'/></a>", 0, _PrimaryAbility.GumpIconID);
				_Primary[i].SetTooltip(provider.GetString(_PrimaryAbility.NameClilocID));

				_Secondary[i].Text = string.Format("<a href='spellicon={0}'><gumpimg src='{1}'/></a>", 1, _SecondaryAbility.GumpIconID);
				_Secondary[i].SetTooltip(provider.GetString(_SecondaryAbility.NameClilocID));
			}
		}

		public override void Update(double totalMS, double frameMS)
		{
			if (_RefreshTime + 0.5d < totalMS) //need to update
			{
				_RefreshTime = totalMS;
				UpdateAbilityIcon();
			}
			base.Update(totalMS, frameMS);
		}

		void SpellCircle_MouseClickEvent(AControl sender, int x, int y, MouseButton button)
		{
			if (button != MouseButton.Left)
				return;
			SetActivePage(sender.GumpLocalID / 2 + 1);
		}

		void PageCorner_MouseClickEvent(AControl sender, int x, int y, MouseButton button)
		{
			if (button != MouseButton.Left)
				return;

			if (sender.GumpLocalID == 0)
			{
				SetActivePage(ActivePage - 1);
			}
			else
			{
				SetActivePage(ActivePage + 1);
			}
		}

		void PageCorner_MouseDoubleClickEvent(AControl sender, int x, int y, MouseButton button)
		{
			if (button != MouseButton.Left)
				return;

			if (sender.GumpLocalID == 0)
			{
				SetActivePage(1);
			}
			else
			{
				SetActivePage(_MaxPage);
			}
		}

		private void MinimizeBook(AControl sender, int x, int y, MouseButton button)
		{
			if (x >= 3 && x <= 19 && y >= 101 && y <= 120) // Area del resize
			{
				UserInterface.AddControl(new SpecialAbilityBookMiniGump(_Mobile), this.X, this.Y);
				this.Dispose(); // Rimuovo libro e creo libro piccolo
			}
		}

		void SetActivePage(int page)
		{
			if (page < 1)
				page = 1;
			if (page > _MaxPage)
				page = _MaxPage;

			ActivePage = page;

			// hide the page corners if we're at the first or final page.
			_PageCornerLeft.Page = (ActivePage != 1) ? 0 : int.MaxValue;
			_PageCornerRight.Page = (ActivePage != _MaxPage) ? 0 : int.MaxValue;

		}

		public override void OnHtmlInputEvent(string href, MouseEvent e)
		{
			if (e == MouseEvent.Click)
			{
				string[] hrefs = href.Split('=');
				if (hrefs.Length != 2)
					return;
				if (hrefs[0] == "page")
				{
					int page;
					if (int.TryParse(hrefs[1], out page))
						SetActivePage(page);

				}
			}
			else if (e == MouseEvent.DragBegin)
			{
				string[] hrefs = href.Split('=');
				if (hrefs.Length != 2)
					return;

				if (hrefs[0] == "spellicon")
				{
					int spell;
					if (!int.TryParse(hrefs[1], out spell))
						return;

					if (spell < 2)
					{
						Core.Diagnostics.Tracing.Tracer.Warn("- Manca salvataggio posizione");
						InputManager input = Service.Get<InputManager>();
						UseSpecialAbilityButtonGump gump = new UseSpecialAbilityButtonGump(spell);
						UserInterface.AddControl(gump, input.MousePosition.X - 22, input.MousePosition.Y - 22);
						UserInterface.AttemptDragControl(gump, input.MousePosition, true);
					}
				}
			}
			else if (e == MouseEvent.DoubleClick)
			{
				string[] hrefs = href.Split('=');
				if (hrefs.Length != 2)
					return;

				if (hrefs[0] == "spellicon")
				{
					int spell;
					if (!int.TryParse(hrefs[1], out spell))
						return;

					if (spell < 2)
					{
						if (spell == 0) // Primary
						{
							// Disattiva secondaria se abilitata
							if (UserInterface.GetControl<UseSpecialAbilityButtonGump>(1) != null)
							{
								if (UserInterface.GetControl<UseSpecialAbilityButtonGump>(1).AbilityEnabled)
								{
									UserInterface.GetControl<UseSpecialAbilityButtonGump>(1).AbilityEnabled = false;
								}
							}

							// Abilita o disattiva primaria a seconda dello stato
							if (UserInterface.GetControl<UseSpecialAbilityButtonGump>(0) != null)
							{

								if (UserInterface.GetControl<UseSpecialAbilityButtonGump>(0).AbilityEnabled)
								{
									_World.Interaction.UseAbility(0);
									UserInterface.GetControl<UseSpecialAbilityButtonGump>(0).AbilityEnabled = false;
									return;
								}
								else
								{
									UserInterface.GetControl<UseSpecialAbilityButtonGump>(0).AbilityEnabled = true;
									_World.Interaction.UseAbility(_PrimaryAbility.ID);
								}							
							}								
						}
						else // Secondary
						{
							// Disattiva primaria se abilitata
							if (UserInterface.GetControl<UseSpecialAbilityButtonGump>(0) != null)
							{
								if (UserInterface.GetControl<UseSpecialAbilityButtonGump>(0).AbilityEnabled)
								{
									UserInterface.GetControl<UseSpecialAbilityButtonGump>(0).AbilityEnabled = false;
								}
							}

							// Abilita o disattiva secondaria a seconda dello stato
							if (UserInterface.GetControl<UseSpecialAbilityButtonGump>(1) != null)
							{
								if (UserInterface.GetControl<UseSpecialAbilityButtonGump>(1).AbilityEnabled)
								{
									_World.Interaction.UseAbility(0);
									UserInterface.GetControl<UseSpecialAbilityButtonGump>(1).AbilityEnabled = false;
									return;
								}
								else
								{
									_World.Interaction.UseAbility(_SecondaryAbility.ID);
									UserInterface.GetControl<UseSpecialAbilityButtonGump>(1).AbilityEnabled = true;
								}
							}
							
						}
					}
				}
			}
		}
	}
}
