﻿/***************************************************************************
 *   WorldViewGump.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.Resources;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.UI.Controls;
using UltimaXNA.Ultima.World;
#endregion

namespace UltimaXNA.Ultima.UI.WorldGumps
{
    /// <summary>
    /// A bordered container that displays the world control.
    /// </summary>
    class WorldViewGump : Gump
    {
        private WorldModel _Model;

        private WorldViewport _Viewport;
        private ChatControl _ChatWindow;
        private GameBorder _border;

        private const int BorderWidth = 5, BorderHeight = 7;
        private int _WorldWidth, _WorldHeight;

        public WorldViewGump()
            : base(0, 0)
        {
            HandlesMouseInput = false;
            IsUncloseableWithRMB = true;
            IsUncloseableWithEsc = true;
            IsMoveable = true;
            MetaData.Layer = UILayer.Under;

            _Model = Service.Get<WorldModel>();

            _WorldWidth = Settings.UserInterface.PlayWindowGumpResolution.Width = 800;
            _WorldHeight = Settings.UserInterface.PlayWindowGumpResolution.Height = 600;

            Position = new Point(32, 32);

            InitControls();
        }

        protected override void OnInitialize()
        {
            SetSavePositionName("worldview");
            base.OnInitialize();
        }

        public override void Update(double totalMS, double frameMS)
        {
            if (_WorldWidth != Settings.UserInterface.PlayWindowGumpResolution.Width || _WorldHeight != Settings.UserInterface.PlayWindowGumpResolution.Height)
            {                
                _WorldWidth = Settings.UserInterface.PlayWindowGumpResolution.Width;
                _WorldHeight = Settings.UserInterface.PlayWindowGumpResolution.Height;

                OnResize();
            }

            base.Update(totalMS, frameMS);
        }

        public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
        {
            base.Draw(spriteBatch, position, frameMS);
        }

        protected override void OnMove()
        {
            // base.OnMove() would make sure that the gump remained at least half on screen, but we want more fine-grained control over movement.
            SpriteBatchUI sb = Service.Get<SpriteBatchUI>();
            Point position = Position;

            if (position.X < -BorderWidth)
                position.X = -BorderWidth;
            if (position.Y < -BorderHeight)
                position.Y = -BorderHeight;
            if (position.X + Width - BorderWidth > sb.GraphicsDevice.Viewport.Width)
                position.X = sb.GraphicsDevice.Viewport.Width - (Width - BorderWidth);
            if (position.Y + Height - BorderHeight > sb.GraphicsDevice.Viewport.Height)
                position.Y = sb.GraphicsDevice.Viewport.Height - (Height - BorderHeight);

            Position = position;
        }


        private void OnResize()
        {       
            Size = new Point(_WorldWidth + BorderWidth * 2, _WorldHeight + BorderHeight * 2);

            _Viewport.Width = _WorldWidth;
            _Viewport.Height = _WorldHeight;

            _border.Width = Size.X;
            _border.Height = Size.Y;

            _ChatWindow.SetSize(_WorldWidth, _WorldHeight);

        }

        private void ResetChat()
        {
            if (Service.Has<ChatControl>())
                Service.Remove<ChatControl>();

            UserInterface.RemoveControl<ChatControl>();

            AddControl(_ChatWindow = new ChatControl(this, BorderWidth, BorderHeight, _WorldWidth, _WorldHeight));

            Service.Add(_ChatWindow);
        }

        private void InitControls()
        {
            AddControl(_border = new GameBorder(this, 0, 0, _WorldWidth + BorderWidth * 2, _WorldHeight + BorderHeight * 2));
            AddControl(_Viewport = new WorldViewport(this, BorderWidth, BorderHeight, _WorldWidth, _WorldHeight));

            ResetChat();
        }
    }

    class ResizerGump : Gump
    {
        public ResizerGump() : base(0, 0)
        {
            AddControl(new GumpPic(this, 0, 0, 0x837, 0));
            MakeThisADragger();
        }
    }

    class GameBorder : AControl
    {
        private Texture2D[] _gumpsBorder = new Texture2D[2];
        private Texture2D[] _button = new Texture2D[2];
        private int _buttonStatus = 0;

        private InputManager _input;

        private bool _isClicked;
 
        public GameBorder(AControl parent, int x, int y, int w, int h) : base(parent)
        {
            Parent = parent;
            Position = new Point(x, y);
            Width = w;
            Height = h;

            IResourceProvider prov = Service.Get<IResourceProvider>();

            _gumpsBorder[0] = prov.GetUITexture(0x0A8C);
            _gumpsBorder[1] = prov.GetUITexture(0x0A8D);

            _button[0] = prov.GetUITexture(0x837); // normale
            _button[1] = prov.GetUITexture(0x838); // premuto

            _input = Service.Get<InputManager>();

            MakeThisADragger();
        }

        private Point _mousePosition;
        public override void Update(double totalMS, double frameMS)
        {
            if (_buttonStatus == 1)
            {
                foreach (InputEventMouse e in _input.GetMouseEvents())
                {
                    if (e.EventType == MouseEvent.Down)
                    {
                        _isClicked = true;
                        _mousePosition = e.Position;
                    }
                    else if (e.EventType == MouseEvent.Up)
                    {
                        _isClicked = false;
                        break;
                    }
                    else if (e.EventType == MouseEvent.Move)
                        _mousePosition = e.Position;
                }

                if (_isClicked)
                {
                    int offX = _mousePosition.X - Width - Parent.X;
                    int offY =  _mousePosition.Y - Height - Parent.Y;

                   // System.Console.WriteLine("{0},{1}", offX, offY);        

                    if (Math.Abs(offX) > 4)
                    {
                        int newW = Width + offX;
                        if (newW < 800)
                            newW = 800;

                        Settings.UserInterface.PlayWindowGumpResolution.Width = newW;
                    }

                    if (Math.Abs(offY) > 4)
                    {
                        int newH = Height + offY;
                        if (newH < 600)
                            newH = 600;

                        Settings.UserInterface.PlayWindowGumpResolution.Height = newH;
                    }

                }
            }

            base.Update(totalMS, frameMS);
        }


        public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
        {
            // sopra
            spriteBatch.Draw2DTiled(_gumpsBorder[0], new Rectangle(position.X, position.Y + _gumpsBorder[0].Height /2, Width - 2, _gumpsBorder[0].Height), Vector3.Zero);
            // sotto
            spriteBatch.Draw2DTiled(_gumpsBorder[0], new Rectangle(position.X, position.Y + Height - _gumpsBorder[0].Height*2 + 1, Width +1, _gumpsBorder[0].Height), Vector3.Zero);
            //sx
            spriteBatch.Draw2DTiled(_gumpsBorder[1], new Rectangle(position.X - _gumpsBorder[1].Width /2 + 1 , position.Y + _gumpsBorder[0].Height / 2, _gumpsBorder[1].Width, Height - _gumpsBorder[0].Height * 2 ), Vector3.Zero);
            //dx
            spriteBatch.Draw2DTiled(_gumpsBorder[1], new Rectangle(position.X + Width - _gumpsBorder[1].Width + 1, position.Y + 2, _gumpsBorder[1].Width, Height - _gumpsBorder[0].Height * 2 ), Vector3.Zero);


            Rectangle rectangle = new Rectangle(position.X + Width - _button[0].Width / 2 + 1, position.Y + Height - _button[0].Height / 2 - 1, _button[0].Width, _button[0].Height);

            if (rectangle.Contains(_input.MousePosition))
            {
                if (_buttonStatus != 1)
                    _buttonStatus = 1;
            }
            else if (_buttonStatus != 0 && !_isClicked)
            {
                _buttonStatus = 0;
            }
                
            spriteBatch.Draw2D(_button[_buttonStatus], rectangle, Vector3.Zero);

            base.Draw(spriteBatch, position, frameMS);
        }
    }
}
