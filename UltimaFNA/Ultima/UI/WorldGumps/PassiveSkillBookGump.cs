﻿#region usings
using System.Collections.Generic;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.UI;
using UltimaXNA.Core.Resources;
using UltimaXNA.Ultima.World;
using UltimaXNA.Ultima.World.Entities.Mobiles;
using UltimaXNA.Ultima.UI.Controls;
using UltimaXNA.Ultima.World.Entities;
using UltimaXNA.Ultima.World.Entities.Items.Containers;
#endregion

namespace UltimaXNA.Ultima.UI.WorldGumps
{
    class PassiveSkillBookGump : Gump
    {
		private GumpPic _PageCornerLeft;
		private GumpPic _PageCornerRight;
		private int _MaxPage;
		private HtmlGumpling[] _Indexes;
		private Mobile _Mobile;

		private readonly string[] _HumanSkillNames = { "Strong Back", "Tough", "Workhorse", "Jack of All Trades" };
		private readonly string[] _ElfSkillNames = { "Night Sight", "Infused with Magic", "Knowledge of Nature", "Difficult to Track", "Perception", "Wisdom" };
		private readonly string[] _GargoyleSkillNames = { "Flying", "Berserk", "Master Artisan", "Deadly Aim", "Mystic Insight" };

		private class PassiveSkill
		{
			private int _icon;
			public int Icon { get { return _icon; } set { _icon = value; } }

			private string _name;
			public string Name { get { return _name; } set { _name = value; } }

			private int _cliloc;
			public int Cliloc { get { return _cliloc; } set { _cliloc = value; } }


			public PassiveSkill(int icon, string name, int cliloc)
			{
				_icon = icon;
				_name = name;
				_cliloc = cliloc;
			}
		}

		private PassiveSkill[] _PassiveSkillList;

		IResourceProvider provider = Service.Get<IResourceProvider>();

		public PassiveSkillBookGump(Mobile mobile)
			: base(mobile.Serial, 0)
		{
			ClearControls();

			if (UserInterface.GetControl<PassiveSkillBookMiniGump>() != null) // Rimuovo vechio libro per evitare doppie aperture
				UserInterface.GetControl<PassiveSkillBookMiniGump>().Dispose();

			_Mobile = mobile;
			IsMoveable = true;

			AddControl(new GumpPic(this, 0, 0, 0x2B29, 0)).MouseClickEvent += MinimizeBook; // spellbook background

			AddControl(_PageCornerLeft = new GumpPic(this, 50, 8, 0x08BB, 0)); // page turn left
			_PageCornerLeft.GumpLocalID = 0;
			_PageCornerLeft.MouseClickEvent += PageCorner_MouseClickEvent;
			_PageCornerLeft.MouseDoubleClickEvent += PageCorner_MouseDoubleClickEvent;
			_PageCornerLeft.Page = int.MaxValue; // Nascondo gira pagina di sinistra se appena aperto

			AddControl(_PageCornerRight = new GumpPic(this, 321, 8, 0x08BC, 0)); // page turn right
			_PageCornerRight.GumpLocalID = 1;
			_PageCornerRight.MouseClickEvent += PageCorner_MouseClickEvent;
			_PageCornerRight.MouseDoubleClickEvent += PageCorner_MouseDoubleClickEvent;

			// Titolo libro
			AddControl(new HtmlGumpling(this, 55, 15, 130, 200, 0, 0,
						string.Format("<span color='#004' style='font-family=uni0;'><center>INDEX</center></span>")),
						1);

			AddControl(new HtmlGumpling(this, 220, 15, 130, 200, 0, 0,
						string.Format("<span color='#004' style='font-family=uni0;'><center>INDEX</center></span>")),
						1);

			GenerateSkillList(mobile);

			// Inserisco skill passive
			int i = 0;
			int y = 2;
			_Indexes = new HtmlGumpling[_PassiveSkillList.Length];

			foreach (PassiveSkill skill in _PassiveSkillList)
			{
				string link = string.Format("<a href='page={1}' hovercolor='#800' style='font-family=ascii9; text-decoration=none;'>{0}</a><br/>",
											skill.Name, 2 + i);

				_Indexes[i] = (HtmlGumpling)AddControl(
					new HtmlGumpling(this, 60, 45 + (i * 18), 130, 200, 0, 0, link),
					1);

				if (((i + 1) % 2) == 1)
				{
					// Se multiplo di 2 
					CreateSkillPage(y, false, _PassiveSkillList[i]);
					if (i+1 <= _PassiveSkillList.Length -1)
						CreateSkillPage(y, true, _PassiveSkillList[i+1]);
					y++;
				}
				i++;
			}
			_MaxPage = y - 1;
		}

		void CreateSkillPage(int page, bool rightPage, PassiveSkill skill)
		{
			AddControl(new HtmlGumpling(this, 104 + (rightPage ? 156 : 0), 38, 88, 40, 0, 0,
						string.Format("<span color='#542' style='font-family=ascii9;'>{0}</span>", skill.Name)), page); // Nome

			if (skill.Icon != 0x5DDA) // se la skill passiva non è il fly gargoyle
			{
				AddControl(new GumpPic(this, 56 + (rightPage ? 156 : 0), 38, skill.Icon, 0), page).SetTooltip(provider.GetString(skill.Cliloc));  // Icona

				AddControl(new HtmlGumpling(this, 104 + (rightPage ? 156 : 0), 68, 88, 40, 0, 0,
						"<span color='#542' style='font-family=ascii9;'>(Passive)</span>"), page);
			}
			else  // fly gargoile
			{
				AddControl(new HtmlGumpling(this, 56 + (rightPage ? 156 : 0), 38, 130, 44, 0, 0,
				string.Format("<a href='spellicon={0}'><gumpimg src='{1}'/></a>",
				1, skill.Icon)), page).SetTooltip(provider.GetString(skill.Cliloc));
			}

			// Divisore
			AddControl(new GumpPicTiled(this, 56 + (rightPage ? 156 : 0), 88, 130, 4, 0x23BF), page);
		}

		private void GenerateSkillList(Mobile m)
		{
			string[] skillname;
			int iconstartid = 0;
			int clilocstartid = 0;

			switch (m.Race)
			{
				case Data.Races.Elf:
					{
						iconstartid = 0x5DD4;
						clilocstartid = 1112202;
						skillname = _ElfSkillNames;
						break;
					}
				case Data.Races.Gargoyle:
					{
						iconstartid = 0x5DDA;
						clilocstartid = 1112208;
						skillname = _GargoyleSkillNames;
						break;
					}
				default:
					{
						iconstartid = 0x5DD0;
						clilocstartid = 1112198;
						skillname = _HumanSkillNames;
						break;
					}

			}
			_PassiveSkillList = new PassiveSkill[skillname.Length];

			for (int i = 0; i < skillname.Length; i++)
			{
				_PassiveSkillList[i] = new PassiveSkill(iconstartid + i, skillname[i], clilocstartid + i);
			}

		}

		public override void Update(double totalMS, double frameMS)
		{
			base.Update(totalMS, frameMS);
		}

		void SpellCircle_MouseClickEvent(AControl sender, int x, int y, MouseButton button)
		{
			if (button != MouseButton.Left)
				return;
			SetActivePage(sender.GumpLocalID / 2 + 1);
		}

		void PageCorner_MouseClickEvent(AControl sender, int x, int y, MouseButton button)
		{
			if (button != MouseButton.Left)
				return;

			if (sender.GumpLocalID == 0)
			{
				SetActivePage(ActivePage - 1);
			}
			else
			{
				SetActivePage(ActivePage + 1);
			}
		}

		void PageCorner_MouseDoubleClickEvent(AControl sender, int x, int y, MouseButton button)
		{
			if (button != MouseButton.Left)
				return;

			if (sender.GumpLocalID == 0)
			{
				SetActivePage(1);
			}
			else
			{
				SetActivePage(_MaxPage);
			}
		}

		private void MinimizeBook(AControl sender, int x, int y, MouseButton button)
		{
			if (x >= 3 && x <= 19 && y >= 101 && y <= 120) // Area del resize
			{
				UserInterface.AddControl(new PassiveSkillBookMiniGump(_Mobile), this.X, this.Y);
				this.Dispose(); // Rimuovo libro e creo libro piccolo
			}
		}

		void SetActivePage(int page)
		{
			if (page < 1)
				page = 1;
			if (page > _MaxPage)
				page = _MaxPage;

			ActivePage = page;

			// hide the page corners if we're at the first or final page.
			_PageCornerLeft.Page = (ActivePage != 1) ? 0 : int.MaxValue;
			_PageCornerRight.Page = (ActivePage != _MaxPage) ? 0 : int.MaxValue;

		}

		public override void OnHtmlInputEvent(string href, MouseEvent e)
		{
			if (e == MouseEvent.Click)
			{
				string[] hrefs = href.Split('=');
				if (hrefs.Length != 2)
					return;
				if (hrefs[0] == "page")
				{
					int page;
					if (int.TryParse(hrefs[1], out page))
						SetActivePage(page);

				}
				else if (hrefs[0] == "spellicon")
				{
					int spell;
					if (int.TryParse(hrefs[1], out spell))
					{
						if (spell == 1)
							Service.Get<WorldModel>().Interaction.ToggleFlyMode();
					}
				}
			}
			else if (e == MouseEvent.DragBegin)
			{
				string[] hrefs = href.Split('=');
				if (hrefs.Length != 2)
					return;

				if (hrefs[0] == "spellicon")
				{
					int spell;
					if (!int.TryParse(hrefs[1], out spell))
						return;

					if (spell == 1)
					{
						Core.Diagnostics.Tracing.Tracer.Warn("- Manca salvataggio posizione");
						InputManager input = Service.Get<InputManager>();
						UseFlyButtonGump gump = new UseFlyButtonGump();
						UserInterface.AddControl(gump, input.MousePosition.X - 22, input.MousePosition.Y - 22);
						UserInterface.AttemptDragControl(gump, input.MousePosition, true);
					}
				}
			}
		}
	}
}
