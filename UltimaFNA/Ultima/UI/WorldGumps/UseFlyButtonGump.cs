﻿#region usings
using Microsoft.Xna.Framework;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.Data;
using UltimaXNA.Ultima.UI.Controls;
using UltimaXNA.Ultima.World;
#endregion

namespace UltimaXNA.Ultima.UI.WorldGumps
{
    public class UseFlyButtonGump : Gump
    {
        // private variables
        private GumpPic _SpellButton;
        // services
        private readonly WorldModel _World;

        public UseFlyButtonGump()
            : base(0, 0)
        {
            while (UserInterface.GetControl<UseFlyButtonGump>() != null)
            {
                UserInterface.GetControl<UseFlyButtonGump>().Dispose();
            }

             _World = Service.Get<WorldModel>();

            IsMoveable = true;
            HandlesMouseInput = true;

            _SpellButton = (GumpPic)AddControl(new GumpPic(this, 0, 0, 0x5DDA, 0));
            _SpellButton.HandlesMouseInput = true;
            _SpellButton.MouseDoubleClickEvent += EventMouseDoubleClick;
			_SpellButton.SetTooltip("Flying");
        }

        public override void Dispose()
        {
            _SpellButton.MouseDoubleClickEvent -= EventMouseDoubleClick;
            base.Dispose();
        }

        public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
        {
            base.Draw(spriteBatch, position, frameMS);
        }

        private void EventMouseDoubleClick(AControl sender, int x, int y, MouseButton button)
        {
            if (button != MouseButton.Left)
                return;
			_World.Interaction.ToggleFlyMode();
        }
    }
}