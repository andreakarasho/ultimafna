﻿/***************************************************************************
 *   TextEntry.cs
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

#region usings
using Microsoft.Xna.Framework;
using System;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.UI;
using UltimaXNA.Core.UI.HTML;
using UltimaXNA.Core.Windows;
#endregion

namespace UltimaXNA.Ultima.UI.Controls
{
    class TextEntry : AControl
    {
        const float MSBetweenCaratBlinks = 500f;
        
        bool _IsFocused;
        bool _CaratBlinkOn;
        float _MSSinceLastCaratBlink;
        RenderedText _RenderedText;
        RenderedText _RenderedCarat;

        private int _currentHue;

        public int EntryID;
        public int MaxCharCount;
        public bool IsPasswordField;
        public bool ReplaceDefaultTextOnFirstKeypress;
        public bool NumericOnly;
        public string LeadingHtmlTag;
        public string LeadingText;
        public string Text;
        public bool LegacyCarat;
        public bool CaretVisible;

        public int Hue;
        public int HoveredColor;
        public int FocusedColor;

        public override bool HandlesMouseInput => base.HandlesMouseInput & IsEditable;
        public override bool HandlesKeyboardFocus => base.HandlesKeyboardFocus & IsEditable;

        // ============================================================================================================
        // Ctors and BuildGumpling Methods
        // ============================================================================================================

        public TextEntry(AControl parent, string[] arguements, string[] lines)
            : this(parent)
        {
            int x, y, width, height, hue, entryID, textIndex, maxCharCount = 0;
            x = Int32.Parse(arguements[1]);
            y = Int32.Parse(arguements[2]);
            width = Int32.Parse(arguements[3]);
            height = Int32.Parse(arguements[4]);
            hue = ServerRecievedHueTransform(Int32.Parse(arguements[5]));
            entryID = Int32.Parse(arguements[6]);
            textIndex = Int32.Parse(arguements[7]);
            if (arguements[0] == "textentrylimited")
            {
                maxCharCount = Int32.Parse(arguements[8]);
            }
            BuildGumpling(x, y, width, height, hue, entryID, maxCharCount, lines[textIndex]);
        }

        public TextEntry(AControl parent, int x, int y, int width, int height, int hue, int entryID, int maxCharCount, string text)
            : this(parent)
        {
            BuildGumpling(x, y, width, height, hue, entryID, maxCharCount, text);
        }

        TextEntry(AControl parent)
            : base(parent)
        {
            base.HandlesMouseInput = true;
            base.HandlesKeyboardFocus = true;
            IsEditable = true;
            CaretVisible = true;
        }

        void BuildGumpling(int x, int y, int width, int height, int hue, int entryID, int maxCharCount, string text)
        {
            Position = new Point(x, y);
            Size = new Point(width, height);
            Hue = HoveredColor = FocusedColor = hue;
            EntryID = entryID;
            Text = text == null ? string.Empty : text;
            MaxCharCount = maxCharCount;
            _CaratBlinkOn = false;
            _RenderedText = new RenderedText(string.Empty, 2048, true);
            _RenderedCarat = new RenderedText(string.Empty, 16, true);
        }

        // ============================================================================================================
        // Update and Draw
        // ============================================================================================================

        public override void Update(double totalMS, double frameMS)
        {
            if (UserInterface.KeyboardFocusControl == this)
            {
                // if we're not already focused, turn the carat on immediately.
                if (!_IsFocused)
                {
                    _IsFocused = true;
                    _CaratBlinkOn = true;
                    _MSSinceLastCaratBlink = 0f;
                    _currentHue = FocusedColor;
                }
                // if we're using the legacy carat, keep it visible. Else blink it every x seconds.
                if (LegacyCarat)
                {
                    _CaratBlinkOn = true;
                }
                else
                {
                    _MSSinceLastCaratBlink += (float)frameMS;
                    if (_MSSinceLastCaratBlink >= MSBetweenCaratBlinks)
                    {
                        _MSSinceLastCaratBlink -= MSBetweenCaratBlinks;
                        if (_CaratBlinkOn == true)
                        {
                            _CaratBlinkOn = false;
                        }
                        else
                        {
                            _CaratBlinkOn = true;
                        }
                    }
                }
            }
            else
            {
                _currentHue = Hue;
                _IsFocused = false;
                _CaratBlinkOn = false;
            }
            string text = Text == null ? null : (IsPasswordField ? new string('*', Text.Length) : Text);
            _RenderedText.Text = $"{LeadingHtmlTag}{LeadingText}{text}";
            _RenderedCarat.Text = $"{LeadingHtmlTag}{(LegacyCarat ? "_" : "|")}";
            base.Update(totalMS, frameMS);
        }

        public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
        {
            Point caratPosition = new Point(position.X, position.Y);
            if (IsEditable)
            {
                if (_RenderedText.Width + _RenderedCarat.Width <= Width)
                {
                    _RenderedText.Draw(spriteBatch, position, Utility.GetHueVector(_currentHue));
                    caratPosition.X += _RenderedText.Width;
                }
                else
                {
                    int textOffset = _RenderedText.Width - (Width - _RenderedCarat.Width);
                    _RenderedText.Draw(spriteBatch, new Rectangle(position.X, position.Y, _RenderedText.Width - textOffset, _RenderedText.Height), textOffset, 0, Utility.GetHueVector(_currentHue));
                    caratPosition.X += (Width - _RenderedCarat.Width);
                }
            }
            else
            {
                caratPosition.X = 0;
                _RenderedText.Draw(spriteBatch, new Rectangle(position.X, position.Y, Width, Height), 0, 0, Utility.GetHueVector(_currentHue));
            }

            if (_CaratBlinkOn && CaretVisible)
            {
                _RenderedCarat.Draw(spriteBatch, caratPosition, Utility.GetHueVector(_currentHue));
            }
            base.Draw(spriteBatch, position, frameMS);
        }

        // ============================================================================================================
        // Input
        // ============================================================================================================

        protected override void OnKeyboardInput(InputEventKeyboard e)
        {
            switch (e.KeyCode)
            {
                case WinKeys.Tab:
                    Parent.KeyboardTabToNextFocus(this);
                    break;
                case WinKeys.Enter:
                    Parent.OnKeyboardReturn(EntryID, Text);
                    break;
                case WinKeys.Back:
                    if (ReplaceDefaultTextOnFirstKeypress)
                    {
                        Text = string.Empty;
                        ReplaceDefaultTextOnFirstKeypress = false;
                    }
                    else if (!string.IsNullOrEmpty(Text))
                    {
                        int escapedLength;
                        if (EscapeCharacters.TryFindEscapeCharacterBackwards(Text, Text.Length - 1, out escapedLength))
                        {
                            Text = Text.Substring(0, Text.Length - escapedLength);
                        }
                        else
                        {
                            Text = Text.Substring(0, Text.Length - 1);
                        }
                    }
                    break;
                default:
                    // place a char, so long as it's within the char count limit.
                    if (MaxCharCount != 0 && Text.Length >= MaxCharCount)
                    {
                        return;
                    }
                    if (NumericOnly && !char.IsNumber(e.KeyChar))
                    {
                        return;
                    }
                    if (ReplaceDefaultTextOnFirstKeypress)
                    {
                        Text = string.Empty;
                        ReplaceDefaultTextOnFirstKeypress = false;
                    }
                    if (e.IsChar && e.KeyChar >= 32)
                    {
                        string escapedCharacter;
                        if (EscapeCharacters.TryMatchChar(e.KeyChar, out escapedCharacter))
                        {
                            Text += escapedCharacter;
                        }
                        else
                        {
                            Text += e.KeyChar;
                        }
                    }
                    break;
            }
        }

        protected override void OnMouseOver(int x, int y)
        {
            if (_currentHue != HoveredColor && !_IsFocused)
                _currentHue = HoveredColor;
        }
    }
}