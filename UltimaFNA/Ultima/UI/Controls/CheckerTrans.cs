﻿/***************************************************************************
 *   CheckerTrans.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.UI;
using UltimaFNA.Ultima.Resources;

namespace UltimaXNA.Ultima.UI.Controls
{
    class CheckerTrans : AControl
    {
        private static Texture2D s_CheckeredTransTexture;
        public static Texture2D CheckeredTransTexture
        {
            get
            {
                if (s_CheckeredTransTexture == null)
                {

                    ushort[] data = new ushort[32 * 32];
                    for (int h = 0; h < 32; h++)
                    {
                        int i = h % 2;
                        for (int w = 0; w < 32; w++)
                        {

                             if (i++ >= 1)
                             {
                                 data[h * 32 + w] = 0x8000;
                                 i = 0;
                             }
                             else
                             {
                                 data[h * 32 + w] = 0x0000;
                             }
                        }
                    }
                    SpriteBatchUI sb = Service.Get<SpriteBatchUI>();
                    s_CheckeredTransTexture = new Texture2D(sb.GraphicsDevice, 32, 32, false, SurfaceFormat.Color);
                    s_CheckeredTransTexture.SetData(TextureConversion.Bgra5551ToArgb32(32,32, data));

                   /* Color[] colors = new Color[32 * 32];
                    for (int h = 0; h < 32; h++)
                    {
                        int i = h % 2;
                        for (int w = 0; w < 32; w++)
                        {

                            if (i++ >= 1)
                            {
                                colors[h * 32 + w] = Color.Black * 0.8f;
                                i = 0;
                            }
                            else
                            {
                                colors[h * 32 + w] = Color.Black * 0.8f;
                            }
                        }
                    }

                    s_CheckeredTransTexture.SetData(colors);*/

                }
                return s_CheckeredTransTexture;
            }
        }

        CheckerTrans(AControl parent)
            : base(parent)
        {

        }

        public CheckerTrans(AControl parent, string[] arguements)
            : this(parent)
        {
            int x, y, width, height;
            x = Int32.Parse(arguements[1]);
            y = Int32.Parse(arguements[2]);
            width = Int32.Parse(arguements[3]);
            height = Int32.Parse(arguements[4]);

            BuildGumpling(x, y, width, height);
        }

        public CheckerTrans(AControl parent, int x, int y, int width, int height)
            : this(parent)
        {
            BuildGumpling(x, y, width, height);
        }

        void BuildGumpling(int x, int y, int width, int height)
        {
            Position = new Point(x, y);
            Size = new Point(width, height);
        }

        public override void Update(double totalMS, double frameMS)
        {
            base.Update(totalMS, frameMS);
        }

        public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
        {
            spriteBatch.Draw2DTiled(CheckeredTransTexture, new Rectangle(position.X, position.Y, Width, Height), Vector3.Zero);
            base.Draw(spriteBatch, position, frameMS);
        }
    }
}
