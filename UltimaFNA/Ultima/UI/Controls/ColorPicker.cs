﻿/***************************************************************************
 *   ColorPicker.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.Resources;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.Resources;
#endregion

namespace UltimaXNA.Ultima.UI.Controls
{
    class ColorPicker : AControl
    {
        protected Texture2D _huesTexture;
        protected Texture2D _selectedIndicator;
        protected Rectangle _openArea;

        protected int _hueWidth, _hueHeight;
        protected int[] _hues;

        protected ColorPicker _ChildColorPicker;

        public int Index
        {
            get;
            set;
        }

        public bool IsChild;
        public ColorPicker ParentColorPicker;

        public int HueValue
        {
            get { return _hues[Index]; }
            set
            {
                for (int i = 0; i < _hues.Length; i++)
                {
                    if (value == _hues[i])
                    {
                        Index = i;
                        break;
                    }
                }
            }
        }

        ColorPicker(AControl parent)
            : base(parent)
        {
            HandlesMouseInput = true;
        }

        public ColorPicker(AControl parent, Rectangle area, int swatchWidth, int swatchHeight, int[] hues)
            : this(parent)
        {
            BuildGumpling(area, swatchWidth, swatchHeight, hues);
        }

        public ColorPicker(AControl parent, Rectangle closedArea, Rectangle openArea, int swatchWidth, int swatchHeight, int[] hues)
            : this(parent)
        {
            _openArea = openArea;
            BuildGumpling(closedArea, swatchWidth, swatchHeight, hues);
        }

        void BuildGumpling(Rectangle area, int swatchWidth, int swatchHeight, int[] hues)
        {
            _hueWidth = swatchWidth;
            _hueHeight = swatchHeight;
            Position = new Point(area.X, area.Y);
            Size = new Point(area.Width, area.Height);
            _hues = hues;
            Index = 0;
        }

        protected override void OnInitialize()
        {
            if (_huesTexture == null)
            {
                if (IsChild) // is a child
                {
                    IResourceProvider provider = Service.Get<IResourceProvider>();
                    _huesTexture = HueData.CreateHueSwatch(_hueWidth, _hueHeight, _hues);
                    _selectedIndicator = provider.GetUITexture(6000);
                }
                else
                {
                    _huesTexture = HueData.CreateHueSwatch(1, 1, new int[1] { _hues[Index] });
                }
            }
        }

        public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
        {
            spriteBatch.Draw2D(_huesTexture, new Rectangle(position.X, position.Y, Width, Height), Vector3.Zero);
            if (IsChild && IsMouseOver)
            {
                spriteBatch.Draw2D(_selectedIndicator, new Vector3(
                    (int)(position.X + (float)(Width / _hueWidth) * ((Index % _hueWidth) + 0.5f) - _selectedIndicator.Width / 2),
                    (int)(position.Y + (float)(Height / _hueHeight) * ((Index / _hueWidth) + 0.5f) - _selectedIndicator.Height / 2),
                    0), Vector3.Zero);
            }
            base.Draw(spriteBatch, position, frameMS);
        }

        protected override void OnMouseClick(int x, int y, MouseButton button)
        {
            if (IsChild) // is a child
            {
                ParentColorPicker.Index = this.Index;
                ParentColorPicker.CloseChildPicker();
            }
            else
            {
                if (_ChildColorPicker == null)
                {
                    _ChildColorPicker = new ColorPicker(this.Parent, _openArea, _hueWidth, _hueHeight, _hues);
                    _ChildColorPicker.IsChild = true;
                    _ChildColorPicker.ParentColorPicker = this;
                    Parent.AddControl(_ChildColorPicker, this.Page);
                }
                else
                {
                    _ChildColorPicker.Dispose();
                    _ChildColorPicker = null;
                }

            }
        }

        protected override void OnMouseOver(int x, int y)
        {
            if (IsChild)
            {
                int clickRow = x / (Width / _hueWidth);
                int clickColumn = y / (Height / _hueHeight);
                ParentColorPicker.Index = Index = clickRow + clickColumn * _hueWidth;
            }
        }

        protected override void OnMouseOut(int x, int y)
        {
        }

        protected void CloseChildPicker()
        {
            if (_ChildColorPicker != null)
            {
                _ChildColorPicker.Dispose();
                _ChildColorPicker = null;
                _huesTexture = HueData.CreateHueSwatch(1, 1, new int[1] { _hues[Index] });
            }
        }
    }
}
