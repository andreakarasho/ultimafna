﻿/***************************************************************************
 *   TextLabelAscii.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using Microsoft.Xna.Framework;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.UI;
#endregion

namespace UltimaXNA.Ultima.UI.Controls
{
    class TextLabelAscii : AControl
    {
        public int Hue;
        public int FontID;

        private readonly RenderedText _Rendered;
        private string _Text;
        private int _Width;

        public string Text
        {
            get
            {
                return _Text;
            }
            set
            {
                if (_Text != value)
                {
                    _Text = value;
                    _Rendered.Text = string.Format("<span style=\"font-family:ascii{0}\">{1}", FontID, _Text);
                }
            }
        }

        public TextLabelAscii(AControl parent, int width = 400)
            : base(parent)
        {
            _Width = width;
            _Rendered = new RenderedText(string.Empty, _Width, true);
        }

        public TextLabelAscii(AControl parent, int x, int y, int font, int hue, string text, int width = 400)
            : this(parent, width)
        {
            BuildGumpling(x, y, font, hue, text);
        }

        void BuildGumpling(int x, int y, int font, int hue, string text)
        {
            Position = new Point(x, y);
            Hue = hue;
            FontID = font;
            Text = text;
        }

        public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
        {
            _Rendered.Draw(spriteBatch, position, Utility.GetHueVector(Hue, true, false, true));
            base.Draw(spriteBatch, position, frameMS);
        }
    }
}
