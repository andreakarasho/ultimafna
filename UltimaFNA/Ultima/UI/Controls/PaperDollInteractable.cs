﻿/***************************************************************************
 *   PaperDollInteractable.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using Microsoft.Xna.Framework;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.World;
using UltimaXNA.Core.Network;
using UltimaXNA.Ultima.Network.Client;
using UltimaXNA.Ultima.World.Entities;
using UltimaXNA.Ultima.World.Entities.Items;
using UltimaXNA.Ultima.World.Entities.Items.Containers;
using UltimaXNA.Ultima.World.Entities.Mobiles;
using UltimaXNA.Ultima.Data;
using UltimaFNA.Core.Network.SocketAsync;
#endregion

namespace UltimaXNA.Ultima.UI.Controls
{
    class PaperDollInteractable : Gump
    {
        bool _isFemale;
        Races _race;
        GumpPicBackpack _Backpack;

        WorldModel _World;

        public PaperDollInteractable(AControl parent, int x, int y, Mobile sourceEntity)
            : base(0, 0)
        {
            Parent = parent;
            Position = new Point(x, y);
            _isFemale = sourceEntity.Flags.IsFemale;
            SourceEntity = sourceEntity;
            _World = Service.Get<WorldModel>();
        }

        public override void Dispose()
        {
            _sourceEntity.ClearCallBacks(OnEntityUpdated, OnEntityDisposed);
            if (_Backpack != null)
            {
                _Backpack.MouseDoubleClickEvent -= On_Dblclick_Backpack;
            }
            base.Dispose();
        }

        public override void Update(double totalMS, double frameMS)
        {
            base.Update(totalMS, frameMS);
        }

        private int GetBody()
        {
            if (_sourceEntity is Mobile)
            {
                bool isfemale = ((Mobile)_sourceEntity).Flags.IsFemale;
                switch ((_sourceEntity as Mobile).Race)
                {
                    default:
                    case Races.Human: return isfemale ? 0x0D : 0x0C;
                    case Races.Elf: return isfemale ? 0x0F : 0x0E;
                    case Races.Gargoyle: return isfemale ? 0x299 : 0x29A;
                }
            }
            return 0;
        }

        void OnEntityUpdated(AEntity entity)
        {
            ClearControls();

            // Add the base gump - the semi-naked paper doll.            
            int bodyID = GetBody(); // ((Mobile)_sourceEntity).BodyID;
            GumpPic paperdoll = (GumpPic)AddControl(new GumpPic(this, 0, 0, bodyID, ((Mobile)_sourceEntity).Hue));
            paperdoll.HandlesMouseInput = true;
            paperdoll.IsPaperdoll = true;
            
            // Loop through the items on the mobile and create the gump pics.
            for (int i = 0; i < s_DrawOrder.Length; i++)
            {
                Item item = ((Mobile)_sourceEntity).GetItem((int)s_DrawOrder[i]);
                if (item == null)
                    continue;

                bool canPickUp = true;
                switch (s_DrawOrder[i])
                {
                    case PaperDollEquipSlots.FacialHair:
                    case PaperDollEquipSlots.Hair:
                        canPickUp = false;
                        break;
                    default:
                        break;
                }

                AddControl(new ItemGumplingPaperdoll(this, 0, 0, item));
                ((ItemGumplingPaperdoll)LastControl).SlotIndex = (int)i;
                ((ItemGumplingPaperdoll)LastControl).IsFemale = _isFemale;
                ((ItemGumplingPaperdoll)LastControl).CanPickUp = canPickUp;
            }
            // If this object has a backpack, add it last.
            if (((Mobile)_sourceEntity).GetItem((int)PaperDollEquipSlots.Backpack) != null)
            {
                Item backpack = ((Mobile)_sourceEntity).GetItem((int)PaperDollEquipSlots.Backpack);
                AddControl(_Backpack = new GumpPicBackpack(this, -7, 0, backpack));
                _Backpack.HandlesMouseInput = true;
                _Backpack.MouseDoubleClickEvent += On_Dblclick_Backpack;
            }

			// Character Profile button
			AddControl(new GumpPic(this, 27, 195-21, 2002, 0));
			LastControl.MouseDoubleClickEvent += On_Dblclick_Profile;
		}

        void OnEntityDisposed(AEntity entity)
        {
            Dispose();
		}
		
		void On_Dblclick_Profile(AControl control, int x, int y, MouseButton button)
		{
			if (button == MouseButton.Left)
			{
				Service.Get<NetworkClient>().Send(new RequestCharProfile(_sourceEntity.Serial, false));
			}
		}
		void On_Dblclick_Backpack(AControl control, int x, int y, MouseButton button)
        {
            ContainerItem backpack = ((Mobile)_sourceEntity).Backpack;
            _World.Interaction.DoubleClick(backpack);
        }

        AEntity _sourceEntity;
        public AEntity SourceEntity
        {
            set
            {
                if (value != _sourceEntity)
                {
                    if (_sourceEntity != null)
                    {
                        _sourceEntity.ClearCallBacks(OnEntityUpdated, OnEntityDisposed);
                        _sourceEntity = null;
                    }
                    if (value is Mobile)
                    {
                        _sourceEntity = value;
                        // update the gump
                        OnEntityUpdated(_sourceEntity);
                        // if the entity changes in the future, update the gump again
                        _sourceEntity.SetCallbacks(OnEntityUpdated, OnEntityDisposed);
                    }
                }
            }
            get
            {
                return _sourceEntity;
            }
        }

        enum PaperDollEquipSlots
        {
            Body = 0,
            RightHand = 1,
            LeftHand = 2,
            Footwear = 3,
            Legging = 4,
            Shirt = 5,
            Head = 6,
            Gloves = 7,
            Ring = 8,
            Talisman = 9,
            Neck = 10,
            Hair = 11,
            Belt = 12,
            Chest = 13,
            Bracelet = 14,
            Unused = 15,
            FacialHair = 16,
            Sash = 17,
            Earring = 18,
            Sleeves = 19,
            Back = 20,
            Backpack = 21,
            Robe = 22,
            Skirt = 23,
            // skip 24, inner legs (!!! do we really skip this?)
        }

        static PaperDollEquipSlots[] s_DrawOrder = {
            PaperDollEquipSlots.Footwear,
            PaperDollEquipSlots.Legging,
            PaperDollEquipSlots.Shirt,
            PaperDollEquipSlots.Sleeves,
			PaperDollEquipSlots.Ring,
			PaperDollEquipSlots.Gloves,
            PaperDollEquipSlots.Neck,
            PaperDollEquipSlots.Belt,
            PaperDollEquipSlots.Chest,
            PaperDollEquipSlots.Bracelet,
            PaperDollEquipSlots.Hair,
            PaperDollEquipSlots.FacialHair,
            PaperDollEquipSlots.Head,
			PaperDollEquipSlots.Robe,
			PaperDollEquipSlots.Sash,
            PaperDollEquipSlots.Earring,
            PaperDollEquipSlots.Back,
            PaperDollEquipSlots.Skirt,         
            PaperDollEquipSlots.LeftHand,
            PaperDollEquipSlots.RightHand,
			PaperDollEquipSlots.Talisman
		};
    }
}
