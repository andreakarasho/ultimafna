﻿/***************************************************************************
 *   TilePic.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Resources;
using UltimaXNA.Core.UI;

namespace UltimaXNA.Ultima.UI.Controls
{
    /// <summary>
    /// A gump that shows a static item.
    /// </summary>
    class StaticPic : AControl
    {
        Texture2D _texture;
        int Hue;
        int _tileID;

        StaticPic(AControl parent)
            : base(parent)
        {

        }

        public StaticPic(AControl parent, string[] arguements)
            : this(parent)
        {
            int x, y, tileID, hue = 0;
            x = Int32.Parse(arguements[1]);
            y = Int32.Parse(arguements[2]);
            tileID = Int32.Parse(arguements[3]);
            if (arguements.Length > 4)
            {
                // has a HUE="XXX" arguement!
                hue = Int32.Parse(arguements[4]);
            }
            BuildGumpling(x, y, tileID, hue);
        }

        public StaticPic(AControl parent, int x, int y, int itemID, int hue)
            : this(parent)
        {
            BuildGumpling(x, y, itemID, hue);
        }

        void BuildGumpling(int x, int y, int tileID, int hue)
        {
            Position = new Point(x, y);
            Hue = hue;
            _tileID = tileID;
        }

        public override void Update(double totalMS, double frameMS)
        {
            if (_texture == null)
            {
                IResourceProvider provider = Service.Get<IResourceProvider>();
                _texture = provider.GetItemTexture(_tileID);
                Size = new Point(_texture.Width, _texture.Height);
            }
            base.Update(totalMS, frameMS);
        }

        public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
        {
            spriteBatch.Draw2D(_texture, new Vector3(position.X, position.Y, 0), Utility.GetHueVector(Hue, false, false, true));
            base.Draw(spriteBatch, position, frameMS);
        }
    }
}
