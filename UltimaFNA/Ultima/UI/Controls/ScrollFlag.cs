﻿/***************************************************************************
 *   ScrollFlag.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.Resources;
using UltimaXNA.Core.UI;
#endregion

namespace UltimaXNA.Ultima.UI.Controls
{
    /// <summary>
    /// A base scrollbar with methods that control min, max, and value
    /// </summary>
    class ScrollFlag : AControl, IScrollBar
    {
        // ============================================================================================================
        // Private variables
        // ============================================================================================================
        private Texture2D _GumpSlider;

        private int _SliderExtentTop, _SliderExtentHeight;
        private float _SliderPosition;
        private float _Value;
        private int _Max, _Min;

        private bool _BtnSliderClicked;
        private Point _ClickPosition;

        // ============================================================================================================
        // Public properties
        // ============================================================================================================
        public int Value
        {
            get
            {
                return (int)_Value;
            }
            set
            {
                _Value = value;
                if (_Value < MinValue)
                    _Value = MinValue;
                if (_Value > MaxValue)
                    _Value = MaxValue;
            }
        }

        public int MinValue
        {
            get
            {
                return _Min;
            }
            set
            {
                _Min = value;
                if (_Value < _Min)
                    _Value = _Min;
            }
        }

        public int MaxValue
        {
            get
            {
                return _Max;
            }
            set
            {
                if (value < 0)
                    value = 0;
                _Max = value;
                if (_Value > _Max)
                    _Value = _Max;
            }
        }

        // ============================================================================================================
        // Ctor, Initialize, Update, and Draw
        // ============================================================================================================
        public ScrollFlag(AControl parent)
            : base(parent)
        {
			HandlesMouseInput = true;
        }

        public ScrollFlag(AControl parent, int x, int y, int height, int minValue, int maxValue, int value)
            : this(parent)
        {
            Position = new Point(x, y);
            _SliderExtentTop = y;
            _SliderExtentHeight = height;

            MinValue = minValue;
            MaxValue = maxValue;
            Value = value;
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();

            IResourceProvider provider = Service.Get<IResourceProvider>();
            _GumpSlider = provider.GetUITexture(0x0828);
            Size = new Point(_GumpSlider.Width, _GumpSlider.Height);
        }

        public override void Update(double totalMS, double frameMS)
        {
            base.Update(totalMS, frameMS);
            
            if (MaxValue <= MinValue || MinValue >= MaxValue)
            {
                Value = MaxValue = MinValue;
            }

            _SliderPosition = CalculateSliderYPosition();
        }

        public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
        {
            // draw slider
            if (MaxValue == MinValue)
            {
                // do nothing.
            }
            else
            {
                spriteBatch.Draw2D(_GumpSlider, new Vector3(position.X - 5, position.Y + _SliderPosition, 0), Vector3.Zero);
            }

            base.Draw(spriteBatch, position, frameMS);
        }

        private float CalculateSliderYPosition()
        {
            if (!IsInitialized)
                return 0f;
            if (MaxValue - MinValue == 0)
                return 0f;
            return CalculateScrollableArea() * ((_Value - MinValue) / (MaxValue - MinValue));
        }

        private float CalculateScrollableArea()
        {
            if (!IsInitialized)
                return 0f;
            return Height - _GumpSlider.Height;
        }

        protected override bool IsPointWithinControl(int x, int y)
        {
            x -= 5;
            Rectangle slider = new Rectangle(0, (int)_SliderPosition, _GumpSlider.Width, _GumpSlider.Height);
            return slider.Contains(x, y);
        }

        protected override void OnMouseDown(int x, int y, MouseButton button)
        {
            if (IsPointWithinControl(x, y))
            {
                // clicked on the slider
                _BtnSliderClicked = true;
                _ClickPosition = new Point(x, y);
            }
        }

        protected override void OnMouseUp(int x, int y, MouseButton button)
        {
            _BtnSliderClicked = false;
        }

        protected override void OnMouseOver(int x, int y)
        {
            if (_BtnSliderClicked)
            {
                if (y != _ClickPosition.Y)
                {
                    float sliderY = _SliderPosition + (y - _ClickPosition.Y);

                    if (sliderY < 0)
                        sliderY = 0;

                    float scrollableArea = CalculateScrollableArea();
                    if (sliderY > scrollableArea)
                        sliderY = scrollableArea;

                    _ClickPosition = new Point(x, y);

                    _Value = ((sliderY / scrollableArea) * (float)((MaxValue - MinValue))) + MinValue;
                    _SliderPosition = sliderY;
                }
            }
        }

        public bool PointWithinControl(int x, int y)
        {
            return IsPointWithinControl(x, y);
        }
    }
}
