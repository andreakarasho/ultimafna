﻿/***************************************************************************
 *   HtmlGumpling.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region using
using System;
using Microsoft.Xna.Framework;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.UI;
using UltimaXNA.Core.UI.HTML;
#endregion

namespace UltimaXNA.Ultima.UI.Controls
{
    public class HtmlGumpling : AControl
    {
        // private variables
        private IScrollBar _Scrollbar;
        private RenderedText _RenderedText;
        private bool _IsMouseDown;
        private int _MouseDownHREF = -1;
        private int _MouseOverHREF = -1;
        // public variables
        public int ScrollX;
        public int ScrollY;

        public string Text
        {
            get { return _RenderedText.Text; }
            set { _RenderedText.Text = value; }
        }

        public int Hue
        {
            get;
            set;
        }

        public bool HasBackground
        {
            get;
            set;
        }

        public bool HasScrollbar
        {
            get;
            set;
        }

        public bool UseFlagScrollbar
        {
            get;
            private set;
        }

        public override int Width
        {
            get
            {
                return base.Width;
            }
            set
            {
                base.Width = value;
            }
        }

        private bool _fromGump;
        public HtmlGumpling(AControl parent, string[] arguements, string[] lines)
            : base(parent)
        {
            int x, y, width, height, textIndex, background, scrollbar;
            x = Int32.Parse(arguements[1]);
            y = Int32.Parse(arguements[2]);
            width = Int32.Parse(arguements[3]);
            height = Int32.Parse(arguements[4]);
            textIndex = Int32.Parse(arguements[5]);
            background = Int32.Parse(arguements[6]);
            scrollbar = Int32.Parse(arguements[7]);

            _fromGump = true;
            string html = $"<a href=\"CLICKABLE={textIndex}\" color='#000' hovercolor='#000' style=\"text-decoration: none\">{lines[textIndex]}</a>"; // serve un pacchetto che invia la risposta, ma è valida per tutti sta cosa?
            BuildGumpling(x, y, width, height, background, scrollbar, html);
        }

        public HtmlGumpling(AControl parent, int x, int y, int width, int height, int background, int scrollbar, string text)
            : base(parent)
        {
            BuildGumpling(x, y, width, height, background, scrollbar, text);
        }

        void BuildGumpling(int x, int y, int width, int height, int background, int scrollbar, string text)
        {
            Position = new Point(x, y);
            base.Width = width;
            Size = new Point(width, height);
            HasBackground = (background == 1) ? true : false;
            HasScrollbar = (scrollbar != 0) ? true : false;
            UseFlagScrollbar = (HasScrollbar && scrollbar == 2) ? true : false;
            _RenderedText = new RenderedText(text, width - (HasScrollbar ? 15 : 0) - (HasBackground ? 8 : 0));

            if (HasBackground)
            {
                this.AddControl(new ResizePic(this, 0, 0, 0x2486, Width - (HasScrollbar ? 15 : 0), Height));
                LastControl.HandlesMouseInput = false;
            }

            if (HasScrollbar)
            {
                if (UseFlagScrollbar)
                {
                    AddControl(new ScrollFlag(this));
                    _Scrollbar = LastControl as IScrollBar;
                    _Scrollbar.Position = new Point(Width - 14, 0);
                }
                else
                {
                    AddControl(new ScrollBar(this));
                    _Scrollbar = LastControl as IScrollBar;
                    _Scrollbar.Position = new Point(Width - 14, 0);
                }
                _Scrollbar.Height = Height;
                _Scrollbar.MinValue = 0;
                _Scrollbar.MaxValue = _RenderedText.Height - Height + (HasBackground ? 8 : 0);
                ScrollY = _Scrollbar.Value;
            }

            if (Width != _RenderedText.Width)
                Width = _RenderedText.Width;
        }

        public override void Update(double totalMS, double frameMS)
        {
            _MouseOverHREF = -1; // this value is changed every frame if we mouse over a region.

            HandlesMouseInput = (_RenderedText.Regions.Count > 0);

            if (HasScrollbar)
            {
                _Scrollbar.Height = Height;
                _Scrollbar.MinValue = 0;
                _Scrollbar.MaxValue = _RenderedText.Height - Height + (HasBackground ? 8 : 0);
                ScrollY = _Scrollbar.Value;
            }

            if (Width != _RenderedText.Width)
                Width = _RenderedText.Width;
            base.Update(totalMS, frameMS);
        }

        public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
        {
            base.Draw(spriteBatch, position, frameMS);

            _RenderedText.MouseOverRegionID = _MouseOverHREF;
            _RenderedText.IsMouseDown = _IsMouseDown;
            _RenderedText.Draw(spriteBatch,
                new Rectangle(position.X + (HasBackground ? 4 : 0), position.Y + (HasBackground ? 4 : 0),
                    Width - (HasBackground ? 8 : 0), Height - (HasBackground ? 8 : 0)), ScrollX, ScrollY);
        }

        protected override bool IsPointWithinControl(int x, int y)
        {
            if (HasScrollbar)
            {
                if (_Scrollbar.PointWithinControl(x - _Scrollbar.Position.X,  y - _Scrollbar.Position.Y))
                    return true;
            }

            if (_RenderedText.Regions.Count > 0)
            {
                HtmlLink region = _RenderedText.Regions.RegionfromPoint(new Point(x + ScrollX, y + ScrollY));
                if (region != null)
                {
                    _MouseOverHREF = region.Index;
                    return true;
                }
            }
            return false;
        }

        protected override void OnMouseDown(int x, int y, MouseButton button)
        {
            _IsMouseDown = true;
            _MouseDownHREF = _MouseOverHREF;
        }

        protected override void OnMouseUp(int x, int y, MouseButton button)
        {
            _IsMouseDown = false;
            _MouseDownHREF = -1;
        }

        protected override void OnMouseClick(int x, int y, MouseButton button)
        {
            if (_MouseOverHREF != -1 && _MouseDownHREF == _MouseOverHREF)
            {
                if (button == MouseButton.Left)
                {
                    if (_RenderedText.Regions.Region(_MouseOverHREF).HREF != null)
                        OnHtmlInputEvent(_RenderedText.Regions.Region(_MouseOverHREF).HREF, MouseEvent.Click);
                }
            }
        }

        protected override void OnMouseOver(int x, int y)
        {
            if (_IsMouseDown && _MouseDownHREF != -1 && _MouseDownHREF != _MouseOverHREF)
            {
                OnHtmlInputEvent(_RenderedText.Regions.Region(_MouseDownHREF).HREF, MouseEvent.DragBegin);
            }
        }

        protected override void OnMouseDoubleClick(int x, int y, MouseButton button)
        {
            if (_MouseOverHREF != -1 && _MouseDownHREF == _MouseOverHREF)
            {
                if (button == MouseButton.Left)
                {
                    if (_RenderedText.Regions.Region(_MouseOverHREF).HREF != null)
                        OnHtmlInputEvent(_RenderedText.Regions.Region(_MouseOverHREF).HREF, MouseEvent.DoubleClick);
                }
            }
        }

        public override void OnHtmlInputEvent(string href, MouseEvent e)
        {
            if (_fromGump)
            {
                int charIndex;
                if (href.Length > 10 && href.StartsWith("CLICKABLE="))
                    charIndex = int.Parse(href.Substring(10));
                else
                    return;
                if (e == MouseEvent.Click)
                {
                    if (href.Length > 10 && href.StartsWith("CLICKABLE="))
                    {
                        World.WorldModel world = Service.Get<World.WorldModel>();
                       // world.Client.SendGumpMenuSelect(RootParent.GumpLocalID, RootParent.GumpServerTypeID, charIndex + 10, null, null);

                    }
                }
                return;
            }
            base.OnHtmlInputEvent(href, e);
        }
    }
}
