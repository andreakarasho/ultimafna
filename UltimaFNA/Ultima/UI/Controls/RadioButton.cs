﻿using UltimaXNA.Core.Input;
using UltimaXNA.Core.UI;

namespace UltimaXNA.Ultima.UI.Controls
{
    class RadioButton : CheckBox
    {
        public int GroupIndex
        {
            get;
            protected set;
        }

        public RadioButton(AControl parent, int groupIndex, string[] arguements, string[] lines)
            : base(parent, arguements, lines)
        {
            GroupIndex = groupIndex;
        }

        public RadioButton(AControl parent, int x, int y, int  inactiveID, int activeID, bool initialState, int switchID, int groupIndex): base(parent, x, y, inactiveID, activeID, initialState, switchID)
        {
            GroupIndex = groupIndex;
        }

        public override bool IsChecked { get => base.IsChecked; set { if (value && IsInitialized) HandleClick(); base.IsChecked = value; } }

        private void HandleClick()
        {
            if (Parent != null)
            {
                foreach (AControl control in Parent.Children)
                {
                    if (control is RadioButton && (control as RadioButton).GroupIndex == GroupIndex)
                        (control as RadioButton).IsChecked = false;
                }
            }
        }
        
        protected override void OnMouseClick(int x, int y, MouseButton button)
        {
            HandleClick();
            base.OnMouseClick(x, y, button);
        }
    }
}
