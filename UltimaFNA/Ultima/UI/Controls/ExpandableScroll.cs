﻿/***************************************************************************
 *   ExpandableScroll.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using Microsoft.Xna.Framework;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.UI;
using UltimaXNA.Core.Windows;
#endregion

namespace UltimaXNA.Ultima.UI.Controls
{
	class ExpandableScroll : Gump
	{
		private GumpPic _GumplingTop, _GumplingBottom;
		private GumpPicTiled _GumplingMiddle;
		private Button _GumplingExpander;
		private bool _ShiftPress;

		private int _ExpandableScrollHeight;
		private const int c_ExpandableScrollHeight_Min = 274; // this is the min from the client.
		private const int c_ExpandableScrollHeight_Max = 1000; // arbitrary large number.

		internal bool ShiftPress { get { return _ShiftPress; } }
		private int _GumplingMidY { get { return _GumplingTop.Height; } }
		private int _GumplingMidHeight { get { return _ExpandableScrollHeight - _GumplingTop.Height - _GumplingBottom.Height - (_GumplingExpander != null ? _GumplingExpander.Height : 0); } }
		private int _GumplingBottomY { get { return _ExpandableScrollHeight - _GumplingBottom.Height - (_GumplingExpander != null ? _GumplingExpander.Height : 0); } }
		private int _GumplingExpanderX { get { return (Width - (_GumplingExpander != null ? _GumplingExpander.Width : 0)) / 2; } }
		private int _GumplingExpanderY { get { return _ExpandableScrollHeight - (_GumplingExpander != null ? _GumplingExpander.Height : 0) - c_GumplingExpanderY_Offset; } }
		private const int c_GumplingExpanderY_Offset = 2; // this is the gap between the pixels of the btm Control texture and the height of the btm Control texture.
		private const int c_GumplingExpander_ButtonID = 0x7FBEEF;

		private bool _IsResizable = true;
		private bool _IsExpanding;
		private int _isExpanding_InitialX, _isExpanding_InitialY, _isExpanding_InitialHeight;

		public ExpandableScroll(AControl parent, int x, int y, int height, bool isResizable = true)
			: base(0, 0)
		{
			Parent = parent;
			Position = new Point(x, y);
			_ExpandableScrollHeight = height;
			_IsResizable = isResizable;
			MakeThisADragger();
        }

        protected override void OnInitialize()
		{
			_GumplingTop = (GumpPic)AddControl(new GumpPic(this, 0, 0, 0x0820, 0));
			_GumplingMiddle = (GumpPicTiled)AddControl(new GumpPicTiled(this, 0, 0, 0, 0, 0x0822));
			_GumplingBottom = (GumpPic)AddControl(new GumpPic(this, 0, 0, 0x0823, 0));

			if (_IsResizable)
			{
				_GumplingExpander = (Button)AddControl(new Button(this, 0, 0, 0x082E, 0x82F, ButtonTypes.Activate, 0, c_GumplingExpander_ButtonID));
				_GumplingExpander.MouseDownEvent += expander_OnMouseDown;
				_GumplingExpander.MouseUpEvent += expander_OnMouseUp;
				_GumplingExpander.MouseOverEvent += expander_OnMouseOver;
			}
		}

		public override void Dispose()
		{
			if (_GumplingExpander != null)
			{
				_GumplingExpander.MouseDownEvent -= expander_OnMouseDown;
				_GumplingExpander.MouseUpEvent -= expander_OnMouseUp;
				_GumplingExpander.MouseOverEvent -= expander_OnMouseOver;
				_GumplingExpander.Dispose();
				_GumplingExpander = null;
			}
			base.Dispose();
		}

		protected override bool IsPointWithinControl(int x, int y)
		{
			Point position = new Point(x + ScreenX, y + ScreenY);
			if (_GumplingTop.HitTest(position, true) != null)
				return true;
			if (_GumplingMiddle.HitTest(position, true) != null)
				return true;
			if (_GumplingBottom.HitTest(position, true) != null)
				return true;
			if (_IsResizable && _GumplingExpander.HitTest(position, true) != null)
				return true;
			return false;
		}

		public override void Update(double totalMS, double frameMS)
		{
			if (_ExpandableScrollHeight < c_ExpandableScrollHeight_Min)
				_ExpandableScrollHeight = c_ExpandableScrollHeight_Min;
			if (_ExpandableScrollHeight > c_ExpandableScrollHeight_Max)
				_ExpandableScrollHeight = c_ExpandableScrollHeight_Max;

			if (_gumplingTitleGumpIDDelta)
			{
				_gumplingTitleGumpIDDelta = false;
				if (_gumplingTitle != null)
					_gumplingTitle.Dispose();
				_gumplingTitle = (GumpPic)AddControl(new GumpPic(this, 0, 0, _gumplingTitleGumpID, 0));
			}

			if (!_GumplingTop.IsInitialized)
			{
				IsVisible = false;
			}
			else
			{
				IsVisible = true;
				_GumplingTop.Position = new Point(0, 0);

				_GumplingMiddle.Position = new Point(17, _GumplingMidY);
				_GumplingMiddle.Width = 263;
				_GumplingMiddle.Height = _GumplingMidHeight;

				_GumplingBottom.Position = new Point(17, _GumplingBottomY);

				if (_IsResizable)
					_GumplingExpander.Position = new Point(_GumplingExpanderX, _GumplingExpanderY);

				if (_gumplingTitle != null && _gumplingTitle.IsInitialized)
				{
					_gumplingTitle.Position = new Point(
						(_GumplingTop.Width - _gumplingTitle.Width) / 2,
						(_GumplingTop.Height - _gumplingTitle.Height) / 2);
				}
			}

			base.Update(totalMS, frameMS);
		}

		public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
		{
			base.Draw(spriteBatch, position, frameMS);
		}

		void expander_OnMouseDown(AControl control, int x, int y, MouseButton button)
		{
			y += _GumplingExpander.Y + ScreenY - Y;
			if (button == MouseButton.Left)
			{
				_IsExpanding = true;
				_isExpanding_InitialHeight = _ExpandableScrollHeight;
				_isExpanding_InitialX = x;
				_isExpanding_InitialY = y;
			}
		}

		void expander_OnMouseUp(AControl control, int x, int y, MouseButton button)
		{
			y += _GumplingExpander.Y + ScreenY - Y;
			if (_IsExpanding)
			{
				_IsExpanding = false;
				_ExpandableScrollHeight = _isExpanding_InitialHeight + (y - _isExpanding_InitialY);
			}
		}

		void expander_OnMouseOver(AControl control, int x, int y)
		{
			y += _GumplingExpander.Y + ScreenY - Y;
			if (_IsExpanding && (y != _isExpanding_InitialY))
			{
				_ExpandableScrollHeight = _isExpanding_InitialHeight + (y - _isExpanding_InitialY);
			}
		}

		bool _gumplingTitleGumpIDDelta;
		int _gumplingTitleGumpID;
		GumpPic _gumplingTitle;
		public int TitleGumpID { set { _gumplingTitleGumpID = value; _gumplingTitleGumpIDDelta = true; } }

		
	}
}
