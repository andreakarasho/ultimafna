﻿/***************************************************************************
 *   MacroDropDownList.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using Microsoft.Xna.Framework;
using System.Collections.Generic;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.Resources;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.Input;

namespace UltimaXNA.Ultima.UI.Controls
{
    class MacroDropDownList : AControl
    {
        public int Index;

        public GumpPic ScrollButton;
        public ResizePic _ResizePic;

        public bool IsFirstvisible = true;
        public List<string> Items;

        private readonly int _Width;
        private bool _CanBeNull;
        private bool _IsListOpen;
        private int _visibleItems = -1;

        private TextLabelAscii _label;
        private ResizePic _openResizePic;
        private ScrollBar _openScrollBar;
        private TextLabelAscii[] _openLabels;

        private const int hue_Text = 1107;
        private const int hue_TextSelected = 588;

        private readonly IFont _Font;

        public MacroDropDownList(AControl parent, int x, int y, int width, string[] items, int itemsVisible, int index, bool canBeNull, int ID, bool firstVisible)
                : base(parent)
        {
            _Font = Service.Get<IResourceProvider>().GetAsciiFont(1);

            GumpLocalID = ID;
            Position = new Point(x, y);
            Items = new List<string>(items);
            _Width = width;
            Index = index;
            _visibleItems = itemsVisible;
            _CanBeNull = canBeNull;
            IsFirstvisible = firstVisible;//hide creating control

            if (IsFirstvisible)//for fill action dropdownlist
                CreateVisual();

            HandlesMouseInput = true;
            
        }

        public void CreateVisual()
        {
            if (_ResizePic != null || _label != null || ScrollButton != null)
                return;

            _ResizePic = (ResizePic)AddControl(new ResizePic(this, 0, 0, 3000, _Width, _Font.Height + 8), 0);
            _ResizePic.GumpLocalID = GumpLocalID;
            _ResizePic.MouseClickEvent += onClickClosedList;
            _ResizePic.MouseOverEvent += onMouseOverClosedList;
            _ResizePic.MouseOutEvent += onMouseOutClosedList;
            _ResizePic.IsEnabled = false;
            _label = (TextLabelAscii)AddControl(new TextLabelAscii(this, 4, 5, 1, hue_Text, string.Empty), 0);
            _label.GumpLocalID = GumpLocalID;
            ScrollButton = (GumpPic)AddControl(new GumpPic(this, _Width - 22, 5, 2086, 0), 0);

            IsFirstvisible = true;//for invisible create control
        }

        public override void Dispose()
        {
            if (_ResizePic != null)
            {
                _ResizePic.MouseClickEvent -= onClickClosedList;
                _ResizePic.MouseOverEvent -= onMouseOverClosedList;
                _ResizePic.MouseOutEvent -= onMouseOutClosedList;
            }
            base.Dispose();
        }

        public override void Update(double totalMS, double frameMS)
        {
            if (Index < 0 || Index >= Items.Count)
                Index = -1;

            if (_IsListOpen)
            {
                // if we have moused off the open list, close it. We check to see if the mouse is over:
                // the resizepic for the closed list (because it takes one update cycle to open the list)
                // the resizepic for the open list, and the scroll bar if it is loaded.
                if (UserInterface.MouseOverControl != _openResizePic &&
                    UserInterface.MouseOverControl != _ResizePic &&
                    (_openScrollBar != null && UserInterface.MouseOverControl != _openScrollBar))
                {
                    closeOpenList();
                }
                else
                {
                    // update the visible items
                    int itemOffset = (_openScrollBar == null ? 0 : _openScrollBar.Value);
                    if (Items.Count != 0)
                    {
                        for (int i = 0; i < _visibleItems; i++)
                        {
                            _openLabels[i].Text = (i + itemOffset < 0) ? string.Empty : Items[i + itemOffset];
                        }
                    }
                }
            }
            else if (IsFirstvisible)//for create hide control
            {
                if (Index == -1)
                {
                    if (Items.Count > 0)
                        _label.Text = Items[0];
                    else
                        _label.Text = "";
                }
                else
                {
                    _label.Text = Items[Index];
                }
            }
            base.Update(totalMS, frameMS);
        }
        
        private void closeOpenList()
        {
            _IsListOpen = false;
            if (_openResizePic != null)
            {
                _openResizePic.MouseClickEvent -= onClickOpenList;
                _openResizePic.MouseOverEvent -= onMouseOverOpenList;
                _openResizePic.MouseOutEvent -= onMouseOutOpenList;
                _openResizePic.Dispose();
                _openResizePic = null;
            }
            if (_openScrollBar != null)
                _openScrollBar.Dispose();
            for (int i = 0; i < _visibleItems; i++)
                _openLabels[i].Dispose();
        }

        private void onClickClosedList(AControl control, int x, int y, MouseButton button)
        {
            if (Items.Count > 0)
            {
                _IsListOpen = true;
                _openResizePic = new ResizePic(Parent, X, Y, 3000, _Width, _Font.Height * _visibleItems + 8);
                _openResizePic.GumpLocalID = GumpLocalID;
                _openResizePic.MouseClickEvent += onClickOpenList;
                _openResizePic.MouseOverEvent += onMouseOverOpenList;
                _openResizePic.MouseOutEvent += onMouseOutOpenList;
                ((Gump)Parent).AddControl(_openResizePic, Page);

                if (_visibleItems > Items.Count)
                {
                    _visibleItems = Items.Count;
                }

                // only show the scrollbar if we need to scroll
                if (_visibleItems < Items.Count)
                {
                    _openScrollBar = new ScrollBar(Parent, X + _Width - 20, Y + 4, _Font.Height * _visibleItems, (_CanBeNull ? -1 : 0), Items.Count - _visibleItems, Index);
                    ((Gump)Parent).AddControl(_openScrollBar, Page);
                }
                _openLabels = new TextLabelAscii[_visibleItems];
                for (int i = 0; i < _visibleItems; i++)
                {
                    _openLabels[i] = new TextLabelAscii(Parent, X + 4, Y + 5 + _Font.Height * i, 1, 1106, string.Empty);
                    ((Gump)Parent).AddControl(_openLabels[i], Page);
                }
            }
        }

        private void onMouseOverClosedList(AControl control, int x, int y)
        {
            _label.Hue = hue_TextSelected;
        }

        private void onMouseOutClosedList(AControl control, int x, int y)
        {
            _label.Hue = hue_Text;
        }

        public void setIndex(int macroType, int valueID)
        {
            ScrollButton.IsVisible = true;
            IsVisible = true;
            Items.Clear();
            Index = -1;

            switch ((MacroType)macroType)
            {
                case MacroType.UseSkill:
                    foreach (MacroDefinition def in Macros.Skills)
                        Items.Add(def.Name);
                    break;

                case MacroType.CastSpell:
                    foreach (MacroDefinition def in Macros.Spells)
                        Items.Add(def.Name);
                    break;

                case MacroType.OpenGump:
                case MacroType.CloseGump:
                    foreach (MacroDefinition def in Macros.Gumps)
                        Items.Add(def.Name);
                    break;

                case MacroType.Move:
                    foreach (MacroDefinition def in Macros.Moves)
                        Items.Add(def.Name);
                    break;

                case MacroType.ArmDisarm:
                    foreach (MacroDefinition def in Macros.ArmDisarms)
                        Items.Add(def.Name);
                    break;

                case MacroType.Say:
                case MacroType.Emote:
                case MacroType.Whisper:
                case MacroType.Yell:
                    ScrollButton.IsVisible = false;
                    break;

                default:
                    // no sub-ids for these types
                    ScrollButton.IsVisible = false;
                    IsVisible = false;
                    break;
            }
            Index = valueID;
        }

        private void onClickOpenList(AControl control, int x, int y, MouseButton button)
        {
            //for macro options
            int id = control.GumpLocalID;
            int controlValueIndex = -1;
            for (int i = 0; i < Parent.Children.Count; i++)
            {
                if (Parent.Children[i].GumpLocalID == (id + 1000))
                {
                    controlValueIndex = i;
                    break;
                }
            }

            int indexOver = getOpenListIndexFromPoint(x, y);
            if (indexOver != -1)
                Index = indexOver + (_openScrollBar == null ? 0 : _openScrollBar.Value);
            closeOpenList();

            //for macro options
            if (controlValueIndex != -1)
            {
                if (indexOver == -1)
                    return;

                MacroType mType = Macros.Types[Index].Type; 
                
                // background image:
                if (!(Parent.Children[controlValueIndex] as MacroDropDownList).IsFirstvisible)
                    (Parent.Children[controlValueIndex] as MacroDropDownList).CreateVisual();

                // clear and show dropdown/text entry:
                (Parent.Children[controlValueIndex] as MacroDropDownList).Items.Clear();
                (Parent.Children[controlValueIndex] as MacroDropDownList).ScrollButton.IsVisible = true;//easy way for visible dropdown list
                (Parent.Children[controlValueIndex] as MacroDropDownList).IsVisible = true;//easy way for visible dropdown list
                (Parent.Children[controlValueIndex + 1] as TextEntry).Text = string.Empty;
                
                
                switch (mType)
                {
                    case MacroType.UseSkill:
                        foreach (MacroDefinition def in Macros.Skills)
                            (Parent.Children[controlValueIndex] as MacroDropDownList).Items.Add(def.Name);
                        (Parent.Children[controlValueIndex + 1] as TextEntry).IsEditable = false;//textentry disabled because i need dropdownlist
                        break;

                    case MacroType.CastSpell:
                        foreach (MacroDefinition def in Macros.Spells)
                            (Parent.Children[controlValueIndex] as MacroDropDownList).Items.Add(def.Name);
                        (Parent.Children[controlValueIndex + 1] as TextEntry).IsEditable = false;//textentry disabled because i need dropdownlist
                        break;

                    case MacroType.OpenGump:
                    case MacroType.CloseGump:
                        foreach (MacroDefinition def in Macros.Gumps)
                            (Parent.Children[controlValueIndex] as MacroDropDownList).Items.Add(def.Name);
                        (Parent.Children[controlValueIndex + 1] as TextEntry).IsEditable = false;//textentry disabled because i need dropdownlist
                        break;

                    case MacroType.Move:
                        foreach (MacroDefinition def in Macros.Moves)
                            (Parent.Children[controlValueIndex] as MacroDropDownList).Items.Add(def.Name);
                        (Parent.Children[controlValueIndex + 1] as TextEntry).IsEditable = false;//textentry disabled because i need dropdownlist
                        break;

                    case MacroType.ArmDisarm:
                        foreach (MacroDefinition def in Macros.ArmDisarms)
                            (Parent.Children[controlValueIndex] as MacroDropDownList).Items.Add(def.Name);
                        (Parent.Children[controlValueIndex + 1] as TextEntry).IsEditable = false;//textentry disabled because i need dropdownlist
                        break;

                    case MacroType.Say:
                    case MacroType.Emote:
                    case MacroType.Whisper:
                    case MacroType.Yell:
                        //(Parent.Children[controlValueIndex] as MacroDropDownList)._scrollButton.IsVisible = false; //as you wish
                        (Parent.Children[controlValueIndex + 1] as TextEntry).IsEditable = true;//textentry activated
                        break;

                    

                    case MacroType.None:
                        (Parent.Children[controlValueIndex] as MacroDropDownList).ScrollButton.IsVisible = false;//i dont need any control :)
                        (Parent.Children[controlValueIndex + 1] as TextEntry).IsEditable = false;//i dont need any control :)
                        (Parent.Children[controlValueIndex] as MacroDropDownList).IsVisible = false;//i dont need any control :)
                        break;

                    default:
                        //unnecessary
                        break;
                }
            }
        }

        private void onMouseOverOpenList(AControl control, int x, int y)
        {
            int indexOver = getOpenListIndexFromPoint(x, y);
            for (int i = 0; i < _openLabels.Length; i++)
            {
                if (i == indexOver)
                    _openLabels[i].Hue = hue_TextSelected;
                else
                    _openLabels[i].Hue = hue_Text;
            }
        }

        private void onMouseOutOpenList(AControl control, int x, int y)
        {
            for (int i = 0; i < _openLabels.Length; i++)
                _openLabels[i].Hue = hue_Text;
        }

        private int getOpenListIndexFromPoint(int x, int y)
        {
            Rectangle r = new Rectangle(4, 5, _Width - 20, _Font.Height);
            for (int i = 0; i < _openLabels.Length; i++)
            {
                if (r.Contains(new Point(x, y)))
                    return i;
                r.Y += _Font.Height;
            }
            return -1;
        }
    }
}