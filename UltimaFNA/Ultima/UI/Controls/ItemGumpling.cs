﻿/***************************************************************************
 *   ItemGumpling.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.World;
using UltimaXNA.Ultima.World.Entities.Items;
using UltimaXNA.Core.Resources;

namespace UltimaXNA.Ultima.UI.Controls
{   
    class ItemGumpling : AControl
    {
        public bool CanPickUp = true;
        public bool HighlightOnMouseOver = true;

        protected Texture2D _Texture;
        HtmlGumpling _Label;

        bool _ClickedCanDrag;
        float _PickUpTime;
        Point _ClickPoint;
        bool _SendClickIfNoDoubleClick;
        float _SingleClickTime;

        readonly WorldModel _World;

        public Item Item
        {
            get;
            private set;
        }

        public ItemGumpling(AControl parent, Item item)
            : base(parent)
        {
            BuildGumpling(item);
            HandlesMouseInput = true;

            _World = Service.Get<WorldModel>();
        }

        void BuildGumpling(Item item)
        {
            Position = item.InContainerPosition;
            Item = item;
        }

        public override void Dispose()
        {
            UpdateLabel(true);
            base.Dispose();
        }

        public override void Update(double totalMS, double frameMS)
        {
            if (Item.IsDisposed)
            {
                Dispose();
                return;
            }

            if (_ClickedCanDrag && totalMS >= _PickUpTime)
            {
                _ClickedCanDrag = false;
                AttemptPickUp();
            }

            if (_SendClickIfNoDoubleClick && totalMS >= _SingleClickTime)
            {
                _SendClickIfNoDoubleClick = false;
                _World.Interaction.SingleClick(Item);
            }

            UpdateLabel();

            base.Update(totalMS, frameMS);
        }

        public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
        {
            if (_Texture == null)
            {
                IResourceProvider provider = Service.Get<IResourceProvider>();
                _Texture = provider.GetItemTexture(Item.DisplayItemID);
                Size = new Point(_Texture.Width, _Texture.Height);
            }
            Vector3 hue = Utility.GetHueVector(IsMouseOver && HighlightOnMouseOver ? WorldView.MouseOverHue : Item.Hue);
            if (Item.Amount > 1 && Item.ItemData.IsGeneric && Item.DisplayItemID == Item.ItemID)
            {
                int offset = Item.ItemData.StackingOffset;
                spriteBatch.Draw2D(_Texture, new Vector3(position.X - 5, position.Y - 5, 0), hue);
            }
            spriteBatch.Draw2D(_Texture, new Vector3(position.X, position.Y, 0), hue);

            base.Draw(spriteBatch, position, frameMS);
        }

        protected override bool IsPointWithinControl(int x, int y)
        {
            // Allow selection if there is a non-transparent pixel below the mouse cursor or at an offset of
            // (-1,0), (0,-1), (1,0), or (1,1). This will allow selection even when the mouse cursor is directly
            // over a transparent pixel, and will also increase the 'selection space' of an item by one pixel in
            // each dimension - thus a very thin object (2-3 pixels wide) will be increased.
            if (IsPointInTexture(x, y))
            {
                return true;
            }
            if (Item.Amount > 1 && Item.ItemData.IsGeneric)
            {
                int offset = Item.ItemData.StackingOffset;
                if (IsPointInTexture(x + offset, y + offset))
                    return true;
            }
            return false;
        }

        bool IsPointInTexture(int x, int y)
        {
            IResourceProvider provider = Service.Get<IResourceProvider>();
            return provider.IsPointInItemTexture(Item.DisplayItemID, x, y, 1);
        }

        protected override void OnMouseDown(int x, int y, MouseButton button)
        {
            // if click, we wait for a moment before picking it up. This allows a single click.
            _ClickedCanDrag = true;
            float totalMS = (float)Service.Get<UltimaGame>().TotalMS;
            _PickUpTime = totalMS + Settings.UserInterface.Mouse.ClickAndPickupMS;
            _ClickPoint = new Point(x, y);
        }

        protected override void OnMouseUp(int x, int y, MouseButton button)
        {
            _ClickedCanDrag = false;
        }

        protected override void OnMouseOver(int x, int y)
        {
            // if we have not yet picked up the item, AND we've moved more than 3 pixels total away from the original item, pick it up!
            if (_ClickedCanDrag && (Math.Abs(_ClickPoint.X - x) + Math.Abs(_ClickPoint.Y - y) > 3))
            {
                _ClickedCanDrag = false;
                AttemptPickUp();
            }
        }

        protected override void OnMouseClick(int x, int y, MouseButton button)
        {
            if (_ClickedCanDrag)
            {
                _ClickedCanDrag = false;
                _SendClickIfNoDoubleClick = true;
                float totalMS = (float)Service.Get<UltimaGame>().TotalMS;
                _SingleClickTime = totalMS + Settings.UserInterface.Mouse.DoubleClickMS;
            }
        }

        protected override void OnMouseDoubleClick(int x, int y, MouseButton button)
        {
            _World.Interaction.DoubleClick(Item);
            _SendClickIfNoDoubleClick = false;
        }

        protected virtual Point InternalGetPickupOffset(Point offset)
        {
            return offset;
        }

        void AttemptPickUp()
        {
            if (CanPickUp)
            {
                if (this is ItemGumplingPaperdoll)
                {
                    int w, h;
                    IResourceProvider provider = Service.Get<IResourceProvider>();
                    provider.GetItemDimensions(Item.DisplayItemID, out w, out h);
                    Point click_point = new Point(w / 2, h / 2);
                    _World.Interaction.PickupItem(Item, InternalGetPickupOffset(click_point));
                }
                else
                {
                    _World.Interaction.PickupItem(Item, InternalGetPickupOffset(_ClickPoint));
                }
            }
        }

        void UpdateLabel(bool isDisposing = false)
        {
            if (!isDisposing && Item.Overheads.Count > 0)
            {
                if (_Label == null)
                {
                    InputManager input = Service.Get<InputManager>();
                    UserInterface.AddControl(_Label = new HtmlGumpling(null, 0, 0, 200, 32, 0, 0,
                        string.Format("<center><span style='font-family: ascii3;'>{0}</center>", Item.Overheads[0].Text)),
                        input.MousePosition.X - 100, input.MousePosition.Y - 12);
                    _Label.MetaData.Layer = UILayer.Over;
                }
            }
            else if (_Label != null)
            {
                {
                    _Label.Dispose();
                    _Label = null;
                }
            }
        }
    }
}
