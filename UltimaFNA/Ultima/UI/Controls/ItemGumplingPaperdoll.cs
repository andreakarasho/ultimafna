﻿/***************************************************************************
 *   ItemGumplingPaperdoll.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Resources;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.Resources;
using UltimaXNA.Ultima.World.Entities.Items;

namespace UltimaXNA.Ultima.UI.Controls
{
    class ItemGumplingPaperdoll : ItemGumpling
    {
        public int SlotIndex;
        public bool IsFemale;

        int _HueOverride;
        int _GumpIndex;

        public ItemGumplingPaperdoll(AControl parent, int x, int y, Item item)
            : base(parent, item)
        {
            Position = new Point(x, y);
            HighlightOnMouseOver = false;
        }

        protected override Point InternalGetPickupOffset(Point offset)
        {
            return offset;
        }

        public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
        {
            if (_Texture == null)
                CalculateTexture();
            int hue = (Item.Hue == 0 & _HueOverride != 0) ? _HueOverride : Item.Hue;
            spriteBatch.Draw2D(_Texture, new Vector3(position.X, position.Y, 0), Utility.GetHueVector(hue));
            base.Draw(spriteBatch, position, frameMS);
        }

        private void CalculateTexture(bool force = false)
        {
            IResourceProvider provider = Service.Get<IResourceProvider>();
            _GumpIndex = Item.ItemData.Animation + (IsFemale && !force ? 60000 : 50000);
            int indexTranslated, hueTranslated;
            if (GumpDefTranslator.ItemHasGumpTranslation(_GumpIndex, out indexTranslated, out hueTranslated))
            {
                _GumpIndex = indexTranslated;
                _HueOverride = hueTranslated;
            }
            _Texture = provider.GetUITexture(_GumpIndex);
            if (_Texture == null)
                CalculateTexture(true);
            else 
                Size = new Point(_Texture.Width, _Texture.Height);
        }

        protected override bool IsPointWithinControl(int x, int y)
        {
            IResourceProvider provider = Service.Get<IResourceProvider>();
            return provider.IsPointInUITexture(_GumpIndex, x, y);
        }
    }
}
