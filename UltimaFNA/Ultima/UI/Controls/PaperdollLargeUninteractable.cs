﻿/***************************************************************************
 *   PaperdollLargeUninteractable.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using Microsoft.Xna.Framework;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Resources;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.Data;

namespace UltimaXNA.Ultima.UI.Controls
{
    class PaperdollLargeUninteractable : AControl
    {
        public enum EquipSlots
        {
            Body = 0, First = Body,
            RightHand = 1,
            LeftHand = 2,
            Footwear = 3,
            Legging = 4,
            Shirt = 5,
            Head = 5,
            Gloves = 7,
            Ring = 8,
            Talisman = 9,
            Neck = 10,
            Hair = 11,
            Belt = 12,
            Chest = 13,
            Bracelet = 14,
            // skip 15, unused
            FacialHair = 16,
            Sash = 17,
            Earring = 18,
            Sleeves = 19,
            Back = 20,
            Backpack = 21,
            Robe = 22,
            Skirt = 23,
            // skip 24, inner legs (!!! do we really skip this?)
            Max = 23,
        }

        public enum RaceType
        {
            Human,
            Elf,
            Gargoyle
        }

        int[] _equipmentSlots = new int[(int)EquipSlots.Max];
        readonly int[] _hueSlots = new int[(int)EquipSlots.Max];

        bool m_isFemale;
        public int Gender { set { m_isFemale = (value == 1) ? true : false; } }

        private RaceType _race;
        public int Race { set => _race = (RaceType)value; }

        public bool IsCharacterCreation;

        public void SetSlotEquipment(EquipSlots slot, int gumpID)
        {
            _equipmentSlots[(int)slot] = gumpID;
        }
        public void SetSlotHue(EquipSlots slot, int gumpID)
        {
            _hueSlots[(int)slot] = gumpID;
        }

        public int this[EquipSlots slot]
        {
            set { _equipmentSlots[(int)slot] = value; }
            get { return _equipmentSlots[(int)slot]; }
        }

        PaperdollLargeUninteractable(AControl parent)
            : base(parent)
        {

        }

        public PaperdollLargeUninteractable(AControl parent, int x, int y)
            : this(parent)
        {
            Position = new Point(x, y);
        }

        public override void Draw(SpriteBatchUI spriteBatch, Point controlpos, double ms)
        {
            EquipSlots[] slotsToDraw = { EquipSlots.Body, EquipSlots.Footwear, EquipSlots.Legging, EquipSlots.Shirt, EquipSlots.Hair, EquipSlots.FacialHair, EquipSlots.Robe };
            for (int i = 0; i < slotsToDraw.Length; i++)
            {
                int bodyID = 0;
                int hue = hueSlot(slotsToDraw[i]);
                bool hueGreyPixelsOnly = true;

                switch (slotsToDraw[i])
                {
                    case EquipSlots.Body:
                        switch (_race)
                        {
                            case RaceType.Human:
                                bodyID = (m_isFemale ? 1888 : 1889);
                                break;
                            case RaceType.Elf:
                                bodyID = (m_isFemale ? 1893 : 1894);
                                break;
                            case RaceType.Gargoyle:
                                bodyID = (m_isFemale ? 0x76A : 0x76B);
                                break;
                        }
                        break;
                    case EquipSlots.Footwear:
                        if (_race != RaceType.Gargoyle)
                            bodyID = (m_isFemale ? 1891 : 1890);
                        break;
                    case EquipSlots.Legging:
                        if (_race != RaceType.Gargoyle)
                            bodyID = (m_isFemale ? 1892 : 1848);
                        break;
                    case EquipSlots.Shirt:
                        if (_race != RaceType.Gargoyle)
                            bodyID = (m_isFemale ? 1812 : 1849);
                        break;
                    case EquipSlots.Hair:
                        if (equipmentSlot(EquipSlots.Hair) != 0)
                        {
                            switch (_race)
                            {
                                case RaceType.Human:
                                    bodyID = m_isFemale ? HairStyles.FemaleGumpIDForCharacterCreationFromItemID(equipmentSlot(EquipSlots.Hair)) : HairStyles.MaleGumpIDForCharacterCreationFromItemID(equipmentSlot(EquipSlots.Hair));
                                    break;
                                case RaceType.Elf:
                                    bodyID = m_isFemale ? HairStyles.GetFemalElfHairs(equipmentSlot(EquipSlots.Hair)) : HairStyles.GetMaleElfHairs(equipmentSlot(EquipSlots.Hair));
                                    break;
                                case RaceType.Gargoyle:
                                    bodyID = m_isFemale ? HairStyles.GetFemaleGargoyleHairs(equipmentSlot(EquipSlots.Hair)) : HairStyles.GetMaleGargoyleHairs(equipmentSlot(EquipSlots.Hair));
                                    break;
                            }
                            hueGreyPixelsOnly = false;
                        }
                        break;
                    case EquipSlots.FacialHair:
                        if (equipmentSlot(EquipSlots.FacialHair) != 0)
                        {
                            if (_race == RaceType.Human)
                                bodyID = m_isFemale ? 0 : HairStyles.FacialHairGumpIDForCharacterCreationFromItemID(equipmentSlot(EquipSlots.FacialHair));
                            else if (_race == RaceType.Gargoyle)
                                bodyID = m_isFemale ? 0 : HairStyles.GetMaleGargoyleBeards(equipmentSlot(EquipSlots.FacialHair));

                            hueGreyPixelsOnly = false;
                        }
                        break;
                    case EquipSlots.Robe:
                        if (_race == RaceType.Gargoyle)
                            bodyID = m_isFemale ? 0x7A7 : 0x778;
                        break;
                }

                if (bodyID != 0)
                {
                    spriteBatch.Draw2D(Service.Get<IResourceProvider>().GetUITexture(bodyID), new Vector3(controlpos.X, controlpos.Y, 0), Utility.GetHueVector(hue, hueGreyPixelsOnly, false, false));
                }
            }
        }

        int equipmentSlot(EquipSlots slotID)
        {
            return _equipmentSlots[(int)slotID];
        }

        int hueSlot(EquipSlots slotID)
        {
            return _hueSlots[(int)slotID];
        }
    }
}
