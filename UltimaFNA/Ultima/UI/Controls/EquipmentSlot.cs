﻿/***************************************************************************
 *   EquipmentSlot.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using Microsoft.Xna.Framework;
using System;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.Resources;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.World;
using UltimaXNA.Ultima.World.Entities.Items;
using UltimaXNA.Ultima.World.Entities.Mobiles;

namespace UltimaXNA.Ultima.UI.Controls
{
    class EquipmentSlot : AControl
    {
        private readonly WorldModel _World;

        private Mobile _Entity;
        private EquipLayer _EquipLayer;
        private Item _Item;
        private StaticPic _ItemGraphic;

        private bool _ClickedCanDrag;
        private float _PickUpTime;
        private Point _ClickPoint;
        private bool _SendClickIfNoDoubleClick;
        private float _SingleClickTime;

        public EquipmentSlot(AControl parent, int x, int y, Mobile entity, EquipLayer layer)
            : base(parent)
        {
            HandlesMouseInput = true;

            _Entity = entity;
            _EquipLayer = layer;
            
            Position = new Point(x, y);
            AddControl(new GumpPicTiled(this, 0, 0, 19, 20, 0x243A));
            AddControl(new GumpPic(this, 0, 0, 0x2344, 0));

            _World = Service.Get<WorldModel>();
        }

        public override void Update(double totalMS, double frameMS)
        {
            if (_Item != null && _Item.IsDisposed)
            {
                _Item = null;
                _ItemGraphic.Dispose();
                _ItemGraphic = null;
            }

            if (_Item != _Entity.Equipment[(int)_EquipLayer])
            {
                if (_ItemGraphic != null)
                {
                    _ItemGraphic.Dispose();
                    _ItemGraphic = null;
                }

                _Item = _Entity.Equipment[(int)_EquipLayer];
                if (_Item != null)
                {
                    _ItemGraphic = (StaticPic)AddControl(new StaticPic(this, 0, 0, _Item.ItemID, _Item.Hue));
                }
            }

            if (_Item != null)
            {
                if (_ClickedCanDrag && totalMS >= _PickUpTime)
                {
                    _ClickedCanDrag = false;
                    AttemptPickUp();
                }

                if (_SendClickIfNoDoubleClick && totalMS >= _SingleClickTime)
                {
                    _SendClickIfNoDoubleClick = false;
                    _World.Interaction.SingleClick(_Item);
                }
            }

            base.Update(totalMS, frameMS);

			if (_ItemGraphic != null)
			{
				_ItemGraphic.Position = new Point(0 - 14, 0);
			}
        }

        protected override void OnMouseDown(int x, int y, MouseButton button)
        {
            if (_Item == null)
                return;

            // if click, we wait for a moment before picking it up. This allows a single click.
            _ClickedCanDrag = true;
            float totalMS = (float)Service.Get<UltimaGame>().TotalMS;
            _PickUpTime = totalMS + Settings.UserInterface.Mouse.ClickAndPickupMS;
            _ClickPoint = new Point(x, y);
        }

        protected override void OnMouseOver(int x, int y)
        {
            if (_Item == null)
                return;

            // if we have not yet picked up the item, AND we've moved more than 3 pixels total away from the original item, pick it up!
            if (_ClickedCanDrag && (Math.Abs(_ClickPoint.X - x) + Math.Abs(_ClickPoint.Y - y) > 3))
            {
                _ClickedCanDrag = false;
                AttemptPickUp();
            }
        }

        protected override void OnMouseClick(int x, int y, MouseButton button)
        {
            if (_Item == null)
                return;

            if (_ClickedCanDrag)
            {
                _ClickedCanDrag = false;
                _SendClickIfNoDoubleClick = true;
                float totalMS = (float)Service.Get<UltimaGame>().TotalMS;
                _SingleClickTime = totalMS + Settings.UserInterface.Mouse.DoubleClickMS;
            }
        }

        protected override void OnMouseDoubleClick(int x, int y, MouseButton button)
        {
            if (_Item == null)
                return;

            _World.Interaction.DoubleClick(_Item);
            _SendClickIfNoDoubleClick = false;
        }

        private void AttemptPickUp()
        {
            int w, h;
            IResourceProvider provider = Service.Get<IResourceProvider>();
            provider.GetItemDimensions(_Item.DisplayItemID, out w, out h);
            Point clickPoint = new Point(w / 2, h / 2);
            _World.Interaction.PickupItem(_Item, clickPoint);
        }
    }
}
