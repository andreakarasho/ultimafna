﻿/***************************************************************************
 *   TextEntryPage.cs
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

#region usings
using System;
using Microsoft.Xna.Framework;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.UI;
using UltimaXNA.Core.UI.HTML;
using UltimaXNA.Core.Windows;
#endregion

namespace UltimaXNA.Ultima.UI.Controls
{
    class TextEntryPage : AControl
    {
        const float MSBetweenCaratBlinks = 500f;
        
        string _Text;
        bool _IsFocused;
        bool _CaratBlinkOn;
        float _MSSinceLastCaratBlink;
        readonly RenderedText _RenderedText;
        RenderedText _RenderedCarat;
        int _CaratAt;
        int? _CaratKeyUpDownX;
        Action<int, string> _OnPageOverflow;
        Action<int> _OnPageUnderflow;
        Action<int> _OnPreviousPage;
        Action<int> _OnNextPage;

        public int PageIndex;
        public string LeadingHtmlTag;
        public string Text
        {
            get
            {
                return (_Text == null) ? string.Empty : _Text;
            }
            set
            {
                if (_Text != value)
                {
                    _Text = value;
                    _RenderedText.Text = $"{LeadingHtmlTag}{Text}";
                }
            }
        }

        public string TextWithLineBreaks
        {
            get
            {
                if (string.IsNullOrEmpty(_Text))
                {
                    return string.Empty;
                }
                return _RenderedText.Document.TextWithLineBreaks;
            }
        }

        public int CaratAt
        {
            get
            {
                if (_CaratAt < 0)
                    _CaratAt = 0;
                if (Text != null && _CaratAt > Text.Length)
                    _CaratAt = Text.Length;
                return _CaratAt;
            }
            set
            {
                _CaratAt = value;
                if (_CaratAt < 0)
                    _CaratAt = 0;
                if (_CaratAt > Text.Length)
                    _CaratAt = Text.Length;
            }
        }

        public override bool HandlesMouseInput => base.HandlesMouseInput & IsEditable;
        public override bool HandlesKeyboardFocus => base.HandlesKeyboardFocus & IsEditable;

        // ============================================================================================================
        // Ctors and BuildGumpling Methods
        // ============================================================================================================

        public TextEntryPage(AControl parent, int x, int y, int width, int height, int pageIndex)
            : base(parent)
        {
            base.HandlesMouseInput = true;
            base.HandlesKeyboardFocus = true;
            IsEditable = true;
            Position = new Point(x, y);
            Size = new Point(width, height);
            PageIndex = pageIndex;
            _CaratBlinkOn = false;
            _RenderedText = new RenderedText(string.Empty, Width, true);
            _RenderedCarat = new RenderedText(string.Empty, 16, true);
        }

        public void SetMaxLines(int maxLines, Action<int, string> onPageOverflow, Action<int> onPageUnderflow)
        {
            _RenderedText.Document.SetMaxLines(maxLines, OnDocumentSplitPage);
            _OnPageOverflow = onPageOverflow;
            _OnPageUnderflow = onPageUnderflow;
        }

        public void SetKeyboardPageControls(Action<int> onPrevious, Action<int> onNext)
        {
            _OnPreviousPage = onPrevious;
            _OnNextPage = onNext;
        }

        void OnDocumentSplitPage(int index)
        {
            string overflowText = Text.Substring(index);
            _Text = Text.Substring(0, index);
            _OnPageOverflow?.Invoke(PageIndex, overflowText);
        }

        // ============================================================================================================
        // Update and Draw
        // ============================================================================================================

        public override void Update(double totalMS, double frameMS)
        {
            if (UserInterface.KeyboardFocusControl == this)
            {
                // if we're not already focused, turn the carat on immediately.
                if (!_IsFocused)
                {
                    _IsFocused = true;
                    _CaratBlinkOn = true;
                    _MSSinceLastCaratBlink = 0f;
                }
                _MSSinceLastCaratBlink += (float)frameMS;
                if (_MSSinceLastCaratBlink >= MSBetweenCaratBlinks)
                {
                    _MSSinceLastCaratBlink -= MSBetweenCaratBlinks;
                    if (_CaratBlinkOn == true)
                    {
                        _CaratBlinkOn = false;
                    }
                    else
                    {
                        _CaratBlinkOn = true;
                    }
                }
            }
            else
            {
                _IsFocused = false;
                _CaratBlinkOn = false;
            }
            base.Update(totalMS, frameMS);
        }

        public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
        {
            _RenderedText.Draw(spriteBatch, new Rectangle(position.X, position.Y, Width, Height), 0, 0);
            if (IsEditable)
            {
                _RenderedCarat.Text = $"{LeadingHtmlTag}|";
                _RenderedText.Draw(spriteBatch, position);
                if (_CaratBlinkOn)
                {
                    Point caratPosition = _RenderedText.Document.GetCaratPositionByIndex(_CaratAt);
                    caratPosition.X += position.X;
                    caratPosition.Y += position.Y;
                    _RenderedCarat.Draw(spriteBatch, caratPosition);
                }
            }
            base.Draw(spriteBatch, position, frameMS);
        }

        void SetBlinkOn()
        {
            _CaratBlinkOn = true;
            _MSSinceLastCaratBlink = 0;
        }

        // ============================================================================================================
        // Text Editing Functions
        // ============================================================================================================

        public void InsertCharacter(int index, char ch)
        {
            string escapedCharacter;
            int caratAt = index;
            string text = Text;
            if (EscapeCharacters.TryMatchChar(ch, out escapedCharacter))
            {
                text = text.Insert(CaratAt, escapedCharacter);
                caratAt += escapedCharacter.Length;
            }
            else
            {
                text = text.Insert(CaratAt, ch.ToString());
                caratAt += 1;
            }
            Text = text;
            CaratAt = caratAt;
        }

        public void RemoveCharacter(int index)
        {
            int escapedLength;
            if (EscapeCharacters.TryFindEscapeCharacterBackwards(Text, index, out escapedLength))
            {
                Text = Text.Substring(0, Text.Length - escapedLength);
                int carat = index - 1;
                string before = (index == 0) ? null : Text.Substring(0, index - 1);
                string after = Text.Substring(index);
                Text = before + after;
                CaratAt = carat;
            }
            else
            {
                int carat = index - 1;
                string before = (index == 0) ? null : Text.Substring(0, index - 1);
                string after = Text.Substring(index);
                Text = before + after;
                CaratAt = carat;
            }
        }

        // ============================================================================================================
        // Input
        // ============================================================================================================

        protected override void OnKeyboardInput(InputEventKeyboard e)
        {
            if (e.KeyCode == WinKeys.Up || e.KeyCode == WinKeys.Down)
            {
                Point current = _RenderedText.Document.GetCaratPositionByIndex(CaratAt);
                if (_CaratKeyUpDownX == null)
                {
                    _CaratKeyUpDownX = current.X;
                }
                Point next = new Point(_CaratKeyUpDownX.Value, current.Y + (e.KeyCode == WinKeys.Up ? -18 : 18));
                int carat = _RenderedText.Document.GetCaratIndexByPosition(next);
                if (carat != -1)
                {
                    CaratAt = carat;
                }
            }
            else
            {
                _CaratKeyUpDownX = null;
                switch (e.KeyCode)
                {
                    case WinKeys.Tab:
                        Parent.KeyboardTabToNextFocus(this);
                        break;
                    case WinKeys.Enter:
                        InsertCharacter(CaratAt, '\n');
                        break;
                    case WinKeys.Back:
                        if (CaratAt == 0)
                        {
                            _OnPageUnderflow.Invoke(PageIndex);
                        }
                        else
                        {
                            RemoveCharacter(CaratAt);
                        }
                        break;
                    case WinKeys.Left:
                        if (CaratAt == 0)
                        {
                            _OnPreviousPage?.Invoke(PageIndex);
                        }
                        else
                        {
                            CaratAt = CaratAt - 1;
                        }
                        break;
                    case WinKeys.Right:
                        if (CaratAt == Text.Length)
                        {
                            _OnNextPage?.Invoke(PageIndex);
                        }
                        else
                        {
                            CaratAt = CaratAt + 1;
                        }
                        break;
                    default:
                        if (e.IsChar && e.KeyChar >= 32)
                        {
                            InsertCharacter(CaratAt, e.KeyChar);
                        }
                        break;
                }
            }
            SetBlinkOn();
        }

        protected override void OnMouseClick(int x, int y, MouseButton button)
        {
            if (button == MouseButton.Left)
            {
                int carat = _RenderedText.Document.GetCaratIndexByPosition(new Point(x, y));
                if (carat != -1)
                {
                    CaratAt = carat;
                    SetBlinkOn();
                    return;
                }
            }
            base.OnMouseClick(x, y, button);
        }
    }
}