﻿/***************************************************************************
 *   AScrollBar.cs
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using UltimaXNA.Core.Graphics;
using UltimaXNA.Core.Input;
using UltimaXNA.Core.UI;
using UltimaXNA.Core.Resources;
#endregion

namespace UltimaXNA.Ultima.UI.Controls
{
    /// <summary>
    /// A base scrollbar with methods that control min, max, and value
    /// </summary>
    class ScrollBar : AControl, IScrollBar
    {
        // ============================================================================================================
        // Private variables
        // ============================================================================================================
        private Texture2D[] _GumpUpButton;
        private Texture2D[] _GumpDownButton;
        private Texture2D[] _GumpBackground;
        private Texture2D _GumpSlider;

        private float _SliderPosition;
        private float _Value;
        private int _Max, _Min;

        private bool _BtnUpClicked;
        private bool _BtnDownClicked;
        private bool _BtnSliderClicked;
        private Point _ClickPosition;

        private float _TimeUntilNextClick;
        private const float c_TimeBetweenClicks = 500f;

        // ============================================================================================================
        // Public properties
        // ============================================================================================================
        public int Value
        {
            get
            {
                return (int)_Value;
            }
            set
            {
                _Value = value;
                if (_Value < MinValue)
                    _Value = MinValue;
                if (_Value > MaxValue)
                    _Value = MaxValue;
            }
        }

        public int MinValue
        {
            get
            {
                return _Min;
            }
            set
            {
                _Min = value;
                if (_Value < _Min)
                    _Value = _Min;
            }
        }

        public int MaxValue
        {
            get
            {
                return _Max;
            }
            set
            {
                if (value < 0)
                    value = 0;
                _Max = value;
                if (_Value > _Max)
                    _Value = _Max;
            }
        }

        // ============================================================================================================
        // Ctors, Initialize, Update, and Draw
        // ============================================================================================================
        public ScrollBar(AControl parent)
            : base(parent)
        {
            HandlesMouseInput = true;

            MouseWheelEvent += ScrollBar_MouseWheelEvent;
        }

        private void ScrollBar_MouseWheelEvent(AControl arg1, int arg2, int arg3, int arg4)
        {
            if (arg4 < 0)
                Value++;
            else
                Value--;
        }

        public ScrollBar(AControl parent, int x, int y, int height, int minValue, int maxValue, int value)
            : this(parent)
        {
            Position = new Point(x, y);
            MinValue = minValue;
            MaxValue = maxValue;
            Height = height;
            Value = value;
        }

        protected override void OnInitialize()
        {
            base.OnInitialize();

            IResourceProvider provider = Service.Get<IResourceProvider>();

            _GumpUpButton = new Texture2D[2];
            _GumpUpButton[0] = provider.GetUITexture(251);
            _GumpUpButton[1] = provider.GetUITexture(250);
            _GumpDownButton = new Texture2D[2];
            _GumpDownButton[0] = provider.GetUITexture(253);
            _GumpDownButton[1] = provider.GetUITexture(252);
            _GumpBackground = new Texture2D[3];
            _GumpBackground[0] = provider.GetUITexture(257);
            _GumpBackground[1] = provider.GetUITexture(256);
            _GumpBackground[2] = provider.GetUITexture(255);
            _GumpSlider = provider.GetUITexture(254);
            Size = new Point(_GumpBackground[0].Width, Height);
        }

        public override void Update(double totalMS, double frameMS)
        {
            base.Update(totalMS, frameMS);

            if (MaxValue <= MinValue || MinValue >= MaxValue)
            {
                Value = MaxValue = MinValue;
            }

            _SliderPosition = CalculateSliderYPosition();

            if (_BtnUpClicked || _BtnDownClicked)
            {
                if (_TimeUntilNextClick <= 0f)
                {
                    _TimeUntilNextClick += c_TimeBetweenClicks;
                    if (_BtnUpClicked)
                        Value -= 1;
                    if (_BtnDownClicked)
                        Value += 1;
                }
                _TimeUntilNextClick -= (float)totalMS;
            }
        }

        public override void Draw(SpriteBatchUI spriteBatch, Point position, double frameMS)
        {
            if (Height <= 0)
                return;

            // draw scrollbar background
            int middleHeight = Height - _GumpUpButton[0].Height - _GumpDownButton[0].Height - _GumpBackground[0].Height - _GumpBackground[2].Height;
            if (middleHeight > 0)
            {
                spriteBatch.Draw2D(_GumpBackground[0], new Vector3(position.X, position.Y + _GumpUpButton[0].Height, 0), Vector3.Zero);
                spriteBatch.Draw2DTiled(_GumpBackground[1], new Rectangle(position.X, position.Y + _GumpUpButton[0].Height + _GumpBackground[0].Height, _GumpBackground[0].Width, middleHeight), Vector3.Zero);
                spriteBatch.Draw2D(_GumpBackground[2], new Vector3(position.X, position.Y + Height - _GumpDownButton[0].Height - _GumpBackground[2].Height, 0), Vector3.Zero);
            }
            else
            {
                middleHeight = Height - _GumpUpButton[0].Height - _GumpDownButton[0].Height;
                spriteBatch.Draw2DTiled(_GumpBackground[1], new Rectangle(position.X, position.Y + _GumpUpButton[0].Height, _GumpBackground[0].Width, middleHeight), Vector3.Zero);
            }

            // draw up button
            spriteBatch.Draw2D(_BtnUpClicked ? _GumpUpButton[1] : _GumpUpButton[0], new Vector3(position.X, position.Y, 0), Vector3.Zero);

            // draw down button
            spriteBatch.Draw2D(_BtnDownClicked ? _GumpDownButton[1] : _GumpDownButton[0], new Vector3(position.X, position.Y + Height - _GumpDownButton[0].Height, 0), Vector3.Zero);

            // draw slider
            if (MaxValue > MinValue && middleHeight > 0)
                spriteBatch.Draw2D(_GumpSlider, new Vector3(position.X + (_GumpBackground[0].Width - _GumpSlider.Width) / 2, position.Y + _GumpUpButton[0].Height + _SliderPosition, 0), Vector3.Zero);

            base.Draw(spriteBatch, position, frameMS);
        }

        private float CalculateSliderYPosition()
        {
            if (!IsInitialized)
                return 0f;
            if (MaxValue - MinValue == 0)
                return 0f;
            return CalculateScrollableArea() * ((_Value - MinValue) / (MaxValue - MinValue));
        }

        private float CalculateScrollableArea()
        {
            if (!IsInitialized)
                return 0f;
            return Height - _GumpUpButton[0].Height - _GumpDownButton[0].Height - _GumpSlider.Height;
        }

        protected override void OnMouseDown(int x, int y, MouseButton button)
        {
            _TimeUntilNextClick = 0f;
            if (new Rectangle(0, Height - _GumpDownButton[0].Height, _GumpDownButton[0].Width, _GumpDownButton[0].Height).Contains(new Point(x, y)))
            {
                // clicked on the down button
                _BtnDownClicked = true;
            }
            else if (new Rectangle(0, 0, _GumpUpButton[0].Width, _GumpUpButton[0].Height).Contains(new Point(x, y)))
            {
                // clicked on the up button
                _BtnUpClicked = true;
            }
            else if (new Rectangle((_GumpBackground[0].Width - _GumpSlider.Width) / 2, _GumpUpButton[0].Height + (int)_SliderPosition, _GumpSlider.Width, _GumpSlider.Height).Contains(new Point(x, y)))
            {
                // clicked on the slider
                _BtnSliderClicked = true;
                _ClickPosition = new Point(x, y);
            }
            else
            {
                // clicked on the bar. This should scroll up a full slider's height worth of entries.
                // not coded yet, obviously.
            }
        }

        protected override void OnMouseUp(int x, int y, MouseButton button)
        {
            _BtnUpClicked = false;
            _BtnDownClicked = false;
            _BtnSliderClicked = false;
        }

        protected override void OnMouseOver(int x, int y)
        {
            if (_BtnSliderClicked)
            {
                if (y != _ClickPosition.Y)
                {
                    float sliderY = _SliderPosition + (y - _ClickPosition.Y);

                    if (sliderY < 0)
                        sliderY = 0;

                    float scrollableArea = CalculateScrollableArea();
                    if (sliderY > scrollableArea)
                        sliderY = scrollableArea;

                    _ClickPosition = new Point(x, y);

                    if (sliderY == 0 && _ClickPosition.Y < _GumpUpButton[0].Height + _GumpSlider.Height / 2)
                        _ClickPosition.Y = _GumpUpButton[0].Height + _GumpSlider.Height / 2;

                    if (sliderY == (scrollableArea) && _ClickPosition.Y > Height - _GumpDownButton[0].Height - _GumpSlider.Height / 2)
                        _ClickPosition.Y = Height - _GumpDownButton[0].Height - _GumpSlider.Height / 2;

                    _Value = ((sliderY / scrollableArea) * (float)((MaxValue - MinValue))) + MinValue;
                    _SliderPosition = sliderY;
                }
            }
        }

        protected override bool IsPointWithinControl(int x, int y)
        {
            Rectangle bounds = new Rectangle(0, 0, Width, Height);
            if (bounds.Contains(x, y))
                return true;
            return false;
        }

        public bool PointWithinControl(int x, int y)
        {
            return IsPointWithinControl(x, y);
        }
    }
}
