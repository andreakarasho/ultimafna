﻿/***************************************************************************
 *   AudioService.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System.Collections.Generic;
using UltimaXNA.Core.Diagnostics.Tracing;
using UltimaXNA.Ultima.Resources;
using UltimaXNA.Core.Audio;
#endregion

namespace UltimaXNA.Ultima.Audio
{
    public class AudioService
    {
        private readonly Dictionary<int, ASound> _Sounds = new Dictionary<int, ASound>();
        private readonly Dictionary<int, ASound> _Music = new Dictionary<int, ASound>();
        private UOMusic _MusicCurrentlyPlaying;

        public void Update()
        {
            if (_MusicCurrentlyPlaying != null)
                _MusicCurrentlyPlaying.Update();
        }

        public void PlaySound(int soundIndex, AudioEffects effect = AudioEffects.None, float volume = 1.0f, bool spamCheck = false)
        {
            if (volume < 0.01f)
                return;
            if (Settings.Audio.SoundOn)
            {
                ASound sound;
                if (_Sounds.TryGetValue(soundIndex, out sound))
                {
                    sound.Play(true, effect, volume, spamCheck);
                }
                else
                {
                    string name;
                    byte[] data;
                    if (SoundData.TryGetSoundData(soundIndex, out data, out name))
                    {
                        sound = new UOSound(name, data);
                        _Sounds.Add(soundIndex, sound);
                        sound.Play(true, effect, volume, spamCheck);
                    }
                }
            }
        }

        public void PlayMusic(int id)
        {
            if (Settings.Audio.MusicOn)
            {
                if (id < 0) // not a valid id, used to stop music.
                {
                    StopMusic();
                    return;
                }

                if (!_Music.ContainsKey(id))
                {
                    string name;
                    bool loops;
                    if (MusicData.TryGetMusicData(id, out name, out loops))
                    {
                        _Music.Add(id, new UOMusic(id, name, loops));
                    }
                    else
                    {
                        Tracer.Error("Received unknown music id {0}", id);
                        return;
                    }
                }

                ASound toPlay = _Music[id];
                if (toPlay != _MusicCurrentlyPlaying)
                {
                    // stop the current song
                    StopMusic();
                    // play the new song!
                    _MusicCurrentlyPlaying = toPlay as UOMusic;
                    _MusicCurrentlyPlaying.Play(false);
                }
            }
        }

        public void StopMusic()
        {
            if (_MusicCurrentlyPlaying != null)
            {
                _MusicCurrentlyPlaying.Stop();
                _MusicCurrentlyPlaying.Dispose();
                _MusicCurrentlyPlaying = null;
            }
        }
    }
}
