﻿/***************************************************************************
 *   UOMusic.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

using Microsoft.Xna.Framework.Audio;
using System;
using UltimaXNA.Core.Audio;
using UltimaXNA.Core.Audio.MP3Sharp;
using UltimaXNA.Core.Diagnostics.Tracing;
using UltimaXNA.Ultima.IO;

namespace UltimaXNA.Ultima.Audio
{
    class UOMusic : ASound
    {
        MP3Stream _Stream;
        const int NUMBER_OF_PC_BYTES_TO_READ_PER_CHUNK = 0x8000; // 32768 bytes, about 0.9 seconds
        readonly byte[] _WaveBuffer = new byte[NUMBER_OF_PC_BYTES_TO_READ_PER_CHUNK];
        bool _Repeat;
        bool _Playing;

        string Path => FileManager.GetPath(string.Format("Music/Digital/{0}.mp3", Name));

        public UOMusic(int index, string name, bool loop)
            : base(name)
        {
            _Repeat = loop;
            _Playing = false;
            Channels = AudioChannels.Stereo;
        }

        public void Update()
        {
            // sanity - if the buffer empties, we will lose our sound effect. Thus we must continually check if it is dead.
            OnBufferNeeded(null, null);
        }

        protected override byte[] GetBuffer()
        {
            if (_Playing)
            {
                int bytesReturned = _Stream.Read(_WaveBuffer, 0, _WaveBuffer.Length);
                if (bytesReturned != NUMBER_OF_PC_BYTES_TO_READ_PER_CHUNK)
                {
                    if (_Repeat)
                    {
                        _Stream.Position = 0;
                        _Stream.Read(_WaveBuffer, bytesReturned, _WaveBuffer.Length - bytesReturned);
                    }
                    else
                    {
                        if (bytesReturned == 0)
                        {
                            Stop();
                        }
                    }
                }
                return _WaveBuffer;
            }
            Stop();
            return null;
        }

        protected override void OnBufferNeeded(object sender, EventArgs e)
        {
            if (_Playing)
            {
                while (_ThisInstance.PendingBufferCount < 3)
                {
                    byte[] buffer = GetBuffer();
                    if (_ThisInstance.IsDisposed)
                        return;
                    _ThisInstance.SubmitBuffer(buffer);
                }
            }
        }

        protected override void BeforePlay()
        {
            if (_Playing)
            {
                Stop();
            }

            try
            {
                _Stream = new MP3Stream(Path, NUMBER_OF_PC_BYTES_TO_READ_PER_CHUNK);
                Frequency = _Stream.Frequency;

                _Playing = true;
            }
            catch (Exception e)
            {
                // file in use or access denied.
                Tracer.Error(e);
                _Playing = false;
            }
        }

        protected override void AfterStop()
        {
            if (_Playing)
            {
                _Playing = false;
                _Stream.Close();
                _Stream = null;
            }
        }
    }
}