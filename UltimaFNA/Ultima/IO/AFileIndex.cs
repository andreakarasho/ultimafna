﻿/***************************************************************************
 *   FileIndexEntry5D.cs
 *   Based on code from UltimaSDK: http://ultimasdk.codeplex.com/
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UltimaXNA.Core.IO;
using UltimaXNA.Core.Network.Compression;
using UltimaXNA.Ultima.IO.UOP;
using UltimaXNA.Ultima.Resources;
#endregion

namespace UltimaXNA.Ultima.IO
{
    public abstract class AFileIndex
    {
        FileIndexEntry3D[] _Index;
        Stream _Stream;

        public FileIndexEntry3D[] Index => _Index;
        public Stream Stream => _Stream;

        public string DataPath { get; private set; }
        public int Length { get; protected set; }

        public bool IsUopAnimation { get; set; }

        protected abstract FileIndexEntry3D[] ReadEntries();

        protected AFileIndex(string dataPath)
        {
            DataPath = dataPath;
        }

        protected AFileIndex(string dataPath, int length)
        {
            Length = length;
            DataPath = dataPath;
        }

        public void Open()
        {
            _Index = ReadEntries();
            Length = _Index.Length;           
        }

        public void OpenUopAnimation()
        {
            _Index = ReadEntries();
        }

        public BinaryFileReader Seek(int index, out int length, out int extra, out bool patched)
        {
            if (Stream == null)
                _Stream = new FileStream(DataPath, FileMode.Open, FileAccess.Read, FileShare.Read);

            if (!IsUopAnimation)
            {
                if (index < 0 || index >= _Index.Length)
                {
                    length = extra = 0;
                    patched = false;
                    return null;
                }

                FileIndexEntry3D e = _Index[index];

                if (e.Lookup < 0)
                {
                    length = extra = 0;
                    patched = false;
                    return null;
                }

                length = e.Length & 0x7FFFFFFF;
                extra = e.Extra;

                if ((e.Length & 0xFF000000) != 0)
                {
                    patched = true;

                    VerData.Stream.Seek(e.Lookup, SeekOrigin.Begin);
                    return new BinaryFileReader(new BinaryReader(VerData.Stream));
                }
                else if (_Stream == null)
                {
                    length = extra = 0;
                    patched = false;
                    return null;
                }

                patched = false;
                _Stream.Position = e.Lookup;
                return new BinaryFileReader(new BinaryReader(_Stream));
            }
            else
            {
                if (index >= _Index.Length || index < 0)
                {
                    length = extra = 0;
                    patched = false;
                    return null;
                }

                FileIndexEntry3D e = _Index[index];
                if (e.UOPData == null)
                {
                    length = extra = 0;
                    patched = false;
                    return null;
                }

                _Stream.Position = e.UOPData.Offset;
                extra = 0;
                length = e.UOPData.DecompressedLength;
                patched = false;
                return new BinaryFileReader(new BinaryReader(_Stream));

                /*_Stream.Position = e.UOPData.Offset;
                byte[] buffer = new byte[e.UOPData.CompressedLength];
                _Stream.Read(buffer, 0,e.UOPData.CompressedLength);

                int clen = e.UOPData.CompressedLength;
                int dlen = e.UOPData.DecompressedLength;

                byte[] decbuffer = new byte[dlen];
                if (ZlibCompression.Unpack(decbuffer, ref dlen, buffer, clen) != ZLibError.Okay)
                {
                    length = extra = 0;
                    patched = false;
                    return null;
                }

                length = decbuffer.Length;
                extra = 0;
                patched = false;
                return new BinaryFileReader(new MemoryStream(decbuffer));*/
            }

        }
    }
}
