﻿using System.IO;

namespace UltimaXNA.Ultima.IO
{
    public class VerData
    {
        private static readonly FileIndexEntry5D[] _Patches;
        private static readonly Stream _Stream;

        public static Stream Stream { get { return _Stream; } }
        public static FileIndexEntry5D[] Patches { get { return _Patches; } }

        static VerData()
        {
            string path = FileManager.GetFilePath("verdata.mul");

            if (!File.Exists(path))
            {
                _Patches = new FileIndexEntry5D[0];
                _Stream = Stream.Null;
            }
            else
            {
                _Stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
                BinaryReader bin = new BinaryReader(_Stream);

                _Patches = new FileIndexEntry5D[bin.ReadInt32()];

                for (int i = 0; i < _Patches.Length; ++i)
                {
                    _Patches[i].file = bin.ReadInt32();
                    _Patches[i].index = bin.ReadInt32();
                    _Patches[i].lookup = bin.ReadInt32();
                    _Patches[i].length = bin.ReadInt32();
                    _Patches[i].extra = bin.ReadInt32();
                }
            }
        }
    }
}