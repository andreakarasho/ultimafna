﻿/***************************************************************************
 *   UltimaUIResourceProvider.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using UltimaXNA.Core.Diagnostics.Tracing;
using UltimaXNA.Core.Resources;
using UltimaXNA.Core.UI;
using UltimaXNA.Ultima.Resources;
#endregion

namespace UltimaXNA.Ultima
{
    class ResourceProvider : IResourceProvider
    {
        AnimationResource _Anim;
        ArtMulResource _Art;
        ClilocResource _Cliloc;
        EffectDataResource _Effects;
        FontsResource _Fonts;
        GumpMulResource _Gumps;
        TexmapResource _Texmaps;
        readonly Dictionary<Type, object> _Resources = new Dictionary<Type, object>();

        public ResourceProvider(Game game)
        {
            _Anim = new AnimationResource(game.GraphicsDevice);
            _Art = new ArtMulResource(game.GraphicsDevice);
            _Cliloc = new ClilocResource("enu");
            _Effects = new EffectDataResource();
            _Fonts = new FontsResource(game.GraphicsDevice);
            _Gumps = new GumpMulResource(game.GraphicsDevice);
            _Texmaps = new TexmapResource(game.GraphicsDevice);
        }

        public AAnimationFrame[] GetAnimation(int body, ref int hue, int action, int direction)
        {
            return _Anim.GetAnimation(body, ref hue, action, direction);
        }

        public Texture2D GetUITexture(int textureID, bool replaceMask080808 = false)
        {
            return _Gumps.GetGumpTexture(textureID, replaceMask080808);
        }

        public bool IsPointInUITexture(int textureID, int x, int y)
        {
            return _Gumps.IsPointInGumpTexture(textureID, x, y);
        }

        public Texture2D GetItemTexture(int itemIndex)
        {
            return _Art.GetStaticTexture(itemIndex);
        }

        public bool IsPointInItemTexture(int textureID, int x, int y, int extraRange = 0)
        {
            return _Art.IsPointInItemTexture(textureID, x, y, extraRange);
        }

        public Texture2D GetLandTexture(int landIndex)
        {
            return _Art.GetLandTexture(landIndex);
        }

        public void GetItemDimensions(int itemIndex, out int width, out int height)
        {
            _Art.GetStaticDimensions(itemIndex, out width, out height);
        }

        public Texture2D GetTexmapTexture(int textureIndex)
        {
            return _Texmaps.GetTexmapTexture(textureIndex);
        }

        /// <summary>
        /// Returns a Ultima Online Hue index that approximates the passed color.
        /// </summary>
        public ushort GetWebSafeHue(Color color)
        {
            return (ushort)HueData.GetWebSafeHue(color);
        }

        public IFont GetUnicodeFont(int fontIndex)
        {
            return _Fonts.GetUniFont(fontIndex);
        }

        public IFont GetAsciiFont(int fontIndex)
        {
            return _Fonts.GetAsciiFont(fontIndex);
        }

        public string GetString(int clilocIndex)
        {
            return _Cliloc.GetString(clilocIndex);
        }

        

        public void RegisterResource<T>(IResource<T> resource)
        {
            Type type = typeof(T);

            if (_Resources.ContainsKey(type))
            {
                Tracer.Critical(string.Format("Attempted to register resource provider of type {0} twice.", type));
                _Resources.Remove(type);
            }

            _Resources.Add(type, resource);
        }

        public T GetResource<T>(int resourceIndex)
        {
            Type type = typeof(T);

            if (_Resources.ContainsKey(type))
            {
                IResource<T> resource = (IResource<T>)_Resources[type];
                return (T)resource.GetResource(resourceIndex);
            }
            else
            {
                Tracer.Critical(string.Format("Attempted to get resource provider of type {0}, but no provider with this type is registered.", type));
                return default(T);
            }
        }
    }
}
