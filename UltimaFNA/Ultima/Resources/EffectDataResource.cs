﻿/***************************************************************************
 *   EffectDataResource.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System.Collections;
using System.IO;
using UltimaXNA.Core.Resources;
using UltimaXNA.Ultima.IO;
#endregion

namespace UltimaXNA.Ultima.Resources
{
    /// <summary>
    /// This file contains information about animated effects.
    /// </summary>
    public class EffectDataResource : IResource<EffectData>
    {
        const int Count = 0x0800;
        private readonly EffectData[][] _AnimData;

        private Hashtable _animData;
        private int[] _header;
        private byte[] _unknown;

        public EffectDataResource()
        {
            _animData = new Hashtable();

            using (FileStream stream = FileManager.GetFile("animdata.mul"))
            {
                using (BinaryReader bin = new BinaryReader(stream))
                {
                    unsafe
                    {
                        int id = 0;
                        int h = 0;
                        byte unk;
                        byte fcount;
                        byte finter;
                        byte fstart;
                        sbyte[] fdata;
                        _header = new int[bin.BaseStream.Length / (4 + 8 * (64 + 4))];
                        while (h < _header.Length)
                        {
                            _header[h++] = bin.ReadInt32();
                            byte[] buffer = bin.ReadBytes(544);
                            fixed (byte* buf = buffer)
                            {
                                byte* data = buf;
                                for (int i = 0; i < 8; ++i, ++id)
                                {
                                    fdata = new sbyte[64];
                                    for (int j = 0; j < 64; ++j)
                                        fdata[j] = (sbyte)*data++;
                                    unk = *data++;
                                    fcount = *data++;
                                    finter = *data++;
                                    fstart = *data++;
                                    if (id == 0x0DDA)
                                    { }
                                    _animData[id] = new EffectData(fdata, unk, fcount, finter, fstart);
                                }
                            }
                        }

                        var remaining = (int)(bin.BaseStream.Length - bin.BaseStream.Position);
                        if (remaining > 0)
                            _unknown = bin.ReadBytes(remaining);
                    }
                }

            }



                // From http://wpdev.sourceforge.net/docs/guide/node167.html:
                // There are 2048 blocks, 8 entries per block, 68 bytes per entry.
                // Thanks to Krrios for figuring out the blocksizes.
                // Each block has an 4 byte header which is currently unknown. The
                // entries correspond with the Static ID. You can lookup an entry
                // for a given static with this formula:
                // Offset = (id>>3)*548+(id&15)*68+4;
                // Here is the record format for each entry:
                // byte[64] Frames
                // byte     Unknown
                // byte     Number of Frames Used
                // byte     Frame Interval
                // byte     Start Interval

             /*   _AnimData = new EffectData[Count][];

            FileStream stream = FileManager.GetFile("animdata.mul");
            BinaryReader reader = new BinaryReader(stream);



            for (int i = 0; i < Count; i++)
            {
                EffectData[] data = new EffectData[8];
                int header = reader.ReadInt32(); // unknown value.
                for (int j = 0; j < 8; j++)
                {
                    data[j] = new EffectData(reader);
                }
                _AnimData[i] = data;
            }*/
        }

        public EffectData GetResource(int itemID)
        {
            itemID &= FileManager.ItemIDMask;
            return  (EffectData)_animData[itemID];
            //return _AnimData[(itemID >> 3)][itemID & 0x07];
        }

       /* public EffectData GetResource(int id)
        {
           if (_animData.Contains(id))
                return ((EffectData)_animData[id]);
            return default(EffectData);
        }*/
    }
}
