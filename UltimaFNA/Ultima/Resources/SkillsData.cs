﻿/***************************************************************************
 *   Skills.cs
 *   Based on code from UltimaSDK: http://ultimasdk.codeplex.com/
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UltimaXNA.Core.IO;
using UltimaXNA.Ultima.IO;
#endregion

namespace UltimaXNA.Ultima.Resources
{
    public class SkillsData
    {
        public static int DefaultLength = 55;

        private static AFileIndex _FileIndex = FileManager.CreateFileIndex("Skills.idx", "Skills.mul", DefaultLength, -1);
        public static AFileIndex FileIndex { get { return _FileIndex; } }

        private static Skill[] _List = new Skill[DefaultLength];
        public static Skill[] List { get { return _List; } }
        private static string[] _listNames;
        public static string[] ListNames
        {
            get
            {
                if (_listNames == null)
                {
                    _listNames = new string[_List.Length];
                    for (int i = 0; i < _List.Length; i++)
                    {
                        _listNames[i] = _List[i].Name;
                    }
                }
                return _listNames;
            }
        }

        public static void Initialize()
        {
            for (int i = 0; i < DefaultLength; i++)
                GetSkill(i);
        }

        public static Skill GetSkill(int index)
        {
            if (_List[index] != null)
                return _List[index];

            int length, extra;
            bool patched;
            BinaryFileReader reader = _FileIndex.Seek(index, out length, out extra, out patched);
            if (reader == null)
                return _List[index] = new Skill(SkillVars.NullV);

            return _List[index] = LoadSkill(index, reader);
        }

        private static unsafe Skill LoadSkill(int index, BinaryFileReader reader)
        {
            int nameLength = _FileIndex.Index[index].Length - 2;
            int extra = _FileIndex.Index[index].Extra;

            byte[] set1 = new byte[1];
            byte[] set2 = new byte[nameLength];
            byte[] set3 = new byte[1];

            set1 = reader.ReadBytes(1);
            set2 = reader.ReadBytes(nameLength);
            set3 = reader.ReadBytes(1);

            bool useBtn = ToBool(set1);
            string name = ToString(set2);

            return new Skill(new SkillVars(index, name, useBtn, extra, set3[0]));
        }

        public static string ToString(byte[] buffer)
        {
            StringBuilder sb = new StringBuilder(buffer.Length);

            for (int i = 0; i < buffer.Length; i++)
            {
                sb.Append(ToString(buffer[i]));
            }

            return sb.ToString();
        }

        public static bool ToBool(byte[] buffer)
        {
            return BitConverter.ToBoolean(buffer, 0);
        }

        public static string ToString(byte b)
        {
            return ToString((char)b);
        }

        public static string ToString(char c)
        {
            return c.ToString();
        }

        public static string ToHexidecimal(int input, int length)
        {
            return String.Format("0x{0:X{1}}", input, length);
        }
    }

    public class Skill
    {
        private SkillVars _Data;
        public SkillVars Data { get { return _Data; } }

        private int _Index = -1;
        public int Index { get { return _Index; } }

        private bool _UseButton;
        public bool UseButton { get { return _UseButton; } set { _UseButton = value; } }

        private string _Name = String.Empty;
        public string Name { get { return _Name; } set { _Name = value; } }

        private SkillCategory _Category;
        public SkillCategory Category { get { return _Category; } set { _Category = value; } }

        private byte _Unknown;
        public byte Unknown { get { return _Unknown; } }

        public int ID { get { return _Index + 1; } }

        public Skill(SkillVars data)
        {
            _Data = data;

            _Index = _Data.Index;
            _UseButton = _Data.UseButton;
            _Name = _Data.Name;
            _Category = _Data.Category;
            _Unknown = _Data.Unknown;
        }

        public void ResetFromData()
        {
            _Index = _Data.Index;
            _UseButton = _Data.UseButton;
            _Name = _Data.Name;
            _Category = _Data.Category;
            _Unknown = _Data.Unknown;
        }

        public void ResetFromData(SkillVars data)
        {
            _Data = data;
            _Index = _Data.Index;
            _UseButton = _Data.UseButton;
            _Name = _Data.Name;
            _Category = _Data.Category;
            _Unknown = _Data.Unknown;
        }

        public override string ToString()
        {
            return String.Format("{0} ({1:X4}) {2} {3}", _Index, _Index, _UseButton ? "[x]" : "[ ]", _Name);
        }
    }

    public sealed class SkillVars
    {
        public static SkillVars NullV { get { return new SkillVars(-1, "null", false, 0, 0x0); } }

        private int _Index = -1;
        public int Index { get { return _Index; } }

        private readonly string _Name = String.Empty;
        public string Name { get { return _Name; } }

        private readonly int _Extra;
        public int Extra { get { return _Extra; } }

        private readonly bool _UseButton;
        public bool UseButton { get { return _UseButton; } }

        private readonly byte _Unknown;
        public byte Unknown { get { return _Unknown; } }

        private SkillCategory _Category;
        public SkillCategory Category { get { return _Category; } }

        public int NameLength { get { return _Name.Length; } }

        public SkillVars(int index, string name, bool useButton, int extra, byte unk)
        {
            _Index = index;
            _Name = name;
            _UseButton = useButton;
            _Extra = extra;
            _Unknown = unk;
        }
    }

    public class SkillCategories
    {
        private static SkillCategory[] _List = new SkillCategory[0];
        public static SkillCategory[] List { get { return _List; } }

        private SkillCategories()
        { }

        public static SkillCategory GetCategory(int index)
        {
            if (_List.Length > 0)
            {
                if (index < _List.Length)
                    return _List[index];
            }

            _List = LoadCategories();

            if (_List.Length > 0)
                return GetCategory(index);

            return new SkillCategory(SkillCategoryData.DefaultData);
        }

        private static unsafe SkillCategory[] LoadCategories()
        {
            SkillCategory[] list = new SkillCategory[0];

            string grpPath = FileManager.GetFilePath("skillgrp.mul");

            if (grpPath == null)
            { return new SkillCategory[0]; }
            else
            {
                List<SkillCategory> toAdd = new List<SkillCategory>();

                using (FileStream stream = new FileStream(grpPath, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    BinaryReader bin = new BinaryReader(stream);

                    byte[] START = new byte[4]; //File Start Offset

                    bin.Read(START, 0, 4);

                    int index = 0;

                    long
                        x = stream.Length,
                        y = 0;


                    while (y < x) //Position < Length
                    {
                        string name = ParseName(stream);
                        long fileIndex = stream.Position - name.Length;

                        if (name.Length > 0)
                        {
                            toAdd.Add(new SkillCategory(new SkillCategoryData(fileIndex, index, name)));

                            y = stream.Position;

                            ++index;
                        }
                    }
                }

                if (toAdd.Count > 0)
                {
                    list = new SkillCategory[toAdd.Count];

                    for (int i = 0; i < toAdd.Count; i++)
                    {
                        list[i] = toAdd[i];
                    }

                    toAdd.Clear();
                }
            }

            return list;
        }

        private static unsafe string ParseName(Stream stream)
        {
            BinaryReader bin = new BinaryReader(stream);

            string tempName = String.Empty;

            bool esc = false;

            while (!esc && bin.PeekChar() != -1)
            {
                byte[] DATA = new byte[1];

                bin.Read(DATA, 0, 1);

                char c = (char)DATA[0];

                if (Char.IsLetterOrDigit(c) || Char.IsWhiteSpace(c) || Char.IsPunctuation(c))
                {
                    tempName += SkillsData.ToString(c);
                    continue;
                }

                esc = true;
            }

            return tempName.Trim();
        }
    }

    public class SkillCategory
    {
        private SkillCategoryData _Data;
        public SkillCategoryData Data { get { return _Data; } }

        private int _Index = -1;
        public int Index { get { return _Index; } }

        private string _Name = String.Empty;
        public string Name { get { return _Name; } }

        public SkillCategory(SkillCategoryData data)
        {
            _Data = data;
            _Index = _Data.Index;
            _Name = _Data.Name;
        }

        public void ResetFromData()
        {
            _Index = _Data.Index;
            _Name = _Data.Name;
        }

        public void ResetFromData(SkillCategoryData data)
        {
            _Data = data;
            _Index = _Data.Index;
            _Name = _Data.Name;
        }
    }

    public sealed class SkillCategoryData
    {
        public static SkillCategoryData DefaultData { get { return new SkillCategoryData(0, -1, "null"); } }

        private long _FileIndex = -1;
        public long FileIndex { get { return _FileIndex; } }

        private int _Index = -1;
        public int Index { get { return _Index; } }

        private readonly string _Name = String.Empty;
        public string Name { get { return _Name; } }

        public SkillCategoryData(long fileIndex, int index, string name)
        {
            _FileIndex = fileIndex;
            _Index = index;
            _Name = name;
        }
    }
}