﻿/***************************************************************************
 *   FontsResource.cs
 *   Copyright (c) 2015 UltimaXNA Development Team
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using Microsoft.Xna.Framework.Graphics;
using System;
using System.IO;
using UltimaXNA.Core.UI.Fonts;
using UltimaXNA.Ultima.IO;
using UltimaXNA.Ultima.Resources.Fonts;
#endregion

namespace UltimaXNA.Ultima.Resources
{
    public class FontsResource
    {
        bool _Initialized;
        GraphicsDevice _GraphicsDevice;

        public const int UniFontCount = 3;
        readonly AFont[] _UnicodeFonts = new AFont[UniFontCount];

        public const int AsciiFontCount = 10;
        readonly AFont[] _AsciiFonts = new AFont[AsciiFontCount];

        internal AFont GetUniFont(int index)
        {
            if (index < 0 || index >= UniFontCount)
                return _UnicodeFonts[0];
            return _UnicodeFonts[index];
        }

        internal AFont GetAsciiFont(int index)
        {
            if (index < 0 || index >= AsciiFontCount)
                return _AsciiFonts[9];
            return _AsciiFonts[index];
        }
       
        public FontsResource(GraphicsDevice graphics)
        {
            _GraphicsDevice = graphics;
            Initialize();
        }

        void Initialize()
        {
            if (!_Initialized)
            {
                _Initialized = true;
                _GraphicsDevice.DeviceReset -= GraphicsDeviceReset;
                _GraphicsDevice.DeviceReset += GraphicsDeviceReset;
                LoadFonts();
            }
        }

        void GraphicsDeviceReset(object sender, EventArgs e)
        {
            LoadFonts();
        }

        void LoadFonts()
        {
            // load Ascii fonts
            using (BinaryReader reader = new BinaryReader(new FileStream(FileManager.GetFilePath("fonts.mul"), FileMode.Open, FileAccess.Read)))
            {
                for (int i = 0; i < AsciiFontCount; i++)
                {
                    _AsciiFonts[i] = new FontAscii();
                    _AsciiFonts[i].Initialize(reader);
                    _AsciiFonts[i].HasBuiltInOutline = true;
                }
            }
            // load Unicode fonts
            int maxHeight = 0; // because all unifonts are designed to be used together, they must all share a single maxheight value.
            for (int i = 0; i < UniFontCount; i++)
            {
                string path = FileManager.GetFilePath("unifont" + (i == 0 ? "" : i.ToString()) + ".mul");
                if (path != null)
                {
                    _UnicodeFonts[i] = new FontUnicode();
                    _UnicodeFonts[i].Initialize(new BinaryReader(new FileStream(path, FileMode.Open, FileAccess.Read)));
                    if (_UnicodeFonts[i].Height > maxHeight)
                    {
                        maxHeight = _UnicodeFonts[i].Height;
                    }
                }
            }
            for (int i = 0; i < UniFontCount; i++)
            {
                if (_UnicodeFonts[i] == null)
                {
                    continue;
                }
                _UnicodeFonts[i].Height = maxHeight;
            }
        }
    }
}
