﻿/***************************************************************************
 *   TileData.cs
 *   Based on code from UltimaSDK: http://ultimasdk.codeplex.com/
 *   And on code from OpenUO: https://github.com/jeffboulanger/OpenUO
 *      Copyright (c) 2011 OpenUO Software Team.
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using UltimaXNA.Core.Diagnostics;
using UltimaXNA.Core.Resources;
using UltimaXNA.Ultima.Data;
using UltimaXNA.Ultima.IO;
#endregion

namespace UltimaXNA.Ultima.Resources
{
    /// <summary>
    ///     Represents land tile data.
    ///     <seealso cref="ItemData" />
    ///     <seealso cref="LandData" />
    /// </summary>
    public struct LandData
    {
        private string m_Name;
        private short m_TexID;
        private TileFlag m_Flags;
        private int m_Unk1;

        public LandData(string name, int TexID, TileFlag flags, int unk1)
        {
            m_Name = name;
            m_TexID = (short)TexID;
            m_Flags = flags;
            m_Unk1 = unk1;
        }

        public unsafe LandData(NewLandTileDataMul mulstruct)
        {
            m_TexID = mulstruct.texID;
            m_Flags = (TileFlag)mulstruct.flags;
            m_Unk1 = mulstruct.unk1;
            m_Name = TileData.ReadNameString(mulstruct.name);
        }

        public unsafe LandData(OldLandTileDataMul mulstruct)
        {
            m_TexID = mulstruct.texID;
            m_Flags = (TileFlag)mulstruct.flags;
            m_Unk1 = 0;
            m_Name = TileData.ReadNameString(mulstruct.name);
        }

        /// <summary>
        ///     Gets the name of this land tile.
        /// </summary>
        public string Name { get { return m_Name; } set { m_Name = value; } }

        /// <summary>
        ///     Gets the Texture ID of this land tile.
        /// </summary>
        public short TextureID { get { return m_TexID; } set { m_TexID = value; } }

        /// <summary>
        ///     Gets a bitfield representing the 32 individual flags of this land tile.
        /// </summary>
        public TileFlag Flags { get { return m_Flags; } set { m_Flags = value; } }

        /// <summary>
        ///     Gets a new UOHSA Unknown Int
        /// </summary>
        public int Unk1 { get { return m_Unk1; } set { m_Unk1 = value; } }

        public void ReadData(string[] split)
        {
            int i = 1;
            m_Name = split[i++];
            m_TexID = (short)TileData.ConvertStringToInt(split[i++]);
            m_Unk1 = TileData.ConvertStringToInt(split[i++]);
            m_Flags = 0;
            int temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Background;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Weapon;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Transparent;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Translucent;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Wall;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Damaging;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Impassable;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Wet;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Unknown1;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Surface;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Bridge;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Generic;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Window;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.NoShoot;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.ArticleA;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.ArticleAn;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Internal;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Foliage;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.PartialHue;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Unknown2;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Map;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Container;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Wearable;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.LightSource;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Animation;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.HoverOver;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Unknown3;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Armor;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Roof;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Door;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.StairBack;
            }
            temp = Convert.ToByte(split[i++]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.StairRight;
            }
        }

        public bool IsWet
        {
            get { return (Flags & TileFlag.Wet) != 0; }
        }

        public bool IsImpassible
        {
            get { return (Flags & TileFlag.Impassable) != 0; }
        }
    }

    /// <summary>
    ///     Represents item tile data.
    ///     <seealso cref="TileData" />
    ///     <seealso cref="LandData" />
    /// </summary>
    public struct ItemData
    {
        internal string m_Name;
        internal TileFlag m_Flags;
        internal int m_Unk1;
        internal byte m_Weight;
        internal byte m_Quality;
        internal byte m_Quantity;
        internal byte m_Value;
        internal byte m_Height;
        internal short m_Animation;
        internal byte m_Hue;
        internal byte m_StackOffset;
        internal short m_MiscData;
        internal byte m_Unk2;
        internal byte m_Unk3;

        public ItemData(
            string name,
            TileFlag flags,
            int unk1,
            int weight,
            int quality,
            int quantity,
            int value,
            int height,
            int anim,
            int hue,
            int stackingoffset,
            int MiscData,
            int unk2,
            int unk3)
        {
            m_Name = name;
            m_Flags = flags;
            m_Unk1 = unk1;
            m_Weight = (byte)weight;
            m_Quality = (byte)quality;
            m_Quantity = (byte)quantity;
            m_Value = (byte)value;
            m_Height = (byte)height;
            m_Animation = (short)anim;
            m_Hue = (byte)hue;
            m_StackOffset = (byte)stackingoffset;
            m_MiscData = (short)MiscData;
            m_Unk2 = (byte)unk2;
            m_Unk3 = (byte)unk3;
        }

        public unsafe ItemData(NewItemTileDataMul mulstruct)
        {
            m_Name = TileData.ReadNameString(mulstruct.name);
            m_Flags = (TileFlag)mulstruct.flags;
            m_Unk1 = mulstruct.unk1;
            m_Weight = mulstruct.weight;
            m_Quality = mulstruct.quality;
            m_Quantity = mulstruct.quantity;
            m_Value = mulstruct.value;
            m_Height = mulstruct.height;
            m_Animation = mulstruct.anim;
            m_Hue = mulstruct.hue;
            m_StackOffset = mulstruct.stackingoffset;
            m_MiscData = mulstruct.miscdata;
            m_Unk2 = mulstruct.unk2;
            m_Unk3 = mulstruct.unk3;
        }

        public unsafe ItemData(OldItemTileDataMul mulstruct)
        {
            m_Name = TileData.ReadNameString(mulstruct.name);
            m_Flags = (TileFlag)mulstruct.flags;
            m_Unk1 = 0;
            m_Weight = mulstruct.weight;
            m_Quality = mulstruct.quality;
            m_Quantity = mulstruct.quantity;
            m_Value = mulstruct.value;
            m_Height = mulstruct.height;
            m_Animation = mulstruct.anim;
            m_Hue = mulstruct.hue;
            m_StackOffset = mulstruct.stackingoffset;
            m_MiscData = mulstruct.miscdata;
            m_Unk2 = mulstruct.unk2;
            m_Unk3 = mulstruct.unk3;
        }

        /// <summary>
        ///     Gets the name of this item.
        /// </summary>
        public string Name { get { return m_Name; } set { m_Name = value; } }

        /// <summary>
        ///     Gets the animation body index of this item.
        ///     <seealso cref="Animations" />
        /// </summary>
        public short Animation { get { return m_Animation; } set { m_Animation = value; } }

        /// <summary>
        ///     Gets a bitfield representing the 32 individual flags of this item.
        ///     <seealso cref="TileFlag" />
        /// </summary>
        public TileFlag Flags { get { return m_Flags; } set { m_Flags = value; } }

        /// <summary>
        ///     Gets an unknown new UOAHS int
        /// </summary>
        public int Unk1 { get { return m_Unk1; } set { m_Unk1 = value; } }

        /// <summary>
        ///     Whether or not this item is flagged as '<see cref="TileFlag.Background" />'.
        ///     <seealso cref="TileFlag" />
        /// </summary>
        public bool Background { get { return ((m_Flags & TileFlag.Background) != 0); } }

        /// <summary>
        ///     Whether or not this item is flagged as '<see cref="TileFlag.Bridge" />'.
        ///     <seealso cref="TileFlag" />
        /// </summary>
        public bool Bridge { get { return ((m_Flags & TileFlag.Bridge) != 0); } }

        /// <summary>
        ///     Whether or not this item is flagged as '<see cref="TileFlag.Impassable" />'.
        ///     <seealso cref="TileFlag" />
        /// </summary>
        public bool Impassable { get { return ((m_Flags & TileFlag.Impassable) != 0); } }

        /// <summary>
        ///     Whether or not this item is flagged as '<see cref="TileFlag.Surface" />'.
        ///     <seealso cref="TileFlag" />
        /// </summary>
        public bool Surface { get { return ((m_Flags & TileFlag.Surface) != 0); } }

        /// <summary>
        ///     Gets the weight of this item.
        /// </summary>
        public byte Weight { get { return m_Weight; } set { m_Weight = value; } }

        /// <summary>
        ///     Gets the 'quality' of this item. For wearable items, this will be the layer.
        /// </summary>
        public byte Quality { get { return m_Quality; } set { m_Quality = value; } }

        /// <summary>
        ///     Gets the 'quantity' of this item.
        /// </summary>
        public byte Quantity { get { return m_Quantity; } set { m_Quantity = value; } }

        /// <summary>
        ///     Gets the 'value' of this item.
        /// </summary>
        public byte Value { get { return m_Value; } set { m_Value = value; } }

        /// <summary>
        ///     Gets the Hue of this item.
        /// </summary>
        public byte Hue { get { return m_Hue; } set { m_Hue = value; } }

        /// <summary>
        ///     Gets the stackingoffset of this item. (If flag Generic)
        /// </summary>
        public byte StackingOffset { get { return m_StackOffset; } set { m_StackOffset = value; } }

        /// <summary>
        ///     Gets the height of this item.
        /// </summary>
        public byte Height { get { return m_Height; } set { m_Height = value; } }

        /// <summary>
        ///     Gets the MiscData of this item. (old UO Demo weapontemplate definition) (Unk1)
        /// </summary>
        public short MiscData { get { return m_MiscData; } set { m_MiscData = value; } }

        /// <summary>
        ///     Gets the unk2 of this item.
        /// </summary>
        public byte Unk2 { get { return m_Unk2; } set { m_Unk2 = value; } }

        /// <summary>
        ///     Gets the unk3 of this item.
        /// </summary>
        public byte Unk3 { get { return m_Unk3; } set { m_Unk3 = value; } }

        /// <summary>
        ///     Gets the 'calculated height' of this item. For <see cref="Bridge">bridges</see>, this will be:
        ///     <c>
        ///         (<see cref="Height" /> / 2)
        ///     </c>
        ///     .
        /// </summary>
        public int CalcHeight
        {
            get
            {
                if ((m_Flags & TileFlag.Bridge) != 0)
                {
                    return m_Height / 2;
                }
                else
                {
                    return m_Height;
                }
            }
        }

        /// <summary>
        ///     Whether or not this item is wearable as '<see cref="TileFlag.Wearable" />'.
        ///     <seealso cref="TileFlag" />
        /// </summary>
        public bool Wearable { get { return ((m_Flags & TileFlag.Wearable) != 0); } }

        public void ReadData(string[] split)
        {
            m_Name = split[1];
            m_Weight = Convert.ToByte(split[2]);
            m_Quality = Convert.ToByte(split[3]);
            m_Animation = (short)TileData.ConvertStringToInt(split[4]);
            m_Height = Convert.ToByte(split[5]);
            m_Hue = Convert.ToByte(split[6]);
            m_Quantity = Convert.ToByte(split[7]);
            m_StackOffset = Convert.ToByte(split[8]);
            m_MiscData = Convert.ToInt16(split[9]);
            m_Unk1 = Convert.ToInt32(split[10]);
            m_Unk2 = Convert.ToByte(split[11]);
            m_Unk3 = Convert.ToByte(split[12]);

            m_Flags = 0;
            int temp = Convert.ToByte(split[13]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Background;
            }
            temp = Convert.ToByte(split[14]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Weapon;
            }
            temp = Convert.ToByte(split[15]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Transparent;
            }
            temp = Convert.ToByte(split[16]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Translucent;
            }
            temp = Convert.ToByte(split[17]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Wall;
            }
            temp = Convert.ToByte(split[18]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Damaging;
            }
            temp = Convert.ToByte(split[19]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Impassable;
            }
            temp = Convert.ToByte(split[20]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Wet;
            }
            temp = Convert.ToByte(split[21]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Unknown1;
            }
            temp = Convert.ToByte(split[22]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Surface;
            }
            temp = Convert.ToByte(split[23]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Bridge;
            }
            temp = Convert.ToByte(split[24]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Generic;
            }
            temp = Convert.ToByte(split[25]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Window;
            }
            temp = Convert.ToByte(split[26]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.NoShoot;
            }
            temp = Convert.ToByte(split[27]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.ArticleA;
            }
            temp = Convert.ToByte(split[28]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.ArticleAn;
            }
            temp = Convert.ToByte(split[29]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Internal;
            }
            temp = Convert.ToByte(split[30]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Foliage;
            }
            temp = Convert.ToByte(split[31]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.PartialHue;
            }
            temp = Convert.ToByte(split[32]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Unknown2;
            }
            temp = Convert.ToByte(split[33]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Map;
            }
            temp = Convert.ToByte(split[34]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Container;
            }
            temp = Convert.ToByte(split[35]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Wearable;
            }
            temp = Convert.ToByte(split[36]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.LightSource;
            }
            temp = Convert.ToByte(split[37]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Animation;
            }
            temp = Convert.ToByte(split[38]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.HoverOver;
            }
            temp = Convert.ToByte(split[39]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Unknown3;
            }
            temp = Convert.ToByte(split[40]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Armor;
            }
            temp = Convert.ToByte(split[41]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Roof;
            }
            temp = Convert.ToByte(split[42]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.Door;
            }
            temp = Convert.ToByte(split[43]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.StairBack;
            }
            temp = Convert.ToByte(split[44]);
            if (temp != 0)
            {
                m_Flags |= TileFlag.StairRight;
            }
        }


        public bool IsAnimation
        {
            get { return (Flags & TileFlag.Animation) != 0; }
        }

        public bool IsContainer
        {
            get { return (Flags & TileFlag.Container) != 0; }
        }

        public bool IsFoliage
        {
            get { return (Flags & TileFlag.Foliage) != 0; }
        }

        public bool IsGeneric
        {
            get { return (Flags & TileFlag.Generic) != 0; }
        }

        public bool IsImpassable
        {
            get { return (Flags & TileFlag.Impassable) != 0; }
        }

        public bool IsLightSource
        {
            get { return (Flags & TileFlag.LightSource) != 0; }
        }

        public bool IsPartialHue
        {
            get { return (Flags & TileFlag.PartialHue) != 0; }
        }

        public bool IsRoof
        {
            get { return (Flags & TileFlag.Roof) != 0; }
        }

        public bool IsDoor
        {
            get { return (Flags & TileFlag.Door) != 0; }
        }

        public bool IsSurface
        {
            get { return (Flags & TileFlag.Surface) != 0; }
        }

        public bool IsWall
        {
            get { return (Flags & TileFlag.Wall) != 0; }
        }

        public bool IsWearable
        {
            get { return (Flags & TileFlag.Wearable) != 0; }
        }

        public bool IsWet
        {
            get { return (Flags & TileFlag.Wet) != 0; }
        }
        public bool IsBackground
        {
            get { return (Flags & TileFlag.Background) != 0; }
        }

        public bool IsBridge
        {
            get { return (Flags & TileFlag.Bridge) != 0; }
        }
    }



    /// <summary>
    ///     Contains lists of <see cref="LandData">land</see> and <see cref="ItemData">item</see> tile data.
    ///     <seealso cref="LandData" />
    ///     <seealso cref="ItemData" />
    /// </summary>
    public sealed class TileData
    {
        private static LandData[] m_LandData;
        private static ItemData[] m_ItemData;
        private static int[] m_HeightTable;

        /// <summary>
        ///     Gets the list of <see cref="LandData">land tile data</see>.
        /// </summary>
        public static LandData[] LandData { get { return m_LandData; } set { m_LandData = value; } }

        /// <summary>
        ///     Gets the list of <see cref="ItemData">item tile data</see>.
        /// </summary>
        public static ItemData[] ItemData { get { return m_ItemData; } set { m_ItemData = value; } }

        public static int[] HeightTable { get { return m_HeightTable; } }

        private static readonly byte[] m_StringBuffer = new byte[20];

        private static string ReadNameString(BinaryReader bin)
        {
            bin.Read(m_StringBuffer, 0, 20);

            int count;

            for (count = 0; count < 20 && m_StringBuffer[count] != 0; ++count)
            {
                ;
            }

            return Encoding.Default.GetString(m_StringBuffer, 0, count);
        }

        public static unsafe string ReadNameString(byte* buffer)
        {
            int count;
            for (count = 0; count < 20 && *buffer != 0; ++count)
            {
                m_StringBuffer[count] = *buffer++;
            }

            return Encoding.Default.GetString(m_StringBuffer, 0, count);
        }

        private TileData()
        { }

        private static int[] landheader;
        private static int[] itemheader;

        static TileData()
        {
            Initialize();
        }

        public static unsafe void Initialize()
        {
            string filePath = FileManager.GetFilePath("tiledata.mul");

            if (filePath != null)
            {
                using (var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    bool useNeWTileDataFormat = ClientVersion.InstallationIsUopFormat;
                    landheader = new int[512];
                    int j = 0;
                    m_LandData = new LandData[0x4000];

                    var buffer = new byte[fs.Length];
                    GCHandle gc = GCHandle.Alloc(buffer, GCHandleType.Pinned);
                    long currpos = 0;
                    try
                    {
                        fs.Read(buffer, 0, buffer.Length);
                        for (int i = 0; i < 0x4000; i += 32)
                        {
                            var ptrheader = new IntPtr((long)gc.AddrOfPinnedObject() + currpos);
                            currpos += 4;
                            landheader[j++] = (int)Marshal.PtrToStructure(ptrheader, typeof(int));
                            for (int count = 0; count < 32; ++count)
                            {
                                var ptr = new IntPtr((long)gc.AddrOfPinnedObject() + currpos);
                                if (useNeWTileDataFormat)
                                {
                                    currpos += sizeof(NewLandTileDataMul);
                                    var cur = (NewLandTileDataMul)Marshal.PtrToStructure(ptr, typeof(NewLandTileDataMul));
                                    m_LandData[i + count] = new LandData(cur);
                                }
                                else
                                {
                                    currpos += sizeof(OldLandTileDataMul);
                                    var cur = (OldLandTileDataMul)Marshal.PtrToStructure(ptr, typeof(OldLandTileDataMul));
                                    m_LandData[i + count] = new LandData(cur);
                                }
                            }
                        }

                        long remaining = buffer.Length - currpos;
                        int structsize = useNeWTileDataFormat ? sizeof(NewItemTileDataMul) : sizeof(OldItemTileDataMul);
                        itemheader = new int[(remaining / ((structsize * 32) + 4))];
                        int itemlength = itemheader.Length * 32;

                        m_ItemData = new ItemData[itemlength];
                        m_HeightTable = new int[itemlength];

                        j = 0;
                        for (int i = 0; i < itemlength; i += 32)
                        {
                            var ptrheader = new IntPtr((long)gc.AddrOfPinnedObject() + currpos);
                            currpos += 4;
                            itemheader[j++] = (int)Marshal.PtrToStructure(ptrheader, typeof(int));
                            for (int count = 0; count < 32; ++count)
                            {
                                var ptr = new IntPtr((long)gc.AddrOfPinnedObject() + currpos);
                                if (useNeWTileDataFormat)
                                {
                                    currpos += sizeof(NewItemTileDataMul);
                                    var cur = (NewItemTileDataMul)Marshal.PtrToStructure(ptr, typeof(NewItemTileDataMul));
                                    m_ItemData[i + count] = new ItemData(cur);
                                    m_HeightTable[i + count] = cur.height;
                                }
                                else
                                {
                                    currpos += sizeof(OldItemTileDataMul);
                                    var cur = (OldItemTileDataMul)Marshal.PtrToStructure(ptr, typeof(OldItemTileDataMul));
                                    m_ItemData[i + count] = new ItemData(cur);
                                    m_HeightTable[i + count] = cur.height;
                                }
                            }
                        }
                    }
                    finally
                    {
                        gc.Free();
                    }
                }
            }
        }

        /// <summary>
        ///     Saves <see cref="LandData" /> and <see cref="ItemData" /> to tiledata.mul
        /// </summary>
        /// <param name="FileName"></param>
        public static void SaveTileData(string FileName)
        {
            using (var fs = new FileStream(FileName, FileMode.Create, FileAccess.Write, FileShare.Write))
            {
                using (var bin = new BinaryWriter(fs))
                {
                    int j = 0;
                    bool useNewTileDataFormat = ClientVersion.InstallationIsUopFormat;
                    for (int i = 0; i < 0x4000; ++i)
                    {
                        if ((i & 0x1F) == 0)
                        {
                            bin.Write(landheader[j++]); //header
                        }

                        bin.Write((int)m_LandData[i].Flags);
                        if (useNewTileDataFormat)
                        {
                            bin.Write(m_LandData[i].Unk1);
                        }

                        bin.Write(m_LandData[i].TextureID);
                        var b = new byte[20];
                        if (m_LandData[i].Name != null)
                        {
                            byte[] bb = Encoding.Default.GetBytes(m_LandData[i].Name);
                            if (bb.Length > 20)
                            {
                                Array.Resize(ref bb, 20);
                            }
                            bb.CopyTo(b, 0);
                        }
                        bin.Write(b);
                    }
                    j = 0;
                    for (int i = 0; i < m_ItemData.Length; ++i)
                    {
                        if ((i & 0x1F) == 0)
                        {
                            bin.Write(itemheader[j++]); // header
                        }

                        bin.Write((int)m_ItemData[i].Flags);
                        if (useNewTileDataFormat)
                        {
                            bin.Write(m_ItemData[i].Unk1);
                        }

                        bin.Write(m_ItemData[i].Weight);
                        bin.Write(m_ItemData[i].Quality);
                        bin.Write(m_ItemData[i].MiscData);
                        bin.Write(m_ItemData[i].Unk2);
                        bin.Write(m_ItemData[i].Quantity);
                        bin.Write(m_ItemData[i].Animation);
                        bin.Write(m_ItemData[i].Unk3);
                        bin.Write(m_ItemData[i].Hue);
                        bin.Write(m_ItemData[i].StackingOffset); //unk4
                        bin.Write(m_ItemData[i].Value); //unk5
                        bin.Write(m_ItemData[i].Height);
                        var b = new byte[20];
                        if (m_ItemData[i].Name != null)
                        {
                            byte[] bb = Encoding.Default.GetBytes(m_ItemData[i].Name);
                            if (bb.Length > 20)
                            {
                                Array.Resize(ref bb, 20);
                            }
                            bb.CopyTo(b, 0);
                        }
                        bin.Write(b);
                    }
                }
            }
        }

        /// <summary>
        ///     Exports <see cref="ItemData" /> to csv file
        /// </summary>
        /// <param name="FileName"></param>
        public static void ExportItemDataToCSV(string FileName)
        {
            using (
                var Tex = new StreamWriter(
                    new FileStream(FileName, FileMode.Create, FileAccess.ReadWrite), Encoding.GetEncoding(1252)))
            {
                Tex.Write(
                    "ID;Name;Weight/Quantity;Layer/Quality;Gump/AnimID;Height;Hue;Class/Quantity;StackingOffset;MiscData;Unknown1;Unknown2;Unknown3");
                Tex.Write(";Background;Weapon;Transparent;Translucent;Wall;Damage;Impassible;Wet;Unknow1");
                Tex.Write(";Surface;Bridge;Generic;Window;NoShoot;PrefixA;PrefixAn;Internal;Foliage;PartialHue");
                Tex.Write(";Unknow2;Map;Container/Height;Wearable;Lightsource;Animation;HoverOver");
                Tex.WriteLine(";Unknow3;Armor;Roof;Door;StairBack;StairRight");

                for (int i = 0; i < m_ItemData.Length; ++i)
                {
                    ItemData tile = m_ItemData[i];
                    Tex.Write(String.Format("0x{0:X4}", i));
                    Tex.Write(String.Format(";{0}", tile.Name));
                    Tex.Write(";" + tile.Weight);
                    Tex.Write(";" + tile.Quality);
                    Tex.Write(String.Format(";0x{0:X4}", tile.Animation));
                    Tex.Write(";" + tile.Height);
                    Tex.Write(";" + tile.Hue);
                    Tex.Write(";" + tile.Quantity);
                    Tex.Write(";" + tile.StackingOffset);
                    Tex.Write(";" + tile.MiscData);
                    Tex.Write(";" + tile.Unk1);
                    Tex.Write(";" + tile.Unk2);
                    Tex.Write(";" + tile.Unk3);

                    Tex.Write(";" + (((tile.Flags & TileFlag.Background) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Weapon) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Transparent) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Translucent) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Wall) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Damaging) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Impassable) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Wet) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Unknown1) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Surface) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Bridge) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Generic) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Window) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.NoShoot) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.ArticleA) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.ArticleAn) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Internal) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Foliage) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.PartialHue) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Unknown2) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Map) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Container) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Wearable) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.LightSource) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Animation) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.HoverOver) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Unknown3) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Armor) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Roof) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Door) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.StairBack) != 0) ? "1" : "0"));
                    Tex.WriteLine(";" + (((tile.Flags & TileFlag.StairRight) != 0) ? "1" : "0"));
                }
            }
        }

        /// <summary>
        ///     Exports <see cref="LandData" /> to csv file
        /// </summary>
        /// <param name="FileName"></param>
        public static void ExportLandDataToCSV(string FileName)
        {
            using (var Tex = new StreamWriter(new FileStream(FileName, FileMode.Create, FileAccess.ReadWrite)))
            {
                Tex.Write("ID;Name;TextureID;HSAUnk1");
                Tex.Write(";Background;Weapon;Transparent;Translucent;Wall;Damage;Impassible;Wet;Unknow1");
                Tex.Write(";Surface;Bridge;Generic;Window;NoShoot;PrefixA;PrefixAn;Internal;Foliage;PartialHue");
                Tex.Write(";Unknow2;Map;Container/Height;Wearable;Lightsource;Animation;HoverOver");
                Tex.WriteLine(";Unknow3;Armor;Roof;Door;StairBack;StairRight");

                for (int i = 0; i < m_LandData.Length; ++i)
                {
                    LandData tile = m_LandData[i];
                    Tex.Write(String.Format("0x{0:X4}", i));
                    Tex.Write(";" + tile.Name);
                    Tex.Write(";" + String.Format("0x{0:X4}", tile.TextureID));
                    Tex.Write(";" + tile.Unk1);

                    Tex.Write(";" + (((tile.Flags & TileFlag.Background) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Weapon) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Transparent) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Translucent) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Wall) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Damaging) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Impassable) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Wet) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Unknown1) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Surface) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Bridge) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Generic) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Window) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.NoShoot) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.ArticleA) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.ArticleAn) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Internal) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Foliage) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.PartialHue) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Unknown2) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Map) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Container) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Wearable) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.LightSource) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Animation) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.HoverOver) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Unknown3) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Armor) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Roof) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.Door) != 0) ? "1" : "0"));
                    Tex.Write(";" + (((tile.Flags & TileFlag.StairBack) != 0) ? "1" : "0"));
                    Tex.WriteLine(";" + (((tile.Flags & TileFlag.StairRight) != 0) ? "1" : "0"));
                }
            }
        }

        public static int ConvertStringToInt(string text)
        {
            int result;
            if (text.Contains("0x"))
            {
                string convert = text.Replace("0x", "");
                int.TryParse(convert, NumberStyles.HexNumber, null, out result);
            }
            else
            {
                int.TryParse(text, NumberStyles.Integer, null, out result);
            }

            return result;
        }

        public static void ImportItemDataFromCSV(string FileName)
        {
            if (!File.Exists(FileName))
            {
                return;
            }
            using (var sr = new StreamReader(FileName))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if ((line = line.Trim()).Length == 0 || line.StartsWith("#"))
                    {
                        continue;
                    }
                    if (line.StartsWith("ID;"))
                    {
                        continue;
                    }
                    try
                    {
                        string[] split = line.Split(';');
                        if (split.Length < 45)
                        {
                            continue;
                        }

                        int id = ConvertStringToInt(split[0]);
                        m_ItemData[id].ReadData(split);
                    }
                    catch
                    { }
                }
            }
        }

        public static void ImportLandDataFromCSV(string FileName)
        {
            if (!File.Exists(FileName))
            {
                return;
            }
            using (var sr = new StreamReader(FileName))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    if ((line = line.Trim()).Length == 0 || line.StartsWith("#"))
                    {
                        continue;
                    }
                    if (line.StartsWith("ID;"))
                    {
                        continue;
                    }
                    try
                    {
                        string[] split = line.Split(';');
                        if (split.Length < 36)
                        {
                            continue;
                        }

                        int id = ConvertStringToInt(split[0]);
                        m_LandData[id].ReadData(split);
                    }
                    catch
                    { }
                }
            }
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct OldLandTileDataMul
    {
        public int flags;
        public short texID;
        public fixed byte name[20];
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct NewLandTileDataMul
    {
        public int flags;
        public int unk1;
        public short texID;
        public fixed byte name[20];
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct OldItemTileDataMul
    {
        public int flags;
        public byte weight;
        public byte quality;
        public short miscdata;
        public byte unk2;
        public byte quantity;
        public short anim;
        public byte unk3;
        public byte hue;
        public byte stackingoffset;
        public byte value;
        public byte height;
        public fixed byte name[20];
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public unsafe struct NewItemTileDataMul
    {
        public int flags;
        public int unk1;
        public byte weight;
        public byte quality;
        public short miscdata;
        public byte unk2;
        public byte quantity;
        public short anim;
        public byte unk3;
        public byte hue;
        public byte stackingoffset;
        public byte value;
        public byte height;
        public fixed byte name[20];
    }

    /* public class TileData
     {
         public static LandData[] LandData = new LandData[0x4000];
         public static ItemData[] ItemData = new ItemData[0x4000];

         public static ItemData ItemData_ByAnimID(int animID)
         {
             for (int i = 0; i < ItemData.Length; i++)
             {
                 if (ItemData[i].AnimID == animID)
                     return ItemData[i];
             }
             return new ItemData();
         }

         // Issue 5 - Statics (bridge, stairs, etc) should be walkable - http://code.google.com/p/ultimaxna/issues/detail?id=5 - Smjert
         // Stairs IDs, taken from RunUO Data folder (stairs.txt)
         private static readonly int[] _StairsID = {
             1006, 1007, 1008, 1009, 1010, 1012, 1014, 1016, 1017,
             1801, 1802, 1803, 1804, 1805, 1807, 1809, 1811, 1812,
             1822, 1823, 1825, 1826, 1827, 1828, 1829, 1831, 1833,
             1835, 1836, 1846, 1847, 1848, 1849, 1850, 1851, 1852,
             1854, 1856, 1861, 1862, 1865, 1867, 1869, 1872, 1873,
             1874, 1875, 1876, 1878, 1880, 1882, 1883, 1900, 1901,
             1902, 1903, 1904, 1906, 1908, 1910, 1911, 1928, 1929,
             1930, 1931, 1932, 1934, 1936, 1938, 1939, 1955, 1956,
             1957, 1958, 1959, 1961, 1963, 1978, 1979, 1980, 1991,
             7600, 7601, 7602, 7603, 7604, 7605, 7606, 7607, 7608,
             7609, 7610, 7611, 7612, 7613, 7614, 7615, 7616, 7617,
             7618, 7619, 7620, 7621, 7622, 7623, 7624, 7625, 7626,
             7627, 7628, 7629, 7630, 7631, 7632, 7633, 7634, 7635,
             7636, 7639
         };
         // Issue 5 - End

         static TileData()
         {
             using (FileStream fileStream = FileManager.GetFile("tiledata.mul"))
             {
                 BinaryReader bin = new BinaryReader(fileStream);

                 LandData landData;
                 ItemData itemData;

                 if (fileStream.Length == 3188736)
                 { // 7.0.9.0
                     LandData = new LandData[0x4000];

                     for (int i = 0; i < 0x4000; ++i)
                     {
                         landData = new LandData();

                         if (i == 1 || (i > 0 && (i & 0x1F) == 0))
                         {
                             bin.ReadInt32();
                         }

                         TileFlag flags = (TileFlag)bin.ReadInt64();

                         int iTextureID = bin.ReadInt16();

                         bin.BaseStream.Seek(20, SeekOrigin.Current);

                         landData.Flags = flags;
                         landData.TextureID = iTextureID;

                         LandData[i] = landData;
                     }

                     ItemData = new ItemData[0x10000];

                     for (int i = 0; i < 0x10000; ++i)
                     {
                         itemData = new ItemData();

                         if ((i & 0x1F) == 0)
                         {
                             bin.ReadInt32();
                         }

                         itemData.Flags = (TileFlag)bin.ReadInt64();
                         itemData.Weight = bin.ReadByte();
                         itemData.Quality = bin.ReadByte();

                         itemData.Unknown1 = bin.ReadByte();
                         itemData.Unknown2 = bin.ReadByte();
                         itemData.Unknown3 = bin.ReadByte();

                         itemData.Quantity = bin.ReadByte();
                         itemData.AnimID = bin.ReadInt16();

                         bin.BaseStream.Seek(2, SeekOrigin.Current); // hue?
                         itemData.Unknown4 = bin.ReadByte();

                         itemData.Value = bin.ReadByte();
                         itemData.Height = bin.ReadByte();

                         itemData.Name = ASCIIEncoding.ASCII.GetString((bin.ReadBytes(20)));
                         itemData.Name = itemData.Name.Trim('\0');
                         // binaryReader.BaseStream.Seek(20, SeekOrigin.Current);

                         // Issue 5 - Statics (bridge, stairs, etc) should be walkable - http://code.google.com/p/ultimaxna/issues/detail?id=5 - Smjert
                         if (i > 1005 && i < 7640)
                             itemData.IsStairs = !(Array.BinarySearch(_StairsID, i) < 0);
                         // Issue 5 - End

                         ItemData[i] = itemData;
                     }
                 }
                 else
                 {
                     LandData = new LandData[0x4000];

                     for (int i = 0; i < 0x4000; ++i)
                     {

                         landData = new LandData();

                         if ((i & 0x1F) == 0)
                         {
                             bin.ReadInt32();
                         }

                         TileFlag flags = (TileFlag)bin.ReadInt32();

                         int iTextureID = bin.ReadInt16();

                         bin.BaseStream.Seek(20, SeekOrigin.Current);

                         landData.Flags = flags;
                         landData.TextureID = iTextureID;

                         LandData[i] = landData;
                     }

                     if (fileStream.Length == 1644544)
                     { // 7.0.0.0
                         ItemData = new ItemData[0x8000];

                         for (int i = 0; i < 0x8000; ++i)
                         {
                             itemData = new ItemData();

                             if ((i & 0x1F) == 0)
                             {
                                 bin.ReadInt32();
                             }

                             itemData.Flags = (TileFlag)bin.ReadInt32();
                             itemData.Weight = bin.ReadByte();
                             itemData.Quality = bin.ReadByte();

                             itemData.Unknown1 = bin.ReadByte();
                             itemData.Unknown2 = bin.ReadByte();
                             itemData.Unknown3 = bin.ReadByte();

                             itemData.Quantity = bin.ReadByte();
                             itemData.AnimID = bin.ReadInt16();

                             bin.BaseStream.Seek(2, SeekOrigin.Current); // hue?
                             itemData.Unknown4 = bin.ReadByte();

                             itemData.Value = bin.ReadByte();
                             itemData.Height = bin.ReadByte();

                             itemData.Name = ASCIIEncoding.ASCII.GetString((bin.ReadBytes(20)));
                             itemData.Name = itemData.Name.Trim('\0');
                             // binaryReader.BaseStream.Seek(20, SeekOrigin.Current);

                             // Issue 5 - Statics (bridge, stairs, etc) should be walkable - http://code.google.com/p/ultimaxna/issues/detail?id=5 - Smjert
                             if (i > 1005 && i < 7640)
                                 itemData.IsStairs = !(Array.BinarySearch(_StairsID, i) < 0);
                             // Issue 5 - End

                             ItemData[i] = itemData;
                         }
                     }
                     else
                     {
                         ItemData = new ItemData[0x4000];

                         for (int i = 0; i < 0x4000; ++i)
                         {
                             itemData = new ItemData();

                             if ((i & 0x1F) == 0)
                             {
                                 bin.ReadInt32();
                             }

                             itemData.Flags = (TileFlag)bin.ReadInt32();
                             itemData.Weight = bin.ReadByte();
                             itemData.Quality = bin.ReadByte();

                             itemData.Unknown1 = bin.ReadByte();
                             itemData.Unknown2 = bin.ReadByte();
                             itemData.Unknown3 = bin.ReadByte();

                             itemData.Quantity = bin.ReadByte();
                             itemData.AnimID = bin.ReadInt16();

                             bin.BaseStream.Seek(2, SeekOrigin.Current); // hue?
                             itemData.Unknown4 = bin.ReadByte();

                             itemData.Value = bin.ReadByte();
                             itemData.Height = bin.ReadByte();

                             itemData.Name = ASCIIEncoding.ASCII.GetString((bin.ReadBytes(20)));
                             itemData.Name = itemData.Name.Trim('\0');
                             // binaryReader.BaseStream.Seek(20, SeekOrigin.Current);

                             // Issue 5 - Statics (bridge, stairs, etc) should be walkable - http://code.google.com/p/ultimaxna/issues/detail?id=5 - Smjert
                             if (i > 1005 && i < 7640)
                                 itemData.IsStairs = !(Array.BinarySearch(_StairsID, i) < 0);
                             // Issue 5 - End

                             ItemData[i] = itemData;
                         }
                     }
                 }

                 Metrics.ReportDataRead((int)bin.BaseStream.Position);
             }
         }
     }

     public struct ItemData
     {
         public int Weight;
         public TileFlag Flags;
         public int Height;
         public int Quality;
         public int Quantity;
         public int AnimID;
         public int Value;
         public string Name;
         public bool IsStairs;

         public byte Unknown1, Unknown2, Unknown3, Unknown4;

         public bool IsBackground
         {
             get { return (Flags & TileFlag.Background) != 0; }
         }

         public bool IsBridge
         {
             get { return (Flags & TileFlag.Bridge) != 0; }
         }

         public int CalcHeight
         {
             get
             {
                 if ((Flags & TileFlag.Bridge) != 0)
                 {
                     return Height; // / 2;
                 }
                 else
                 {
                     return Height;
                 }
             }
         }

         public bool IsAnimation
         {
             get { return (Flags & TileFlag.Animation) != 0; }
         }

         public bool IsContainer
         {
             get { return (Flags & TileFlag.Container) != 0; }
         }

         public bool IsFoliage
         {
             get { return (Flags & TileFlag.Foliage) != 0; }
         }

         public bool IsGeneric
         {
             get { return (Flags & TileFlag.Generic) != 0; }
         }

         public bool IsImpassable
         {
             get { return (Flags & TileFlag.Impassable) != 0; }
         }

         public bool IsLightSource
         {
             get { return (Flags & TileFlag.LightSource) != 0; }
         }

         public bool IsPartialHue
         {
             get { return (Flags & TileFlag.PartialHue) != 0; }
         }

         public bool IsRoof
         {
             get { return (Flags & TileFlag.Roof) != 0; }
         }

         public bool IsDoor
         {
             get { return (Flags & TileFlag.Door) != 0; }
         }

         public bool IsSurface
         {
             get { return (Flags & TileFlag.Surface) != 0; }
         }

         public bool IsWall
         {
             get { return (Flags & TileFlag.Wall) != 0; }
         }

         public bool IsWearable
         {
             get { return (Flags & TileFlag.Wearable) != 0; }
         }

         public bool IsWet
         {
             get { return (Flags & TileFlag.Wet) != 0; }
         }
     }

     public struct LandData
     {
         public TileFlag Flags;
         public int TextureID;

         public bool IsWet
         {
             get { return (Flags & TileFlag.Wet) != 0; }
         }

         public bool IsImpassible
         {
             get { return (Flags & TileFlag.Impassable) != 0; }
         }
     }*/
}