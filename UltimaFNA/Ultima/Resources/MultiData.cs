﻿/***************************************************************************
 *   MultiData.cs
 *   Based on code from UltimaSDK: http://ultimasdk.codeplex.com/
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using Microsoft.Xna.Framework;
using System.Linq;
using UltimaXNA.Core.Diagnostics;
using UltimaXNA.Core.IO;
using UltimaXNA.Ultima.Data;
using UltimaXNA.Ultima.IO;
#endregion

namespace UltimaXNA.Ultima.Resources
{
    public class MultiData
    {
        private static MultiComponentList[] _Components = new MultiComponentList[0x2000];
        public static MultiComponentList[] Cache { get { return _Components; } }

        private static readonly AFileIndex _FileIndex = FileManager.CreateFileIndex("Multi.idx", "Multi.mul", 0x2000, 14);
        public static AFileIndex FileIndex { get { return _FileIndex; } }

        public static MultiComponentList GetComponents(int index)
        {
            MultiComponentList mcl;

            index &= FileManager.ItemIDMask;

            if (index >= 0 && index < _Components.Length)
            {
                mcl = _Components[index];

                if (mcl == null)
                    _Components[index] = mcl = Load(index);
            }
            else
            {
                mcl = MultiComponentList.Empty;
            }

            return mcl;
        }

        public static MultiComponentList Load(int index)
        {
            try
            {
                BinaryFileReader reader = _FileIndex.Seek(index, out int length, out int extra, out bool patched);
                if (reader == null)
                    return MultiComponentList.Empty;
                
                if (ClientVersion.ClientInfo >= ClientVersionTypes.HighSeas)
                    return new MultiComponentList(reader, length / 16);
                else
                    return new MultiComponentList(reader, length / 12);
            }
            catch
            {
                return MultiComponentList.Empty;
            }
        }
    }

    public sealed class MultiComponentList
    {
        private Point _Min, _Max, _Center;
        private int _Width, _Height;

        public static readonly MultiComponentList Empty = new MultiComponentList();

        public Point Min { get { return _Min; } }
        public Point Max { get { return _Max; } }
        public Point Center { get { return _Center; } }
        public int Width { get { return _Width; } }
        public int Height { get { return _Height; } }

        private StaticTile[][][] _tiles;
        public StaticTile[][][] Tiles => _tiles;

        public MultiItem[] Items
        {
            get;
            private set;
        }

        public struct MultiItem
        {
            public short ItemID;
            public short OffsetX, OffsetY, OffsetZ;
            public int Flags;

            public override string ToString()
            {
                return string.Format("{0:X4} {1} {2} {3} {4:X4}", ItemID, OffsetX, OffsetY, OffsetZ, Flags);
            }
        }

        public MultiComponentList(BinaryFileReader reader, int count)
        {
            int metrics_dataread_start = (int)reader.Position;

            _Min = _Max = Point.Zero;

           Items = new MultiItem[count];

            for (int i = 0; i < count; ++i)
            {
                Items[i].ItemID = reader.ReadShort();
                Items[i].OffsetX = reader.ReadShort();
                Items[i].OffsetY = reader.ReadShort();
                Items[i].OffsetZ = reader.ReadShort();
                Items[i].Flags = reader.ReadInt();

                if (ClientVersion.ClientInfo >= ClientVersionTypes.HighSeas)
                    reader.ReadInt();

                if (Items[i].OffsetX < _Min.X)
                    _Min.X = Items[i].OffsetX;

                if (Items[i].OffsetY < _Min.Y)
                    _Min.Y = Items[i].OffsetY;

                if (Items[i].OffsetX > _Max.X)
                    _Max.X = Items[i].OffsetX;

                if (Items[i].OffsetY > _Max.Y)
                    _Max.Y = Items[i].OffsetY;
            }

            _Center = new Point(-_Min.X, -_Min.Y);
            _Width = (_Max.X - _Min.X) + 1;
            _Height = (_Max.Y - _Min.Y) + 1;

            StaticTileList[][] tiles = new StaticTileList[_Width][];
            _tiles = new StaticTile[_Width][][];

            for (int x = 0; x < _Width; ++x)
            {
                tiles[x] = new StaticTileList[_Height];
                _tiles[x] = new StaticTile[_Height][];

                for (int y = 0; y < _Height; ++y)
                    tiles[x][y] = new StaticTileList();
            }

            for (int i = 0; i < Items.Length; ++i)
            {
                int xOffset = Items[i].OffsetX + Center.X;
                int yOffset = Items[i].OffsetY + Center.Y;

                tiles[xOffset][yOffset].Add((short)((Items[i].ItemID & FileManager.ItemIDMask) /*+ 0x4000*/), (sbyte)Items[i].OffsetZ);
            }

            for (int x = 0; x < _Width; ++x)
            {
                for (int y = 0; y < _Height; ++y)
                {
                    _tiles[x][y] = tiles[x][y].ToArray();
                }

            }
                // SortMultiComponentList();

                Metrics.ReportDataRead((int)reader.Position - metrics_dataread_start);
        }

        private void SortMultiComponentList()
        {
            Items = Items.OrderBy(a => a.OffsetY).ThenBy(a => a.OffsetX).ToArray();
        }

        private MultiComponentList()
        {
             Items = new MultiItem[0];
            _tiles = new StaticTile[0][][];
        }
    }

    public class StaticTileList
    {
        private static StaticTile[] m_EmptyTiles = new StaticTile[0];

        private int m_Count;
        private StaticTile[] m_StaticTiles;

        public StaticTileList()
        {
            m_Count = 0;
            m_StaticTiles = new StaticTile[8];
        }

        public int Count
        {
            get { return m_Count; }
        }

        public void AddRange(StaticTile[] tiles)
        {
            if ((m_Count + tiles.Length) > m_StaticTiles.Length)
            {
                StaticTile[] old = m_StaticTiles;

                m_StaticTiles = new StaticTile[(m_Count + tiles.Length) * 2];

                for (int i = 0; i < old.Length; ++i)
                {
                    m_StaticTiles[i] = old[i];
                }
            }

            for (int i = 0; i < tiles.Length; ++i)
            {
                m_StaticTiles[m_Count++] = tiles[i];
            }
        }

        public void Add(short id, sbyte z)
        {
            if ((m_Count + 1) > m_StaticTiles.Length)
            {
                StaticTile[] old = m_StaticTiles;

                m_StaticTiles = new StaticTile[old.Length * 2];

                for (int i = 0; i < old.Length; ++i)
                {
                    m_StaticTiles[i] = old[i];
                }
            }

            m_StaticTiles[m_Count].ID = id;
            m_StaticTiles[m_Count].Z = z;

            m_Count++;
        }

        public StaticTile[] ToArray()
        {
            if (m_Count == 0)
            {
                return m_EmptyTiles;
            }

            StaticTile[] staticTiles = new StaticTile[m_Count];

            for (int i = 0; i < m_Count; ++i)
            {
                staticTiles[i] = m_StaticTiles[i];
            }

            m_Count = 0;

            return staticTiles;
        }
    }
}