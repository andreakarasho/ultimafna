﻿/***************************************************************************
 *   AnimationResource.cs
 *   Based on code from UltimaSDK: http://ultimasdk.codeplex.com/
 *   
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using UltimaXNA.Core.IO;
using UltimaXNA.Core.Network.Compression;
using UltimaXNA.Core.Resources;
using UltimaXNA.Ultima.IO;
using UltimaXNA.Ultima.IO.UOP;
#endregion

namespace UltimaXNA.Ultima.Resources
{
    internal sealed class AnimationResource
    {
        private struct UopDataFrame
        {
            public short FrameID;
            public int PixelDataOffset;
            public int DataStart;

            public UopDataFrame(short frameid, int offset, int start)
            {
                FrameID = frameid; PixelDataOffset = offset; DataStart = start;
            }

            public static UopDataFrame NullUopFrame = new UopDataFrame(0,0,-1);
        }


        private const int ANIM_COUNT = 0x0800;
        private const int ANIM_MASK = 0x07FF;

        public const int HUMANOID_STAND_INDEX = 0x04;
        public const int HUMANOID_MOUNT_INDEX = 0x19;
        public const int HUMANOID_SIT_INDEX = 0x23; // 35

        private const int COUNT_ANIMS = 0x1000;
        private const int COUNT_ACTIONS = 36; // max UO action index is 34 (0-based, thus 35), we add one additional index for the humanoid sitting action.
        private const int COUNT_DIRECTIONS = 8;

        private AFileIndex m_FileIndex = FileManager.CreateFileIndex("Anim.idx", "Anim.mul", 0x40000, 6);
        public AFileIndex FileIndex { get { return m_FileIndex; } }

        private AFileIndex m_FileIndex2 = FileManager.CreateFileIndex("Anim2.idx", "Anim2.mul", 0x10000, -1);
        public AFileIndex FileIndex2 { get { return m_FileIndex2; } }

        private AFileIndex m_FileIndex3 = FileManager.CreateFileIndex("Anim3.idx", "Anim3.mul", 0x20000, -1);
        public AFileIndex FileIndex3 { get { return m_FileIndex3; } }

        private AFileIndex m_FileIndex4 = FileManager.CreateFileIndex("Anim4.idx", "Anim4.mul", 0x20000, -1);
        public AFileIndex FileIndex4 { get { return m_FileIndex4; } }

        private AFileIndex m_FileIndex5 = FileManager.CreateFileIndex("Anim5.idx", "Anim5.mul", 0x20000, -1);
        public AFileIndex FileIndex5 { get { return m_FileIndex5; } }

        private List<UopFileIndex> _uopIndexes = new List<UopFileIndex>();

        private AAnimationFrame[][][][] m_Cache, _cacheUop;
        private GraphicsDevice m_Graphics;

        private bool _isUop;


     //   private bool[] _anims;

        public AnimationResource(GraphicsDevice graphics)
        {
            m_Graphics = graphics;
            m_Cache = new AAnimationFrame[COUNT_ANIMS][][][];
            _cacheUop = new AAnimationFrame[2048][][][];
          //  _anims = new bool[4096];

            for (int i = 1; i < 5; i++)
            {
                string file = $"AnimationFrame{i}.uop";
                if (File.Exists(FileManager.GetPath(file)))
                {
                    var f = (UopFileIndex)FileManager.CreateFileIndex(file);
                   /* foreach (int index in f.AnimationIdx)
                        _anims[index] = true;*/
                    _uopIndexes.Add(f);
                }
            }          
        }

        public AAnimationFrame[] GetAnimation(int body, ref int hue, int action, int direction)
        {
            int animIndex;
            AFileIndex fileIndex;
            AnimationFrame.SittingTransformation sitting = AnimationFrame.SittingTransformation.None;

            if (body <= 0)
                return null;

            if (action < 0)
                action = 0;

            if (direction < 0)
                direction = 0;


            if (!BodyExists(body))
                BodyDef.TranslateBodyAndHue(ref body, ref hue);

            AAnimationFrame[] frames = CheckCache(body, action, direction);
            if (frames != null)
                return frames;

            if (action == HUMANOID_SIT_INDEX)
            {
                if (direction == 3 || direction == 5)
                {
                    sitting = AnimationFrame.SittingTransformation.MountNorth;
                    GetIndexes(body, HUMANOID_MOUNT_INDEX, direction, out animIndex, out fileIndex);
                }
                else if (direction == 1 || direction == 7)
                {
                    sitting = AnimationFrame.SittingTransformation.StandSouth;
                    GetIndexes(body, HUMANOID_STAND_INDEX, direction, out animIndex, out fileIndex);
                }
                else
                {
                    GetIndexes(body, action, direction, out animIndex, out fileIndex);
                }
            }
            else
            {
                GetIndexes(body, action, direction, out animIndex, out fileIndex);
            }

            BinaryFileReader reader = fileIndex.Seek(animIndex, out int length, out int extra, out bool patched);
            if (reader == null)
                return null;

            if (!_isUop)
            {
                frames = LoadAnimation(reader, sitting, body, action, direction);
                return m_Cache[body][action][direction] = frames;
            }
            else
            {
                frames = LoadUopAnimation((UopFileIndex)fileIndex, animIndex, sitting, body, action, direction);
                return _cacheUop[body][action][direction] = frames;
            } 
        }

        private AAnimationFrame[] LoadUopAnimation(UopFileIndex fileindex, int index, AnimationFrame.SittingTransformation sitting, int body, int action, int direction)
        {
             FileIndexEntry3D e = fileindex.Index[index];

             fileindex.Stream.Position = e.UOPData.Offset;
             byte[] buffer = new byte[e.UOPData.CompressedLength];
             fileindex.Stream.Read(buffer, 0, e.UOPData.CompressedLength);

             int clen = e.UOPData.CompressedLength;
             int dlen = e.UOPData.DecompressedLength;

             byte[] decbuffer = new byte[dlen];
             if (ZlibCompression.Unpack(decbuffer, ref dlen, buffer, clen) != ZLibError.Okay)
                 return AnimationFrame.NullFrames;

            int start = 0;// (int)fileindex.Stream.Position;

            BinaryFileReader reader = new BinaryFileReader(new MemoryStream(decbuffer));

            reader.ReadInt();
            reader.ReadInt();
            int dcsize = reader.ReadInt();
            int animid = reader.ReadInt();
            reader.ReadInt();
            reader.ReadInt();
            reader.ReadShort();
            reader.ReadShort();
            reader.ReadInt();
            int framecount = reader.ReadInt();
            int datastart = start + reader.ReadInt();

            reader.Position = datastart;
            List<UopDataFrame> datas = new List<UopDataFrame>();
            for (int i = 0; i < framecount; i++)
            {
                UopDataFrame data = new UopDataFrame();
                data.DataStart = (int)reader.Position;
                reader.ReadShort();
                data.FrameID = reader.ReadShort();
                reader.ReadInt();
                reader.ReadInt();
                data.PixelDataOffset = reader.ReadInt();

                int vsize = datas.Count;
                if (vsize + 1 != data.FrameID)
                {
                    while (vsize + 1 != data.FrameID)
                    {
                        datas.Add(UopDataFrame.NullUopFrame);
                        vsize++;
                    }
                }
                datas.Add(data);
            }

            int animframecount = datas.Count / 5; 
            AnimationFrame[] frames = new AnimationFrame[animframecount];

            int dir = direction & 7;

            if (dir > 4)
                dir = dir - (dir - 4) * 2;

            int framestartindx = animframecount * dir; 

            for (int i = 0; i < animframecount; i++)
            {
                UopDataFrame framedata = datas[i + framestartindx];
                if (framedata.DataStart == -1)
                {
                    frames[i] = AnimationFrame.NullFrame;
                    continue;
                }

                reader.Position = framedata.DataStart + framedata.PixelDataOffset;

                ushort[] palette = GetPalette(reader);

                int uniqueAnimationIndex = ((body & 0xfff) << 20) + ((action & 0x3f) << 12) + ((direction & 0x0f) << 8);
                frames[i] = new AnimationFrame(uniqueAnimationIndex, m_Graphics, palette, reader, sitting);
            }

            return frames;    
        }

        private AAnimationFrame[] LoadAnimation(BinaryFileReader reader, AnimationFrame.SittingTransformation sitting, int body, int action, int direction)
        {
            ushort[] palette = GetPalette(reader); // 0x100 * 2 = 0x0200 bytes
            int read_start = (int)reader.Position; // save file position after palette.

            int frameCount = reader.ReadInt(); // 0x04 bytes

            int[] lookups = new int[frameCount]; // frameCount * 0x04 bytes
            for (int i = 0; i < frameCount; ++i) { lookups[i] = reader.ReadInt(); }

            AAnimationFrame[] frames = new AAnimationFrame[frameCount];
            for (int i = 0; i < frameCount; ++i)
            {
                if (lookups[i] < lookups[0])
                {
                    frames[i] = AnimationFrame.NullFrame; // Fix for broken animations, per issue13
                }
                else
                {
                    int uniqueAnimationIndex = ((body & 0xfff) << 20) + ((action & 0x3f) << 12) + ((direction & 0x0f) << 8);

                    reader.Seek(read_start + lookups[i], SeekOrigin.Begin);
                    frames[i] = new AnimationFrame(uniqueAnimationIndex, m_Graphics, palette, reader, sitting);
                }
            }
            return frames;
        }

        private ushort[] GetPalette(BinaryFileReader reader)
        {
            ushort[] pal = new ushort[0x100];
            for (int i = 0; i < pal.Length; ++i)
            {
                pal[i] = (ushort)(reader.ReadUShort() | 0x8000);
            }
            return pal;
        }

        private AAnimationFrame[] CheckCache(int body, int action, int direction)
        {
            if (_isUop)
            {
                if (_cacheUop[body] == null)
                    _cacheUop[body] = new AAnimationFrame[COUNT_ACTIONS][][]; // max 35 actions
                if (_cacheUop[body][action] == null)
                    _cacheUop[body][action] = new AAnimationFrame[COUNT_DIRECTIONS][];
                if (_cacheUop[body][action][direction] == null)
                    _cacheUop[body][action][direction] = new AAnimationFrame[1];
                if (_cacheUop[body][action][direction][0] != null)
                    return _cacheUop[body][action][direction];
                else
                    return null;
            }
            else
            {
                // Make sure the cache is complete.
                if (m_Cache[body] == null)
                    m_Cache[body] = new AAnimationFrame[COUNT_ACTIONS][][]; // max 35 actions
                if (m_Cache[body][action] == null)
                    m_Cache[body][action] = new AAnimationFrame[COUNT_DIRECTIONS][];
                if (m_Cache[body][action][direction] == null)
                    m_Cache[body][action][direction] = new AAnimationFrame[1];
                if (m_Cache[body][action][direction][0] != null)
                    return m_Cache[body][action][direction];
                else
                    return null;
            }
        }

        private void GetIndexes(int body, int action, int direction, out int index, out AFileIndex fileIndex)
        {
            if (body < 0 || body >= COUNT_ANIMS)
                body = 0;

            if (_isUop)
            {
                foreach (UopFileIndex file in _uopIndexes)
                {
                    if (file.AnimationIdx.Contains(body))
                    {
                        for (int i = 0; i < file.Index.Length; i++)
                        {
                            FileIndexEntry3D e = file.Index[i];
                            if (e.IndexAnim == body)
                            {
                                index = i;
                                fileIndex = file;
                                return;
                            }
                        }
                    }
                }
                _isUop = false;
            }

            int animIndex = BodyConverter.Convert(ref body);
            switch (animIndex)
            {
                default:
                case 1:
                    {
                        fileIndex = m_FileIndex;

                        if (body < 200)
                            index = body * 110;
                        else if (body < 400)
                            index = 22000 + ((body - 200) * 65);
                        else
                            index = 35000 + ((body - 400) * 175);
                        break;
                    }
                case 2:
                    {
                        fileIndex = m_FileIndex2;

                        if (body < 200)
                            index = body * 110;
                        else
                            index = 22000 + ((body - 200) * 65);
                        break;
                    }
                case 3:
                    {
                        fileIndex = m_FileIndex3;

                        /*  if (body < 300)
                              index = body * 65;
                          else if (body < 400)
                              index = 33000 + ((body - 300) * 110);
                          else
                              index = 35000 + ((body - 400) * 175);*/


                        if (body >= 200)
                        {
                            if (body >= 400)
                                index = ((body - 400) * 175) + 35000;
                            else
                                index = ((body - 200) * 110) + 22000;
                        }
                        else
                            index = (body * 65) + 9000;

                        break;
                    }
                case 4:
                    {
                        fileIndex = m_FileIndex4;

                        if (body < 200)
                            index = body * 110;
                        else if (body < 400)
                            index = 22000 + ((body - 200) * 65);
                        else
                            index = 35000 + ((body - 400) * 175);

                        break;
                    }
                case 5:
                    {
                        fileIndex = m_FileIndex5;
                        /*if ((body < 200) && (body != 34)) // looks strange, though it works.
                            index = body * 110;
                        else if (body < 400)
                            index = 22000 + ((body - 200) * 65);
                        else
                            index = 35000 + ((body - 400) * 175);*/

                        if (body == 34)
                            index = ((body - 200) * 65) + 22000;
                        else if (body >= 200)
                        {
                            if (body >= 400)
                                index = ((body - 400) * 175) + 35000;
                            else
                                index = ((body - 200) * 65) + 22000;
                        }
                        else
                            index = body * 110;

                        break;
                    }
            }

            index += action * 5;

            if (direction <= 4)
                index += direction;
            else
                index += direction - (direction - 4) * 2;
        }


        private bool BodyExists(int body)
        {
            for (int i = 0; i < 2; i++)
            {
                _isUop = i == 0;
                GetIndexes(body, 0, 0, out int animIndex, out AFileIndex fileIndex);                
                BinaryFileReader reader = fileIndex.Seek(animIndex, out int length, out int extra, out bool patched);
                if (reader != null)
                    return true;
            }
            return false;
        }
    }
}
