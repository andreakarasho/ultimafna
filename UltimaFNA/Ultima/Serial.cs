﻿/***************************************************************************
 *   Serial.cs
 *   Based on code from RunUO: http://www.runuo.com
 * 
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 3 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/
#region usings
using System;
#endregion

namespace UltimaXNA.Ultima
{
    public struct Serial : IComparable, IComparable<Serial>
    {
        public static Serial ProtectedAction = int.MinValue;

        public static Serial Null
        {
            get { return 0; }
        }

        public readonly static Serial World = unchecked((int)0xFFFFFFFF);

        private readonly int _Serial;

        private Serial(int serial)
        {
            _Serial = serial;
        }

        public int Value
        {
            get
            {
                return _Serial;
            }
        }

        public bool IsMobile
        {
            get
            {
                return (_Serial > 0 && _Serial < 0x40000000);
            }
        }

        public bool IsItem
        {
            get
            {
                return (_Serial >= 0x40000000);
            }
        }

        public bool IsValid
        {
            get
            {
                return (_Serial > 0);
            }
        }

        public bool IsDynamic
        {
            get
            {
                return (_Serial < 0);
            }
        }

        private static int s_NextDynamicSerial = -1;
        public static int NewDynamicSerial
        {
            get { return s_NextDynamicSerial--; }
        }

        public override int GetHashCode()
        {
            return _Serial;
        }

        public int CompareTo(Serial other)
        {
            return _Serial.CompareTo(other._Serial);
        }

        public int CompareTo(object other)
        {
            if (other is Serial)
                return CompareTo((Serial)other);
            else if (other == null)
                return -1;

            throw new ArgumentException();
        }

        public override bool Equals(object o)
        {
            if (o == null || !(o is Serial)) return false;

            return ((Serial)o)._Serial == _Serial;
        }

        public static bool operator ==(Serial l, Serial r)
        {
            return l._Serial == r._Serial;
        }

        public static bool operator !=(Serial l, Serial r)
        {
            return l._Serial != r._Serial;
        }

        public static bool operator >(Serial l, Serial r)
        {
            return l._Serial > r._Serial;
        }

        public static bool operator <(Serial l, Serial r)
        {
            return l._Serial < r._Serial;
        }

        public static bool operator >=(Serial l, Serial r)
        {
            return l._Serial >= r._Serial;
        }

        public static bool operator <=(Serial l, Serial r)
        {
            return l._Serial <= r._Serial;
        }

        public override string ToString()
        {
            return String.Format("0x{0:X8}", _Serial);
        }

        public static implicit operator int(Serial a)
        {
            return a._Serial;
        }

        public static implicit operator Serial(int a)
        {
            return new Serial(a);
        }
    }
}
